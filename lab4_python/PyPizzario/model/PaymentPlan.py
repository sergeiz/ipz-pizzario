from abc import ABCMeta, abstractmethod

class PaymentPlan:
    
    def __init__( self ):
        self.payed = False
        
    def mark_payed( self ):
        self.payed = True
        
    __metaclass__ = ABCMeta
    
    @abstractmethod
    def clone( self ):
        pass
    
    @abstractmethod
    def expect_prepayment( self ):
        pass