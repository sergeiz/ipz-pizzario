from model.PizzaKind import PizzaKind
from model.PizzaSize import PizzaSize

import unittest
from unittest.mock import Mock

       
class PizzaKindTests( unittest.TestCase ):
    
    def test_constructor_returns_expected_defaults( self ):
        kind = PizzaKindTests.make_kind()
        
        self.assertEqual( kind.name , "Carbonara" )
        self.assertEqual( kind.description , "" )
        self.assertIsNotNone( kind.description )
        self.assertEqual( kind.votes , 0 )
        self.assertEqual( kind.total_rating , 0 )
        self.assertFalse( kind.hidden )
        self.assertIsNotNone( kind.recipe )
        
        
    def test_name_constructing_with_empty_forbidden( self ):
        self.assertRaises( Exception , PizzaKind , ( None ) )
        self.assertRaises( Exception , PizzaKind , ( "" ) )
        
        
    def test_name_assigning_to_empty_forbidden( self ):
        kind = PizzaKindTests.make_kind()
        
        self.assertRaises( Exception , kind.name , ( None ) )
        self.assertRaises( Exception , kind.name , ( "" ) )
        
        
    def test_name_reassignment_without_listener_passes_silently( self ):
        kind = PizzaKindTests.make_kind()
        kind.name = "New Carbonara"
        
        self.assertEqual( kind.name , "New Carbonara" )
        
        
    def test_name_reassignment_sends_event( self ):
        kind = PizzaKindTests.make_kind();
        m = Mock()
        kind.before_name_changed += m
        kind.name = "Milano"
        m.assert_called_once_with( "Carbonara", "Milano" )
         
        
    def test_name_reassignment_to_same_does_not_send_event( self ):
        kind = PizzaKindTests.make_kind();
        m = Mock()
        kind.before_name_changed += m
        kind.name = "Carbonara"
        self.assertFalse( m.called )
 
        
    def test_rating_when_no_votes_zero( self ):
        kind = PizzaKindTests.make_kind()
        
        self.assertEqual( kind.votes , 0 )
        self.assertEqual( kind.total_rating , 0 )
        self.assertEqual( kind.get_average_rating() , 0 )
        
        
    def test_rating_when_single_vote_returns_it( self ):
        kind = PizzaKindTests.make_kind()
        kind.rate( 3 )
        
        self.assertEqual( kind.votes , 1 )
        self.assertEqual( kind.total_rating , 3 )
        self.assertEqual( kind.get_average_rating() , 3 )
        
        
    def test_rating_when_two_votes_returns_average( self ):
        kind = PizzaKindTests.make_kind()
        kind.rate( 3 )
        kind.rate( 5 )
        
        self.assertEqual( kind.votes , 2 )
        self.assertEqual( kind.total_rating , 8 )
        self.assertEqual( kind.get_average_rating() , 4 ) 
        
        
    def test_rating_when_three_votes_returns_average( self ):
        kind = PizzaKindTests.make_kind()
        kind.rate( 2 )
        kind.rate( 3 )
        kind.rate( 5 )
        
        self.assertEqual( kind.votes , 3 )
        self.assertEqual( kind.total_rating , 10 )
        self.assertAlmostEqual( kind.get_average_rating() , ( 10 / 3 ) , 6 )
        
        
    def test_rating_zero_votes_forbidden( self ):
        kind = PizzaKindTests.make_kind()
            
        self.assertRaises( Exception , kind.rate , ( 0 ) )
        
        
    def test_rating_negative_votes_forbidden( self ):
        kind = PizzaKindTests.make_kind()
            
        self.assertRaises( Exception , kind.rate , ( -1 ) )
        
        
    def test_rating_votes_beyond_maximum_forbidden( self ):
        kind = PizzaKindTests.make_kind()
            
        self.assertRaises( Exception , kind.rate , ( PizzaKind.MAXVOTE + 1 ) )
        
        
    def test_rating_setup_votes_start_from_concrete_returns_same( self ):
        kind = PizzaKindTests.make_kind()
        kind.setup_rating_stats( 5 , 25 )
        
        self.assertEqual( kind.votes , 5 )
        self.assertEqual( kind.total_rating , 25 )
        self.assertEqual( kind.get_average_rating() , 5 )
        
        
    def test_rating_setup_votes_before_setup_get_overwritten( self ):
        kind = PizzaKindTests.make_kind()
        kind.rate( 5 )
        kind.setup_rating_stats( 5 , 25 )
        
        self.assertEqual( kind.votes , 5 )
        self.assertEqual( kind.total_rating , 25 )
        self.assertEqual( kind.get_average_rating() , 5 )
        
        
    def test_rating_setup_votes_after_setup_append_to_setup(self ):
        kind = PizzaKindTests.make_kind()
        kind.setup_rating_stats( 5 , 25 )
        kind.rate( 5 )
        
        self.assertEqual( kind.votes , 6 )
        self.assertEqual( kind.total_rating , 30 )
        self.assertEqual( kind.get_average_rating() , 5 )
        
        
    def test_rating_setup_negative_votes_setup_forbidden( self ):
        kind = PizzaKindTests.make_kind()
        
        self.assertRaises( Exception , kind.setup_rating_stats , ( -1 , 0 ) )
        
        
    def test_rating_setup_zero_votes_is_fine_when_zero_total( self ):
        kind = PizzaKindTests.make_kind()
        kind.setup_rating_stats( 0, 0 )
        
        
    def test_rating_setup_zero_votes_is_not_fine_when_zero_total( self ):
        kind = PizzaKindTests.make_kind()
        self.assertRaises( Exception , kind.setup_rating_stats , ( 0 , 1 ) )
        self.assertRaises( Exception , kind.setup_rating_stats , ( 0 , -1 ) )
        
        
    def test_rating_setup_total_smaller_than_votes_not_allowed( self ):
        kind = PizzaKindTests.make_kind()
        kind.setup_rating_stats( 5, 5 )
        self.assertRaises( Exception , kind.setup_rating_stats , ( 5 , 4 ) )
        self.assertRaises( Exception , kind.setup_rating_stats , ( 1 , 0 ) )
        
        
    def test_rating_setup_total_large_than_max_votes_not_allowed( self ):
        kind = PizzaKindTests.make_kind()
        kind.setup_rating_stats( 5, 25 )
        self.assertRaises( Exception , kind.setup_rating_stats , ( 5 , 5 * PizzaKind.MAXVOTE + 1 ) )
        self.assertRaises( Exception , kind.setup_rating_stats , ( 1 , PizzaKind.MAXVOTE + 1 ) )
        
        
    def test_prices_initially_zeroes( self ):
        kind = PizzaKindTests.make_kind()
        
        self.assertEqual( kind.get_current_price( PizzaSize.SMALL ) , 0 )
        self.assertEqual( kind.get_current_price( PizzaSize.MEDIUM ) , 0 )
        self.assertEqual( kind.get_current_price( PizzaSize.LARGE ) , 0 )
        
        
    def test_prices_update_each_confirm_assigned_values( self ):
        kind = PizzaKindTests.make_kind()
        kind.update_price( PizzaSize.SMALL , 3.0 )
        kind.update_price( PizzaSize.MEDIUM , 5.0 )
        kind.update_price( PizzaSize.LARGE , 7.0 )
        
        self.assertEqual( kind.get_current_price( PizzaSize.SMALL ) , 3.0 )
        self.assertEqual( kind.get_current_price( PizzaSize.MEDIUM ) , 5.0 )
        self.assertEqual( kind.get_current_price( PizzaSize.LARGE ) , 7.0 )
        
        
    def test_prices_update_same_type_twice_saves_second_price( self ):
        kind = PizzaKindTests.make_kind()
        kind.update_price( PizzaSize.SMALL , 3.0 )
        kind.update_price( PizzaSize.SMALL , 3.5 )
        
        self.assertEqual( kind.get_current_price( PizzaSize.SMALL ) , 3.5 )
        
        
    def test_prices_update_to_zero_allowed( self ):
        kind = PizzaKindTests.make_kind()
        kind.update_price( PizzaSize.SMALL , 0.0 )
        
        self.assertEqual( kind.get_current_price( PizzaSize.SMALL ) , 0.0 )
        
        
    def test_prices_update_to_negative_forbidden( self ):
        kind = PizzaKindTests.make_kind()
        
        self.assertRaises( Exception , kind.update_price , ( PizzaSize.SMALL , -0.001 ) )
        
        
    @staticmethod     
    def make_kind():
        return PizzaKind( "Carbonara" )

    