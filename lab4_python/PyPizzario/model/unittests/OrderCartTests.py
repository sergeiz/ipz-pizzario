from model.OrderCart import OrderCart
from model.OrderItem import OrderItem
from model.PizzaKind import PizzaKind
from model.PizzaSize import PizzaSize

import unittest
from unittest.mock import Mock

        
class OrderCartTests( unittest.TestCase ):
    
    def test_items_none_initially( self ):
        cart = OrderCart()

        self.assertEqual( cart.get_items_count() , 0 )
        
        
    def test_add_one_list_one( self ):
        cart = OrderCart()
        item = OrderCartTests.make_small_item()
        cart.add_item( item )

        self.assertEqual( cart.get_items_count(), 1 )
        self.assertEqual( cart.get_item( 0 ), item )
        
        
    def test_add_two_list_two( self ):
        cart = OrderCart()
        item1 = OrderCartTests.make_small_item()
        item2 = OrderCartTests.make_medium_item()
        cart.add_item( item1 )
        cart.add_item( item2 )
        
        self.assertEqual( cart.get_items_count() , 2 )
        self.assertEqual( cart.get_item( 0 ) , item1 )
        self.assertEqual( cart.get_item( 1 ) , item2 )
        
        
    def test_add_two_same_kind_size_fails( self ):
        cart = OrderCart()
        kind = OrderCartTests.make_pizza_kind()
        cart.add_item( OrderItem( kind , PizzaSize.SMALL , 3.0 , 1 ) )
        
        self.assertRaises( Exception , cart.add_item ,  OrderItem( kind , PizzaSize.SMALL , 3.0 , 1 ) )
        
        
    def test_add_two_same_kind_different_size_passes( self ):  
        cart = OrderCart()
        kind = OrderCartTests.make_pizza_kind()
        itemS = OrderItem( kind , PizzaSize.SMALL ,  3.0 , 1 )
        itemM = OrderItem( kind , PizzaSize.MEDIUM , 3.0 , 1 )
        cart.add_item( itemS )
        cart.add_item( itemM )
        
        self.assertEqual( cart.get_items_count(), 2 )
        self.assertEqual( cart.get_item( 0 ) , itemS )
        self.assertEqual( cart.get_item( 1 ) , itemM ) 
        
        
    def test_add_two_different_kind_same_size_passes( self ):
        cart = OrderCart()
        kind1 = OrderCartTests.make_pizza_kind()
        kind2 = OrderCartTests.make_another_pizza_kind()
        item1 = OrderItem( kind1, PizzaSize.SMALL, 3.0, 1 )
        item2 = OrderItem( kind2, PizzaSize.SMALL, 3.0, 1 )
        cart.add_item( item1 )
        cart.add_item( item2 )

        self.assertEqual( cart.get_items_count() , 2 )
        self.assertEqual( cart.get_item( 0 ) , item1 )
        self.assertEqual( cart.get_item( 1 ) , item2 )
        
        
    def test_update_on_empty_fails( self ):
        cart = OrderCart()
        
        self.assertRaises( Exception , cart.update_item , ( 0, OrderCartTests.make_small_item() ) )
        
        
    def test_update_on_non_empty_good_index_passes( self ):
        cart = OrderCart()
        cart.add_item( OrderCartTests.make_small_item() )
        newItem = OrderCartTests.make_medium_item()
        cart.update_item( 0 , newItem )
        
        self.assertEqual( cart.get_items_count() , 1 )
        self.assertEqual( cart.get_item( 0 ) , newItem )
        
        
    def test_update_on_non_empty_bad_index_fails( self ):
        cart = OrderCart()
        cart.add_item( OrderCartTests.make_small_item() )
        
        self.assertRaises( Exception , cart.update_item , ( 1 , OrderCartTests.make_medium_item() ) )
        self.assertRaises( Exception , cart.update_item , ( 0, OrderCartTests.make_medium_item() ) )
        
        
    def test_drop_empty_fails( self ):
        cart = OrderCart()
        
        self.assertRaises( Exception , cart.drop_item , ( 0 ) )
        
        
    def test_drop_non_empty_good_index_passes( self ):
        cart = OrderCart()
        cart.add_item( OrderCartTests.make_small_item() )
        cart.drop_item( 0 )

        self.assertEqual( cart.get_items_count(), 0 )
        
        
    def test_drop_non_empty_bad_index_fails( self ):
        cart = OrderCart()
        cart.add_item( OrderCartTests.make_small_item() )
        
        self.assertRaises( Exception , cart.drop_item , ( 1 ) )
        self.assertRaises( Exception , cart.drop_item , ( -1 ) )
        
        
    def test_clear_empty_passes( self ):
        cart = OrderCart()
        cart.clear_items()
        
        self.assertEqual( cart.get_items_count(), 0 )
        
        
    def test_clear_non_empty_passes( self ):
        cart = OrderCart()
        cart.add_item( OrderCartTests.make_small_item() )
        cart.add_item( OrderCartTests.make_medium_item() )
        cart.clear_items()
        
        self.assertEqual( cart.get_items_count(), 0 )
        
    
    def test_cost_empty_zero( self ):
        cart = OrderCart()
        
        self.assertEqual( cart.cost() ,0 )
        
        
    def test_cost_single_item_matches_item( self ):
        cart = OrderCart()
        cart.add_item( OrderCartTests.make_small_item() )
        
        self.assertEqual( cart.cost() , cart.get_item( 0 ).cost() )
        
        
    def test_cost_two_items_matches_sum_items( self ):
        cart = OrderCart()
        cart.add_item( OrderCartTests.make_small_item() )
        cart.add_item( OrderCartTests.make_medium_item() )
        
        self.assertEqual( cart.cost() , cart.get_item( 0 ).cost() + cart.get_item( 1 ).cost() )
        
        
    def test_modifiable_is_true_initially( self ):
        cart = OrderCart()
        
        self.assertTrue( cart.modifiable )
        
    
    def test_modifiable_after_checkout_is_false( self ):
        cart = OrderCart()
        cart.add_item( OrderCartTests.make_small_item() )
        cart.checkout()
        
        self.assertFalse( cart.modifiable )
        
        
    def test_checkout_empty_fails( self ):
        cart = OrderCart()
        
        self.assertRaises( Exception , cart.checkout, () )
        
        
    def test_checkout_sends_event( self ):
        cart = OrderCart()
        cart.add_item( OrderCartTests.make_small_item() )
        
        m = Mock()
        cart.checkout_event += m
        cart.checkout()
        
        m.assert_called_once_with( cart )
        
        
    def test_unmodifiable_readingItems_Passes( self ):
        cart = OrderCart()
        item1 = OrderCartTests.make_small_item()
        item2 = OrderCartTests.make_medium_item()
        cart.add_item( item1 )
        cart.add_item( item2 )     
        
        cart.checkout()
        
        self.assertEqual( cart.get_items_count(), 2 )
        self.assertEqual( cart.get_item( 0 ) , item1 )
        self.assertEqual( cart.get_item( 1 ) , item2 )
        
        
    def test_unmodifiable_add_item_forbidden( self ):
        cart = OrderCart()
        cart.add_item( OrderCartTests.make_small_item() )
        
        cart.checkout()
        
        self.assertRaises( Exception , cart.add_item , ( OrderCartTests.make_medium_item() ) )
        
        
    def test_unmodifiable_update_item_forbidden( self ):
        cart = OrderCart()
        cart.add_item( OrderCartTests.make_small_item() )
        cart.checkout()
        
        self.assertRaises( Exception , cart.update_item , ( 0 , OrderCartTests.make_medium_item() ) )
        
        
    def test_unmodifiable_drop_item_forbidden( self ):
        cart = OrderCart()
        cart.add_item( OrderCartTests.make_small_item() )
        cart.checkout()
        
        self.assertRaises( Exception , cart.drop_item , ( 0 ) )
        
        
    def test_unmodifiable_clear_items_forbidden( self ):
        cart = OrderCart()
        cart.add_item( OrderCartTests.make_small_item() )
        cart.checkout()
        
        self.assertRaises( Exception , cart.clear_items , () )    
            
    @staticmethod       
    def make_pizza_kind():
        return PizzaKind( "Carbonara" )
    
    
    @staticmethod 
    def make_another_pizza_kind():
        return PizzaKind( "Milano" )          
            
            
    @staticmethod        
    def make_small_item():
        return OrderItem( OrderCartTests.make_pizza_kind() , PizzaSize.MEDIUM , 3.0 , 1 )
    
    
    @staticmethod        
    def make_medium_item():
        return OrderItem( OrderCartTests.make_another_pizza_kind() , PizzaSize.MEDIUM , 5.0 , 1 )
    