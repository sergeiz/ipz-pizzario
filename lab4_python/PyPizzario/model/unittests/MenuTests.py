from model.Menu import Menu
from model.PizzaKind import PizzaKind
from model.PizzaSize import PizzaSize

import unittest

class MenuTests( unittest.TestCase ): 
    
    
    def test_kinds_initially_empty( self ):
        menu = Menu()
        self.assertEqual( menu.get_pizza_kinds_count() , 0 )
        
        
    def test_kinds_add_one_find_it( self ):
        menu = Menu()
        carbonara = PizzaKind( "Carbonara" )
        menu.add_pizza_kind( carbonara )

        self.assertEqual( menu.get_pizza_kinds_count() , 1 )
        self.assertEqual( menu.find_pizza_kind( "Carbonara" ), carbonara )
        
    
    def test_kinds_add_two_find_both( self ):
        menu = Menu()
        carbonara = PizzaKind( "Carbonara" )
        milano = PizzaKind( "Milano" )
        menu.add_pizza_kind( carbonara )
        menu.add_pizza_kind( milano )
        
        self.assertEqual( menu.get_pizza_kinds_count() , 2 )
        self.assertEqual( menu.find_pizza_kind( "Carbonara" ), carbonara )
        self.assertEqual( menu.find_pizza_kind( "Milano" ), milano )
        
        
    def test_kinds_find_missing_in_empty_returns_none( self ):
        menu = Menu()
        
        self.assertIsNone(menu.find_pizza_kind( "Carbonara" ) )
        
        
    def test_kinds_find_missing_in_nonEmpty_returns_none( self ):
        menu = Menu()
        menu.add_pizza_kind( PizzaKind( "Milano" ) )
        
        self.assertIsNone(menu.find_pizza_kind( "Carbonara" ) )
        
        
    def test_kinds_add_duplicate_fails( self ):
        menu = Menu()
        menu.add_pizza_kind( PizzaKind( "Carbonara" ) )
        
        self.assertRaises( Exception , menu.add_pizza_kind , ( PizzaKind( "Carbonara" ) ) )
        
        
    def test_kinds_delete_single_makes_empty( self ):
        menu = Menu()
        menu.add_pizza_kind( PizzaKind( "Carbonara" ) )
        menu.delete_pizza_kind( "Carbonara" )
        
        self.assertEqual( menu.get_pizza_kinds_count() , 0 )
        
        
    def test_kinds_delete_one_of_two_leaves_other( self ):
        menu = Menu()
        menu.add_pizza_kind( PizzaKind( "Carbonara" ) )
        menu.add_pizza_kind( PizzaKind( "Milano" ) )
        menu.delete_pizza_kind( "Carbonara" )
        
        self.assertEqual( menu.get_pizza_kinds_count() , 1 )
        
        pizza_kind = next( iter ( menu.pizza_kinds_by_name ) )
        self.assertEqual(  pizza_kind , "Milano" )
        
        
    def test_kinds_delete_missing_in_empty_forbidden( self ):
        menu = Menu()
                
        self.assertRaises( Exception , menu.delete_pizza_kind , ( "Carbonara" )  )
        
        
    def test_kinds_delete_missing_in_non_empty_forbidden( self ):
        menu = Menu()
        menu.add_pizza_kind( PizzaKind( "Milano" ) )
                
        self.assertRaises( Exception , menu.delete_pizza_kind , ( "Carbonara" ) )
        
        
    def test_rename_the_only_accessible_under_new_name( self ):
        menu = Menu();
        carbonara = PizzaKind( "carbonara" )
        menu.add_pizza_kind( carbonara )

        carbonara.name = "Carbonara"

        self.assertEqual( menu.find_pizza_kind( "Carbonara" ), carbonara )
        self.assertEqual( menu.get_pizza_kinds_count(), 1 )
        
        
    def test_rename_one_of_two_accessible_under_new_name_second_untouched( self ):
        menu = Menu()
        carbonara = PizzaKind( "carbonara" )
        milano = PizzaKind( "Milano" )
        menu.add_pizza_kind( carbonara )
        menu.add_pizza_kind( milano )

        carbonara.name = "Carbonara"

        self.assertEqual( menu.find_pizza_kind( "Carbonara" ), carbonara )
        self.assertEqual( menu.find_pizza_kind( "Milano" ), milano )
        self.assertEqual( menu.get_pizza_kinds_count(), 2 )
        
        
    def test_rename_to_existing_forbidden( self ):
        menu = Menu()
        carbonara = PizzaKind( "Carbonara" )
        milano = PizzaKind( "Milano" )
        menu.add_pizza_kind( carbonara )
        menu.add_pizza_kind( milano )
        
        self.assertRaises( Exception , milano.name , ( "Carbonara" ) )
        
        
    def test_rename_deleted_unaffects_menu( self ):
        menu = Menu()
        carbonara = PizzaKind( "Carbonara" )
        menu.add_pizza_kind( carbonara )
        menu.delete_pizza_kind( "Carbonara" )

        self.assertEqual( menu.get_pizza_kinds_count(), 0 )
        carbonara.name = "New Carbonara"
        
        
    def test_order_item_test_expected_result( self ):
        carbonara = PizzaKind( "Carbonara" )
        carbonara.update_price( PizzaSize.SMALL, 3.0 )

        menu = Menu()
        menu.add_pizza_kind( carbonara )

        item = menu.make_order_item( "Carbonara", PizzaSize.SMALL, 1 )
        self.assertEqual( item.kind, carbonara )
        self.assertEqual( item.quantity, 1 )
        self.assertEqual( item.size, PizzaSize.SMALL )
        self.assertEqual( item.fixed_price, 3.0 )
