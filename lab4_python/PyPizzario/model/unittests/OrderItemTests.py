from model.OrderItem import OrderItem
from model.PizzaKind import PizzaKind
from model.PizzaSize import PizzaSize

import unittest

class OrderItemTests( unittest.TestCase ): 
        
    def test_constructor_projects_fields_correctly( self ):
        kind = OrderItemTests.make_pizza_kind()
        
        item = OrderItem( kind, PizzaSize.SMALL, 3.0, 2 )

        self.assertEqual( item.kind, kind )
        self.assertEqual( item.quantity, 2 )
        self.assertEqual( item.size, PizzaSize.SMALL )
        self.assertEqual( item.fixed_price, 3.0 )
        
        
    def test_constructor_negative_price_forbidden( self ):
        self.assertRaises( Exception , OrderItem , ( OrderItemTests.make_pizza_kind(), PizzaSize.SMALL, -0.01, 1 ) )
        OrderItem( OrderItemTests.make_pizza_kind(), PizzaSize.SMALL, 0.00, 1 )
        
    
    def test_constructor_non_positive_quantity_forbidden( self ):
        self.assertRaises( Exception , OrderItem , ( OrderItemTests.make_pizza_kind(), PizzaSize.SMALL, 0.01, -1 ) )
        self.assertRaises( Exception , OrderItem , ( OrderItemTests.make_pizza_kind(), PizzaSize.SMALL, 0.01, 0 ) )
        
        
    def test_cost_single_item_matches_price( self ):
        item = OrderItem( OrderItemTests.make_pizza_kind(), PizzaSize.SMALL, 3.0, 1 )

        self.assertEqual( item.cost() , item.fixed_price )
        
    
    def test_cost_nItems_multiplies_price( self ):
        item = OrderItem( OrderItemTests.make_pizza_kind(), PizzaSize.SMALL, 3.0, 8 )

        self.assertEqual( item.cost() , item.fixed_price * 8 )
        
        
    @staticmethod    
    def make_pizza_kind():
        return PizzaKind( "Carbonara" )
