from model.PizzaRecipe import PizzaRecipe
import unittest

        
class PizzaRecipeTests( unittest.TestCase ):
    
    def test_ingredients_initially_none( self ):
        recipe = PizzaRecipe()
        self.assertEqual( recipe.get_ingredients_count() , 0 )
        
    def test_ingredients_ask_missing_in_empty_returns_zero( self ):
        recipe = PizzaRecipe()
        self.assertEqual( recipe.get_ingredient_weight( "Pineapple" ) , 0 )
        
    def test_ingredients_insert_one_return_weight( self ):
        recipe = PizzaRecipe()
        recipe.add_ingredient( "Cheese", 10 )
        
        self.assertEqual( recipe.get_ingredients_count() , 1 )
        self.assertEqual( recipe.get_ingredient_weight( "Cheese" ) , 10 )
        
    def test_ingredients_ask_missing_in_non_empty_returns_zero( self ):
        recipe = PizzaRecipe()
        recipe.add_ingredient( "Cheese", 10 )
        
        self.assertEqual( recipe.get_ingredient_weight( "Ham" ) , 0 )
        
    def test_ingredients_insert_two_return_each_weight( self ):
        recipe = PizzaRecipe()
        recipe.add_ingredient( "Cheese", 10 )
        recipe.add_ingredient( "Ham", 20 )
        
        self.assertEqual( recipe.get_ingredients_count() , 2 )
        self.assertEqual( recipe.get_ingredient_weight( "Cheese" ) , 10 )
        self.assertEqual( recipe.get_ingredient_weight( "Ham" ) , 20 )
        
    def test_ingredients_insert_duplicate_forbidden( self ):
        recipe = PizzaRecipe()
        recipe.add_ingredient( "Cheese", 10 )
        
        self.assertRaises( Exception , recipe.add_ingredient , ( "Cheese", 20 ) )
        
    def test_ingredients_insert_and_update_return_updated_weight( self ):
        recipe = PizzaRecipe()
        recipe.add_ingredient( "Cheese", 10 )
        recipe.update_ingredient( "Cheese" , 20 )
        
        self.assertEqual( recipe.get_ingredient_weight( "Cheese" ) , 20 )
        
    def test_ingredients_updating_one_of_two_does_not_alter_second( self ):
        recipe = PizzaRecipe()
        recipe.add_ingredient( "Cheese", 10 )
        recipe.add_ingredient( "Ham", 20 )
        recipe.update_ingredient( "Cheese" , 30 )
        
        self.assertEqual( recipe.get_ingredient_weight( "Cheese" ) , 30 )
        self.assertEqual( recipe.get_ingredient_weight( "Ham" ) , 20 )
        
    def test_ingredients_delete_single_makes_empty( self ):
        recipe = PizzaRecipe()
        recipe.add_ingredient( "Cheese", 10 )
        recipe.remove_ingredient( "Cheese" )
        
        self.assertEqual( recipe.get_ingredients_count() , 0 )
        
    def test_ingredients_delete_one_of_two_leaves_second( self ):
        recipe = PizzaRecipe()
        recipe.add_ingredient( "Cheese", 10 )
        recipe.add_ingredient( "Ham", 20 )
        recipe.remove_ingredient( "Cheese" )
        
        self.assertEqual( recipe.get_ingredients_count() , 1 )
        self.assertEqual( recipe.get_ingredient_weight( "Cheese" ) , 0 )
        self.assertEqual( recipe.get_ingredient_weight( "Ham" ) , 20 )
        
    def test_ingredients_delete_missing_from_empty_forbidden( self ):
        recipe = PizzaRecipe()
                
        self.assertRaises( Exception , recipe.remove_ingredient , ( "Cheese" ) )
        
    def test_ingredients_delete_missing_from_non_empty_forbidden( self ):
        recipe = PizzaRecipe()
        recipe.add_ingredient( "Cheese", 10 )
        
        self.assertRaises( Exception , recipe.remove_ingredient , ( "Ham" ) )
