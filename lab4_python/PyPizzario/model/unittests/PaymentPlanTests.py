from model.CashPaymentPlan import CashPaymentPlan
from model.WireTransferPaymentPlan import WireTransferPaymentPlan

import unittest
    
class CashPaymentPlanTests( unittest.TestCase ):
    
    def test_payed_initially_false( self ):
        p = CashPaymentPlan()
        self.assertFalse( p.payed )
        
        
    def test_payed_set_payed_to_true_confirmed( self ):
        p = CashPaymentPlan()
        p.mark_payed()
        self.assertTrue( p.payed )
        
        
    def test_prepayment_not_expected( self ):
        p = CashPaymentPlan()
        self.assertFalse( p.expect_prepayment() )
        
        
    def test_clone_payed_still_payed( self ):
        p1 = CashPaymentPlan()
        p1.mark_payed()
        p2 = p1.clone()
        self.assertTrue( p2.payed )
        
        
    def test_clone_unpayed_still_unpayed( self ):
        p1 = CashPaymentPlan()
        p2 = p1.clone()
        self.assertFalse( p2.payed )


class WireTransferPaymentPlanTests( unittest.TestCase ):
    
    def test_constructor_projects_fields_correctly( self ):
        plan = WireTransferPaymentPlan( "123", "Ivanov" )    
        self.assertEqual( plan.card_code , "123" )
        self.assertEqual( plan.card_holder , "Ivanov" )
        
        
    def test_payed_initially_false( self ):
        p = WireTransferPaymentPlanTests.make_wire_transfer_plan()
        self.assertFalse( p.payed )
        
        
    def test_payed_set_payed_to_true_confirmed( self ):
        p = WireTransferPaymentPlanTests.make_wire_transfer_plan()
        p.mark_payed()
        self.assertTrue( p.payed )
        
        
    def test_prepayment_expected( self ):
        p = WireTransferPaymentPlanTests.make_wire_transfer_plan()
        self.assertTrue( p.expect_prepayment() )
        
        
    def test_clone_payed_still_payed( self ):
        p1 = WireTransferPaymentPlanTests.make_wire_transfer_plan()
        p1.mark_payed()
        p2 = p1.clone()
        self.assertTrue( p2.payed )
        
        
    def test_clone_unpayed_still_unpayed( self ):
        p1 = WireTransferPaymentPlanTests.make_wire_transfer_plan()
        p2 = p1.clone()
        self.assertFalse( p2.payed )
        
        
    def test_clone_copies_fields_correctly( self ):
        plan = WireTransferPaymentPlan( "123", "Ivanov" )
        plan2 = plan.clone()
        self.assertEqual( plan2.card_code , "123" )
        self.assertEqual( plan2.card_holder , "Ivanov" )
        
    
    @staticmethod    
    def make_wire_transfer_plan():
        return WireTransferPaymentPlan( "1234 5678 1234 5678", "Ivan Ivanov" )
    