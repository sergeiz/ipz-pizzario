from model.OrderStatus import OrderStatus
from model.CookingStatus import CookingStatus
from model.Pizza import Pizza


class Order:
    
    def __init__( self , order_id, cart, contact, pay_plan ):
        
        if ( not cart.modifiable ):
            raise Exception( "Order: initializing with cart that is already checked out" )
                
        self.order_id = order_id
        self.cart = cart
        self.contact = contact
        self.pay_plan = pay_plan
        
        self.discount = 0.0
        self.pizzas = []
        
        self.status = OrderStatus.NEW
        
        self.cart.checkout_event += self.handle_checkout
        
        
    def get_pizza_count( self ):
        return len( self.pizzas )
    
    
    def set_discount( self , value ):
        if ( value < 0.0 or value > 1.0 ):
            raise Exception( "Order: discount should be between [0.0;1.0]" )

        self.discount = value
        
        
    def total_cost( self ):
        return self.cart.cost() * ( 1.0 - self.discount )
    
    
    def get_pizza( self , index ):
        if index < 0:
            raise Exception( "Order out of range" )
        return self.pizzas[ index ]
    
    
    def cancel( self ):
        if self.status not in [ OrderStatus.NEW , OrderStatus.REGISTRED , 
                              OrderStatus.COOKING , OrderStatus.READY4DELIVERY ]:
            raise Exception( "Order.Cancel - cannot cancel in current order state" )
        
        self.status = OrderStatus.CANCELLED
        
        for p in self.pizzas :
            if ( p.status != CookingStatus.FINISHED ):
                p.on_cooking_cancelled()
                
                
    def on_started_delivery( self ):
        if self.status != OrderStatus.READY4DELIVERY:
            raise Exception( "Order.OnStartedDelivery - can only happen in Ready4Delivery state" )

        self.status = OrderStatus.DELIVERING
            
            
    def on_delivered( self ):
        if self.status != OrderStatus.DELIVERING:
            raise Exception( "Order.OnDelivered - can only happen in Delivering state" )

        if ( not self.pay_plan.expect_prepayment() ) and ( not self.pay_plan.payed ):
            raise Exception( "Order.OnDelivered - post-payment should have been collected by now" )

        self.status = OrderStatus.DELIVERED
        
    
    def handle_checkout( self , cart ):
        if ( self.status != OrderStatus.NEW ):
            raise Exception( "Order.HandleCheckout - Can only process new order!" )
        
        if ( ( self.pay_plan.expect_prepayment() ) and ( not self.pay_plan.payed ) ):
            raise Exception( "Order.HandleCheckout - pre-payment unavailable" )
        
        self.status = OrderStatus.REGISTRED
        
        n_items = self.cart.get_items_count()
        for i in range ( 0 , n_items ):
            item = self.cart.get_item( i )
            for k in range ( 0 , item.quantity ):
                p = Pizza( item.kind , item.size )
                self.pizzas.append( p )
                
                p.cooking_started += self.handle_pizza_cooking_started
                p.cooking_finished += self.handle_pizza_ready
                
                
    def handle_pizza_cooking_started( self ):
        if self.status == OrderStatus.REGISTRED:
            self.status = OrderStatus.COOKING
            
        elif ( self.status != OrderStatus.COOKING ):
            raise Exception( "Order.HandlePizzaCookingStarted - can only happen in Registered & Cooking states" )
        
    
    def handle_pizza_ready( self ):
        if self.status != OrderStatus.COOKING:
            raise Exception( "Order.HandlePizzaReady - can only happen in Cooking state" )
        
        for p in self.pizzas:
            if p.status != CookingStatus.FINISHED:
                return
            
        self.status = OrderStatus.READY4DELIVERY
