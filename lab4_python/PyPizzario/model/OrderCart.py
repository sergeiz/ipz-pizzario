from model.Events import Event

class OrderCart:
    
    def __init__( self ):
        self.items = []
        self.modifiable = True
        
        self.checkout_event = Event()
        
    
    def cost( self ):
        total_cost = 0
        for x in self.items:
            total_cost += x.cost()    
        return total_cost
    
    
    def get_items_count( self ):
        return len( self.items )
    
    
    def get_item( self , index ):
        if index < 0:
            raise Exception( "OrderCart.DropItem: out of range" )
        return self.items[ index ]
    
    
    def add_item( self , item ):
        if ( not self.modifiable ):
            raise Exception( "OrderCart.add_item: unmodifiable cart" )

        for i in self.items:
            if ( i.kind == item.kind and i.size == item.size ):
                raise Exception( "OrderCart.add_item: duplicate kind-size pair added" )
        
        self.items.append( item )
        
        
    def update_item( self , index , item ):
        if ( not self.modifiable ):
            raise Exception( "OrderCart.UpdateItem: unmodifiable cart" )
        if index < 0:
            raise Exception( "OrderCart.DropItem: out of range" )
        
        self.items[ index ] = item
        
        
    def drop_item( self , index ):
        if ( not self.modifiable ):
            raise Exception( "OrderCart.DropItem: unmodifiable cart" )
        
        if index < 0:
            raise Exception( "OrderCart.DropItem: out of range" )
        
        self.items.pop( index )
        
        
    def clear_items( self ):
        if ( not self.modifiable ):
            raise Exception( "OrderCart.ClearItems: unmodifiable cart" )
        
        self.items.clear()
        
        
    def checkout( self ):
        if ( self.items.count == 0 ):
            raise Exception( "OrderCart.Checkout: checkout of empty cart" )

        self.modifiable = False
        self.checkout_event( self )
        