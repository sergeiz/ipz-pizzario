from model.PizzaRecipe import PizzaRecipe
from model.PizzaSize import PizzaSize
from model.Events import Event

class PizzaKind:
    
    MAXVOTE = 5


    def __init__( self , name ):
        
        self.recipe = PizzaRecipe()
        
        self.before_name_changed = Event()
        
        self.name = name
        self.description = ""
        self.image_URL = ""
        self.votes = 0
        self.total_rating = 0
        self.hidden = False
        self.price_by_size = [ 0.0 ] *  PizzaSize.Total
        
        
    def __setattr__( self , name , value ):
        if name == "name":
            if value == None or value == "":
                raise Exception( "PizzaKind: empty name" )
            if ( hasattr( self, "name" ) and self.name != value ):
                self.before_name_changed( self.name , value )
        object.__setattr__( self , name , value )
        
        
        
    def get_average_rating( self ):
        if self.votes != 0:
            return float( self.total_rating ) / self.votes
        else:
            return 0.0
        
        
    def rate( self , rating ):
        if ( rating not in range( 1 , ( PizzaKind.MAXVOTE + 1 ) ) ): 
            raise Exception( "PizzaKind.rate: expecting rating within [1;" + PizzaKind.MAXVOTE + "] range" )
        else:
            self.total_rating += rating
            self.votes += 1
            
            
    def setup_rating_stats( self , votes, total_rating ): 
        if ( votes < 0 ):
            raise Exception( "PizzaKind.setup_rating_stats: negative vote" )
        elif ( votes == 0 and total_rating != 0 ):
            raise Exception( "PizzaKind.setup_rating_stats: zero votes can only give zero ratings" )
        elif ( votes > 0 and total_rating < votes ):
            raise Exception( "PizzaKind.setup_rating_stats: total rating may not be smaller than number of votes" )
        elif ( votes > 0 and total_rating > ( votes * PizzaKind.MAXVOTE ) ):
            raise Exception( "PizzaKind.setup_rating_stats: total rating may not exceed maximum from all votes" )
        
        self.votes = votes
        self.total_rating = total_rating
        
        
    def  get_current_price( self , size ):
        PizzaSize.validate( size )
        return self.price_by_size[ size ]
        
        
    def update_price( self , size, price ): 
        if price < 0:
            raise Exception( "PizzaKind.update_price: expecting nonnegative price" )
        self.price_by_size[ size ] = price
        
