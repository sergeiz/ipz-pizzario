from model.PaymentPlan import PaymentPlan

class WireTransferPaymentPlan( PaymentPlan ):
    
    def __init__( self , card_code , card_holder ):
        PaymentPlan.__init__( self )
        self.card_code = card_code
        self.card_holder = card_holder
        
        
    def expect_prepayment( self ):
        return True
    
    
    def clone( self ):
        copy = WireTransferPaymentPlan( self.card_code, self.card_holder )
        if ( self.payed ):
            copy.mark_payed()
        return copy
