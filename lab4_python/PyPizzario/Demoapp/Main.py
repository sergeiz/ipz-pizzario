from ReportGenerator import ReportGenerator
from TestModelGenerator import TestModelGenerator

import sys

modelGenerator = TestModelGenerator()
network = modelGenerator.generate()

reportGenerator = ReportGenerator( sys.stdout, network )
reportGenerator.generate()
