from model.PizzarioNetwork import PizzarioNetwork
from model.PizzaKind import PizzaKind
from model.PizzaSize import PizzaSize
from model.DeliveryContact import DeliveryContact
from model.CashPaymentPlan import CashPaymentPlan


class TestModelGenerator:
    
    @staticmethod
    def generate():
        network = PizzarioNetwork()
        TestModelGenerator.fill_pizza_menu( network.menu )
        TestModelGenerator.fill_orders( network, network.menu )
        return network
    
    @staticmethod
    def fill_pizza_menu( m ):
        '''
        '''

        carbonara = PizzaKind( "Carbonara" )
        m.add_pizza_kind( carbonara )

        carbonara.description = "One of the most popular pizza recipes in the world"
        carbonara.setup_rating_stats( 170, 812 )
        carbonara.image_url = "http://pizzario.com.ua/images/carbonara.jpg"

        carbonara.recipe.add_ingredient( "Cheese", 100 )
        carbonara.recipe.add_ingredient( "Olive Oil", 30 )

        carbonara.update_price( PizzaSize.SMALL , 3.00 )
        carbonara.update_price( PizzaSize.MEDIUM , 5.00 )
        carbonara.update_price( PizzaSize.LARGE , 7.00 )

        '''
        '''

        milano = PizzaKind( "Milano" )
        m.add_pizza_kind( milano )

        milano.description = "Classic recipe with ham, mushrooms and tomatoes"
        milano.setup_rating_stats( 102, 415 )
        milano.image_url = "http://pizzario.com.ua/images/milano.jpg"

        milano.recipe.add_ingredient( "Ham", 100 )
        milano.recipe.add_ingredient( "Mushrooms", 50 )
        milano.recipe.add_ingredient( "Tomatoes", 50 )

        milano.update_price( PizzaSize.SMALL, 2.50 )
        milano.update_price( PizzaSize.MEDIUM, 4.15 )
        milano.update_price( PizzaSize.LARGE, 5.80 )
        
     
    @staticmethod   
    def fill_orders( network , m ):

        order1 = network.new_order( DeliveryContact( "Lenin av. 14, room 320", "702-13-26", "APVT Department" ) , CashPaymentPlan() ) 
        order1.cart.add_item( m.make_order_item( "Carbonara", PizzaSize.MEDIUM, 2 ) )
        order1.cart.add_item( m.make_order_item( "Milano", PizzaSize.LARGE, 1 ) )
        order1.discount = 0.025
        order1.cart.checkout()
        
        order2 = network.new_order( DeliveryContact( "23rd August Str 2, ap. 2", "333-33-33", "Knock 3 times" ) , CashPaymentPlan() )
        
        order2.cart.add_item( m.make_order_item( "Carbonara", PizzaSize.SMALL, 1 ) )
    