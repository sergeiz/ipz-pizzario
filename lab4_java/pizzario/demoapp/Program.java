package pizzario.demoapp;


import pizzario.model.PizzarioNetwork;
import pizzario.model.test.ReportGenerator;
import pizzario.model.test.TestModelGenerator;

public class Program {

	public static void main(String[] args) 
	{
        Program p = new Program();
        p.fillTestModel();
        p.generateModelReport();
    }

    private void fillTestModel ()
    {
        TestModelGenerator generator = new TestModelGenerator();
        network = generator.generate();
    }

    private void generateModelReport ()
    {
        ReportGenerator generator = new ReportGenerator( System.out , network );
        generator.generate();
    }
    
    private PizzarioNetwork network = new PizzarioNetwork();

}


