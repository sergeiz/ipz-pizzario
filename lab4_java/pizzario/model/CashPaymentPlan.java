package pizzario.model;

public class CashPaymentPlan extends PaymentPlan 
{
	
	@Override
	public PaymentPlan clone () 
	{
		CashPaymentPlan copy = new CashPaymentPlan();
        if ( wasPayed() )
            copy.markPayed();
        return copy;
	}

	
	@Override
	public boolean expectPrepayment () 
	{
		return false;
	}
	
	
	@Override
	public String toString ()
    {
		return "Cash payment on delivery";
    }
	
}
