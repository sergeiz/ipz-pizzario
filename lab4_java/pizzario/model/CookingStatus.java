package pizzario.model;

public enum CookingStatus 
{
	 NotStarted,
     Started,
     Finished,
     Cancelled;
}
