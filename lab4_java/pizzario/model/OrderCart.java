package pizzario.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class OrderCart
{
	public interface CheckoutListener
	{
		void onOrderCartCheckout ();
	}
	
	public OrderCart ()
	{
		this.checkoutListener = new ArrayList< CheckoutListener >();
		this.items = new ArrayList< OrderItem >();
		this.modifiable = true;
	}
	
	
	public Iterable< OrderItem > browseItems ()
	{
		return this.items;
	}
	
	
	public boolean isModifiable ()
    {
        return this.modifiable;
    }
	
	
	public void addCheckoutListener ( CheckoutListener listener )
	{
		checkoutListener.add( listener );
	}

	
	public void removeCheckoutListener ( CheckoutListener listener )
	{
		checkoutListener.remove( listener );
	}

	
	public int getItemsCount ()
    {
		return items.size();
    }
	 
	public OrderItem getItem ( int index )
	{
		return items.get( index );
	}
	 
	
	public void addItem ( OrderItem item )
    {
		if ( ! this.modifiable )
			throw new RuntimeException( "OrderCart.addItem: unmodifiable cart" );

        for ( OrderItem i: this.items )
        {
        	if ( i.getPizzaKind() == item.getPizzaKind() && 
        		 i.getPizzaSize() == item.getPizzaSize() )
        		throw new RuntimeException( "OrderCart.addItem: duplicate kind-size pair added" );
        }

        items.add( item );
	}
	 
	public void updateItem ( int index, OrderItem item )
    {
		if ( ! modifiable )
			throw new RuntimeException( "OrderCart.updateItem: unmodifiable cart" );
        
		items.set( index , item );
    }
	 
	public void dropItem ( int index )
    {
		if ( ! modifiable )
			throw new RuntimeException( "OrderCart.dropItem: unmodifiable cart" );
        
		items.remove( index );
    }
	 
	
	public void clearItems ()
    {
		if ( ! modifiable )
			throw new RuntimeException( "OrderCart.clearItems: unmodifiable cart" );
		
		items.clear();
    }
	 
	 
	public BigDecimal getCost ()
	{
		BigDecimal totalCost = BigDecimal.ZERO;
	    for ( OrderItem i : items )
	    	totalCost = totalCost.add( i.getCost() );
	    return totalCost;
	}
	 
	
	public void checkout ()
    {
		if ( this.items.isEmpty() )
			throw new RuntimeException( "OrderCart.checkout: checkout of empty cart" );

		this.modifiable = false;

		if ( checkoutListener != null )
			for ( CheckoutListener l : checkoutListener )
				l.onOrderCartCheckout();
	}

	
	private List< CheckoutListener > checkoutListener;
	private List< OrderItem > items;
	private boolean modifiable;
}
