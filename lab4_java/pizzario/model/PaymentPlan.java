package pizzario.model;

public abstract class PaymentPlan
{
	protected PaymentPlan ()
    {
        this.payed = false;
    }

	
	public boolean wasPayed ()
	{
		return payed;
	}


    public void markPayed ()
    {
        this.payed = true;
    }

    
    public abstract PaymentPlan clone ();
	
    
    public abstract boolean expectPrepayment ();
    
    
    private boolean payed;
}
