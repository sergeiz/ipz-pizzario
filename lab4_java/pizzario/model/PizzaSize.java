package pizzario.model;

public enum PizzaSize 
{
	SMALL,
	MEDIUM,
	LARGE;
}
