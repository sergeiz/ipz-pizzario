package pizzario.model;

import java.util.Map;
import java.util.TreeMap;

public class PizzaRecipe {
	
	public Iterable< String > browseIngredients ()
	{
		return ingredientsWeight.keySet();
	}
	
	
	public int getIngredientsCount ()
	{
		return ingredientsWeight.size();
	}
	
	
	public int getIngredientWeight ( String ingredient )
	{
		if ( ingredientsWeight.containsKey( ingredient ) )
			return ingredientsWeight.get( ingredient );
		return 0;
	}
	
	
	public void addIngredient ( String ingredient, int weight )
	{
		if ( ingredientsWeight.containsKey( ingredient ) )
			throw new RuntimeException( "PizzaKind.addIngredient: duplicate ingredient" );
		else
			ingredientsWeight.put( ingredient, weight );
	}
	
	
	public void updateIngredient ( String ingredient, int weight )
	{
		if ( ingredientsWeight.containsKey( ingredient ) )
			ingredientsWeight.put( ingredient, weight );
		else
			throw new RuntimeException( "PizzaKind.updateIngredient: missing ingredient" );
	}
	
	
	public void removeIngredient ( String ingredient )
	{
		if ( ingredientsWeight.containsKey( ingredient ) )
			ingredientsWeight.remove( ingredient );
		else
			throw new RuntimeException( "PizzaKind.removeIngredient: missing ingredient" );
	}
	
	
	private Map< String , Integer > ingredientsWeight =
		new TreeMap< String , Integer >();
}
