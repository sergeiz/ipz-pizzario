package pizzario.model;

public class WireTransferPaymentPlan extends PaymentPlan 
{
	public WireTransferPaymentPlan ( String cardCode, String cardHolder )
    {
        this.cardCode = cardCode;
        this.cardHolder = cardHolder;
    }

	
	public String getCardCode ()
	{
		return this.cardCode;
	}
	
	
	public String getCardHolder ()
	{
		return this.cardHolder;
	}

	
	@Override
	public PaymentPlan clone () 
	{
		WireTransferPaymentPlan copy = new WireTransferPaymentPlan( cardCode, cardHolder );
        if ( wasPayed() )
            copy.markPayed();
        return copy;
	}

	
	@Override
	public boolean expectPrepayment ()
	{
		return true;
	}
	
	
	@Override
	public String toString ()
    {
        return "Wire transfer";
    }
	
	
	private String cardCode;
	private String cardHolder;
}
