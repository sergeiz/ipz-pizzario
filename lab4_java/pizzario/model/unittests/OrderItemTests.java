package pizzario.model.unittests;

import java.math.BigDecimal;

import pizzario.model.PizzaKind;
import pizzario.model.OrderItem;
import pizzario.model.PizzaSize;
import static org.junit.Assert.*;

import org.junit.Test;

public class OrderItemTests
{
	@Test
    public void Constructor_ProjectsFields_Correctly ()
    {
        PizzaKind kind = makePizzaKind();
        OrderItem item = new OrderItem( kind, PizzaSize.SMALL,  new BigDecimal ( 3.0 ), 2 );

        assertSame( item.getPizzaKind(), kind );
        assertEquals( item.getPizzaSize(), PizzaSize.SMALL );
        assertEquals( item.getFixedPrice(),  new BigDecimal ( 3.0 ) );
        assertEquals( item.getQuantity(), 2 );
    }


    @Test( expected = Exception.class )
    public void Constructor_NegativePrice_Forbidden ()
    {
        new OrderItem( makePizzaKind(), PizzaSize.SMALL ,  new BigDecimal ( -0.01 ), 1 );
        new OrderItem( makePizzaKind(), PizzaSize.SMALL,  new BigDecimal( 0.00 ) , 1 );
    }


    @Test( expected = Exception.class )
    public void Constructor_NonPositiveQuantity_Forbidden ()
    {
        new OrderItem( makePizzaKind(), PizzaSize.SMALL,  new BigDecimal ( 0.01 ) , -1  );
        new OrderItem( makePizzaKind(), PizzaSize.SMALL,  new BigDecimal ( 0.01 ) , 0 );
    }


    @Test
    public void Cost_SingleItem_MatchesPrice ()
    {
        OrderItem item = new OrderItem( makePizzaKind(), PizzaSize.SMALL,  new BigDecimal ( 3.0 ) , 1 );

        assertEquals( item.getCost().doubleValue(), 
        			  item.getFixedPrice().doubleValue(), 
        			  0.001 );
    }


    @Test
    public void Cost_NItems_MultipliesPrice ()
    {
        OrderItem item = new OrderItem( makePizzaKind(), PizzaSize.SMALL, new BigDecimal( 3.0 ), 8 );
        assertEquals( item.getCost(), item.getFixedPrice().multiply( new BigDecimal( 8.0 ) ) );
    }


    private PizzaKind makePizzaKind ()
    {
        return new PizzaKind( "Carbonara" );
    }
}

