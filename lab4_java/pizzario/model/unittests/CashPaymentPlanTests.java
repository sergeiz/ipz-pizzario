package pizzario.model.unittests;

import pizzario.model.PaymentPlan;
import pizzario.model.CashPaymentPlan;

import static org.junit.Assert.*;

import org.junit.Test;


public class CashPaymentPlanTests 
{
	@Test
    public void Payed_Initially_False ()
    {
        PaymentPlan p = new CashPaymentPlan();
        assertFalse( p.wasPayed() );
    }


    @Test
    public void Payed_SetPayedToTrue_Confirmed ()
    {
        PaymentPlan p = new CashPaymentPlan();
        p.markPayed();
        assertTrue( p.wasPayed() );
    }


    @Test
    public void Prepayment_NotExpected ()
    {
        PaymentPlan p = new CashPaymentPlan();
        assertFalse( p.expectPrepayment() );
    }


    @Test
    public void Clone_Payed_StillPayed ()
    {
        PaymentPlan p1 = new CashPaymentPlan();
        p1.markPayed();
        PaymentPlan p2 = p1.clone();

        assertTrue( p2.wasPayed() );
    }


    @Test
    public void Clone_Unpayed_StillUnpayed ()
    {
        PaymentPlan p1 = new CashPaymentPlan();
        PaymentPlan p2 = p1.clone();

        assertFalse( p2.wasPayed() );
    }
}

