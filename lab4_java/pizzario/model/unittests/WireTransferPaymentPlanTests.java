package pizzario.model.unittests;

import pizzario.model.WireTransferPaymentPlan;
import pizzario.model.PaymentPlan;


import static org.junit.Assert.*;

import org.junit.Test;

public class WireTransferPaymentPlanTests 
{
	@Test
    public void Constructor_ProjectsFields_Correctly ()
    {
        WireTransferPaymentPlan plan = new WireTransferPaymentPlan( "123", "Ivanov" );

        assertEquals( plan.getCardCode(), "123" );
        assertEquals( plan.getCardHolder(), "Ivanov" );
    }


    @Test
    public void Payed_Initially_False ()
    {
        PaymentPlan p = makeWireTransferPlan();
        assertFalse( p.wasPayed() );
    }


    @Test
    public void Payed_SetPayedToTrue_Confirmed ()
    {
        PaymentPlan p = makeWireTransferPlan();
        p.markPayed();
        assertTrue( p.wasPayed() );
    }


    @Test
    public void Prepayment_Expected ()
    {
        PaymentPlan p = makeWireTransferPlan();
        assertTrue( p.expectPrepayment() );
    }


    @Test
    public void Clone_Payed_StillPayed ()
    {
        PaymentPlan p1 = makeWireTransferPlan();
        p1.markPayed();
        PaymentPlan p2 = p1.clone();

        assertTrue( p2.wasPayed() );
    }


    @Test
    public void Clone_Unpayed_StillUnpayed ()
    {
        PaymentPlan p1 = makeWireTransferPlan();
        PaymentPlan p2 = p1.clone();
        assertFalse( p2.wasPayed() );
    }

    
    @Test
    public void Clone_CopiesFields_Correctly ()
    {
        WireTransferPaymentPlan plan = new WireTransferPaymentPlan( "123", "Ivanov" );
        WireTransferPaymentPlan plan2 = ( WireTransferPaymentPlan ) plan.clone();

        assertEquals( plan2.getCardCode(), "123" );
        assertEquals( plan2.getCardHolder(), "Ivanov" );
    }

    
    private static WireTransferPaymentPlan makeWireTransferPlan ()
    {
        return new WireTransferPaymentPlan( "1234 5678 1234 5678", "Ivan Ivanov" );
    }
}

