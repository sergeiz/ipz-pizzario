package pizzario.model.unittests;

import java.math.BigDecimal;

import pizzario.model.CookingStatus;
import pizzario.model.OrderStatus;
import pizzario.model.PizzaKind;
import pizzario.model.OrderCart;
import pizzario.model.Order;
import pizzario.model.OrderItem;
import pizzario.model.DeliveryContact;
import pizzario.model.PaymentPlan;
import pizzario.model.CashPaymentPlan;
import pizzario.model.PizzaSize;
import pizzario.model.Pizza;
import pizzario.model.WireTransferPaymentPlan;
import static org.junit.Assert.*;

import org.junit.Test;

public class OrderTests 
{
	@Test
    public void Constructor_ProjectsProperties_Correctly ()
    {
        OrderCart cart = new OrderCart();
        DeliveryContact contact = new DeliveryContact( "" , "" , "" );
        PaymentPlan payPlan = new CashPaymentPlan();

        Order o = new Order( cart , contact, payPlan );

        assertSame( o.getOrderCart(), cart );
        assertSame( o.getDeliveryContact(), contact );
        assertSame( o.getPaymentPlan(), payPlan );
    }


    @Test( expected = Exception.class )
    public void Constructor_FromUnmodifiableCart_Forbidden ()
    {
        OrderCart cart = new OrderCart();
        cart.addItem( makeSomeItem() );
        cart.checkout();

        makeOrder( cart );
    }


    @Test
    public void Constructor_NoPizzasInitially_EvenWhenCartHasItems ()
    {
        OrderCart cart = new OrderCart();
        cart.addItem( makeSomeItem() );

        Order o = makeOrder( cart );

        assertEquals( o.getPizzasCount(), 0 );
    }


    @Test
    public void TotalCost_WithoutDiscount_MatchesCartCost ()
    {
        Order o = makeOrder();
        o.setDiscount( 0.0 );
        assertEquals( o.getTotalCost(), o.getOrderCart().getCost() );
    }


    @Test
    public void TotalCost_WithDiscount_ReducesCartCost ()
    {
        Order o = makeOrder();
        o.setDiscount( 0.5 );
        assertEquals( o.getTotalCost(), o.getOrderCart().getCost().divide( new BigDecimal( 2.0 )) );
    }


    @Test
    public void TotalCost_WithFullDiscount_MakesOrderFree ()
    {
        Order o = makeOrder();
        o.setDiscount( 1.0 );
        assertEquals( o.getTotalCost() , BigDecimal.ZERO );
    }


    @Test
    public void Discount_Initially_Zero ()
    {
        Order o = makeOrder();
        assertEquals( o.getDiscount() , 0.0 , 0.001 );
    }


    @Test
    public void Discount_SetValid_ReadBack ()
    {
        Order o = makeOrder();
        o.setDiscount(  0.2 );
        assertEquals( o.getDiscount() , 0.2 , 0.001 );
    }


    @Test( expected = Exception.class )
    public void Discount_SetNegative_Forbidden ()
    {
        Order o = makeOrder();
        o.setDiscount( -0.01 );
    }


    @Test( expected = Exception.class )
    public void Discount_SetMoreThan100Percent_Forbidden ()
    {
        Order o = makeOrder();
        o.setDiscount( 1.01 );
    }


    @Test
    public void Pizzas_ForSingleItem_SingleMatchingPizza ()
    {
        OrderCart cart = new OrderCart();
        cart.addItem( makeSomeItem( 1 ) );
        Order o = makeOrder( cart );
        cart.checkout();

        assertEquals( o.getPizzasCount(), 1 );
        assertSame( o.getPizza( 0 ).getPizzaKind(), cart.getItem( 0 ).getPizzaKind() );
        assertEquals( o.getPizza( 0 ).getPizzaSize(), cart.getItem( 0 ).getPizzaSize() );
    }


    @Test
    public void Pizzas_ForDoubleItem_TwoMatchingPizzas ()
    {
        OrderCart cart = new OrderCart();
        cart.addItem( makeSomeItem( 2 ) );
        Order o = makeOrder( cart );
        cart.checkout();

        assertEquals( o.getPizzasCount(), 2 );
        assertSame(  o.getPizza( 0 ).getPizzaKind(), cart.getItem( 0 ).getPizzaKind() );
        assertSame(  o.getPizza( 1 ).getPizzaKind(), cart.getItem( 0 ).getPizzaKind() );
        assertEquals( o.getPizza( 0 ).getPizzaSize(), cart.getItem( 0 ).getPizzaSize() );
        assertEquals( o.getPizza( 1 ).getPizzaSize(), cart.getItem( 0 ).getPizzaSize() );
    }


    @Test
    public void Pizzas_ForTwoSingleItems_TwoDifferentMatchingPizzas ()
    {
        OrderCart cart = new OrderCart();
        cart.addItem( makeSomeItem( 1 ) );
        cart.addItem( makeSomeOtherItem( 1 ) );
        Order o = makeOrder( cart );
        cart.checkout();

        assertEquals( o.getPizzasCount(), 2 );
        assertSame( o.getPizza( 0 ).getPizzaKind(), cart.getItem( 0 ).getPizzaKind() );
        assertSame( o.getPizza( 1 ).getPizzaKind(), cart.getItem( 1 ).getPizzaKind() );
        assertEquals( o.getPizza( 0 ).getPizzaSize(), cart.getItem( 0 ).getPizzaSize() );
        assertEquals( o.getPizza( 1 ).getPizzaSize(), cart.getItem( 1 ).getPizzaSize() );
    }


    @Test
    public void Pizzas_ForOneSingleDoubleOther_ThreeMatchingPizzas ()
    {
        OrderCart cart = new OrderCart();
        cart.addItem( makeSomeItem( 1 ) );
        cart.addItem( makeSomeOtherItem( 2 ) );
        Order o = makeOrder( cart );
        cart.checkout();

        assertEquals( o.getPizzasCount(), 3 );
        assertSame( o.getPizza( 0 ).getPizzaKind(), cart.getItem( 0 ).getPizzaKind() );
        assertSame( o.getPizza( 1 ).getPizzaKind(), cart.getItem( 1 ).getPizzaKind() );
        assertSame( o.getPizza( 2 ).getPizzaKind(), cart.getItem( 1 ).getPizzaKind() );
        assertEquals( o.getPizza( 0 ).getPizzaSize(), cart.getItem( 0 ).getPizzaSize() );
        assertEquals( o.getPizza( 1 ).getPizzaSize(), cart.getItem( 1 ).getPizzaSize() );
        assertEquals( o.getPizza( 2 ).getPizzaSize(), cart.getItem( 1 ).getPizzaSize() );
    }


    @Test
    public void Status_Initially_New ()
    {
        Order o = makeOrder();
        assertEquals( o.getOrderStatus(), OrderStatus.New );
    }


    @Test
    public void Status_AfterCheckout_Registered ()
    {
        Order o = makeOrder();
        o.getOrderCart().checkout();
        assertEquals( o.getOrderStatus(), OrderStatus.Registered );
    }


    @Test
    public void Status_AfterSinglePizzaReady_Ready4Delivery ()
    {
        Order o = makeOrder();
        o.getOrderCart().checkout();
        CookAllPizzas( o );
        assertEquals( o.getOrderStatus(), OrderStatus.Ready4Delivery );
    }


    @Test
    public void Status_AfterSinglePizzaStarted_Cooking ()
    {
        Order o = makeOrder();
        o.getOrderCart().checkout();

        Pizza p = o.getPizza( 0 );
        p.onCookingStarted();

        assertEquals( o.getOrderStatus(), OrderStatus.Cooking );
    }


    @Test
    public void Status_AfterOneOfTwoStarted_Cooking ()
    {
        OrderCart cart = new OrderCart();
        cart.addItem( makeSomeItem( 2 ) );
        Order o = makeOrder( cart );
        cart.checkout();

        Pizza p = o.getPizza( 0 );
        p.onCookingStarted();

        assertEquals( o.getOrderStatus(), OrderStatus.Cooking );
    }


    @Test
    public void Status_AfterOneOfTwoReady_Cooking()
    {
        OrderCart cart = new OrderCart();
        cart.addItem( makeSomeItem( 2 ) );
        Order o = makeOrder( cart );
        cart.checkout();

        CookPizza( o.getPizza( 0 ) );

        assertEquals( o.getOrderStatus(), OrderStatus.Cooking );
    }


    @Test
    public void Status_AfterBothReady_Ready4Delivery ()
    {
        OrderCart cart = new OrderCart();
        cart.addItem( makeSomeItem( 2 ) );
        Order o = makeOrder( cart );
        cart.checkout();

        CookAllPizzas( o );

        assertEquals( o.getOrderStatus(), OrderStatus.Ready4Delivery );
    }


    @Test
    public void Status_AfterStartedDelivery_Delivering ()
    {
        Order o = makeOrder();
        o.getOrderCart().checkout();
        CookAllPizzas( o );

        o.onStartedDelivery();

        assertEquals( o.getOrderStatus(), OrderStatus.Delivering );
    }


    @Test
    public void Status_AfterDelivery_Delivered ()
    {
        Order o = makeOrder();
        o.getOrderCart().checkout();
        CookAllPizzas( o );
        o.onStartedDelivery();
        o.getPaymentPlan().markPayed();

        o.onDelivered();

        assertEquals( o.getOrderStatus(), OrderStatus.Delivered );
    }


    @Test( expected = Exception.class )
    public void StartOrFinishDelivery_WhenNewOrder_Forbidden ()
    {
        Order o = makeOrder();
        o.onStartedDelivery();
        o.onDelivered();
    }


    @Test( expected = Exception.class )
    public void StartOrFinishDelivery_WhenRegisteredOrder_Forbidden ()
    {
        Order o = makeOrder();
        o.getOrderCart().checkout();
        o.onStartedDelivery();
        o.onDelivered();
    }


    @Test( expected = Exception.class )
    public void StartOrFinishDelivery_WhenStillCookingOrder_Forbidden ()
    {
        OrderCart cart = new OrderCart();
        cart.addItem( makeSomeItem( 2 ) );
        Order o = makeOrder( cart );
        o.getOrderCart().checkout();
        CookPizza( o.getPizza( 0 ) );

        o.onStartedDelivery();
        o.onDelivered();
    }


    @Test( expected = Exception.class )
    public void StartDelivery_WhenAlreadyStarted_Forbidden ()
    {
        Order o = makeOrder();
        o.getOrderCart().checkout();
        CookAllPizzas( o );
        o.onStartedDelivery();

        o.onStartedDelivery();
    }


    @Test( expected = Exception.class )
    public void StartDelivery_WhenDelivered_Forbidden ()
    {
        Order o = makeOrder();
        o.getOrderCart().checkout();
        CookAllPizzas( o );
        o.onStartedDelivery();
        o.getPaymentPlan().markPayed();
        o.onDelivered();

        o.onStartedDelivery();
    }


    @Test( expected = Exception.class )
    public void FinishDelivery_WhenDeliveryNotStarted_Forbidden ()
    {
        Order o = makeOrder();
        o.getOrderCart().checkout();
        CookAllPizzas( o );
        o.getPaymentPlan().markPayed();

        o.onDelivered();
    }


    @Test( expected = Exception.class )
    public void FinishDelivery_WhenAlreadyDelivered_Forbidden ()
    {
        Order o = makeOrder();
        o.getOrderCart().checkout();
        CookAllPizzas( o );
        o.onStartedDelivery();
        o.getPaymentPlan().markPayed();

        o.onDelivered();

        o.onDelivered();
    }


    @Test
    public void Cancel_NewOrder_Passes ()
    {
        Order o = makeOrder();
        o.cancel();

        assertEquals( o.getOrderStatus(), OrderStatus.Cancelled );
        assertEquals( o.getPizzasCount(), 0 );
    }


    @Test
    public void Cancel_RegisteredOrder_Passes ()
    {
        OrderCart cart = new OrderCart();
        cart.addItem( makeSomeItem( 2 ) );
        Order o = makeOrder( cart );
        cart.checkout();

        o.cancel();

        assertEquals( o.getOrderStatus(), OrderStatus.Cancelled );
        assertEquals( o.getPizzasCount(), 2 );
        assertEquals( o.getPizza( 0 ).getCookingStatus(), CookingStatus.Cancelled );
        assertEquals( o.getPizza( 1 ).getCookingStatus(), CookingStatus.Cancelled );
    }


    @Test
    public void Cancel_CookingOrder_PassesAndCancelsStartedPizzas ()
    {
        OrderCart cart = new OrderCart();
        cart.addItem( makeSomeItem( 3 ) );
        Order o = makeOrder( cart );
        cart.checkout();

        o.getPizza( 0 ).onCookingStarted();
        o.getPizza( 0 ).onCookingFinished();
        o.getPizza( 1 ).onCookingStarted();

        o.cancel();

        assertEquals( o.getOrderStatus(), OrderStatus.Cancelled );
        assertEquals( o.getPizzasCount(), 3 );
        assertEquals( o.getPizza( 0 ).getCookingStatus(), CookingStatus.Finished );
        assertEquals( o.getPizza( 1 ).getCookingStatus(), CookingStatus.Cancelled );
        assertEquals( o.getPizza( 2 ).getCookingStatus(), CookingStatus.Cancelled );
    }


    @Test
    public void Cancel_ReadyOrder_PassesButNotTouchesPizzas ()
    {
        OrderCart cart = new OrderCart();
        cart.addItem( makeSomeItem( 2 ) );
        Order o = makeOrder( cart );
        cart.checkout();
        CookAllPizzas( o );

        o.cancel();

        assertEquals( o.getOrderStatus(), OrderStatus.Cancelled );
        assertEquals( o.getPizzasCount(), 2 );
        assertEquals( o.getPizza( 0 ).getCookingStatus(), CookingStatus.Finished );
        assertEquals( o.getPizza( 1 ).getCookingStatus(), CookingStatus.Finished );
    }


    @Test( expected = Exception.class )
    public void Cancel_OrderThatLeftKitchen_Forbidden ()
    {
        Order o = makeOrder();
        o.getOrderCart().checkout();
        CookAllPizzas( o );
        
        o.onStartedDelivery();

        o.cancel();

        o.getPaymentPlan().markPayed();

        o.onDelivered();

        o.cancel();
    }


    @Test( expected = Exception.class )
    public void Cancel_AlreadyCancelledOrder_Forbidden ()
    {
        Order o = makeOrder();
        o.cancel();

        o.cancel();
    }


    @Test( expected = Exception.class )
    public void Payment_CashNotCollectedOnDelivery_Forbidden ()
    {
        Order o = makeOrder();
        o.getOrderCart().checkout();
        CookAllPizzas( o );
        o.onStartedDelivery();

        o.onDelivered();
    }


    @Test( expected = Exception.class )
    public void Payment_WhenPrepaymentExpected_CannotCheckoutWithoutPayment ()
    {
        Order o = makeOrder( makeWireTransferPlan() );
        
        o.getOrderCart().checkout();
    }

    
    @Test
    public void Payment_WhenPrepaymentExpected_CheckoutSucceedsWhenPayed ()
    {
        PaymentPlan plan = makeWireTransferPlan();
        Order o = makeOrder( plan );
        plan.markPayed();
        o.getOrderCart().checkout();
    }


    private static Order makeOrder ()
    {
        OrderCart cart = new OrderCart();
        cart.addItem( makeSomeItem() );

        return makeOrder( cart );
    }


    private static Order makeOrder ( OrderCart cart )
    {
        return new Order( cart , new DeliveryContact( "" , "" , "" ), new CashPaymentPlan() );
    }


    private static Order makeOrder ( PaymentPlan plan )
    {
        OrderCart cart = new OrderCart();
        cart.addItem( makeSomeItem() );
        return new Order( cart , new DeliveryContact( "" , "" , "" ), plan );
    }


    private static OrderItem makeSomeItem ( int quantity )
    {
        return new OrderItem( new PizzaKind( "some" ), PizzaSize.SMALL, new BigDecimal( 1.0 ), quantity );
    }
    
    private static OrderItem makeSomeItem ( )
    {
    	return makeSomeItem( 1 );
    }


    private static OrderItem makeSomeOtherItem ( int quantity )
    {
        return new OrderItem( new PizzaKind( "other" ), PizzaSize.MEDIUM, new BigDecimal( 2.0 ), quantity );
    }


    private static PaymentPlan makeWireTransferPlan ()
    {
        return new WireTransferPaymentPlan( "1234 5678 1234 5678", "Ivan Ivanov" );
    }


    private static void CookPizza ( Pizza p )
    {
        p.onCookingStarted();
        p.onCookingFinished();
    }


    private static void CookAllPizzas ( Order o  )
    {
        for ( Pizza p : o.getPizzas() )
            CookPizza( p );
    }
}


