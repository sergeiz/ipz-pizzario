package pizzario.model.unittests;

import pizzario.model.PizzaRecipe;

import static org.junit.Assert.*;
import org.junit.Test;

public class PizzaRecipeTests
{
	@Test
	public void Ingredients_Initially_None ()
	{
		PizzaRecipe recipe = new PizzaRecipe();
		assertEquals( recipe.getIngredientsCount(), 0 );
	}
	
	@Test
    public void Ingredients_AskMissingInEmpty_ReturnsZero ()
    {
        PizzaRecipe recipe = new PizzaRecipe();
        assertEquals( recipe.getIngredientWeight( "cheese" ), 0 );
    }


    @Test
    public void Ingredients_InsertOne_ReturnWeight ()
    {
        PizzaRecipe recipe = new PizzaRecipe();
        recipe.addIngredient( "cheese", 10 );

        assertEquals( recipe.getIngredientsCount(), 1 );
        assertEquals( recipe.getIngredientWeight( "cheese" ), 10 );
    }
    

    @Test
    public void Ingredients_AskMissingInNonEmpty_ReturnsZero ()
    {
        PizzaRecipe recipe = new PizzaRecipe();
        recipe.addIngredient( "cheese", 10 );
        assertEquals( recipe.getIngredientWeight( "ham" ), 0 );
    }


    @Test
    public void Ingredients_InsertTwo_ReturnEachWeight ()
    {
        PizzaRecipe recipe = new PizzaRecipe();
        recipe.addIngredient( "cheese", 10 );
        recipe.addIngredient( "ham", 20 );

        assertEquals( recipe.getIngredientsCount(), 2 );
        assertEquals( recipe.getIngredientWeight( "cheese" ), 10 );
        assertEquals( recipe.getIngredientWeight( "ham" ), 20 );
    }


    @Test( expected = Exception.class )
    public void Ingredients_InsertDuplicate_Forbidden ()
    {
        PizzaRecipe recipe = new PizzaRecipe();
        recipe.addIngredient( "cheese", 10 );
        recipe.addIngredient( "cheese", 20 );
    }

    @Test
    public void Ingredients_InsertAndUpdate_ReturnUpdatedWeight ()
    {
        PizzaRecipe recipe = new PizzaRecipe();
        recipe.addIngredient( "cheese", 10 );
        recipe.updateIngredient( "cheese", 20 );
        assertEquals( recipe.getIngredientWeight( "cheese" ), 20 );
    }


    @Test
    public void Ingredients_UpdatingOneOfTwo_DoesNotAlterSecond ()
    {
        PizzaRecipe recipe = new PizzaRecipe();
        recipe.addIngredient( "cheese", 10 );
        recipe.addIngredient( "ham", 20 );
        recipe.updateIngredient( "cheese", 30 );

        assertEquals( recipe.getIngredientWeight( "cheese" ), 30 );
        assertEquals( recipe.getIngredientWeight( "ham" ), 20 );
    }


    @Test
    public void Ingredients_DeleteSingle_MakesEmpty ()
    {
        PizzaRecipe recipe = new PizzaRecipe();
        recipe.addIngredient( "cheese", 10 );
        recipe.removeIngredient( "cheese" );
        assertEquals( recipe.getIngredientsCount(), 0 );
    }


    @Test
    public void Ingredients_DeleteOneOfTwo_LeavesSecond ()
    {
        PizzaRecipe recipe = new PizzaRecipe();
        recipe.addIngredient( "cheese", 10 );
        recipe.addIngredient( "ham", 20 );
        recipe.removeIngredient( "cheese" );

        assertEquals( recipe.getIngredientsCount(), 1 );
        assertEquals( recipe.getIngredientWeight( "ham" ), 20 );
        assertEquals( recipe.getIngredientWeight( "cheese" ), 0 );
    }


    @Test( expected = Exception.class )
    public void Ingredients_DeleteMissingFromEmpty_Forbidden ()
    {
        PizzaRecipe recipe = new PizzaRecipe();
        recipe.removeIngredient( "cheese" );
    }


    @Test( expected = Exception.class )
    public void Ingredients_DeleteMissingFromNonEmpty_Forbidden ()
    {
        PizzaRecipe recipe = new PizzaRecipe();
        recipe.addIngredient( "ham", 50 );
        recipe.removeIngredient( "cheese" );
    }
}