package pizzario.model.unittests;

import java.math.BigDecimal;

import pizzario.model.PizzaKind;
import pizzario.model.PizzaSize;
import static org.junit.Assert.*;

import org.junit.Test;

import org.jmock.Mockery;
import org.jmock.Expectations;

public class PizzaKindTests
{
    @Test
    public void Constructor_ReturnsExpectedDefaults ()
    {
        PizzaKind kind = makeKind();
        
        assertEquals( kind.getName(), "Carbonara" );
        assertEquals( kind.getDescription() , "" );
        assertEquals( kind.getImageUrl() , "" );
        assertEquals( kind.getRatingVotesCount(), 0 );
        assertEquals( kind.getTotalRating(), 0 );
        assertFalse( kind.isHidden() );
        assertNotNull( kind.getRecipe() );
    }


    @Test( expected = Exception.class )
    public void Name_ConstructingWithEmpty_Forbidden ()
    {
        new PizzaKind( null ) ;
        new PizzaKind( "" ) ;
    }


    @Test( expected = Exception.class )
    public void Name_AssigningToEmpty_Forbidden ()
    {
        PizzaKind kind = makeKind();
        kind.rename( null ) ;
        kind.rename( "" )  ;
    }


    @Test
    public void Name_Reassignment_SendsEvent ()
    {
        PizzaKind kind = new PizzaKind( "Carbonara" );
        
        Mockery context = new Mockery();
        
        final PizzaKind.NameChangedListener listener = context.mock( PizzaKind.NameChangedListener.class );
        
        kind.addNameChangedListener( listener );
        
        context.checking( new Expectations() {
        	{
        		oneOf( listener ).onPizzaKindNameChanged( "Carbonara" , "Milano" );
        	}
        } );

        kind.rename( "Milano" );
        
        context.assertIsSatisfied();
        
    }


    @Test
    public void Name_ReassignmentWithoutListener_PassesSilently ()
    {
        PizzaKind kind = new PizzaKind( "Carbonara" );
        kind.rename( "New Carbonara" ) ;
        assertEquals( kind.getName(), "New Carbonara" );
    }


    @Test
    public void Name_ReassignmentToSame_DoesNotSendEvent ()
    {
    	PizzaKind kind = new PizzaKind( "Carbonara" );
        
        Mockery context = new Mockery();
        
        final PizzaKind.NameChangedListener listener = context.mock( PizzaKind.NameChangedListener.class );
        
        kind.addNameChangedListener( listener );

        kind.rename( "Carbonara" );

        context.assertIsSatisfied();
    }


    @Test
    public void Rating_WhenNoVotes_Zero ()
    {
        PizzaKind kind = makeKind();

        assertEquals( kind.getRatingVotesCount(), 0 );
        assertEquals( kind.getTotalRating(), 0 );
        assertEquals( kind.getAverageRating(), 0.0 , 0.001 );
    }


    @Test
    public void Rating_WhenSingleVote_ReturnsIt ()
    {
        PizzaKind kind = makeKind();
        kind.rate( 3 );

        assertEquals( kind.getRatingVotesCount(), 1 );
        assertEquals( kind.getTotalRating(), 3 );
        assertEquals( kind.getAverageRating(), 3.0, 0.001 );
    }


    @Test
    public void Rating_WhenTwoVotes_ReturnsAverage ()
    {
        PizzaKind kind = makeKind();
        kind.rate( 3 );
        kind.rate( 5 );

        assertEquals( kind.getRatingVotesCount(), 2 );
        assertEquals( kind.getTotalRating(), 8 );
        assertEquals( kind.getAverageRating(), 4.0, 0.001 );
    }


    @Test
    public void Rating_WhenThreeVotes_ReturnsAverage ()
    {
        PizzaKind kind = makeKind();
        kind.rate( 1 );
        kind.rate( 3 );
        kind.rate( 5 );

        assertEquals( kind.getRatingVotesCount(), 3 );
        assertEquals( kind.getTotalRating(), 9 );
        assertEquals( kind.getAverageRating(), 3.0, 0.001 );
    }


    @Test( expected = Exception.class )
    public void Rating_ZeroVotes_Forbidden ()
    {
        PizzaKind kind = makeKind();
        kind.rate( 0 ) ;
    }


    @Test( expected = Exception.class )
    public void Rating_NegativeVotes_Forbidden ()
    {
        PizzaKind kind = makeKind();
        kind.rate( -1 ) ;
    }


    @Test( expected = Exception.class )
    public void Rating_VotesBeyondMaximum_Forbidden ()
    {
        PizzaKind kind = makeKind();
        kind.rate( PizzaKind.MaximumVote );
        kind.rate( PizzaKind.MaximumVote + 1 ) ;
    }


    @Test
    public void RatingSetup_VotesStartFromConcrete_ReturnsSame ()
    {
        PizzaKind kind = makeKind();
        kind.setupRatingStats( 5, 25 );

        assertEquals( kind.getRatingVotesCount(), 5 );
        assertEquals( kind.getTotalRating(), 25 );
        assertEquals( kind.getAverageRating(), 5.0, 0.001 );
    }

    
    @Test
    public void RatingSetup_VotesBeforeSetup_GetOverwritten ()
    {
        PizzaKind kind = makeKind();
        kind.rate( 5 );
        kind.setupRatingStats( 5, 25 );

        assertEquals( kind.getRatingVotesCount(), 5 );
        assertEquals( kind.getTotalRating(), 25 );
        assertEquals( kind.getAverageRating(), 5.0, 0.001 );
    }


    @Test
    public void RatingSetup_VotesAfterSetup_AppendToSetup ()
    {
        PizzaKind kind = makeKind();
        kind.setupRatingStats( 5, 25 );
        kind.rate( 5 );

        assertEquals( kind.getRatingVotesCount(), 6 );
        assertEquals( kind.getTotalRating(), 30 );
        assertEquals( kind.getAverageRating(), 5.0, 0.001 );
    }


    @Test( expected = Exception.class )
    public void RatingSetup_NegativeVotesSetup_Forbidden ()
    {
        PizzaKind kind = makeKind();
        kind.setupRatingStats( -1, 0 ) ;
    }


    @Test
    public void RatingSetup_ZeroVotes_IsFineWhenZeroTotal ()
    {
        PizzaKind kind = makeKind();
        kind.setupRatingStats( 0, 0 ) ;
    }


    @Test( expected = Exception.class )
    public void RatingSetup_ZeroVotes_IsNotFineWithNonZeroTotal ()
    {
        PizzaKind kind = makeKind();

        kind.setupRatingStats( 0, 1 );
        kind.setupRatingStats( 0, -1 );
    }


    @Test( expected = Exception.class )
    public void RatingSetup_TotalSmallerThanVotes_NotAllowed ()
    {
        PizzaKind kind = makeKind();
        kind.setupRatingStats( 5, 5 ) ;
        kind.setupRatingStats( 5, 4 ) ;
        kind.setupRatingStats( 1, 0 ) ;
    }


    @Test( expected = Exception.class )
    public void RatingSetup_TotalLargeThanMaxVotes_NotAllowed ()
    {
        PizzaKind kind = makeKind();
        kind.setupRatingStats( 5, 25 ) ;
        kind.setupRatingStats( 5, 5 * PizzaKind.MaximumVote + 1 ) ;
        kind.setupRatingStats( 1, PizzaKind.MaximumVote + 1 ) ;
    }


    @Test
    public void Prices_Initially_Zeroes ()
    {
        PizzaKind kind = makeKind();

        assertEquals( kind.getCurrentPrice( PizzaSize.SMALL ),  new BigDecimal( 0.0 ) );
        assertEquals( kind.getCurrentPrice( PizzaSize.MEDIUM ), new BigDecimal( 0.0 ) );
        assertEquals( kind.getCurrentPrice( PizzaSize.LARGE ), new BigDecimal( 0.0 ) );
    }


    @Test
    public void Prices_UpdateEach_ConfirmAssignedValues ()
    {
        PizzaKind kind = makeKind();
        kind.updatePrice( PizzaSize.SMALL, new BigDecimal( 3.0 ) );
        kind.updatePrice( PizzaSize.MEDIUM, new BigDecimal( 5.0 ) );
        kind.updatePrice( PizzaSize.LARGE, new BigDecimal( 7.0 ) );

        assertEquals( kind.getCurrentPrice( PizzaSize.SMALL ), new BigDecimal( 3.0 ) );
        assertEquals( kind.getCurrentPrice( PizzaSize.MEDIUM ), new BigDecimal( 5.0 ) );
        assertEquals( kind.getCurrentPrice( PizzaSize.LARGE ), new BigDecimal( 7.0 ) );
    }


    @Test
    public void Prices_UpdateSameTypeTwice_SavesSecondPrice ()
    {
        PizzaKind kind = makeKind();
        kind.updatePrice( PizzaSize.SMALL, new BigDecimal( 3.0 ) );
        kind.updatePrice( PizzaSize.SMALL, new BigDecimal( 3.5 ) );

        assertEquals( kind.getCurrentPrice( PizzaSize.SMALL ), new BigDecimal( 3.5 ) );
    }


    @Test
    public void Prices_UpdateToZero_Allowed ()
    {
        PizzaKind kind = makeKind();
        kind.updatePrice( PizzaSize.SMALL, BigDecimal.ZERO );

        assertEquals( kind.getCurrentPrice( PizzaSize.SMALL ), new BigDecimal( 0.0 ) );
    }


    @Test( expected = Exception.class )
    public void Prices_UpdateToNegative_Forbidden ()
    {
        PizzaKind kind = makeKind();
        kind.updatePrice( PizzaSize.SMALL, new BigDecimal( -0.01 ) ) ;
    }


    private PizzaKind makeKind ()
    {
        return new PizzaKind( "Carbonara" );
    }
}
