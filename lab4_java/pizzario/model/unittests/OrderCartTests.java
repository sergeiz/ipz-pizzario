package pizzario.model.unittests;

import java.math.BigDecimal;

import pizzario.model.PizzaKind;
import pizzario.model.OrderItem;
import pizzario.model.OrderCart;
import pizzario.model.PizzaSize;
import static org.junit.Assert.*;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Test;

public class OrderCartTests 
{
	@Test
    public void Items_None_Initially ()
    {
        OrderCart cart = new OrderCart();
        assertEquals( cart.getItemsCount(), 0 );
    }


    @Test
    public void Add_One_ListOne ()
    {
        OrderCart cart = new OrderCart();
        OrderItem item = makeSmallItem();
        cart.addItem( item );

        assertEquals( cart.getItemsCount(), 1 );
        assertSame( cart.getItem( 0 ), item );
    }


    @Test
    public void Add_Two_ListTwo ()
    {
        OrderCart cart = new OrderCart();
        OrderItem item1 = makeSmallItem();
        OrderItem item2 = makeMediumItem();
        cart.addItem( item1 );
        cart.addItem( item2 );

        assertEquals( cart.getItemsCount(), 2 );
        assertSame( cart.getItem( 0 ), item1 );
        assertSame( cart.getItem( 1 ), item2 );
    }


    @Test( expected = Exception.class )
    public void Add_TwoSameKindSize_Fails ()
    {
        OrderCart cart = new OrderCart();
        PizzaKind kind = makePizzaKind();
        cart.addItem( new OrderItem( kind, PizzaSize.SMALL, new BigDecimal( 3.0 ), 1 ) );
        
        cart.addItem( new OrderItem( kind, PizzaSize.SMALL, new BigDecimal( 3.0 ), 1 ) ) ;
    }


    @Test
    public void Add_TwoSameKindDifferentSize_Passes ()
    {
        OrderCart cart = new OrderCart();
        PizzaKind kind = makePizzaKind();
        OrderItem itemS = new OrderItem( kind, PizzaSize.SMALL, new BigDecimal( 3.0 ), 1 );
        OrderItem itemM = new OrderItem( kind, PizzaSize.MEDIUM, new BigDecimal( 3.0 ), 1 );
        cart.addItem( itemS );
        cart.addItem( itemM );

        assertEquals( cart.getItemsCount(), 2 );
        assertSame( cart.getItem( 0 ), itemS );
        assertSame( cart.getItem( 1 ), itemM );
    }


    @Test
    public void Add_TwoDifferentKindSameSize_Passes ()
    {
        OrderCart cart = new OrderCart();
        PizzaKind kind1 = makePizzaKind();
        PizzaKind kind2 = makeAnotherPizzaKind();
        OrderItem item1 = new OrderItem( kind1, PizzaSize.SMALL, new BigDecimal( 3.0 ), 1 );
        OrderItem item2 = new OrderItem( kind2, PizzaSize.SMALL, new BigDecimal( 3.0 ), 1 );
        cart.addItem( item1 );
        cart.addItem( item2 );

        assertEquals( cart.getItemsCount(), 2 );
        assertSame( cart.getItem( 0 ), item1 );
        assertSame( cart.getItem( 1 ), item2 );
    }


    @Test( expected = Exception.class )
    public void Update_OnEmpty_Fails ()
    {
        OrderCart cart = new OrderCart();
        cart.updateItem( 0, makeSmallItem() ) ;
    }


    @Test
    public void Update_OnNonEmptyGoodIndex_Passes ()
    {
        OrderCart cart = new OrderCart();
        cart.addItem( makeSmallItem() );
        OrderItem newItem = makeMediumItem();
        cart.updateItem( 0, newItem );

        assertEquals( cart.getItemsCount(), 1 );
        assertSame( cart.getItem( 0 ), newItem );
    }


    @Test( expected = Exception.class )
    public void Update_OnNonEmptyBadIndex_Fails ()
    {
        OrderCart cart = new OrderCart();
        cart.addItem( makeSmallItem() );
        
        cart.updateItem( 1, makeMediumItem() );
        cart.updateItem( -1, makeMediumItem() );
    }

    @Test( expected = Exception.class )
    public void Drop_Empty_Fails ()
    {
        OrderCart cart = new OrderCart();

        cart.dropItem( 0 );
    }


    @Test
    public void Drop_NonEmptyGoodIndex_Passes ()
    {
        OrderCart cart = new OrderCart();
        cart.addItem( makeSmallItem() );
        cart.dropItem( 0 );

        assertEquals( cart.getItemsCount(), 0 );
    }


    @Test( expected = Exception.class )
    public void Drop_NonEmptyBadIndex_Fails ()
    {
        OrderCart cart = new OrderCart();
        cart.addItem( makeSmallItem() );

        cart.dropItem( 1 );
        cart.dropItem( -1 );
    }


    @Test
    public void Clear_Empty_Passes ()
    {
        OrderCart cart = new OrderCart();
        cart.clearItems();
        assertEquals( cart.getItemsCount(), 0 );
    }


    @Test
    public void Clear_NonEmpty_Passes ()
    {
        OrderCart cart = new OrderCart();
        cart.addItem( makeSmallItem() );
        cart.addItem( makeMediumItem() );
        cart.clearItems();
        assertEquals( cart.getItemsCount(), 0 );
    }


    @Test
    public void Cost_Empty_Zero ()
    {
        OrderCart cart = new OrderCart();

        assertEquals( cart.getCost(), new BigDecimal( 0.0 ) );
    }


    @Test
    public void Cost_SingleItem_MatchesItem ()
    {
        OrderCart cart = new OrderCart();
        cart.addItem( makeSmallItem() );

        assertEquals( cart.getCost(), cart.getItem( 0 ).getCost() );
    }


    @Test
    public void Cost_TwoItems_MatchesSumItems ()
    {
        OrderCart cart = new OrderCart();
        cart.addItem( makeSmallItem() );
        cart.addItem( makeMediumItem() );

        assertEquals( cart.getCost(), cart.getItem( 0 ).getCost().add(
        							  cart.getItem( 1 ).getCost() ) );
    }


    @Test
    public void Modifiable_IsTrue_Initially ()
    {
        OrderCart cart = new OrderCart();
        assertTrue( cart.isModifiable() );
    }


    @Test
    public void Modifiable_AfterCheckout_IsFalse ()
    {
        OrderCart cart = new OrderCart();
        cart.addItem( makeSmallItem() );
        cart.checkout();

        assertFalse( cart.isModifiable() );
    }


    @Test( expected = Exception.class )
    public void Checkout_Empty_Fails ()
    {
        OrderCart cart = new OrderCart();
        cart.checkout();
    }


    @Test
    public void Checkout_SendsEvent ()
    {
    	OrderCart cart = new OrderCart();
    	cart.addItem( makeSmallItem() );
    	
    	Mockery context = new Mockery();
        
        final OrderCart.CheckoutListener listener = context.mock( OrderCart.CheckoutListener.class );
        cart.addCheckoutListener( listener );
        
        context.checking( new Expectations() {
        	{
        		oneOf( listener ).onOrderCartCheckout();
        	}
        } );

        cart.checkout();
        
        context.assertIsSatisfied();
    }


    @Test
    public void Unmodifiable_ReadingItems_Passes ()
    {
        OrderCart cart = new OrderCart();
        OrderItem item1 = makeSmallItem();
        OrderItem item2 = makeMediumItem();
        cart.addItem( item1 );
        cart.addItem( item2 );

        cart.checkout();

        assertEquals( cart.getItemsCount(), 2 );
        assertSame( cart.getItem( 0 ), item1 );
        assertSame( cart.getItem( 1 ), item2 );
    }


    @Test( expected = Exception.class )
    public void Unmodifiable_AddItem_Forbidden ()
    {
        OrderCart cart = new OrderCart();
        cart.addItem( makeSmallItem() );
        cart.checkout();

        cart.addItem( makeMediumItem() );
    }


    @Test( expected = Exception.class )
    public void Unmodifiable_UpdateItem_Forbidden ()
    {
        OrderCart cart = new OrderCart();
        cart.addItem( makeSmallItem() );
        cart.checkout();

        cart.updateItem( 0, makeMediumItem() );
    }


    @Test( expected = Exception.class )
    public void Unmodifiable_DropItem_Forbidden ()
    {
        OrderCart cart = new OrderCart();
        cart.addItem( makeSmallItem() );
        cart.checkout();

        cart.dropItem( 0 );
    }


    @Test( expected = Exception.class )
    public void Unmodifiable_ClearItems_Forbidden ()
    {
        OrderCart cart = new OrderCart();
        cart.addItem( makeSmallItem() );
        cart.checkout();

        cart.clearItems();
    }


    private PizzaKind makePizzaKind ()
    {
        return new PizzaKind( "Carbonara" );
    }


    private PizzaKind makeAnotherPizzaKind ()
    {
        return new PizzaKind( "Milano" );
    }


    private OrderItem makeSmallItem ()
    {
        return new OrderItem( makePizzaKind(), PizzaSize.SMALL, new BigDecimal( 3.0 ), 1 );
    }


    private OrderItem makeMediumItem ()
    {
        return new OrderItem( makeAnotherPizzaKind(), PizzaSize.MEDIUM, new BigDecimal( 5.0 ), 1 );
    }
}


