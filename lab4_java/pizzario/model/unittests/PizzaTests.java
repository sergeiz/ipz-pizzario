package pizzario.model.unittests;

import pizzario.model.CookingStatus;
import pizzario.model.PizzaKind;
import pizzario.model.Pizza;
import pizzario.model.PizzaSize;
import static org.junit.Assert.*;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Test;

public class PizzaTests 
{
	@Test
    public void Constructor_ProjectsFields_Correctly ()
    {
        PizzaKind kind = new PizzaKind( "Carbonara" );
        Pizza p = new Pizza( kind, PizzaSize.SMALL );

        assertSame( p.getPizzaKind(), kind );
        assertEquals( p.getPizzaSize(), PizzaSize.SMALL );
    }


    @Test
    public void Constructor_Initially_NotStarted ()
    {
        Pizza p = makeSimplePizza();
        assertEquals( p.getCookingStatus(), CookingStatus.NotStarted );
    }


    @Test
    public void CookingStarted_FromInitialState_Passes ()
    {
        Pizza p = makeSimplePizza();
        p.onCookingStarted();
        assertEquals( p.getCookingStatus(), CookingStatus.Started );
    }


    @Test( expected = Exception.class )
    public void CookingStarted_AlreadyStarted_Fails ()
    {
        Pizza p = makeSimplePizza();
        p.onCookingStarted();
        p.onCookingStarted() ;
    }


    @Test( expected = Exception.class )
    public void CookingStarted_AfterFinish_Fails ()
    {
        Pizza p = makeSimplePizza();
        p.onCookingStarted();
        p.onCookingFinished();
        p.onCookingStarted();
    }

    
    @Test( expected = Exception.class )
    public void CookingStarted_AfterCancelled_Fails ()
    {
        Pizza p = makeSimplePizza();
        p.onCookingStarted();
        p.onCookingCancelled();
        p.onCookingStarted() ;
    }


    @Test
    public void CookingStarted_SendsEvent ()
    {
        Pizza p = makeSimplePizza();
        
        Mockery context = new Mockery();
        
        final Pizza.PizzaEventListener listener = context.mock( Pizza.PizzaEventListener.class );
        p.addPizzaEventListener( listener );
        
        context.checking( new Expectations(){
        	{
        		oneOf( listener ).onCookingStarted();
        	}
        });
        
        p.onCookingStarted();

        context.assertIsSatisfied();
    }


    @Test
    public void CookingCancelled_FromStarted_Passes ()
    {
        Pizza p = makeSimplePizza();
        p.onCookingStarted();
        p.onCookingCancelled();
        assertEquals( p.getCookingStatus(), CookingStatus.Cancelled );
    }


    @Test
    public void CookingCancelled_FromInitial_Passes ()
    {
        Pizza p = makeSimplePizza();
        p.onCookingCancelled();
        assertEquals( p.getCookingStatus(), CookingStatus.Cancelled );
    }


    @Test( expected = Exception.class )
    public void CookingCancelled_AlreadyCancelled_Fails ()
    {
        Pizza p = makeSimplePizza();
        p.onCookingStarted();
        p.onCookingCancelled();
        p.onCookingCancelled();
    }


    @Test( expected = Exception.class )
    public void CookingCancelled_AfterFinish_Fails ()
    {
        Pizza p = makeSimplePizza();
        p.onCookingStarted();
        p.onCookingFinished();
        p.onCookingCancelled();
    }


    @Test( expected = Exception.class )
    public void CookingFinished_FromInitial_Fails ()
    {
        Pizza p = makeSimplePizza();
        p.onCookingFinished();
    }


    @Test
    public void CookingFinished_FromStarted_Passes ()
    {
        Pizza p = makeSimplePizza();
        p.onCookingStarted();
        p.onCookingFinished();
        assertEquals( p.getCookingStatus(), CookingStatus.Finished );
    }


    @Test( expected = Exception.class )
    public void CookingFinished_AlreadyFinished_Fails ()
    {
        Pizza p = makeSimplePizza();
        p.onCookingStarted();
        p.onCookingFinished();
        p.onCookingFinished();
    }


    @Test( expected = Exception.class )
    public void CookingFinished_WasCancelled_Fails ()
    {
        Pizza p = makeSimplePizza();
        p.onCookingStarted();
        p.onCookingCancelled();
        p.onCookingFinished();
    }
    

    @Test
    public void CookingFinished_SendsEvent ()
    {
    	Pizza p = makeSimplePizza();
        
        Mockery context = new Mockery();
        
        final Pizza.PizzaEventListener listener = context.mock( Pizza.PizzaEventListener.class );
        p.addPizzaEventListener( listener );
        
        context.checking( new Expectations() {
        	{
        		oneOf( listener ).onCookingStarted();
        		oneOf( listener ).onCookingFinished();
        	}
        } );
        
        p.onCookingStarted();
        p.onCookingFinished();

        context.assertIsSatisfied();
    }

    private static Pizza makeSimplePizza ()
    {
        return new Pizza( new PizzaKind( "some" ), PizzaSize.SMALL );
    }
}


