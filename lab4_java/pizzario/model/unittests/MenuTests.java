package pizzario.model.unittests;

import java.math.BigDecimal;
import java.util.Iterator;

import pizzario.model.PizzaKind;
import pizzario.model.Menu;
import pizzario.model.OrderItem;
import pizzario.model.PizzaSize;
import static org.junit.Assert.*;

import org.junit.Test;

public class MenuTests 
{
	@Test
    public void Kinds_Initially_Empty ()
    {
        Menu menu = new Menu();
        assertEquals( menu.getPizzaKindsCount(), 0 );
    }


    @Test
    public void Kinds_AddOne_FindIt ()
    {
        Menu menu = new Menu();
        PizzaKind carbonara = new PizzaKind( "Carbonara" );
        menu.addPizzaKind( carbonara );

        assertEquals( menu.getPizzaKindsCount(), 1 );
        assertSame( menu.findPizzaKind( "Carbonara" ), carbonara );
    }


    @Test
    public void Kinds_AddTwo_FindBoth ()
    {
        Menu menu = new Menu();
        
        PizzaKind carbonara = new PizzaKind( "Carbonara" );
        PizzaKind milano = new PizzaKind( "Milano" );
        menu.addPizzaKind( carbonara );
        menu.addPizzaKind( milano );

        assertEquals( menu.getPizzaKindsCount(), 2 );
        assertSame( menu.findPizzaKind( "Carbonara" ), carbonara );
        assertSame( menu.findPizzaKind( "Milano" ), milano );
    }


    @Test
    public void Kinds_FindMissingInEmpty_ReturnsNull ()
    {
        Menu menu = new Menu();
        assertNull( menu.findPizzaKind( "Carbonara" ) );
    }


    @Test
    public void Kinds_FindMissingInNonEmpty_ReturnsNull ()
    {
        Menu menu = new Menu();
        menu.addPizzaKind( new PizzaKind( "Carbonara" ) );
        assertNull( menu.findPizzaKind( "Milano" ) );
    }


    @Test( expected = Exception.class )
    public void Kinds_AddDuplicate_Fails ()
    {
        Menu menu = new Menu();
        menu.addPizzaKind( new PizzaKind( "Carbonara" ) );
        menu.addPizzaKind( new PizzaKind( "Carbonara" ) ) ;
    }


    @Test
    public void Kinds_DeleteSingle_MakesEmpty ()
    {
        Menu menu = new Menu();
        menu.addPizzaKind( new PizzaKind( "Carbonara" ) );
        menu.deletePizzaKind( "Carbonara" );
        assertEquals( menu.getPizzaKindsCount(), 0 );
    }


    @Test
    public void Kinds_DeleteOneOfTwo_LeavesOther ()
    {
        Menu menu = new Menu();
        menu.addPizzaKind( new PizzaKind( "Carbonara" ) );
        menu.addPizzaKind( new PizzaKind( "Milano" ) );
        menu.deletePizzaKind( "Carbonara" );

        assertEquals( menu.getPizzaKindsCount(), 1 );

        Iterator < PizzaKind > pizzaKindsIterator = menu.pizzaKinds().iterator();
        PizzaKind kind = pizzaKindsIterator.next();
        assertEquals( kind.getName() , "Milano" );
    }


    @Test( expected = Exception.class )
    public void Kinds_DeleteMissingInEmpty_Forbidden ()
    {
        Menu menu = new Menu();
        menu.deletePizzaKind( "Milano" ) ;
    }


    @Test( expected = Exception.class )
    public void Kinds_DeleteMissingInNonEmpty_Forbidden ()
    {
        Menu menu = new Menu();
        menu.addPizzaKind( new PizzaKind( "Carbonara" ) );
        menu.deletePizzaKind( "Milano" );
    }


    @Test
    public void Rename_TheOnly_AccessibleUnderNewName ()
    {
        Menu menu = new Menu();
        PizzaKind carbonara = new PizzaKind( "carbonara" );
        menu.addPizzaKind( carbonara );

        carbonara.rename( "Carbonara" );

        assertSame( menu.findPizzaKind( "Carbonara" ), carbonara );
        assertEquals( menu.getPizzaKindsCount(), 1 );
    }


    @Test
    public void Rename_OneOfTwo_AccessibleUnderNewName_SecondUntouched ()
    {
        Menu menu = new Menu();
        PizzaKind carbonara = new PizzaKind( "carbonara" );
        PizzaKind milano = new PizzaKind( "Milano" );
        menu.addPizzaKind( carbonara );
        menu.addPizzaKind( milano );

        carbonara.rename( "Carbonara" );

        assertSame( menu.findPizzaKind( "Carbonara" ), carbonara );
        assertSame( menu.findPizzaKind( "Milano" ), milano );
        assertEquals( menu.getPizzaKindsCount(), 2 );
    }


    @Test( expected = Exception.class )
    public void Rename_ToExisting_Forbidden ()
    {
        Menu menu = new Menu();
        PizzaKind carbonara = new PizzaKind( "Carbonara" );
        PizzaKind milano = new PizzaKind( "Milano" );
        menu.addPizzaKind( carbonara );
        menu.addPizzaKind( milano );

        carbonara.rename( "Milano" );
    }


    @Test
    public void Rename_Deleted_UnaffectsMenu ()
    {
        Menu menu = new Menu();
        PizzaKind carbonara = new PizzaKind( "Carbonara" );
        menu.addPizzaKind( carbonara );
        menu.deletePizzaKind( "Carbonara" );

        assertEquals( menu.getPizzaKindsCount(), 0 );
        carbonara.rename( "New Carbonara" );
    }


    @Test
    public void OrderItem_Test_ExpectedResult ()
    {
        PizzaKind carbonara = new PizzaKind( "Carbonara" );
        carbonara.updatePrice( PizzaSize.SMALL, new BigDecimal ( 3.0 ) );

        Menu menu = new Menu();
        menu.addPizzaKind( carbonara );

        OrderItem item = menu.makeOrderItem( "Carbonara", PizzaSize.SMALL, 1 );
        assertSame( item.getPizzaKind(), carbonara );
        assertEquals( item.getQuantity(), 1 );
        assertEquals( item.getPizzaSize(), PizzaSize.SMALL );
        assertEquals( item.getFixedPrice(), new BigDecimal( 3.0 ) );
    }
}
