package pizzario.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class PizzaKind
{
	public interface NameChangedListener
	{
		void onPizzaKindNameChanged ( String oldName, String newName );
	}
	
	
	public final static int MaximumVote = 5;
	
	
	public PizzaKind ( String name )
	{		
		validateName( name );
		
		this.nameChangedListeners = new ArrayList< NameChangedListener >();
		this.recipe = new PizzaRecipe();
		
		int nPrices = PizzaSize.values().length;
		this.priceBySize = new BigDecimal[ nPrices ];
		for ( int i = 0; i < nPrices; i++ )
			this.priceBySize[ i ] = BigDecimal.ZERO;
		
		this.name = name;
		this.description = "";
		this.imageURL = "";
		this.votes = 0;
		this.totalRating = 0;
		this.hidden = false;
	}
	
	
	private void validateName ( String value )
	{
		if ( value == null || value.length() == 0 )
            throw new RuntimeException( "PizzaKind: empty name" );
	}
	

	public String getName ()
	{
		return name;
	}
	
	
	public void rename ( String value )
	{
		if ( value == null || value.isEmpty() )
            throw new RuntimeException( "PizzaKind: empty name" );
		
        if ( name != value )
        	for ( NameChangedListener l : nameChangedListeners )
        		l.onPizzaKindNameChanged( name, value );
 
        name = value;
	}
	
	
	public void addNameChangedListener ( NameChangedListener listener )
	{
		nameChangedListeners.add( listener );
	}

	
	public void removeNameChangedListener ( NameChangedListener listener )
	{
		nameChangedListeners.remove( listener );
	}
	
	
	public String getDescription ()
	{
		return description;
	}
	
	
	public void updateDescription ( String description )
	{
		this.description = description;
	}
	
	
	public String getImageUrl ()
	{
		return imageURL;
	}
	
	
	public void setImageURL ( String imageURL )
	{
		this.imageURL = imageURL;
	}
	
	
	public int getRatingVotesCount ()
	{
		return votes;
	}
	
	
	public int getTotalRating ()
	{
		return totalRating;
	}
	
	
	public boolean isHidden ()
	{
		return hidden;
	}
	
	
	public void setHidden ( boolean hidden )
	{
		this.hidden = hidden;
	}
	
	
	public PizzaRecipe getRecipe ()
	{
		return recipe;
	}
	
	
	public double getAverageRating ()
	{
		if ( votes != 0 )
			return ( double ) ( totalRating ) / votes;
		else
			return 0.0;
	}
	
	
	public void rate ( int rating )
	{
		if ( rating <= 0 || rating > MaximumVote )
            throw new RuntimeException( "PizzaKind.rate: expecting rating within [1;5] range" );
		
        else
        {
        	this.totalRating += rating;
        	this.votes ++;
        }
	}
	
	
	public void setupRatingStats ( int votes, int totalRating )
	{
		if ( votes < 0 )
            throw new RuntimeException( "PizzaKind.setupRatingStats: negative votes" );

        else if ( votes == 0 && totalRating != 0 )
            throw new RuntimeException( "PizzaKind.setupRatingStats: zero votes can only give zero ratings" );

        else if ( votes > 0 && totalRating < votes )
            throw new RuntimeException( "PizzaKind.setupRatingStats: total rating may not be smaller than number of votes" );

        else if ( votes > 0 && totalRating > ( votes * MaximumVote ) )
            throw new RuntimeException( "PizzaKind.setupRatingStats: total rating may not exceed maximum from all votes" );
		
		this.votes = votes;
		this.totalRating = totalRating;
	}
	
	
	public BigDecimal getCurrentPrice ( PizzaSize size )
	{
		return priceBySize[ size.ordinal() ];
	}
	
	
	public void updatePrice ( PizzaSize size, BigDecimal price )
	{
		if ( price.compareTo( BigDecimal.ZERO ) == -1 )
            throw new RuntimeException( "PizzaKind.updatePrice: price cannot be negative" );
		
		priceBySize[ size.ordinal() ] = price;
	}
	
	
	private List< NameChangedListener > nameChangedListeners;
	private String name;
	private String description;
	private String imageURL;
	private int votes;
	private int totalRating;
	private PizzaRecipe recipe;
	private BigDecimal[] priceBySize;
	private boolean hidden;	
}
