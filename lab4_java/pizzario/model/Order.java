package pizzario.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Order 
	implements OrderCart.CheckoutListener, 
			   Pizza.PizzaEventListener
{
    public Order ( OrderCart cart, DeliveryContact contact, PaymentPlan payPlan )
    {
    	if ( ! cart.isModifiable() )
            throw new RuntimeException( "Order: initializing with cart that is already checked out" );
    	
        this.contact = contact;
        this.payPlan = payPlan;
        this.cart = cart;
        this.status = OrderStatus.New;
        this.discount = 0.0;
        this.pizzas = new ArrayList< Pizza >();
        
        cart.addCheckoutListener( this );
    }
	
    
	public OrderCart getOrderCart ()
	{
		return cart;
	}
	
	
	public PaymentPlan getPaymentPlan ()
	{
		return payPlan;
	}
	
	
	public DeliveryContact getDeliveryContact ()
	{
		return contact;
	}
	
	
	public void setDeliveryContact ( DeliveryContact contact )
	{
		this.contact = contact;
	}
	
	
	public OrderStatus getOrderStatus ()
	{
		return status;
	}
	
	
	public double getDiscount ()
	{
		return discount;
	}
	
	
	public Iterable< Pizza > getPizzas ()
	{
		return pizzas;
	}
	
	
	public int getPizzasCount ()
    {
        return pizzas.size();
    }
	 
	
	public Pizza getPizza ( int index )
    {
        return pizzas.get( index );
    }
	
	
	public BigDecimal getTotalCost ()
    {
		BigDecimal cost = cart.getCost();
        return ( cost.multiply( new BigDecimal( 1.0 - discount ) ) );
    } 
	
	
	public void setDiscount ( double discount )
    {
        if ( discount < 0.0 || discount > 1.0 )
            throw new RuntimeException( "Order.setDisconut - invalid percentage value" );

        this.discount = discount;
    }
	
	public void process()
	{
		if ( status != OrderStatus.New )
	        throw new RuntimeException( "Order.process - Can only process new order!" );
		
		status = OrderStatus.Registered;
		
		int nItems = cart.getItemsCount();
        for ( int i = 0; i < nItems; i++ )
        {
            OrderItem item = cart.getItem( i );
            for ( int k = 0; k < item.getQuantity(); k++ )
                pizzas.add( new Pizza( item.getPizzaKind(), item.getPizzaSize() ) );
        }
	}
	
	
	public void cancel ()
    {
        switch ( status )
        {
	        case New:
	        case Registered:
	        case Cooking:
	        case Ready4Delivery:
		        break;

	        default:
		        throw new RuntimeException( "Order::cancel - cannot cancel in current order state" );
        }

        status = OrderStatus.Cancelled;
        for ( Pizza p : pizzas )
        	if ( p.getCookingStatus() != CookingStatus.Finished )
        		p.onCookingCancelled();
    }

	
    public void onPizzaCookingStarted () 
    {
    	if ( status == OrderStatus.Registered )
            status = OrderStatus.Cooking;

        else if ( status != OrderStatus.Cooking )
            throw new RuntimeException( "Order.onPizzaCookingStarted - can only happen in Registered & Cooking states" );		
	}

    
	public void onPizzaReady () 
	{
		if ( status != OrderStatus.Cooking )
            throw new RuntimeException( "Order.onPizzaReady - can only happen in Cooking state" );

		for ( Pizza pizza : pizzas )
		{
			if ( pizza.getCookingStatus() != CookingStatus.Finished )
				return;
		}
		
		status = OrderStatus.Ready4Delivery;
	}
	
	
	public void onStartedDelivery ()
    {
        if ( status != OrderStatus.Ready4Delivery )
            throw new RuntimeException( "Order.onStartedDelivery - can only happen in Ready4Delivery state");

        status = OrderStatus.Delivering;
    }
	
	
	public void onDelivered ()
    {
        if ( status != OrderStatus.Delivering )
	        throw new RuntimeException( "Order.onDelivered - can only happen in Delivering state" );
        
        if ( ! payPlan.expectPrepayment() && ! payPlan.wasPayed() )
            throw new RuntimeException( "Order.onDelivered - post-payment should have been collected by now" );

        status = OrderStatus.Delivered;
    }
	

	@Override
	public void onCookingStarted () 
	{
		if ( status == OrderStatus.Registered )
            status = OrderStatus.Cooking;

        else if ( status != OrderStatus.Cooking )
            throw new RuntimeException( "Order.onCookingStarted - can only happen in Registered & Cooking states" );
	}
	
	
	@Override
	public void onCookingFinished () 
	{
		if ( status != OrderStatus.Cooking )
            throw new RuntimeException( "Order.onCookingFinished - can only happen in Cooking state" );

		for ( Pizza p : pizzas )
			if ( p.getCookingStatus() != CookingStatus.Finished )
				return;
		
		status = OrderStatus.Ready4Delivery;
	}
	
	
	@Override
	public void onOrderCartCheckout () 
	{
		if ( status != OrderStatus.New )
            throw new RuntimeException( "Order.onOrderCartCheckout - Can only process new order!" );

        if ( payPlan.expectPrepayment() && ! payPlan.wasPayed() )
            throw new RuntimeException( "Order.onOrderCartCheckout - pre-payment unavailable" );

        status = OrderStatus.Registered;
        
        int nItems = cart.getItemsCount();
        for ( int i = 0; i < nItems; i++ )
        {
            OrderItem item = cart.getItem( i );
            for ( int k = 0; k < item.getQuantity(); k++ )
            {
                Pizza p = new Pizza( item.getPizzaKind(), item.getPizzaSize() );
                pizzas.add( p );
                
                p.addPizzaEventListener( this );
            }
        }
	}
	
	
	private OrderCart cart;
	private PaymentPlan payPlan;
	private DeliveryContact contact;
	private OrderStatus status;
	private double discount;
	private List< Pizza > pizzas;
}
