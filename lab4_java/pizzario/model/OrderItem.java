package pizzario.model;

import java.math.BigDecimal;

public class OrderItem 
{
	
	public OrderItem ( 
		PizzaKind kind, 
		PizzaSize size, 
		BigDecimal fixedPrice, 
		int quantity 
	)
	{
		if ( fixedPrice.compareTo( BigDecimal.ZERO ) == -1 )
			throw new RuntimeException( "OrderItem: negative price" );

		if ( quantity <= 0 )
			throw new RuntimeException( "OrderItem: non-positive quantity" );
            
		this.kind = kind;
		this.size = size;
		this.fixedPrice = fixedPrice;
		this.quantity = quantity;
	}
	
	
	public PizzaKind getPizzaKind ()
	{
		return kind;
	}
	
	
	public PizzaSize getPizzaSize ()
	{
		return size;
	}
	
	
	public BigDecimal getFixedPrice ()
	{
		return fixedPrice;
	}
	
	
	public int getQuantity ()
	{
		return quantity;
	}
	
	public BigDecimal getCost ()
    {
        return fixedPrice.multiply( new BigDecimal( quantity ) );
    }
	
	
	private PizzaKind kind;
	private PizzaSize size;
	private BigDecimal fixedPrice;
	private int quantity;
}
