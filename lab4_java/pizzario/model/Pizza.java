package pizzario.model;

import java.util.ArrayList;
import java.util.List;


public class Pizza 
{
	public interface PizzaEventListener
	{
		void onCookingStarted ();
		void onCookingFinished ();
	}
	
	
	public Pizza ( PizzaKind kind, PizzaSize size )
    {
		this.pizzaEventListener = new ArrayList< PizzaEventListener >();
        this.kind = kind;
        this.size = size;
        this.status = CookingStatus.NotStarted;
    }
	
	
	public void addPizzaEventListener ( PizzaEventListener listener )
	{
		pizzaEventListener.add( listener );
	}

	
	public void removePizzaEventListener ( PizzaEventListener listener )
	{
		pizzaEventListener.remove( listener );
	}
	
	
	public PizzaKind getPizzaKind ()
	{
		return kind;
	}
	
	
	public PizzaSize getPizzaSize ()
	{
		return size;
	}
	
	
	public CookingStatus getCookingStatus ()
	{
		return status;
	}
	
	
	public void onCookingStarted ()
	{
		if ( status == CookingStatus.NotStarted )
		{
			status = CookingStatus.Started;
			if ( pizzaEventListener != null )
				for( PizzaEventListener l : pizzaEventListener )
					l.onCookingStarted();
        }
        else
        	throw new RuntimeException( "Pizza.onCookingStarted: may only start cooking if not started yet" );
	}
	
	
	public void onCookingCancelled ()
    {
        if ( status == CookingStatus.Started || status == CookingStatus.NotStarted )
            status = CookingStatus.Cancelled;

        else
            throw new RuntimeException( "Pizza.onCookingCancelled: may only abort unfinished pizza" );
    }
	
	
	public void onCookingFinished ()
    {
        if ( status == CookingStatus.Started )
        {
            status = CookingStatus.Finished;
            if ( pizzaEventListener != null )
            	for( PizzaEventListener l : pizzaEventListener )
             		l.onCookingFinished();
        }
        else
            throw new RuntimeException( "Pizza.onCookingFinished: may only finish cooking if started" );
    }
	
	
	private List< PizzaEventListener > pizzaEventListener;
	private PizzaKind kind;
	private PizzaSize size;
	private CookingStatus status;
}
