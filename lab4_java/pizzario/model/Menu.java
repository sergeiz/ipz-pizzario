package pizzario.model;

import java.util.Map;
import java.util.TreeMap;

public class Menu implements PizzaKind.NameChangedListener
{
	public Iterable< String > pizzaKindNames ()
	{
		return pizzaKindsByName.keySet();
	}
	
	
	public Iterable< PizzaKind > pizzaKinds ()
	{
		return pizzaKindsByName.values();
	}
	
	
	public int getPizzaKindsCount ()
	{
		return pizzaKindsByName.size();
	}
	 
	
	public PizzaKind findPizzaKind ( String name )
	{
		return pizzaKindsByName.get( name );
	}
	
	
	public void addPizzaKind ( PizzaKind kind )
	{
		if ( pizzaKindsByName.containsKey( kind.getName() ) )
			throw new RuntimeException( "Menu.addPizzaKind: duplicate kind" );
		
		pizzaKindsByName.put( kind.getName(), kind );
		kind.addNameChangedListener( this );
	}
	
	
	public void deletePizzaKind ( String name )
	{
		if ( pizzaKindsByName.containsKey( name ) || name != null )
		{
			findPizzaKind( name ).removeNameChangedListener( this );
			pizzaKindsByName.remove( name );
		}
		else
			throw new RuntimeException( "Menu.deletePizzaKind: unregistered kind" );
	}
	 
	 
	public OrderItem makeOrderItem ( String name, PizzaSize size, int quantity )
	{
		PizzaKind kind = findPizzaKind( name );
        if ( kind == null )
        	throw new RuntimeException( "Menu.makeOrderItem: unregistered kind" );

		return new OrderItem( kind, size, kind.getCurrentPrice( size ), quantity );
	}
	

	@Override
	public void onPizzaKindNameChanged ( String oldName, String newName )
	{
		 PizzaKind kind = pizzaKindsByName.get( oldName );
		 if ( kind == null )
			 throw new RuntimeException( "Menu.onPizzaKindNameChanged: unregistered kind" );
		 
		 if ( oldName == newName )
            return;
		 
         if ( findPizzaKind( newName ) != null )
             throw new RuntimeException( "Menu.onPizzaKindNameChanged: target name already reserved" );
 
		 pizzaKindsByName.remove( oldName );
		 pizzaKindsByName.put( newName, kind );
	}
	
	private Map< String, PizzaKind > pizzaKindsByName = 
		new TreeMap< String, PizzaKind >();
}
