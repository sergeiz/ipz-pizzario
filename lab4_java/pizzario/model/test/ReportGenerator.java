package pizzario.model.test;

import java.io.PrintStream;

import pizzario.model.*;

public class ReportGenerator {
	
	public ReportGenerator ( PrintStream output, PizzarioNetwork network )
    {
        this.output = output;
        this.network = network;
    }

    public void generate ()
    {
        showMenu( network.getMenu() );
        showOrders( network.getOrders() );
    }

    private void showMenu ( Menu m )
    {
        output.println(" ==== Menu ==== ");

        for ( String name : m.pizzaKindNames() )
        {
            PizzaKind kind = m.findPizzaKind( name );
            output.println();
            output.println( "Pizza \"" + name + "\":" );
            output.println( "\tDescription: " + kind.getDescription() );
            output.println( "\tImage URL: " + kind.getImageUrl() );

            showPizzaRatings( kind );
            showPizzaIngredients( kind );
            showPizzaPrices( kind );
        }

        output.println();
        output.println(" ==== End Menu ==== ");
    }

    private void showPizzaRatings ( PizzaKind kind )
    {
        output.print( "\tRating: " );
        output.print( kind.getAverageRating() );
        output.println( " ( " + kind.getRatingVotesCount() + " votes)" );
    }

    private void showPizzaIngredients ( PizzaKind kind )
    {
        PizzaRecipe recipe = kind.getRecipe();

        boolean first = true;

        output.print( "\tIngredients: " );

        for ( String ingredient : recipe.browseIngredients() )
        {
            if ( ! first )
                output.print( ", " );
            else
                first = false;

            output.print( recipe.getIngredientWeight( ingredient ) );
            output.print( "g " );
            output.print( ingredient );
        }

        output.println();
    }

    private void showPizzaPrices ( PizzaKind kind )
    {
        output.print( "\tPrices: " );
        output.print( kind.getCurrentPrice( PizzaSize.SMALL ) );
        output.print( " small, " );
        output.print( kind.getCurrentPrice( PizzaSize.MEDIUM ) );
        output.print( " medium, " );
        output.print( kind.getCurrentPrice( PizzaSize.LARGE ) );
        output.println( " large" );
    }

    private void showOrders ( Iterable< Order > orders )
    {
        output.println( " ==== Orders ==== " );

        for ( Order order : orders )
            showOrder( order );

        output.println();
        output.println( " ==== End Orders ==== " );
    }


    private void showOrder ( Order order )
    {
        output.println();
        output.print( "Order :" );

        output.print( "\tStatus: " );
        output.println( order.getOrderStatus().name() );

        output.print( "\tTotal cost: " );
        output.println( order.getTotalCost() );

        output.print( "\tPayment plan: " );
        output.println( order.getPaymentPlan() );

        output.print( "\tDiscount: " );
        output.print( order.getDiscount() * 100.0 );
        output.println( '%' );

        showDeliveryContact( order.getDeliveryContact() );
        showCart( order.getOrderCart() );
        showOrderedPizzas( order );
    }


    private void showDeliveryContact ( DeliveryContact contact )
    {
        output.print( "\tContact: " );
        output.println( contact.getAddress() );

        output.print( "\tPhone: " );
        output.println( contact.getPhone() );

        output.print( "\tComment: " );
        output.println( contact.getComment() );
    }


    private void showCart ( OrderCart cart )
    {
        output.println( "\tCart:" );

        int nItems = cart.getItemsCount();
        for ( int i = 0; i < nItems; i++ )
        {
            output.print( "\t  " );
            output.print( i + 1 );
            output.print( ") " );

            OrderItem item = cart.getItem( i );

            output.print( item.getPizzaKind().getName() );
            output.print( ' ' );
            output.print( item.getPizzaSize().name() );
            output.print( ", price " );
            output.print( item.getFixedPrice() );
            output.print( ", quantity " );
            output.print( item.getQuantity() );
            output.print( ", cost " );
            output.println( item.getCost() );
        }
    }


    private void showOrderedPizzas ( Order order )
    {
        if ( order.getPizzasCount() == 0 )
            return;

        output.println( "\tPizzas:" );

        int nPizzas = order.getPizzasCount();
        for ( int i = 0; i < nPizzas; i++ )
        {
            output.print( "\t  " );
            output.print( i + 1 );
            output.print( ") Pizza " );

            Pizza p = order.getPizza( i );
            output.print( p.getPizzaKind().getName() );
            output.print( ' ' );
            output.print( p.getPizzaSize().name() );
            output.print( ", cooking status: " );
            output.println( p.getCookingStatus().name() );
        }
    }

    private PizzarioNetwork network;
    private PrintStream output;
}

