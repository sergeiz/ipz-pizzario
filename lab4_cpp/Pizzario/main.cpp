/** (C) 2015, Sergei Zaychenko, KNURE */

#include "application/application.hpp"

#include <iostream>

/*=============================================================================*/ 


int main ()
{
	try
	{
		Pizzario::Application theApp;
		theApp.run();
	}
	catch ( std::exception & e )
	{
		std::cout << "Error: " << e.what() << std::endl;
	}
	catch ( ... )
	{
		std::cout << "Unknown Error" << std::endl;
	}
}


/*=============================================================================*/ 
