/** (C) 2015, Sergei Zaychenko, KNURE */

#include "application.hpp"

#include "model/pizzario_network.hpp"
#include "model/menu.hpp"
#include "model/pizza_kind.hpp"
#include "model/pizza_recipe.hpp"
#include "model/order.hpp"
#include "model/order_cart.hpp"
#include "model/cash_payment_plan.hpp"
#include "model/delivery_contact.hpp"
#include "model/pizza.hpp"

#include <iostream>
#include <algorithm>

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/ 


Application::Application ()
{
	m_model.reset( new PizzarioNetwork() );

} // Application::Application


/*-----------------------------------------------------------------------------*/


Application::~Application ()
{
	// Model is destroyed automatically

} // Application::~Application


/*-----------------------------------------------------------------------------*/


void
Application::run ()
{
	fillTestModel();
	generateModelReport();

} // Application::run


/*-----------------------------------------------------------------------------*/


void
Application::fillTestModel ()
{
	fillPizzaMenu( m_model->getMenu() );
	fillOrder( m_model->getMenu() );

} // Application::fillTestModel


/*-----------------------------------------------------------------------------*/


void
Application::fillPizzaMenu ( Menu & _m )
{
	////

	std::unique_ptr< PizzaKind > pCarbonara( new PizzaKind( "Carbonara") );
	PizzaKind & carbonara = * pCarbonara;
	_m.addPizzaKind( std::move( pCarbonara ) );

	carbonara.updateDescription( "One of the most popular pizza recipes in the world" );
	carbonara.setupRatingStats( 170, 812 );
	carbonara.setImageURL( "http://pizzario.com.ua/images/carbonara.jpg");

	carbonara.getRecipe().addIngredient( "Cheese", 100 );
	carbonara.getRecipe().addIngredient( "Olive Oil", 30 );

	carbonara.updatePrice( PizzaSize::Small,  Money( 3, 0 ) );
	carbonara.updatePrice( PizzaSize::Medium, Money( 5, 0 ) );
	carbonara.updatePrice( PizzaSize::Large,  Money( 7, 0 ) );

	////

	std::unique_ptr< PizzaKind > pMilano( new PizzaKind( "Milano" ) );
	PizzaKind & milano = * pMilano;
	_m.addPizzaKind( std::move( pMilano ) );

	milano.updateDescription( "Classic recipe with ham, mushrooms and tomatoes" );
	milano.setupRatingStats( 102, 415 );
	milano.setImageURL( "http://pizzario.com.ua/images/milano.jpg");

	milano.getRecipe().addIngredient( "Ham", 100 );
	milano.getRecipe().addIngredient( "Mushrooms", 50 );
	milano.getRecipe().addIngredient( "Tomatoes", 50 );
	
	milano.updatePrice( PizzaSize::Small,  Money( 2, 50 ) );
	milano.updatePrice( PizzaSize::Medium, Money( 4, 15 ) );
	milano.updatePrice( PizzaSize::Large,  Money( 5, 80 ) );

} // Application::fillPizzaMenu


/*-----------------------------------------------------------------------------*/


void
Application::fillOrder ( Menu const & _menu )
{
	PizzaKind const & carbonara = * _menu.findPizzaKind( "Carbonara" );
	PizzaKind const & milano    = * _menu.findPizzaKind( "Milano" );

	Order & order1 = m_model->newOrder(
			DeliveryContact( "Lenin av. 14, room 320", "702-13-26", "APVT Department" )
		,	std::unique_ptr< PaymentPlan >( new CashPaymentPlan() )
	);

	order1.getCart().addItem(
		_menu.makeOrderItem( "Carbonara", PizzaSize::Medium, 2 )
	);

	order1.getCart().addItem(
		_menu.makeOrderItem( "Milano", PizzaSize::Large, 1 )
	);

	order1.setDiscountPercentage( 0.025 );

	order1.getCart().checkout();

	
	Order & order2 = m_model->newOrder(
			DeliveryContact( "23rd August Str 2, ap. 2", "333-33-33", "Knock 3 times" )
		,	std::unique_ptr< PaymentPlan >( new CashPaymentPlan() )
	);

	order2.getCart().addItem(
		_menu.makeOrderItem( "Carbonara", PizzaSize::Small, 1 )
	);


} // Application::fillOrder


/*-----------------------------------------------------------------------------*/


void
Application::generateModelReport ()
{
	showMenu( std::cout, m_model->getMenu() );
	showOrders( std::cout );

} // Application::generateModelReport


/*-----------------------------------------------------------------------------*/


void
Application::showMenu ( std::ostream & _o, Menu const & _menu )
{
	_o << " ==== Menu ==== " << std::endl;

	std::for_each(
			_menu.pizzaKindNamesBegin()
		,	_menu.pizzaKindNamesEnd()
		,	[ & ] ( std::string const & _pizzaName )
			{
				_o << std::endl << "Pizza \"" << _pizzaName << "\":" << std::endl;
				
				PizzaKind const & pizzaKind = * _menu.findPizzaKind( _pizzaName );

				_o << "\tDescription: " << pizzaKind.getDescription() << std::endl;
				_o << "\tImage URL: "   << pizzaKind.getImageURL()    << std::endl;

				showPizzaRatings( _o, pizzaKind );
				showPizzaIngredients( _o, pizzaKind );
				showPizzaPrices( _o, pizzaKind );
			}
	);

	_o << std::endl << " ==== End Menu ==== " << std::endl;

} // Application::showMenu


/*-----------------------------------------------------------------------------*/


void
Application::showPizzaRatings ( std::ostream & _o, PizzaKind const & _pizzaKind )
{
	_o << "\tRating: ";
	_o.precision( 2 );
	_o << _pizzaKind.getAverageRating();
	_o << " (" << _pizzaKind.getRatingVotesCount() << " votes)";
	_o << std::endl;

} // Application::showPizzaRatings



/*-----------------------------------------------------------------------------*/


void
Application::showPizzaIngredients ( std::ostream & _o, PizzaKind const & _pizzaKind )
{
	PizzaRecipe const & recipe = _pizzaKind.getRecipe();

	bool first = true;

	_o << "\tIngredients: ";

	std::for_each(
			recipe.ingredientNamesBegin()
		,	recipe.ingredientNamesEnd()
		,	[ & ] ( std::string const & _ingredient )
			{
				if ( ! first )
					_o << ", ";
				else
					first = false;

				_o << recipe.getIngredientWeight( _ingredient ) << "g " << _ingredient;
			}
	);

	_o << std::endl;

} // Application::showPizzaIngredients



/*-----------------------------------------------------------------------------*/


void
Application::showPizzaPrices ( std::ostream & _o, PizzaKind const & _pizzaKind )
{
	_o << "\tPrices: ";

	_o << _pizzaKind.getCurrentPrice( PizzaSize::Small )  << " " << PizzaSize::toString( PizzaSize::Small ) << ", ";
	_o << _pizzaKind.getCurrentPrice( PizzaSize::Medium ) << " " << PizzaSize::toString( PizzaSize::Medium ) << ", ";
	_o << _pizzaKind.getCurrentPrice( PizzaSize::Large )  << " " << PizzaSize::toString( PizzaSize::Large );

	_o << std::endl;

} // Application::showPizzaIngredients


/*-----------------------------------------------------------------------------*/


void
Application::showOrders ( std::ostream & _o )
{
	_o << std::endl << " ==== Orders ==== " << std::endl;

	int nOrders = m_model->getOrdersCount();
	for ( int i = 0; i < nOrders; i++ )
	{
		Order const & order = m_model->getOrder( i );
		showOrder( _o, order );
	}
	
	_o << std::endl << " ==== End Orders ==== " << std::endl;

} // Application::showOrders


/*-----------------------------------------------------------------------------*/


void
Application::showOrder ( std::ostream & _o, Order const & _order )
{
	_o << std::endl << "Order #" << _order.getID() << ':' << std::endl;
	
	_o << "\tStatus: " << OrderStatus::toString( _order.getStatus() ) << std::endl;
	_o << "\tTotal cost: " << _order.getTotalCost() << std::endl;
	_o << "\tPayment plan: " << _order.getPaymentPlan().describe() << std::endl;
	_o << "\tDiscount: " << _order.getDiscountPercentage() * 100.0 << "%" << std::endl;

	showDeliveryContact( _o, _order.getDeliveryContact() );
	showCart( _o, _order.getCart() );
	showOrderedPizzas( _o, _order );

} // Application::showOrder 


/*-----------------------------------------------------------------------------*/


void 
Application::showDeliveryContact ( std::ostream & _o, DeliveryContact const & _contact )
{
	_o << "\tContact: " << _contact.getTargetAddress() << std::endl;
	_o << "\tPhone: "	<< _contact.getPhoneNumber()   << std::endl;
	_o << "\tComment: " << _contact.getComment()	   << std::endl;

} // Application::showDeliveryContact


/*-----------------------------------------------------------------------------*/


void
Application::showCart ( std::ostream & _o, OrderCart const & _cart )
{
	_o << "\tCart:" << std::endl;

	int nItems = _cart.getItemsCount();
	for ( int i = 0; i < nItems; i++ )
	{
		_o << "\t  " << ( i + 1 ) << ") ";

		OrderItem const & item = _cart.getItem( i );
		_o  << item.getKind().getName() 
			<< " " << PizzaSize::toString( item.getSize() ) 
			<< ", price " << item.getFixedPrice() 
			<< ", quantity " << item.getQuantity() 
			<< ", cost " << item.getCost() 
			<< std::endl;
	}

} // Application::showCart


/*-----------------------------------------------------------------------------*/


void 
Application::showOrderedPizzas ( std::ostream & _o, Order const & _order )
{
	if ( ! _order.getPizzasCount() )
		return;

	_o << "\tPizzas:" << std::endl;

	int nPizzas = _order.getPizzasCount();
	for ( int i = 0; i < nPizzas; i++ )
	{
		_o << "\t  " << ( i + 1 ) << ") Pizza ";
		Pizza const & p = _order.getPizza( i );
		_o << p.getKind().getName() << " ";
		_o << PizzaSize::toString( p.getSize() );
		_o << ", cooking status: " << CookingStatus::toString( p.getCookingStatus() );
		_o << std::endl;
	}


} // Application::showOrderedPizzas


/*=============================================================================*/ 

} // namespace Pizzario

/*=============================================================================*/ 
