/** (C) 2015, Sergei Zaychenko, KNURE */

#ifndef _APPLICATION_HPP_
#define _APPLICATION_HPP_

/*-----------------------------------------------------------------------------*/

#include "model/pizzario_network.hpp"

#include <memory>

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/ 

class PizzaKind;
class Order;
class OrderCart;
class DeliveryContact;

/*-----------------------------------------------------------------------------*/


class Application
{

/*-----------------------------------------------------------------------------*/

public:

/*-----------------------------------------------------------------------------*/

	Application ();

	~Application ();

	void run ();

/*-----------------------------------------------------------------------------*/

private:

/*-----------------------------------------------------------------------------*/

	void fillTestModel ();

	void fillPizzaMenu ( Menu & _menu );

	void fillOrder ( Menu const & _menu );
	
/*-----------------------------------------------------------------------------*/

	void generateModelReport ();

	void showMenu ( std::ostream & _o, Menu const & _menu );

	void showPizzaRatings ( std::ostream & _o, PizzaKind const & _pizzaKind );

	void showPizzaIngredients ( std::ostream & _o, PizzaKind const & _pizzaKind );
	
	void showPizzaPrices ( std::ostream & _o, PizzaKind const & _pizzaKind );

	void showOrders ( std::ostream & _o );

	void showOrder ( std::ostream & _o, Order const & _order );

	void showDeliveryContact ( std::ostream & _o, DeliveryContact const & _contact );

	void showCart ( std::ostream & _o, OrderCart const & _cart );

	void showOrderedPizzas ( std::ostream & _o, Order const & _order );

/*-----------------------------------------------------------------------------*/

	std::unique_ptr< PizzarioNetwork > m_model;

/*-----------------------------------------------------------------------------*/


};


/*=============================================================================*/ 

} // namespace Pizzario

/*=============================================================================*/ 

#endif //  _APPLICATION_HPP_