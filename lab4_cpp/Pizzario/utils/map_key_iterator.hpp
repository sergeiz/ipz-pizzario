/** (C) 2015, Sergei Zaychenko, KNURE */

#ifndef _MAP_KEY_ITERATOR_HPP_
#define _MAP_KEY_ITERATOR_HPP_

/*-----------------------------------------------------------------------------*/

#include <memory>

/*=============================================================================*/ 

namespace Utils {

/*=============================================================================*/


template< typename _MapType >
class MapKeyIterator
{

/*-----------------------------------------------------------------------------*/

public:

/*-----------------------------------------------------------------------------*/

	typedef typename _MapType::const_iterator const_map_iterator;

	typedef typename const _MapType::key_type & reference_type;
	typedef typename _MapType::key_type value_type;
	typedef typename _MapType::difference_type difference_type;
	typedef typename _MapType::pointer pointer;
	typedef typename _MapType::reference reference;

/*-----------------------------------------------------------------------------*/

	MapKeyIterator ( const_map_iterator _base );

/*-----------------------------------------------------------------------------*/

	bool operator == ( MapKeyIterator< _MapType > _it ) const;

	bool operator != ( MapKeyIterator< _MapType > _it ) const;

	MapKeyIterator< _MapType > & operator ++ ();

	reference_type operator * () const;

/*-----------------------------------------------------------------------------*/

private:

/*-----------------------------------------------------------------------------*/

	const_map_iterator m_baseIt;

/*-----------------------------------------------------------------------------*/

};


/*-----------------------------------------------------------------------------*/


template< typename _MapType >
MapKeyIterator< _MapType >::MapKeyIterator ( const_map_iterator _baseIt )
	:	m_baseIt( _baseIt )
{
} // MapKeyIterator< _MapType >::MapKeyIterator


/*-----------------------------------------------------------------------------*/


template< typename _MapType >
bool 
MapKeyIterator< _MapType >::operator == ( MapKeyIterator< _MapType > _it ) const
{
	return m_baseIt == _it.m_baseIt;

} // MapKeyIterator< _MapType >::operator ==


/*-----------------------------------------------------------------------------*/


template< typename _MapType >
bool 
MapKeyIterator< _MapType >::operator != ( MapKeyIterator< _MapType > _it ) const
{
	return m_baseIt != _it.m_baseIt;

} // MapKeyIterator< _MapType >::operator !=


/*-----------------------------------------------------------------------------*/


template< typename _MapType >
MapKeyIterator< _MapType > &
MapKeyIterator< _MapType >::operator ++ ()
{
	++ m_baseIt;
	return * this;

} // MapKeyIterator< _MapType >::operator ++


/*-----------------------------------------------------------------------------*/


template< typename _MapType >
typename MapKeyIterator< _MapType >::reference_type 
MapKeyIterator< _MapType >::operator * () const
{
	return m_baseIt->first;

} // MapKeyIterator< _MapType >::operator *


/*=============================================================================*/ 

} // namespace Utils

/*=============================================================================*/ 

#endif //  _MAP_KEY_ITERATOR_HPP_
