/** (C) 2015, Sergei Zaychenko, KNURE */

#ifndef _SIMPLE_COLLECTION_ITERATOR_HPP_
#define _SIMPLE_COLLECTION_ITERATOR_HPP_

/*-----------------------------------------------------------------------------*/

#include <memory>

/*=============================================================================*/ 

namespace Utils {

/*=============================================================================*/


template< typename _CollectionType >
class SimpleCollectionIterator
{

/*-----------------------------------------------------------------------------*/

public:

/*-----------------------------------------------------------------------------*/

	typedef typename _CollectionType::const_iterator const_collection_iterator;

	typedef typename const _CollectionType::value_type & reference_type;
	typedef typename _CollectionType::value_type value_type;
	typedef typename _CollectionType::difference_type difference_type;
	typedef typename _CollectionType::pointer pointer;
	typedef typename _CollectionType::reference reference;

/*-----------------------------------------------------------------------------*/

	SimpleCollectionIterator ( const_collection_iterator _base );

/*-----------------------------------------------------------------------------*/

	bool operator == ( SimpleCollectionIterator< _CollectionType > _it ) const;

	bool operator != ( SimpleCollectionIterator< _CollectionType > _it ) const;

	SimpleCollectionIterator< _CollectionType > & operator ++ ();

	reference_type operator * () const;

/*-----------------------------------------------------------------------------*/

private:

/*-----------------------------------------------------------------------------*/

	const_collection_iterator m_baseIt;

/*-----------------------------------------------------------------------------*/

};


/*-----------------------------------------------------------------------------*/


template< typename _CollectionType >
SimpleCollectionIterator< _CollectionType >::SimpleCollectionIterator ( const_collection_iterator _baseIt )
	:	m_baseIt( _baseIt )
{
} // SimpleCollectionIterator< _CollectionType >::SimpleCollectionIterator


/*-----------------------------------------------------------------------------*/


template< typename _CollectionType >
bool 
SimpleCollectionIterator< _CollectionType >::operator == ( SimpleCollectionIterator< _CollectionType > _it ) const
{
	return m_baseIt == _it.m_baseIt;

} // SimpleCollectionIterator< _CollectionType >::operator ==


/*-----------------------------------------------------------------------------*/


template< typename _CollectionType >
bool 
SimpleCollectionIterator< _CollectionType >::operator != ( SimpleCollectionIterator< _CollectionType > _it ) const
{
	return m_baseIt != _it.m_baseIt;

} // SimpleCollectionIterator< _CollectionType >::operator !=


/*-----------------------------------------------------------------------------*/


template< typename _CollectionType >
SimpleCollectionIterator< _CollectionType > &
SimpleCollectionIterator< _CollectionType >::operator ++ ()
{
	++ m_baseIt;
	return * this;

} // SimpleCollectionIterator< _CollectionType >::operator ++


/*-----------------------------------------------------------------------------*/


template< typename _CollectionType >
typename SimpleCollectionIterator< _CollectionType >::reference_type 
SimpleCollectionIterator< _CollectionType >::operator * () const
{
	return * m_baseIt;

} // SimpleCollectionIterator< _CollectionType >::operator *


/*=============================================================================*/ 

} // namespace Utils

/*=============================================================================*/ 

#endif //  _SIMPLE_COLLECTION_ITERATOR_HPP_
