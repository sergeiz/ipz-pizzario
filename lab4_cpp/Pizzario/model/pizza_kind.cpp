/** (C) 2015, Sergei Zaychenko, KNURE */

#include "model/pizza_kind.hpp"
#include "model/pizza_recipe.hpp"

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/ 


PizzaKind::PizzaKind ( std::string const & _name )
	:	m_name( _name )
	,	m_nVotes( 0 )
	,	m_totalRating( 0 )
	,	m_hidden( false )
{
	if ( _name.empty() )
		throw std::runtime_error( "PizzaKind: empty name");

	m_recipe.reset( new PizzaRecipe() );

} // PizzaKind::PizzaKind


/*-----------------------------------------------------------------------------*/


PizzaKind::~PizzaKind ()
{
	// Recipe is destroyed automatically

} // PizzaKind::~PizzaKind


/*-----------------------------------------------------------------------------*/


void
PizzaKind::rename ( std::string const & _newName )
{
	if ( _newName.empty() )
		throw std::runtime_error( "PizzaKind::rename: empty name");

	else if ( m_name == _newName )
		return;

	for ( INameChangedListener * pListener : m_nameChangedListeners )
		pListener->onPizzaKindNameChanged( m_name, _newName );

	m_name = _newName;

} // PizzaKind::rename 


/*-----------------------------------------------------------------------------*/


void
PizzaKind::addNameChangedListener ( INameChangedListener & _listener )
{
	m_nameChangedListeners.push_back( & _listener );

} // PizzaKind::addNameChangedListener


/*-----------------------------------------------------------------------------*/


void
PizzaKind::removeNameChangedListener ( INameChangedListener & _listener )
{
	auto itListeners = std::find( m_nameChangedListeners.begin(), m_nameChangedListeners.end(), & _listener );
	if ( itListeners != m_nameChangedListeners.end() )
		m_nameChangedListeners.erase( itListeners );

	else
		throw std::runtime_error( "PizzaKind::removeNameChangedListener: removing unregistered listener");

} // PizzaKind::removeNameChangedListener


/*-----------------------------------------------------------------------------*/

	
double 
PizzaKind::getAverageRating () const
{
	if ( m_nVotes )
		return static_cast< double >( m_totalRating ) / m_nVotes;
	else
		return 0.0;

} // PizzaKind::getAverageRating


/*-----------------------------------------------------------------------------*/


void 
PizzaKind::rate ( int _rating )
{
	if ( _rating <= 0 || _rating > MaximumVote )
	{
		char buf[ 100 ];
		sprintf( buf, "PizzaKind::rate: expecting rating within [1;%d] range", MaximumVote );
		throw std::runtime_error( buf );
	}

	m_nVotes++;
	m_totalRating += _rating;

} // PizzaKind::rate


/*-----------------------------------------------------------------------------*/


void
PizzaKind::setupRatingStats ( int _nVotes, int _totalRating )
{
    if ( _nVotes < 0 )
        throw std::runtime_error( "PizzaKind.SetupRatingStats: negative votes" );

    else if ( _nVotes == 0 && _totalRating != 0 )
        throw std::runtime_error( "PizzaKind.SetupRatingStats: zero votes can only give zero ratings" );

    else if ( _nVotes > 0 && _totalRating < _nVotes )
        throw std::runtime_error( "PizzaKind.SetupRatingStats: total rating may not be smaller than number of votes" );

    else if ( _nVotes > 0 && _totalRating > ( _nVotes * MaximumVote ) )
        throw std::runtime_error( "PizzaKind.SetupRatingStats: total rating may not exceed maximum from all votes" );

	m_nVotes = _nVotes;
	m_totalRating = _totalRating;

} // PizzaKind::setupRatingStats



/*-----------------------------------------------------------------------------*/


Money PizzaKind::getCurrentPrice ( PizzaSize::Enum _size ) const
{
    return m_prices[ _size ];
}


/*-----------------------------------------------------------------------------*/


void PizzaKind::updatePrice ( PizzaSize::Enum _size, Money _price )
{
    if ( _price.isNegative() )
        throw std::runtime_error( "PizzaKind::updatePrice: price cannot be negative" );

    m_prices[ _size ] = _price;
}


/*=============================================================================*/ 

} // namespace Pizzario

/*=============================================================================*/ 
