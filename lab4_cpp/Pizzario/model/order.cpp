/** (C) 2015, Sergei Zaychenko, KNURE */

#include "model/order.hpp"
#include "model/order_cart.hpp"
#include "model/pizza.hpp"
#include "model/cash_payment_plan.hpp"

#include <cassert>
#include <algorithm>

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/ 


Order::Order (
		int _ID
	,	std::unique_ptr< OrderCart > _cart
	,	DeliveryContact const & _contact
	,	std::unique_ptr< PaymentPlan > _paymentPlan
)
	:	m_orderId( _ID )
	,	m_cart( std::move( _cart ) )
	,	m_deliveryContact( _contact )
	,	m_paymentPlan( std::move( _paymentPlan ) )
	,	m_discountPercent( 0.0 )
	,	m_status( OrderStatus::New )
{
	if ( ! m_cart->isModifiable() )
		throw std::runtime_error( "Order: initializing with cart that is already checked out" );

	m_cart->addCheckoutListener( * this );

} // Order::Order


/*-----------------------------------------------------------------------------*/


Order::~Order ()
{
	// Payment plan, cart and pizzas are destroyed automatically

} // Order::~Order


/*-----------------------------------------------------------------------------*/

	
Money 
Order::getTotalCost () const
{
	return m_cart->getCost() * ( 1.0 - getDiscountPercentage() );

} // Order::getTotalCost


/*-----------------------------------------------------------------------------*/


void 
Order::setDiscountPercentage ( double _percentage )
{
	if ( _percentage < 0.0 || _percentage > 1.0 )
		throw std::runtime_error( "Order::setDiscountPercentage - invalid percentage value" );

	m_discountPercent = _percentage;

} // Order::setDiscountPercentage
	

/*-----------------------------------------------------------------------------*/


void 
Order::onCartCheckout ()
{
	if ( m_status != OrderStatus::New )
		throw std::runtime_error( "Order::onCartCheckout - Can only process new order!" );

    if ( m_paymentPlan->expectPrepayment() && ! m_paymentPlan->wasPayed() )
        throw std::runtime_error( "Order::onCartCheckout - pre-payment unavailable" );

	m_status = OrderStatus::Registered;

	assert( m_pizzas.empty() );

	int nOrderItems = getCart().getItemsCount();
	for ( int i = 0; i < nOrderItems; i++ )
	{
		OrderItem const & item = getCart().getItem( i );

		for ( int k = 0; k < item.getQuantity(); k++ )
		{
			m_pizzas.emplace_back( 
				std::unique_ptr< Pizza >( 
					new Pizza( item.getKind(), item.getSize() ) 
				) 
			);

			m_pizzas.back()->addEventsListener( * this );
		}
	}

} // Order::onCartCheckout


/*-----------------------------------------------------------------------------*/


void 
Order::cancel ()
{
	switch ( m_status )
	{
		case OrderStatus::New:
		case OrderStatus::Registered:
		case OrderStatus::Cooking:
		case OrderStatus::Ready4Delivery:
			break;

		default:
			throw std::runtime_error( "Order::cancel - cannot cancel in current order state" );
	}

    for ( std::unique_ptr< Pizza > & pizza : m_pizzas )
		if ( pizza->getCookingStatus() != CookingStatus::Finished )
			pizza->onCookingCancelled();

	m_status = OrderStatus::Cancelled;

} // Order::cancel


/*-----------------------------------------------------------------------------*/


void
Order::onPizzaCookingStarted ()
{
	if ( m_status == OrderStatus::Registered )
		m_status = OrderStatus::Cooking;

	else if ( m_status != OrderStatus::Cooking )
		throw std::runtime_error( "Order::onPizzaCookingStarted - can only happen in Registered & Cooking states");

} // Order::onPizzaCookingStarted


/*-----------------------------------------------------------------------------*/

	
void
Order::onPizzaCookingFinished ()
{
	if ( m_status != OrderStatus::Cooking )
		throw std::runtime_error( "Order::onPizzaReady - can only happen in Cooking state");

	bool allReady = std::all_of( 
				m_pizzas.begin()
			,	m_pizzas.end()
			,	[ & ] ( std::unique_ptr< Pizza > const & _pPizza )
				{
					return _pPizza->getCookingStatus() == CookingStatus::Finished;
				}
		);

	if ( allReady )
		m_status = OrderStatus::Ready4Delivery;

} // Order::onPizzaCookingFinished


/*-----------------------------------------------------------------------------*/


void 
Order::onStartedDelivery ()
{
	if ( m_status != OrderStatus::Ready4Delivery )
		throw std::runtime_error( "Order::onStartedDelivery - can only happen in Ready4Delivery state");

	m_status = OrderStatus::Delivering;

} // Order::onStartedDelivery


/*-----------------------------------------------------------------------------*/


void 
Order::onDelivered ()
{
	if ( m_status != OrderStatus::Delivering )
		throw std::runtime_error( "Order::onDelivered - can only happen in Delivering state");

    if ( ! m_paymentPlan->expectPrepayment() && ! m_paymentPlan->wasPayed() )
        throw std::runtime_error( "Order::onDelivered - post-payment should have been collected by now" );

	m_status = OrderStatus::Delivered;

} // Order::onDelivered 


/*=============================================================================*/ 

} // namespace Pizzario

/*=============================================================================*/ 
