/** (C) 2015, Sergei Zaychenko, KNURE */

#ifndef _WIRE_TRANSFER_PAYMENT_PLAN_HPP_
#define _WIRE_TRANSFER_PAYMENT_PLAN_HPP_

/*-----------------------------------------------------------------------------*/

#include "model/payment_plan.hpp"

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/ 


class WireTransferPaymentPlan
	:	public PaymentPlan
{

/*-----------------------------------------------------------------------------*/

public:

/*-----------------------------------------------------------------------------*/

	WireTransferPaymentPlan ( std::string const & _cardCode, std::string const & _cardHolder );

	std::string const & getCardCode () const;

	std::string const & getCardHolder () const;
		
	std::string describe () const override;
	
	WireTransferPaymentPlan * clone () const override;

	bool expectPrepayment () const override;

/*-----------------------------------------------------------------------------*/

private:

/*-----------------------------------------------------------------------------*/

	std::string const m_cardCode;

	std::string const m_cardHolder;

/*-----------------------------------------------------------------------------*/

};


/*-----------------------------------------------------------------------------*/


inline 
std::string const &
WireTransferPaymentPlan::getCardCode () const
{
	return m_cardCode;

} // WireTransferPaymentPlan::getCardCode


/*-----------------------------------------------------------------------------*/


inline 
std::string const &
WireTransferPaymentPlan::getCardHolder () const
{
	return m_cardHolder;

} // WireTransferPaymentPlan::getCardHolder 


/*=============================================================================*/ 

} // namespace Pizzario

/*=============================================================================*/ 

#endif //  _WIRE_TRANSFER_PAYMENT_PLAN_HPP_