/** (C) 2015, Sergei Zaychenko, KNURE */

#include "model/menu.hpp"

#include <algorithm>
#include <cassert>

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/ 


Menu::Menu ()
{
} // Menu::Menu


/*-----------------------------------------------------------------------------*/


Menu::~Menu ()
{
	// Pizza kinds will be destroyed automatically

}  // Menu::~Menu


/*-----------------------------------------------------------------------------*/


Menu::PizzaNameIterator
Menu::pizzaKindNamesBegin () const
{
	return PizzaNameIterator( new PizzaKindsByNameKeyIterator( m_pizzaKinds.begin() ) );

} // Menu::pizzaKindNamesBegin


/*-----------------------------------------------------------------------------*/
	

Menu::PizzaNameIterator
Menu::pizzaKindNamesEnd () const
{
	return PizzaNameIterator( new PizzaKindsByNameKeyIterator( m_pizzaKinds.end() ) );

} // Menu::pizzaKindNamesEnd


/*-----------------------------------------------------------------------------*/


PizzaKind * 
Menu::findPizzaKind ( std::string const & _name ) const
{
	auto it = m_pizzaKinds.find( _name );
	return ( it != m_pizzaKinds.end() ) ? it->second.get() : nullptr;

} // Menu::findPizzaKind


/*-----------------------------------------------------------------------------*/


void 
Menu::addPizzaKind ( std::unique_ptr< PizzaKind > _kind )
{
	assert( _kind.get() );

	if ( findPizzaKind( _kind->getName() ) )
		throw std::runtime_error( "Menu::addPizzaKind: duplicate kind" );

	_kind->addNameChangedListener( * this );

	m_pizzaKinds[ _kind->getName() ] = std::move( _kind );

} // Menu::addPizzaKind


/*-----------------------------------------------------------------------------*/


void
Menu::deletePizzaKind ( std::string const & _name )
{
	auto it = m_pizzaKinds.find( _name );
	if ( it == m_pizzaKinds.end() )
		throw std::runtime_error( "Menu::removePizzaKind: unregistered kind" );

	it->second->removeNameChangedListener( * this );
	m_pizzaKinds.erase( it );

} // Menu::removePizzaKind


/*-----------------------------------------------------------------------------*/


void 
Menu::onPizzaKindNameChanged ( std::string const & _name, std::string const & _newName )
{
	auto it = m_pizzaKinds.find( _name );
	if ( it == m_pizzaKinds.end() )
		throw std::runtime_error( "Menu::onPizzaKindNameChanged: unregistered kind" );

	if ( _name == _newName )
		return;

    if ( findPizzaKind( _newName ) )
        throw std::runtime_error( "Menu::onPizzaKindNameChanged: target name already reserved" );
	
	std::unique_ptr< PizzaKind > pizza = std::move( it->second );
	m_pizzaKinds.erase( it );

	m_pizzaKinds[ _newName ] = std::move( pizza );

} // Menu::renamePizzaKind


/*-----------------------------------------------------------------------------*/


OrderItem 
Menu::makeOrderItem (  std::string const & _name, PizzaSize::Enum _size, int _quantity ) const
{
	PizzaKind * pKind = findPizzaKind( _name );
    if ( ! pKind)
        throw std::runtime_error( "Menu::makeOrderItem: unregistered kind" );

	return OrderItem( * pKind, _size, pKind->getCurrentPrice( _size ), _quantity );

} // Menu::makeOrderItem


/*=============================================================================*/ 


} // namespace Pizzario

/*=============================================================================*/ 
