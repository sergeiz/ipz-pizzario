/** (C) 2015, Sergei Zaychenko, KNURE */

#include "model/wire_transfer_payment_plan.hpp"

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/
	
	
WireTransferPaymentPlan::WireTransferPaymentPlan ( 
		std::string const & _cardCode
	,	std::string const & _cardHolder 
)
	:	m_cardCode( _cardCode )
	,	m_cardHolder( _cardHolder )
{
} // WireTransferPaymentPlan::WireTransferPaymentPlan


/*-----------------------------------------------------------------------------*/


std::string
WireTransferPaymentPlan::describe () const
{
	return "Wire transfer over the Internet";

} // WireTransferPaymentPlan::describe


/*-----------------------------------------------------------------------------*/


WireTransferPaymentPlan * 
WireTransferPaymentPlan::clone () const
{
	WireTransferPaymentPlan * pPlan = new WireTransferPaymentPlan( getCardCode(), getCardHolder() );
	if ( wasPayed() )
		pPlan->markPayed();
	return pPlan;

} // WireTransferPaymentPlan::clone


/*-----------------------------------------------------------------------------*/


bool 
WireTransferPaymentPlan::expectPrepayment () const
{
	return true;

} // WireTransferPaymentPlan::expectPrepayment 


/*=============================================================================*/ 

} // namespace Pizzario

/*=============================================================================*/ 
