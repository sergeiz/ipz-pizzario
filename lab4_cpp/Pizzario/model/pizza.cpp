/** (C) 2015, Sergei Zaychenko, KNURE */

#include "model/pizza.hpp"
#include "model/order.hpp"

#include <stdexcept>

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/


Pizza::Pizza ( 
		PizzaKind const & _kind
	,	PizzaSize::Enum _size
)
	:	m_kind( _kind )
	,	m_size( _size )
	,	m_cookingStatus( CookingStatus::NotStarted )
{
} // Pizza::Pizza


/*-----------------------------------------------------------------------------*/


void 
Pizza::onCookingStarted ()
{
	if ( m_cookingStatus == CookingStatus::NotStarted )
	{
		m_cookingStatus = CookingStatus::Started;

		for ( IPizzaEventsListener * pListener : m_eventsListeners )
			pListener->onPizzaCookingStarted();
	}
	else
		throw std::runtime_error( "Pizza::onCookingStarted: may only start cooking if not started yet" );

} // Pizza::onCookingStarted


/*-----------------------------------------------------------------------------*/


void
Pizza::onCookingCancelled ()
{
	if ( m_cookingStatus == CookingStatus::Started || m_cookingStatus == CookingStatus::NotStarted )
		m_cookingStatus = CookingStatus::Cancelled;

	else
		throw std::runtime_error( "Pizza::onCookingCancelled: may only abort unfinished pizza" );

} // PizzaKind::onCookingCancelled


/*-----------------------------------------------------------------------------*/


void 
Pizza::onCookingFinished ()
{
	if ( m_cookingStatus == CookingStatus::Started )
	{
		m_cookingStatus = CookingStatus::Finished;

		for ( IPizzaEventsListener * pListener : m_eventsListeners )
			pListener->onPizzaCookingFinished();
	}
	else
		throw std::runtime_error( "Pizza::onCookingFinished: may only finish cooking if started" );

} // Pizza::onCookingFinished


/*-----------------------------------------------------------------------------*/


void 
Pizza::addEventsListener ( IPizzaEventsListener & _listener )
{
	m_eventsListeners.push_back( & _listener );

} // Pizza::addEventsListener


/*-----------------------------------------------------------------------------*/


void 
Pizza::removeEventsListener ( IPizzaEventsListener & _listener )
{
	auto itListeners = std::find( m_eventsListeners.begin(), m_eventsListeners.end(), & _listener );
	if ( itListeners != m_eventsListeners.end() )
		m_eventsListeners.erase( itListeners );

	else
		throw std::runtime_error( "Pizza::removeEventsListener: removing unregistered listener");

} // Pizza::removeEventsListener


/*=============================================================================*/ 

} // namespace Pizzario

/*=============================================================================*/ 
