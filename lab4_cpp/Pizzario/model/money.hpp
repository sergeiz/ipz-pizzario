/** (C) 2015, Sergei Zaychenko, KNURE */

#ifndef _MONEY_HPP_
#define _MONEY_HPP_

/*-----------------------------------------------------------------------------*/

#include <iostream>

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/


class Money
{

/*-----------------------------------------------------------------------------*/

public:

/*-----------------------------------------------------------------------------*/

	Money ();

	Money ( int _dollars, int _cents );

	int getDollars () const;

	int getCents () const;

	bool isNegative () const;

	bool operator == ( Money m ) const;

	bool operator != ( Money m ) const;

	bool operator < ( Money m ) const;

	bool operator <= ( Money m ) const;

	bool operator > ( Money m ) const;

	bool operator >= ( Money m ) const;

	Money operator + ( Money m ) const;

	Money & operator += ( Money m );

	Money operator * ( double c ) const;
	
	Money & operator *= ( double c );

/*-----------------------------------------------------------------------------*/

private:

/*-----------------------------------------------------------------------------*/

	explicit Money ( int _totalCents );
	
/*-----------------------------------------------------------------------------*/

	int m_totalCents;

/*-----------------------------------------------------------------------------*/

};


/*-----------------------------------------------------------------------------*/


std::ostream & operator << ( std::ostream &, Money );


/*-----------------------------------------------------------------------------*/


inline int
Money::getCents () const
{
	return m_totalCents % 100;
}


/*-----------------------------------------------------------------------------*/


inline int
Money::getDollars () const
{
	return m_totalCents / 100;
}


/*-----------------------------------------------------------------------------*/


inline bool
Money::isNegative () const
{
	return m_totalCents < 0;
}


/*=============================================================================*/ 

} // namespace Pizzario

/*=============================================================================*/ 

#endif //  _MONEY_HPP_
