/** (C) 2015, Sergei Zaychenko, KNURE */

#include "model/money.hpp"

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/ 


Money::Money ()
	:	m_totalCents( 0 )
{
} // Money::Money


/*-----------------------------------------------------------------------------*/


Money::Money ( int _dollars, int _cents )
	:	m_totalCents( _dollars * 100 + _cents )
{
} // Money::Money


/*-----------------------------------------------------------------------------*/


Money::Money ( int _totalCents )
	:	m_totalCents( _totalCents )
{
}


/*-----------------------------------------------------------------------------*/


bool 
Money::operator == ( Money m ) const
{
	return m_totalCents == m.m_totalCents;

} // Money::operator ==


/*-----------------------------------------------------------------------------*/


bool
Money::operator != ( Money m ) const
{
	return m_totalCents != m.m_totalCents;

} // Money::operator !=


/*-----------------------------------------------------------------------------*/


bool
Money::operator < ( Money m ) const
{
	return m_totalCents < m.m_totalCents;

} // Money::operator <


/*-----------------------------------------------------------------------------*/


bool 
Money::operator <= ( Money m ) const
{
	return m_totalCents <= m.m_totalCents;

} // Money::operator <=


/*-----------------------------------------------------------------------------*/


bool 
Money::operator > ( Money m ) const
{
	return m_totalCents > m.m_totalCents;

} // Money::operator >


/*-----------------------------------------------------------------------------*/


bool 
Money::operator >= ( Money m ) const
{
	return m_totalCents >= m.m_totalCents;

} // Money::operator >=


/*-----------------------------------------------------------------------------*/


Money 
Money::operator + ( Money _m ) const
{
	return Money( m_totalCents + _m.m_totalCents );

} // Money::operator +


/*-----------------------------------------------------------------------------*/


Money &
Money::operator += ( Money _m )
{
	Money temp = * this + _m;
	* this = temp;
	return * this;

} // Money::operator += 


/*-----------------------------------------------------------------------------*/


Money
Money::operator * ( double _c ) const
{
	return Money( static_cast< int >( m_totalCents * _c ) );

} // Money::operator *


/*-----------------------------------------------------------------------------*/


Money &
Money::operator *= ( double _c )
{
	Money temp = * this * _c;
	* this = temp;
	return * this;

} // Money::operator *= 


/*-----------------------------------------------------------------------------*/


std::ostream & operator << ( std::ostream & _o, Money _m )
{
	static char centsBuf[ 3 ];
	sprintf( centsBuf, "%02d", _m.getCents() );
	_o << '$' << _m.getDollars() << '.' << centsBuf;
	return _o;
}


/*=============================================================================*/ 

} // namespace Pizzario

/*=============================================================================*/ 
