/** (C) 2015, Sergei Zaychenko, KNURE */

#ifndef _ORDER_STATUS_HPP_
#define _ORDER_STATUS_HPP_

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/


struct OrderStatus
{
	enum Enum
	{
			New
		,	Registered
		,	Cooking
		,	Ready4Delivery
		,	Delivering
		,	Delivered
		,	Cancelled

		,	Last
	};

	static const char * toString ( Enum e )
	{
		switch ( e )
		{
			case New:				return "New";
			case Registered:		return "Registered";
			case Cooking:			return "Cooking";
			case Ready4Delivery:	return "Ready for delivery";
			case Delivering:		return "Delivering";
			case Delivered:			return "Delivered";
			case Cancelled:			return "Cancelled";

			default:
				return nullptr;
		}
	}
};


/*=============================================================================*/ 

} // namespace Pizzario

/*=============================================================================*/ 

#endif //  _ORDER_STATUS_HPP_
