/** (C) 2015, Sergei Zaychenko, KNURE */

#include "model/cash_payment_plan.hpp"

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/


std::string
CashPaymentPlan::describe () const
{
	return "Cash payment on delivery";

} // CashPaymentPlan::describe


/*-----------------------------------------------------------------------------*/


CashPaymentPlan * 
CashPaymentPlan::clone () const
{
	CashPaymentPlan * pPlan = new CashPaymentPlan();
	if ( wasPayed() )
		pPlan->markPayed();
	return pPlan;

} // CashPaymentPlan::clone


/*-----------------------------------------------------------------------------*/


bool 
CashPaymentPlan::expectPrepayment () const
{
	return false;

} // CashPaymentPlan::expectPrepayment 


/*=============================================================================*/ 

} // namespace Pizzario

/*=============================================================================*/ 
