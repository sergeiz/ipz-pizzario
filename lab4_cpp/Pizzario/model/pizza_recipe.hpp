/** (C) 2015, Sergei Zaychenko, KNURE */

#ifndef _PIZZA_RECIPE_HPP_
#define _PIZZA_RECIPE_HPP_

/*-----------------------------------------------------------------------------*/

#include "utils/iterator_utils.hpp"
#include "utils/map_key_iterator.hpp"

#include <string>
#include <map>

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/ 


class PizzaRecipe
{

/*-----------------------------------------------------------------------------*/

	typedef std::map< std::string, int > IngredientsWeight;
	typedef Utils::MapKeyIterator< IngredientsWeight > IngredientsWeightKeyIterator;

/*-----------------------------------------------------------------------------*/

public:

/*-----------------------------------------------------------------------------*/

	typedef Utils::Iterator< IngredientsWeightKeyIterator > IngredientNamesIterator;

/*-----------------------------------------------------------------------------*/

	PizzaRecipe ();

	int getIngredientsCount () const;

	IngredientNamesIterator ingredientNamesBegin () const;

	IngredientNamesIterator ingredientNamesEnd () const;

	int getIngredientWeight ( std::string const & _ingredient ) const;

	void addIngredient ( std::string const & _ingredient, int _weightGrams );

	void updateIngredient ( std::string const & _ingredient, int _weightGrams );

	void removeIngredient ( std::string const & _ingredient );

/*-----------------------------------------------------------------------------*/

private:

/*-----------------------------------------------------------------------------*/
	
	PizzaRecipe ( const PizzaRecipe & );
	PizzaRecipe & operator = ( const PizzaRecipe & );

/*-----------------------------------------------------------------------------*/
	
	IngredientsWeight m_ingredientsWeight;

/*-----------------------------------------------------------------------------*/

};


/*=============================================================================*/ 

} // namespace Pizzario

/*=============================================================================*/ 

#endif //  _PIZZA_RECIPE_HPP_