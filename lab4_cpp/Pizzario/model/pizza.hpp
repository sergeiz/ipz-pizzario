/** (C) 2015, Sergei Zaychenko, KNURE */

#ifndef _PIZZA_HPP_
#define _PIZZA_HPP_

/*-----------------------------------------------------------------------------*/

#include "model/pizza_size.hpp"
#include "model/cooking_status.hpp"

#include <vector>

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/ 

class PizzaKind;

/*-----------------------------------------------------------------------------*/


class Pizza
{

/*-----------------------------------------------------------------------------*/

public:

/*-----------------------------------------------------------------------------*/
	
	class IPizzaEventsListener
	{
	public:
		virtual ~IPizzaEventsListener () {}
		virtual void onPizzaCookingStarted () = 0;
		virtual void onPizzaCookingFinished () = 0;
	};

/*-----------------------------------------------------------------------------*/

	Pizza ( 
			PizzaKind const & _kind
		,	PizzaSize::Enum _size
	);

	const PizzaKind & getKind () const;

	PizzaSize::Enum getSize () const;

	CookingStatus::Enum getCookingStatus () const;

	void onCookingStarted ();

	void onCookingCancelled ();

	void onCookingFinished ();

	void addEventsListener ( IPizzaEventsListener & _listener );

	void removeEventsListener ( IPizzaEventsListener & _listener );

/*-----------------------------------------------------------------------------*/

private:

/*-----------------------------------------------------------------------------*/
	
	Pizza ( const Pizza & );
	Pizza & operator = ( const Pizza & );

/*-----------------------------------------------------------------------------*/

	std::vector< IPizzaEventsListener * > m_eventsListeners;
	
	PizzaKind const & m_kind;

	PizzaSize::Enum const m_size;

	CookingStatus::Enum m_cookingStatus;

/*-----------------------------------------------------------------------------*/

};


/*-----------------------------------------------------------------------------*/


inline
const PizzaKind & 
Pizza::getKind () const
{
	return m_kind;

} // Pizza::getKind


/*-----------------------------------------------------------------------------*/


inline
PizzaSize::Enum
Pizza::getSize () const
{
	return m_size;

} // Pizza::getSize


/*-----------------------------------------------------------------------------*/


inline CookingStatus::Enum 
Pizza::getCookingStatus () const
{
	return m_cookingStatus;

} // Pizza::getCookingStatus


/*=============================================================================*/ 

} // namespace Pizzario

/*=============================================================================*/ 

#endif //  _PIZZA_RECIPE_HPP_