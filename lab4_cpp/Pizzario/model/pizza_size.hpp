/** (C) 2015, Sergei Zaychenko, KNURE */

#ifndef _PIZZA_SIZE_HPP_
#define _PIZZA_SIZE_HPP_

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/


struct PizzaSize
{
	enum Enum
	{
			Small
		,	Medium
		,	Large

		,	Last
	};

	static const char * toString ( Enum e )
	{
		switch ( e )
		{
			case Small:		return "Small";
			case Medium:	return "Medium";
			case Large:		return "Large";

			default:
				return nullptr;
		}
	}
};


/*=============================================================================*/ 

} // namespace Pizzario

/*=============================================================================*/ 

#endif //  _PIZZA_SIZE_HPP_
