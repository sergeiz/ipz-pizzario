/** (C) 2015, Sergei Zaychenko, KNURE */

#ifndef _ORDER_HPP_
#define _ORDER_HPP_

/*-----------------------------------------------------------------------------*/

#include "model/order_cart.hpp"
#include "model/pizza.hpp"
#include "model/order_status.hpp"
#include "model/delivery_contact.hpp"

#include <vector>
#include <memory>

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/

class Pizza;
class PaymentPlan;
class OrderCart;

/*-----------------------------------------------------------------------------*/


class Order
	:	private Pizza::IPizzaEventsListener
	,	private OrderCart::ICheckoutListener
{

/*-----------------------------------------------------------------------------*/

public:

/*-----------------------------------------------------------------------------*/

	Order ( 
			int _ID
		,	std::unique_ptr< OrderCart > _cart
		,	DeliveryContact const & _contact
		,	std::unique_ptr< PaymentPlan > _paymentPlan
	);

	~ Order ();

	int getID () const;

	DeliveryContact const & getDeliveryContact () const;

	void updateDeliveryContact ( DeliveryContact const & _contact );
	
	PaymentPlan & getPaymentPlan () const;

	OrderCart & getCart ();

	OrderCart const & getCart () const;

	int getPizzasCount () const;

	Pizza & getPizza ( int _index ) const;
	
	Money getTotalCost () const;

	double getDiscountPercentage () const;

	void setDiscountPercentage ( double _percentage );
	
	OrderStatus::Enum getStatus () const;

	void cancel ();
		
	void onStartedDelivery ();

	void onDelivered ();

/*-----------------------------------------------------------------------------*/

private:

/*-----------------------------------------------------------------------------*/

	Order ( const Order & );
	Order & operator = ( const Order &  );

	void onCartCheckout () override;
		
	void onPizzaCookingStarted () override;

	void onPizzaCookingFinished () override;

/*-----------------------------------------------------------------------------*/
	
	std::unique_ptr< OrderCart > m_cart;

	std::vector< std::unique_ptr< Pizza > > m_pizzas;

	std::unique_ptr< PaymentPlan > m_paymentPlan;

	DeliveryContact m_deliveryContact;

	const int m_orderId;

	OrderStatus::Enum m_status;

	double m_discountPercent;

/*-----------------------------------------------------------------------------*/

};


/*-----------------------------------------------------------------------------*/


inline int 
Order::getID () const
{
	return m_orderId;

} // Order::getID


/*-----------------------------------------------------------------------------*/


inline
DeliveryContact const & 
Order::getDeliveryContact () const
{
	return m_deliveryContact;

} // Order::getDeliveryContact


/*-----------------------------------------------------------------------------*/
	

inline
PaymentPlan & 
Order::getPaymentPlan () const
{
	return * m_paymentPlan;
}


/*-----------------------------------------------------------------------------*/


inline void 
Order::updateDeliveryContact ( DeliveryContact const & _contact )
{
	m_deliveryContact = _contact;

} // Order::updateDeliveryContact


/*-----------------------------------------------------------------------------*/


inline
OrderCart & 
Order::getCart ()
{
	return * m_cart;

} // Order::getCart


/*-----------------------------------------------------------------------------*/

	
inline
OrderCart const &
Order::getCart () const
{
	return * m_cart;

} // Order::getCart


/*-----------------------------------------------------------------------------*/


inline int
Order::getPizzasCount () const
{
	return static_cast< int >( m_pizzas.size() );

} // Order::getPizzasCount


/*-----------------------------------------------------------------------------*/


inline Pizza &
Order::getPizza ( int _index ) const
{
	return *( m_pizzas.at( _index ) );

} // Order::getPizza
	

/*-----------------------------------------------------------------------------*/


inline double 
Order::getDiscountPercentage () const
{
	return m_discountPercent;

} // Order::getDiscountPercentage


/*-----------------------------------------------------------------------------*/


inline
OrderStatus::Enum 
Order::getStatus () const
{
	return m_status;

} // Order::getStatus


/*=============================================================================*/ 

} // namespace Pizzario

/*=============================================================================*/ 

#endif //  _ORDER_HPP_
