/** (C) 2015, Sergei Zaychenko, KNURE */

#ifndef _ORDER_CART_HPP_
#define _ORDER_CART_HPP_

/*-----------------------------------------------------------------------------*/

#include "model/order_item.hpp"

#include <vector>

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/


class OrderCart
{

/*-----------------------------------------------------------------------------*/

public:

/*-----------------------------------------------------------------------------*/
	
	class ICheckoutListener
	{
	public:
		virtual ~ICheckoutListener () {}
		virtual void onCartCheckout () = 0;
	};

/*-----------------------------------------------------------------------------*/

	OrderCart ();

	int getItemsCount () const;

	OrderItem const & getItem ( int _index ) const;

	void addItem ( const OrderItem & _item );

	void updateItem ( int _index, const OrderItem & _item );

	void dropItem ( int _index );

	void clearItems ();

	Money getCost () const;
	
	bool isModifiable () const;

	void checkout ();

	void addCheckoutListener ( ICheckoutListener & _listener );

	void removeCheckoutListener ( ICheckoutListener & _listener );

/*-----------------------------------------------------------------------------*/

private:

/*-----------------------------------------------------------------------------*/
		
	std::vector< OrderItem > m_items;

	std::vector< ICheckoutListener * > m_checkoutListeners;

	bool m_modifiable;

/*-----------------------------------------------------------------------------*/

};


/*-----------------------------------------------------------------------------*/


inline int 
OrderCart::getItemsCount () const
{
	return m_items.size();

} // OrderCart::getItemsCount


/*-----------------------------------------------------------------------------*/


inline OrderItem const &
OrderCart::getItem ( int _index ) const
{
	return m_items.at( _index );

} // OrderCart::getItem


/*-----------------------------------------------------------------------------*/


inline bool
OrderCart::isModifiable () const
{
	return m_modifiable;

} // OrderCart::isModifiable


/*=============================================================================*/ 

} // namespace Pizzario

/*=============================================================================*/ 

#endif //  _ORDER_CART_HPP_
