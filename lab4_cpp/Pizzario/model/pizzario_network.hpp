/** (C) 2015, Sergei Zaychenko, KNURE */

#ifndef _PIZZARIO_NETWORK_HPP_
#define _PIZZARIO_NETWORK_HPP_

/*-----------------------------------------------------------------------------*/

#include <vector>
#include <memory>

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/ 

class Menu;
class Order;
class DeliveryContact;
class PaymentPlan;

/*-----------------------------------------------------------------------------*/

class PizzarioNetwork
{

/*-----------------------------------------------------------------------------*/

public:

/*-----------------------------------------------------------------------------*/

	PizzarioNetwork ();

	virtual ~ PizzarioNetwork ();

	Menu & getMenu () const;

/*-----------------------------------------------------------------------------*/

	int getOrdersCount () const;

	Order & getOrder ( int _index ) const;

	Order & newOrder ( DeliveryContact const & _contact, std::unique_ptr< PaymentPlan > _paymentPlan );

/*-----------------------------------------------------------------------------*/

private:

/*-----------------------------------------------------------------------------*/
	
	PizzarioNetwork ( const PizzarioNetwork & );
	PizzarioNetwork & operator = ( const PizzarioNetwork & );

/*-----------------------------------------------------------------------------*/
	
	std::unique_ptr< Menu > m_menu;

	std::vector< std::unique_ptr< Order > > m_orders;

/*-----------------------------------------------------------------------------*/

};


/*-----------------------------------------------------------------------------*/


inline
Menu & 
PizzarioNetwork::getMenu () const
{
	return * m_menu;

} // PizzarioNetwork::getMenu


/*-----------------------------------------------------------------------------*/


inline
int 
PizzarioNetwork::getOrdersCount () const
{
	return m_orders.size();

} // PizzarioNetwork::getOrdersCount


/*-----------------------------------------------------------------------------*/


inline
Order & 
PizzarioNetwork::getOrder ( int _index ) const
{
	return *( m_orders.at( _index ) );

} // PizzarioNetwork::getOrder
	

/*=============================================================================*/ 

} // namespace Pizzario

/*=============================================================================*/ 

#endif //  _PIZZARIO_NETWORK_HPP_