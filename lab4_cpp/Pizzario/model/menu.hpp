/** (C) 2015, Sergei Zaychenko, KNURE */

#ifndef _MENU_HPP_
#define _MENU_HPP_

/*-----------------------------------------------------------------------------*/

#include "model/order_item.hpp"
#include "model/pizza_kind.hpp"

#include "utils/iterator_utils.hpp"
#include "utils/map_key_iterator.hpp"

#include <unordered_map>
#include <vector>

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/ 

class PizzaKind;

/*-----------------------------------------------------------------------------*/


class Menu
	:	private PizzaKind::INameChangedListener
{

/*-----------------------------------------------------------------------------*/

	typedef std::unordered_map< std::string, std::unique_ptr< PizzaKind > > PizzaKindsByName;
	typedef Utils::MapKeyIterator< PizzaKindsByName > PizzaKindsByNameKeyIterator;

/*-----------------------------------------------------------------------------*/

public:

/*-----------------------------------------------------------------------------*/
	
	typedef Utils::Iterator< PizzaKindsByNameKeyIterator > PizzaNameIterator;

/*-----------------------------------------------------------------------------*/

	Menu ();

	~Menu ();

	int getPizzaKindsCount () const;

	PizzaNameIterator pizzaKindNamesBegin () const;
	PizzaNameIterator pizzaKindNamesEnd () const;

	PizzaKind * findPizzaKind ( std::string const & _name ) const;

	void addPizzaKind ( std::unique_ptr< PizzaKind > _kind );

	void deletePizzaKind ( std::string const & _name ); 

	OrderItem makeOrderItem (  std::string const & _name, PizzaSize::Enum _size, int _quantity ) const;

/*-----------------------------------------------------------------------------*/

private:

/*-----------------------------------------------------------------------------*/
	
	Menu ( const Menu & );
	Menu & operator = ( const Menu & );
	
	void onPizzaKindNameChanged ( std::string const & _oldName, std::string const & _newName ) override;
	
/*-----------------------------------------------------------------------------*/

	PizzaKindsByName m_pizzaKinds;

/*-----------------------------------------------------------------------------*/

};


/*-----------------------------------------------------------------------------*/


inline int 
Menu::getPizzaKindsCount () const
{
	return m_pizzaKinds.size();

} // Menu::getPizzaKindsCount


/*-----------------------------------------------------------------------------*/

} // namespace Pizzario

/*=============================================================================*/ 

#endif //  _MENU_HPP_