/** (C) 2015, Sergei Zaychenko, KNURE */

#include "model/pizzario_network.hpp"
#include "model/menu.hpp"
#include "model/order.hpp"
#include "model/payment_plan.hpp"

#include <ctime>

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/


PizzarioNetwork::PizzarioNetwork ()
	:	m_menu( new Menu() )
{
} // PizzarioNetwork::PizzarioNetwork


/*-----------------------------------------------------------------------------*/


PizzarioNetwork::~PizzarioNetwork ()
{
	// Automatic destruction of
	//  - menu
	//  - orders

} // PizzarioNetwork::~PizzarioNetwork


/*-----------------------------------------------------------------------------*/

	
Order & 
PizzarioNetwork::newOrder ( DeliveryContact const & _contact, std::unique_ptr< PaymentPlan > _paymentPlan )
{
	Order * pOrder = 
		new Order(	
				m_orders.size()
			,	std::unique_ptr< OrderCart >( new OrderCart() )
			,	_contact
			,	std::move( _paymentPlan ) 
		);

	m_orders.emplace_back( std::unique_ptr< Order >( pOrder ) );
	return * pOrder;

} // PizzarioNetwork::newOrder


/*=============================================================================*/ 

} // namespace Pizzario

/*=============================================================================*/ 
