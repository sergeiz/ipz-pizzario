/** (C) 2015, Sergei Zaychenko, KNURE */

#include "model/order_cart.hpp"

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/ 


OrderCart::OrderCart ()
	:	m_modifiable( true )
{
} // OrderCart::OrderCart


/*-----------------------------------------------------------------------------*/


void 
OrderCart::addItem ( const OrderItem & _item )
{
    if ( ! m_modifiable )
        throw std::runtime_error( "OrderCart::addItem: unmodifiable cart" );

    for ( OrderItem const & i : m_items )
    {
        if ( & i.getKind() == & _item.getKind() && i.getSize() == _item.getSize() )
            throw std::runtime_error( "OrderCart::addItem: duplicate kind-size pair added" );
    }

	m_items.push_back( _item );

} // OrderCart::addItem


/*-----------------------------------------------------------------------------*/


void 
OrderCart::updateItem ( int _index, const OrderItem & _item )
{
    if ( ! m_modifiable )
        throw std::runtime_error( "OrderCart::updateItem: unmodifiable cart" );

	int nItems = m_items.size();
	for ( int i = 0; i < nItems; i++ )
	{
		if ( i == _index )
			continue;

		OrderItem const & item = m_items[ i ];
		if ( & item.getKind() == & _item.getKind() && item.getSize() == _item.getSize() )
            throw std::runtime_error( "OrderCart::updateItem: duplicate kind-size pair added" );
	}

	m_items.at( _index ) = _item;

} // OrderCart::updateItem


/*-----------------------------------------------------------------------------*/


void 
OrderCart::dropItem ( int _index )
{
    if ( ! m_modifiable )
        throw std::runtime_error( "OrderCart::dropItem: unmodifiable cart" );

	if ( _index < 0 || _index >= static_cast< int >( m_items.size() ) )
		throw std::out_of_range( "OrderCart::dropItem" );

	m_items.erase( m_items.begin() + _index );

} // OrderCart::dropItem


/*-----------------------------------------------------------------------------*/


void 
OrderCart::clearItems ()
{
    if ( ! m_modifiable )
        throw std::runtime_error( "OrderCart::dropItem: unmodifiable cart" );

	m_items.clear();

} // OrderCart::clearItems


/*-----------------------------------------------------------------------------*/


Money
OrderCart::getCost () const
{
	Money sum;
	
	int nItems = getItemsCount();
	for ( int i = 0; i < nItems; i++ ) 
		sum += getItem( i ).getCost();

	return sum;

} // OrderCart::getCost


/*-----------------------------------------------------------------------------*/


void
OrderCart::checkout ()
{
    if ( m_items.empty() )
        throw std::runtime_error( "OrderCart::checkout: checkout of empty cart" );

    m_modifiable = false;
	
	for ( ICheckoutListener * pListener : m_checkoutListeners )
		pListener->onCartCheckout();

} // OrderCart::checkout


/*-----------------------------------------------------------------------------*/


void 
OrderCart::addCheckoutListener ( ICheckoutListener & _listener )
{
	m_checkoutListeners.push_back( & _listener );

} // OrderCart::addCheckoutListener


/*-----------------------------------------------------------------------------*/


void 
OrderCart::removeCheckoutListener ( ICheckoutListener & _listener )
{
	auto itListeners = std::find( m_checkoutListeners.begin(), m_checkoutListeners.end(), & _listener );
	if ( itListeners != m_checkoutListeners.end() )
		m_checkoutListeners.erase( itListeners );

	else
		throw std::runtime_error( "OrderCart::removeCheckoutListener: removing unregistered listener");

} // OrderCart::removeCheckoutListener


/*=============================================================================*/ 

} // namespace Pizzario

/*=============================================================================*/ 
