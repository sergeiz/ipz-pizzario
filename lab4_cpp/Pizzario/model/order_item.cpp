/** (C) 2015, Sergei Zaychenko, KNURE */

#include "model/order_item.hpp"

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/


OrderItem::OrderItem (
		PizzaKind const & _kind
	,	PizzaSize::Enum _size
	,	Money _fixedPrice
	,	int _quantity 
)
	:	m_pKind( & _kind )
	,	m_size( _size )
	,	m_fixedPrice( _fixedPrice )
	,	m_quantity( _quantity )
{
    if ( _fixedPrice.isNegative() )
        throw std::runtime_error( "OrderItem: negative price" );

    if ( _quantity <= 0 )
        throw std::runtime_error( "OrderItem: non-positive quantity" );

} // OrderItem::OrderItem


/*-----------------------------------------------------------------------------*/


Money 
OrderItem::getCost () const
{
	return m_fixedPrice * m_quantity;

} // OrderItem::getCost


/*-----------------------------------------------------------------------------*/


bool 
OrderItem::operator == ( const OrderItem & _item ) const
{
	return m_pKind == _item.m_pKind &&
		   m_quantity == _item.m_quantity &&
		   m_fixedPrice == _item.m_fixedPrice &&
		   m_size == _item.m_size;

} // OrderItem::operator == 


/*-----------------------------------------------------------------------------*/


bool 
OrderItem::operator != ( const OrderItem & _item ) const
{
	return !( * this == _item );

} // OrderItem::operator !=


/*=============================================================================*/ 

} // namespace Pizzario

/*=============================================================================*/ 
