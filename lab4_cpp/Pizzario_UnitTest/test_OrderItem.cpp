/** (C) 2015, Sergei Zaychenko, KNURE */

#include "model/order_item.hpp"
#include "model/pizza_kind.hpp"

#include "gtest/gtest.h"

#include <memory>

/*=============================================================================*/ 

namespace Pizzario {
namespace Test {

/*=============================================================================*/


class OrderItemTests
    :    public testing::Test
{
protected:
    static std::unique_ptr< PizzaKind > makePizzaKind ()
    {
         return std::unique_ptr< PizzaKind >( new PizzaKind( "Carbonara" ) );
    }
};


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderItemTests, Constructor_ProjectsFields_Correctly ) 
{
    auto kind = makePizzaKind();
    OrderItem item( * kind, PizzaSize::Small, Money( 3,00 ), 2 );

    ASSERT_EQ( & item.getKind(), kind.get() );
    ASSERT_EQ( item.getSize(), PizzaSize::Small );
    ASSERT_EQ( item.getFixedPrice(), Money( 3, 00 ) );
    ASSERT_EQ( item.getQuantity(), 2 );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderItemTests, Constructor_NegativePrice_Forbidden ) 
{
    auto kind = makePizzaKind();

    ASSERT_THROW(
            OrderItem( * kind, PizzaSize::Small, Money( -1, 00 ), 1 )
        ,    std::runtime_error
    );

    ASSERT_NO_THROW(
        OrderItem( * kind, PizzaSize::Small, Money(), 1 )
    );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderItemTests, Constructor_NonPositiveQuantity_Forbidden ) 
{
    auto kind = makePizzaKind();

    ASSERT_THROW(
            OrderItem( * kind, PizzaSize::Small, Money( 1, 00 ), -1 )
        ,    std::runtime_error
    );

    ASSERT_NO_THROW(
        OrderItem( * kind, PizzaSize::Small, Money( 1, 00 ), 1 )
    );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderItemTests, Cost_SingleItem_MatchesPrice ) 
{
    auto kind = makePizzaKind();
    OrderItem item( * kind, PizzaSize::Small, Money( 3,00 ), 1 );
    ASSERT_EQ( item.getCost(), item.getFixedPrice() );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderItemTests, Cost_NItems_MultipliesPrice ) 
{
    auto kind = makePizzaKind();
    OrderItem item( * kind, PizzaSize::Small, Money( 3,00 ), 8 );
    ASSERT_EQ( item.getCost(), item.getFixedPrice() * 8 );
}


/*=============================================================================*/ 

} // namespace Test
} // namespace Pizzario

/*=============================================================================*/ 
