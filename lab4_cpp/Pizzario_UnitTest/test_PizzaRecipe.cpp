/** (C) 2015, Sergei Zaychenko, KNURE */

#include "model/pizza_recipe.hpp"
#include "gtest/gtest.h"

/*=============================================================================*/ 

namespace Pizzario {
namespace Test {

/*=============================================================================*/ 


TEST ( PizzaRecipeTest, Ingredients_Initially_None ) 
{
    PizzaRecipe recipe;
    ASSERT_EQ( recipe.getIngredientsCount(), 0 );
}


/*-----------------------------------------------------------------------------*/


TEST ( PizzaRecipeTest, Ingredients_AskMissingInEmpty_ReturnsZero )
{
    PizzaRecipe recipe;
    ASSERT_EQ( recipe.getIngredientWeight( "cheese" ), 0 );
}


/*-----------------------------------------------------------------------------*/


TEST ( PizzaRecipeTest, Ingredients_InsertOne_ReturnWeight )
{
    PizzaRecipe recipe;
    recipe.addIngredient( "cheese", 10 );

    ASSERT_EQ( recipe.getIngredientsCount(), 1 );
    ASSERT_EQ( recipe.getIngredientWeight( "cheese" ), 10 );
}


/*-----------------------------------------------------------------------------*/



TEST ( PizzaRecipeTest, Ingredients_AskMissingInNonEmpty_ReturnsZero )
{
    PizzaRecipe recipe;
    recipe.addIngredient( "cheese", 10 );

    ASSERT_EQ( recipe.getIngredientWeight( "ham" ), 0 );
}


/*-----------------------------------------------------------------------------*/


TEST ( PizzaRecipeTest, Ingredients_InsertTwo_ReturnEachWeight )
{
    PizzaRecipe recipe;
    recipe.addIngredient( "cheese", 10 );
    recipe.addIngredient( "ham", 20 );

    ASSERT_EQ( recipe.getIngredientsCount(), 2 );
    ASSERT_EQ( recipe.getIngredientWeight( "cheese" ), 10 );
    ASSERT_EQ( recipe.getIngredientWeight( "ham" ), 20 );
}


/*-----------------------------------------------------------------------------*/


TEST ( PizzaRecipeTest, Ingredients_InsertDuplicate_Forbidden )
{
    PizzaRecipe recipe;
    recipe.addIngredient( "cheese", 10 );
    ASSERT_THROW( recipe.addIngredient( "cheese", 20 ), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST ( PizzaRecipeTest, Ingredients_InsertAndUpdate_ReturnUpdatedWeight )
{
    PizzaRecipe recipe;
    recipe.addIngredient( "cheese", 10 );
    recipe.updateIngredient( "cheese", 20 );

    ASSERT_EQ( recipe.getIngredientWeight( "cheese" ), 20 );
}


/*-----------------------------------------------------------------------------*/


TEST ( PizzaRecipeTest, Ingredients_UpdatingOneOfTwo_DoesNotAlterSecond )
{
    PizzaRecipe recipe;
    recipe.addIngredient( "cheese", 10 );
    recipe.addIngredient( "ham", 20 );
    recipe.updateIngredient( "cheese", 30 );

    ASSERT_EQ( recipe.getIngredientWeight( "cheese" ), 30 );
    ASSERT_EQ( recipe.getIngredientWeight( "ham" ), 20 );
}


/*-----------------------------------------------------------------------------*/


TEST ( PizzaRecipeTest, Ingredients_DeleteSingle_MakesEmpty )
{
    PizzaRecipe recipe;
    recipe.addIngredient( "cheese", 10 );
    recipe.removeIngredient( "cheese" );

    ASSERT_EQ( recipe.getIngredientsCount(), 0 );
}


/*-----------------------------------------------------------------------------*/


TEST ( PizzaRecipeTest, Ingredients_DeleteOneOfTwo_LeavesSecond )
{
    PizzaRecipe recipe;
    recipe.addIngredient( "cheese", 10 );
    recipe.addIngredient( "ham", 20 );
    recipe.removeIngredient( "cheese" );

    ASSERT_EQ( recipe.getIngredientsCount(), 1 );
    ASSERT_EQ( recipe.getIngredientWeight( "ham" ), 20 );
    ASSERT_EQ( recipe.getIngredientWeight( "cheese" ), 0 );
}


/*-----------------------------------------------------------------------------*/


TEST ( PizzaRecipeTest, Ingredients_DeleteMissingFromEmpty_Forbidden )
{
    PizzaRecipe recipe;
    ASSERT_THROW( recipe.removeIngredient( "cheese" ), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST ( PizzaRecipeTest, Ingredients_DeleteMissingFromNonEmpty_Forbidden )
{
    PizzaRecipe recipe;
    recipe.addIngredient( "ham", 50 );
    ASSERT_THROW( recipe.removeIngredient( "cheese" ), std::runtime_error );
}


/*=============================================================================*/ 

} // namespace Test
} // namespace Pizzario

/*=============================================================================*/ 
