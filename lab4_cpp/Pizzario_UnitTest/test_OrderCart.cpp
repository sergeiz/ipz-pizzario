/** (C) 2015, Sergei Zaychenko, KNURE */

#include "model/order_cart.hpp"
#include "model/pizza_kind.hpp"

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include <memory>

/*=============================================================================*/ 

namespace Pizzario {
namespace Test {

/*=============================================================================*/


class OrderCartTests
    :    public testing::Test
{
protected:

    OrderCartTests ()
        :    m_carbonara( new PizzaKind( "Carbonara" ) )
        ,    m_milano( new PizzaKind( "Milano" ) )
    {
    }

    OrderItem makeSmallItem () const
    {
        return OrderItem( * m_carbonara, PizzaSize::Small, Money( 3, 00 ), 1 );
    }
    
    OrderItem makeMediumItem () const
    {
        return OrderItem( * m_milano, PizzaSize::Medium, Money( 5, 00 ), 1 );
    }

    std::unique_ptr< PizzaKind > m_carbonara, m_milano;
};


/*-----------------------------------------------------------------------------*/


class MockCartCheckoutListener : public OrderCart::ICheckoutListener
{
public:
    MOCK_METHOD0( onCartCheckout, void () );
};


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderCartTests, Items_None_Initially ) 
{
    OrderCart cart;
    ASSERT_EQ( cart.getItemsCount(), 0 );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderCartTests, Add_One_ListOne ) 
{
    OrderCart cart;
    OrderItem item = makeSmallItem();
    cart.addItem( item );

    ASSERT_EQ( cart.getItemsCount(), 1 );
    ASSERT_EQ( cart.getItem( 0 ), item );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderCartTests, Add_Two_ListTwo ) 
{
    OrderCart cart;
    OrderItem item1 = makeSmallItem();
    OrderItem item2 = makeMediumItem();

    cart.addItem( item1 );
    cart.addItem( item2 );

    ASSERT_EQ( cart.getItemsCount(), 2 );
    ASSERT_EQ( cart.getItem( 0 ), item1 );
    ASSERT_EQ( cart.getItem( 1 ), item2 );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderCartTests, Add_TwoSameKindSize_Fails ) 
{
    OrderCart cart;
    OrderItem item = makeSmallItem();
    cart.addItem( item );

    ASSERT_THROW( cart.addItem( item ), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderCartTests, Add_TwoSameKindDifferentSize_Passes ) 
{
    OrderCart cart;
    OrderItem itemS( * m_carbonara, PizzaSize::Small,    Money( 3, 00 ), 1 );
    OrderItem itemM( * m_carbonara, PizzaSize::Medium,    Money( 3, 00 ), 1 );
    cart.addItem( itemS );
    cart.addItem( itemM );

    ASSERT_EQ( cart.getItemsCount(), 2 );
    ASSERT_EQ( cart.getItem( 0 ), itemS );
    ASSERT_EQ( cart.getItem( 1 ), itemM );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderCartTests, Add_TwoDifferentKindSameSize_Passes ) 
{
    OrderCart cart;
    OrderItem itemC( * m_carbonara, PizzaSize::Small,    Money( 3, 00 ), 1 );
    OrderItem itemM( * m_milano,    PizzaSize::Small,    Money( 3, 00 ), 1 );
    cart.addItem( itemC );
    cart.addItem( itemM );

    ASSERT_EQ( cart.getItemsCount(), 2 );
    ASSERT_EQ( cart.getItem( 0 ), itemC );
    ASSERT_EQ( cart.getItem( 1 ), itemM );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderCartTests, Update_OnEmpty_Fails ) 
{
    OrderCart cart;
    ASSERT_THROW( cart.updateItem( 0, makeSmallItem() ), std::out_of_range );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderCartTests, Update_OnNonEmptyGoodIndex_Passes ) 
{
    OrderCart cart;
    cart.addItem( makeSmallItem() );
    OrderItem newItem = makeMediumItem();
    cart.updateItem( 0, newItem );

    ASSERT_EQ( cart.getItemsCount(), 1 );
    ASSERT_EQ( cart.getItem( 0 ), newItem );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderCartTests, Update_OnNonEmptyBadIndex_Fails ) 
{
    OrderCart cart;
    cart.addItem( makeSmallItem() );

    ASSERT_THROW( cart.updateItem( 1, makeMediumItem() ), std::out_of_range );
    ASSERT_THROW( cart.updateItem( -1, makeMediumItem() ), std::out_of_range );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderCartTests, Update_ThatBreakSamePizzaKindSizeInvariant_Forbidden ) 
{
    OrderCart cart;
    cart.addItem( makeSmallItem() );
    cart.addItem( makeMediumItem() );

    ASSERT_THROW( cart.updateItem( 1, makeSmallItem() ), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderCartTests, Drop_Empty_Fails ) 
{
    OrderCart cart;

    ASSERT_THROW( cart.dropItem( 0 ), std::out_of_range );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderCartTests, Drop_NonEmptyGoodIndex_Passes ) 
{
    OrderCart cart;
    cart.addItem( makeSmallItem() );
    cart.dropItem( 0 );

    ASSERT_EQ( cart.getItemsCount(), 0 );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderCartTests, Drop_NonEmptyBadIndex_Fails ) 
{
    OrderCart cart;
    cart.addItem( makeSmallItem() );
    ASSERT_THROW( cart.dropItem( 1 ), std::out_of_range );
    ASSERT_THROW( cart.dropItem( -1 ), std::out_of_range );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderCartTests, Clear_Empty_Passes ) 
{
    OrderCart cart;
    cart.clearItems();
    ASSERT_EQ( cart.getItemsCount(), 0 );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderCartTests, Clear_NonEmpty_Passes ) 
{
    OrderCart cart;
    cart.addItem( makeSmallItem() );
    cart.clearItems();
    ASSERT_EQ( cart.getItemsCount(), 0 );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderCartTests, Cost_Empty_Zero ) 
{
    OrderCart cart;
    ASSERT_EQ( cart.getCost(), Money() );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderCartTests, Cost_SingleItem_MatchesItem ) 
{
    OrderCart cart;
    cart.addItem( makeSmallItem() );
    ASSERT_EQ( cart.getCost(), cart.getItem( 0 ).getCost() );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderCartTests, Cost_TwoItems_MatchesSumItems ) 
{
    OrderCart cart;
    cart.addItem( makeSmallItem() );
    cart.addItem( makeMediumItem() );

    ASSERT_EQ( cart.getCost(), cart.getItem( 0 ).getCost() + cart.getItem( 1 ).getCost() );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderCartTests, Modifiable_IsTrue_Initially ) 
{
    OrderCart cart;
    ASSERT_TRUE( cart.isModifiable() );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderCartTests, Modifiable_AfterCheckout_IsFalse ) 
{
    OrderCart cart;
    cart.addItem( makeSmallItem()  );
    cart.checkout();
    ASSERT_FALSE( cart.isModifiable() );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderCartTests, Checkout_Empty_Fails ) 
{
    OrderCart cart;
    ASSERT_THROW( cart.checkout(), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderCartTests, Checkout_SendsEvent ) 
{
    OrderCart cart;
    cart.addItem( makeSmallItem() );

    MockCartCheckoutListener mockListener;
    cart.addCheckoutListener( mockListener );    

    EXPECT_CALL( mockListener, onCartCheckout() ).Times( 1 );

    cart.checkout();
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderCartTests, Unmodifiable_ReadingItems_Passes ) 
{
    OrderCart cart;

    OrderItem item1 = makeSmallItem();
    OrderItem item2 = makeMediumItem();
    cart.addItem( item1 );
    cart.addItem( item2 );

    cart.checkout();

    ASSERT_EQ( cart.getItemsCount(), 2 );
    ASSERT_EQ( cart.getItem( 0 ), item1 );
    ASSERT_EQ( cart.getItem( 1 ), item2 );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderCartTests, Unmodifiable_AddItem_Forbidden ) 
{
    OrderCart cart;
    cart.addItem( makeSmallItem() );

    cart.checkout();

    ASSERT_THROW( cart.addItem( makeMediumItem() ), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderCartTests, Unmodifiable_UpdateItem_Forbidden ) 
{
    OrderCart cart;
    cart.addItem( makeSmallItem() );

    cart.checkout();

    ASSERT_THROW( cart.updateItem( 0, makeMediumItem() ), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderCartTests, Unmodifiable_DropItem_Forbidden ) 
{
    OrderCart cart;
    cart.addItem( makeSmallItem() );
    cart.checkout();

    ASSERT_THROW( cart.dropItem( 0 ), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderCartTests, Unmodifiable_ClearItems_Forbidden ) 
{
    OrderCart cart;
    cart.addItem( makeSmallItem() );
    cart.checkout();

    ASSERT_THROW( cart.clearItems(), std::runtime_error );
}


/*=============================================================================*/ 

} // namespace Test
} // namespace Pizzario

/*=============================================================================*/ 
