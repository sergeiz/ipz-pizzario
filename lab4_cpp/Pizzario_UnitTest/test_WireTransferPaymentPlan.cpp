/** (C) 2015, Sergei Zaychenko, KNURE */

#include "model/wire_transfer_payment_plan.hpp"
#include "gtest/gtest.h"

#include <memory>

/*=============================================================================*/ 

namespace Pizzario {
namespace Test {

/*=============================================================================*/ 


TEST( WireTransferPaymentPlanTests, Constructor_Projects_Fields_Correctly )
{
    WireTransferPaymentPlan p( "12345", "Ivan Ivanov" );

    ASSERT_EQ( p.getCardCode(), "12345" );
    ASSERT_EQ( p.getCardHolder(), "Ivan Ivanov" );
}


/*-----------------------------------------------------------------------------*/


TEST( WireTransferPaymentPlanTests, Clone_Copies_Fields_Correctly )
{
    WireTransferPaymentPlan p1( "12345", "Ivan Ivanov" );
    std::auto_ptr< WireTransferPaymentPlan > p2( p1.clone() );

    ASSERT_EQ( p2->getCardCode(), "12345" );
    ASSERT_EQ( p2->getCardHolder(), "Ivan Ivanov" );
}


/*-----------------------------------------------------------------------------*/


TEST ( WireTransferPaymentPlanTests, Payed_Initially_False ) 
{
    WireTransferPaymentPlan p( "12345", "Ivan Ivanov" );
    ASSERT_FALSE( p.wasPayed() );
}


/*-----------------------------------------------------------------------------*/


TEST ( WireTransferPaymentPlanTests, Payed_SetPayedToTrue_Confirmed ) 
{
    WireTransferPaymentPlan p( "12345", "Ivan Ivanov" );
    p.markPayed();
    ASSERT_TRUE( p.wasPayed() );
}


/*-----------------------------------------------------------------------------*/


TEST ( WireTransferPaymentPlanTests, Prepayment_Expected ) 
{
    WireTransferPaymentPlan p( "12345", "Ivan Ivanov" );
    ASSERT_TRUE( p.expectPrepayment() );
}


/*-----------------------------------------------------------------------------*/


TEST ( WireTransferPaymentPlanTests, Clone_Payed_StillPayed ) 
{
    WireTransferPaymentPlan p1( "12345", "Ivan Ivanov" );
    p1.markPayed();

    std::unique_ptr< WireTransferPaymentPlan > p2( p1.clone() );
    ASSERT_TRUE( p2->wasPayed() );
}


/*-----------------------------------------------------------------------------*/



TEST ( WireTransferPaymentPlanTests, Clone_Unpayed_StillUnpayed ) 
{
    WireTransferPaymentPlan p1( "12345", "Ivan Ivanov" );
    std::unique_ptr< WireTransferPaymentPlan > p2( p1.clone() );
    ASSERT_FALSE( p2->wasPayed() );
}


/*=============================================================================*/ 

} // namespace Test
} // namespace Pizzario

/*=============================================================================*/ 
