/** (C) 2015, Sergei Zaychenko, KNURE */

#include "model/pizza_kind.hpp"
#include "gtest/gtest.h"
#include "gmock/gmock.h" 

/*=============================================================================*/ 

namespace Pizzario {
namespace Test {

/*=============================================================================*/ 


class PizzaKindTest
    :    public testing::Test
{
protected:

    static std::unique_ptr< PizzaKind > makeKind ()
    {
         return std::unique_ptr< PizzaKind >( new PizzaKind( "Carbonara" ) );
    }
};


/*-----------------------------------------------------------------------------*/


class MockNameChangedListener : public PizzaKind::INameChangedListener
{
 
public:

    MOCK_METHOD2( onPizzaKindNameChanged, void ( std::string const &, std::string const & ) );

};


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaKindTest, Constructor_ReturnsExpectedDefaults ) 
{
    auto kind = makeKind();

    ASSERT_EQ( kind->getName(), "Carbonara" );
    ASSERT_EQ( kind->getDescription(), "" );
    ASSERT_EQ( kind->getImageURL(), "" );
    ASSERT_EQ( kind->getRatingVotesCount(), 0 );
    ASSERT_EQ( kind->getTotalRating(), 0 );
    ASSERT_FALSE( kind->isHidden() );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaKindTest, Name_ConstructingWithEmpty_Forbidden ) 
{
    ASSERT_THROW( new PizzaKind( "" ), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaKindTest, Name_AssigningToEmpty_Forbidden ) 
{
    auto kind = makeKind();
    ASSERT_THROW( kind->rename( "" ), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaKindTest, Rating_WhenNoVotes_Zero ) 
{
    auto kind = makeKind();

    ASSERT_EQ( kind->getRatingVotesCount(), 0 );
    ASSERT_EQ( kind->getTotalRating(), 0 );
    ASSERT_EQ( kind->getAverageRating(), 0 );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaKindTest, Rating_WhenSingleVote_ReturnsIt ) 
{
    auto kind = makeKind();
    kind->rate( 3 );

    ASSERT_EQ( kind->getRatingVotesCount(), 1 );
    ASSERT_EQ( kind->getTotalRating(), 3 );
    ASSERT_EQ( kind->getAverageRating(), 3.0 );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaKindTest, Rating_WhenTwoVotes_ReturnsAverage ) 
{
    auto kind = makeKind();
    kind->rate( 3 );
    kind->rate( 5 );

    ASSERT_EQ( kind->getRatingVotesCount(), 2 );
    ASSERT_EQ( kind->getTotalRating(), 8 );
    ASSERT_EQ( kind->getAverageRating(), 4.0 );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaKindTest, Rating_WhenThreeVotes_ReturnsAverage ) 
{
    auto kind = makeKind();
    kind->rate( 1 );
    kind->rate( 3 );
    kind->rate( 5 );

    ASSERT_EQ( kind->getRatingVotesCount(), 3 );
    ASSERT_EQ( kind->getTotalRating(), 9 );
    ASSERT_EQ( kind->getAverageRating(), 3 );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaKindTest, Rating_ZeroVotes_Forbidden ) 
{
    auto kind = makeKind();
    ASSERT_THROW( kind->rate( 0 ), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaKindTest, Rating_NegativeVotes_Forbidden ) 
{
    auto kind = makeKind();
    ASSERT_THROW( kind->rate( -1 ), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaKindTest, Rating_VotesBeyondMaximum_Forbidden ) 
{
    auto kind = makeKind();
    ASSERT_NO_THROW( kind->rate( PizzaKind::MaximumVote ) );
    ASSERT_THROW( kind->rate( PizzaKind::MaximumVote + 1 ), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaKindTest, RatingSetup_VotesStartFromConcrete_ReturnsSame ) 
{
    auto kind = makeKind();
    kind->setupRatingStats( 5, 25 );

    ASSERT_EQ( kind->getRatingVotesCount(), 5 );
    ASSERT_EQ( kind->getTotalRating(), 25 );
    ASSERT_EQ( kind->getAverageRating(), 5 );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaKindTest, RatingSetup_VotesBeforeSetup_GetOverwritten ) 
{
    auto kind = makeKind();
    kind->rate( 5 );
    kind->setupRatingStats( 5, 25 );

    ASSERT_EQ( kind->getRatingVotesCount(), 5 );
    ASSERT_EQ( kind->getTotalRating(), 25 );
    ASSERT_EQ( kind->getAverageRating(), 5 );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaKindTest, RatingSetup_VotesAfterSetup_AppendToSetup ) 
{
    auto kind = makeKind();
    kind->setupRatingStats( 5, 25 );
    kind->rate( 5 );

    ASSERT_EQ( kind->getRatingVotesCount(), 6 );
    ASSERT_EQ( kind->getTotalRating(), 30 );
    ASSERT_EQ( kind->getAverageRating(), 5 );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaKindTest, RatingSetup_NegativeVotesSetup_Forbidden ) 
{
    auto kind = makeKind();
    ASSERT_THROW( kind->setupRatingStats( -1, 0 ), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaKindTest, RatingSetup_ZeroVotes_IsFineWhenZeroTotal ) 
{
    auto kind = makeKind();
    ASSERT_NO_THROW( kind->setupRatingStats( 0, 0 ) );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaKindTest, RatingSetup_ZeroVotes_IsNotFineWithNonZeroTotal ) 
{
    auto kind = makeKind();
    ASSERT_THROW( kind->setupRatingStats( 0, 1 ), std::runtime_error );
    ASSERT_THROW( kind->setupRatingStats( 0, -1 ), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaKindTest, RatingSetup_TotalSmallerThanVotes_NotAllowed ) 
{
    auto kind = makeKind();
    ASSERT_NO_THROW( kind->setupRatingStats( 5, 5 ) );
    ASSERT_THROW( kind->setupRatingStats( 5, 4 ), std::runtime_error );
    ASSERT_THROW( kind->setupRatingStats( 1, 0 ), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaKindTest, RatingSetup_TotalLargeThanMaxVotes_NotAllowed ) 
{
    auto kind = makeKind();
    ASSERT_NO_THROW( kind->setupRatingStats( 5, 25 ) );
    ASSERT_THROW( kind->setupRatingStats( 5, 5 * PizzaKind::MaximumVote + 1 ),    std::runtime_error );
    ASSERT_THROW( kind->setupRatingStats( 1, PizzaKind::MaximumVote + 1 ),        std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaKindTest, Prices_Initially_Zeroes ) 
{
    auto kind = makeKind();
    ASSERT_EQ( kind->getCurrentPrice( PizzaSize::Small ),  Money() );
    ASSERT_EQ( kind->getCurrentPrice( PizzaSize::Medium ), Money() );
    ASSERT_EQ( kind->getCurrentPrice( PizzaSize::Large ),  Money() );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaKindTest, Prices_UpdateEach_ConfirmAssignedValues ) 
{
    auto kind = makeKind();
    kind->updatePrice( PizzaSize::Small,    Money( 3, 00 ) );
    kind->updatePrice( PizzaSize::Medium,    Money( 5, 00 ) );
    kind->updatePrice( PizzaSize::Large,    Money( 7, 00 ) );

    ASSERT_EQ( kind->getCurrentPrice( PizzaSize::Small ),    Money( 3, 00 ) );
    ASSERT_EQ( kind->getCurrentPrice( PizzaSize::Medium ),    Money( 5, 00 ) );
    ASSERT_EQ( kind->getCurrentPrice( PizzaSize::Large ),    Money( 7, 00 ) );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaKindTest, Prices_UpdateSameTypeTwice_SavesSecondPrice ) 
{
    auto kind = makeKind();
    kind->updatePrice( PizzaSize::Small, Money( 3, 00 ) );
    kind->updatePrice( PizzaSize::Small, Money( 3, 50 ) );

    ASSERT_EQ( kind->getCurrentPrice( PizzaSize::Small ), Money( 3, 50 ) );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaKindTest, Prices_UpdateToZero_Allowed ) 
{
    auto kind = makeKind();
    kind->updatePrice( PizzaSize::Small, Money() );
    ASSERT_EQ( kind->getCurrentPrice( PizzaSize::Small ), Money() );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaKindTest, Prices_UpdateToNegative_Forbidden ) 
{
    auto kind = makeKind();
    ASSERT_THROW( kind->updatePrice( PizzaSize::Small, Money( -1, 00 ) ), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaKindTest, Name_Reassignment_SendsEvent ) 
{
    PizzaKind kind( "Carbonara" );

    MockNameChangedListener mockListener;
    kind.addNameChangedListener( mockListener );

    EXPECT_CALL( mockListener, onPizzaKindNameChanged( "Carbonara", "Milano" ) ).Times( 1 );

    kind.rename( "Milano" );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaKindTest, Name_ReassignmentWithoutListener_PassesSilently ) 
{
    PizzaKind kind( "Carbonara" );
    kind.rename( "New Carbonara" );
    ASSERT_EQ( kind.getName(), "New Carbonara" );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaKindTest, Name_ReassignmentToSame_DoesNotSendEvent ) 
{
    PizzaKind kind( "Carbonara" );

    MockNameChangedListener mockListener;
    kind.addNameChangedListener( mockListener );

    using ::testing::_;

    EXPECT_CALL( mockListener, onPizzaKindNameChanged( _, _ ) ).Times( 0 );

    kind.rename( "Carbonara" );
}


/*=============================================================================*/ 

} // namespace Test
} // namespace Pizzario

/*=============================================================================*/ 
