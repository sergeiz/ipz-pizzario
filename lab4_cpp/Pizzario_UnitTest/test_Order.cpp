/** (C) 2015, Sergei Zaychenko, KNURE */

#include "model/order.hpp"
#include "model/pizza_kind.hpp"
#include "model/order_cart.hpp"
#include "model/cash_payment_plan.hpp"
#include "model/wire_transfer_payment_plan.hpp"

#include "gtest/gtest.h"

#include <memory>

/*=============================================================================*/ 

namespace Pizzario {
namespace Test {

/*=============================================================================*/


class OrderTests
    :    public testing::Test
{
protected:

    OrderTests ()
        :    m_carbonara( new PizzaKind( "Carbonara" ) )
        ,    m_milano( new PizzaKind( "Milano" ) ) 
    {
    }


    std::unique_ptr< Order > makeOrder ()
    {
        std::unique_ptr< OrderCart > cart( new OrderCart() );
        cart->addItem( makeSomeItem() );
        return makeOrder( std::move( cart ) );
    }


    std::unique_ptr< Order > makeOrder ( std::unique_ptr< OrderCart > _cart )
    {
        return std::unique_ptr< Order >( new Order(
                    1
                ,    std::move( _cart )
                ,    DeliveryContact( "", "", "" )
                ,    std::unique_ptr< PaymentPlan >( new CashPaymentPlan() )
            ) );
    }


    std::unique_ptr< Order > makeOrder ( std::unique_ptr< PaymentPlan > _plan )
    {
        std::unique_ptr< OrderCart > cart( new OrderCart() );
        cart->addItem( makeSomeItem() );
        return std::unique_ptr< Order >( new Order( 
                        1
                    ,    std::move( cart )
                    ,    DeliveryContact( "", "", "" )
                    ,    std::move( _plan  )
                ));
    }


    OrderItem makeSomeItem ( int quantity = 1 )
    {
        return OrderItem( * m_carbonara, PizzaSize::Small, Money( 1, 00 ), quantity );
    }


    OrderItem makeSomeOtherItem ( int quantity = 1 )
    {
        return OrderItem( * m_milano, PizzaSize::Medium, Money( 2, 00 ), quantity );
    }


    std::unique_ptr< PaymentPlan > makeWireTransferPlan ()
    {
        return std::unique_ptr< PaymentPlan >(
            new WireTransferPaymentPlan( "1234 5678 1234 5678", "Ivan Ivanov" )
        );
    }


    void cookPizza ( Pizza & _p )
    {
        _p.onCookingStarted();
        _p.onCookingFinished();
    }


    void cookAllPizzas ( Order & o )
    {
        int nPizzas = o.getPizzasCount();
        for ( int i = 0; i < nPizzas; i++ )
            cookPizza( o.getPizza( i ) );
    }

    std::unique_ptr< PizzaKind > m_carbonara;
    std::unique_ptr< PizzaKind > m_milano;
};


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, Constructor_ProjectsFieldsCorrectly ) 
{
    OrderCart * pCart = new OrderCart();
    DeliveryContact contact( "Sumskaya 1", "123-45-67", "Know 3 times" );
    PaymentPlan * pPlan = new CashPaymentPlan();

    std::unique_ptr< Order > order( 
        new Order(
                1
            ,    std::unique_ptr< OrderCart >( pCart )
            ,    contact
            ,    std::unique_ptr< PaymentPlan >( pPlan ) 
        )
    );

    ASSERT_EQ( order->getID(), 1 );
    ASSERT_EQ( & order->getCart(), pCart );
    ASSERT_EQ( order->getDeliveryContact(), contact );
    ASSERT_EQ( & order->getPaymentPlan(), pPlan );
}


/*-----------------------------------------------------------------------------*/



TEST_F ( OrderTests, Constructor_FromUnmodifiableCart_Forbidden ) 
{
    std::unique_ptr< OrderCart > cart( new OrderCart() );
    cart->addItem( makeSomeItem() );
    cart->checkout();

    ASSERT_THROW( makeOrder( std::move( cart ) ), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, Constructor_NoPizzasInitially_EvenWhenCartHasItems ) 
{
    std::unique_ptr< OrderCart > cart( new OrderCart() );
    cart->addItem( makeSomeItem() );
    
    auto order = makeOrder();

    ASSERT_EQ( order->getPizzasCount(), 0 );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, TotalCost_WithoutDiscount_MatchesCartCost ) 
{
    auto order = makeOrder();
    order->setDiscountPercentage( 0.0 );

    ASSERT_EQ( order->getTotalCost(), order->getCart().getCost() );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, TotalCost_WithDiscount_ReducesCartCost ) 
{
    auto order = makeOrder();
    order->setDiscountPercentage( 0.5 );

    ASSERT_EQ( order->getTotalCost() * 2, order->getCart().getCost() );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, TotalCost_WithFullDiscount_MakesOrderFree ) 
{
    auto order = makeOrder();
    order->setDiscountPercentage( 1.0 );

    ASSERT_EQ( order->getTotalCost(), Money() );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, Discount_Initially_Zero ) 
{
    auto order = makeOrder();
    ASSERT_EQ( order->getDiscountPercentage(), 0.0 );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, Discount_SetValid_ReadBack ) 
{
    auto order = makeOrder();
    order->setDiscountPercentage( 0.2 );
    ASSERT_EQ( order->getDiscountPercentage(), 0.2 );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, Discount_SetNegative_Forbidden ) 
{
    auto order = makeOrder();
    ASSERT_THROW( order->setDiscountPercentage( -0.01 ), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, Discount_SetMoreThan100Percent_Forbidden ) 
{
    auto order = makeOrder();
    ASSERT_THROW( order->setDiscountPercentage( 1.01 ), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, Pizzas_ForSingleItem_SingleMatchingPizza ) 
{
    std::unique_ptr< OrderCart > cart( new OrderCart() );
    cart->addItem( makeSomeItem() );
    auto order = makeOrder( std::move( cart ) );
    
    order->getCart().checkout();

    ASSERT_EQ( order->getPizzasCount(), 1 );
    ASSERT_EQ( & order->getPizza( 0 ).getKind(), & order->getCart().getItem( 0 ).getKind() );
    ASSERT_EQ( order->getPizza( 0 ).getSize(), order->getCart().getItem( 0 ).getSize() );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, Pizzas_ForDoubleItem_TwoMatchingPizzas ) 
{
    std::unique_ptr< OrderCart > cart( new OrderCart() );
    cart->addItem( makeSomeItem( 2 ) );
    auto order = makeOrder( std::move( cart ) );
    
    order->getCart().checkout();

    ASSERT_EQ( order->getPizzasCount(), 2 );
    ASSERT_EQ( & order->getPizza( 0 ).getKind(), & order->getCart().getItem( 0 ).getKind() );
    ASSERT_EQ( order->getPizza( 0 ).getSize(), order->getCart().getItem( 0 ).getSize() );
    ASSERT_EQ( & order->getPizza( 1 ).getKind(), & order->getCart().getItem( 0 ).getKind() );
    ASSERT_EQ( order->getPizza( 1 ).getSize(), order->getCart().getItem( 0 ).getSize() );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, Pizzas_ForTwoSingleItems_TwoDifferentMatchingPizzas ) 
{
    std::unique_ptr< OrderCart > cart( new OrderCart() );
    cart->addItem( makeSomeItem( 1 ) );
    cart->addItem( makeSomeOtherItem( 1 ) );
    auto order = makeOrder( std::move( cart ) );
    
    order->getCart().checkout();

    ASSERT_EQ( order->getPizzasCount(), 2 );
    ASSERT_EQ( & order->getPizza( 0 ).getKind(), & order->getCart().getItem( 0 ).getKind() );
    ASSERT_EQ( order->getPizza( 0 ).getSize(), order->getCart().getItem( 0 ).getSize() );
    ASSERT_EQ( & order->getPizza( 1 ).getKind(), & order->getCart().getItem( 1 ).getKind() );
    ASSERT_EQ( order->getPizza( 1 ).getSize(), order->getCart().getItem( 1 ).getSize() );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, Pizzas_ForOneSingleDoubleOther_ThreeMatchingPizzas ) 
{
    std::unique_ptr< OrderCart > cart( new OrderCart() );
    cart->addItem( makeSomeItem( 1 ) );
    cart->addItem( makeSomeOtherItem( 2 ) );
    auto order = makeOrder( std::move( cart ) );
    
    order->getCart().checkout();

    ASSERT_EQ( order->getPizzasCount(), 3 );
    ASSERT_EQ( & order->getPizza( 0 ).getKind(), & order->getCart().getItem( 0 ).getKind() );
    ASSERT_EQ( order->getPizza( 0 ).getSize(), order->getCart().getItem( 0 ).getSize() );
    ASSERT_EQ( & order->getPizza( 1 ).getKind(), & order->getCart().getItem( 1 ).getKind() );
    ASSERT_EQ( order->getPizza( 1 ).getSize(), order->getCart().getItem( 1 ).getSize() );
    ASSERT_EQ( & order->getPizza( 2 ).getKind(), & order->getCart().getItem( 1 ).getKind() );
    ASSERT_EQ( order->getPizza( 2 ).getSize(), order->getCart().getItem( 1 ).getSize() );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, Status_Initially_New ) 
{
    auto order = makeOrder();
    ASSERT_EQ( order->getStatus(), OrderStatus::New );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, Status_AfterCheckout_Registered ) 
{
    auto order = makeOrder();
    order->getCart().checkout();
    ASSERT_EQ( order->getStatus(), OrderStatus::Registered );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, Status_AfterSinglePizzaReady_Ready4Delivery ) 
{
    auto order = makeOrder();
    order->getCart().checkout();
    cookAllPizzas( * order );
    ASSERT_EQ( order->getStatus(), OrderStatus::Ready4Delivery );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, Status_AfterSinglePizzaStarted_Cooking ) 
{
    auto order = makeOrder();
    order->getCart().checkout();
    order->getPizza( 0 ).onCookingStarted();
    ASSERT_EQ( order->getStatus(), OrderStatus::Cooking );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, Status_AfterOneOfTwoStarted_Cooking ) 
{
    std::unique_ptr< OrderCart > cart( new OrderCart() );
    cart->addItem( makeSomeItem( 2 ) );
    auto order = makeOrder( std::move( cart ) );
    order->getCart().checkout();
    order->getPizza( 0 ).onCookingStarted();

    ASSERT_EQ( order->getStatus(), OrderStatus::Cooking );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, Status_AfterOneOfTwoReady_Cooking ) 
{
    std::unique_ptr< OrderCart > cart( new OrderCart() );
    cart->addItem( makeSomeItem( 2 ) );
    auto order = makeOrder( std::move( cart ) );
    order->getCart().checkout();
    cookPizza( order->getPizza( 0 ) );

    ASSERT_EQ( order->getStatus(), OrderStatus::Cooking );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, Status_AfterBothReady_Ready4Delivery ) 
{
    std::unique_ptr< OrderCart > cart( new OrderCart() );
    cart->addItem( makeSomeItem( 2 ) );
    auto order = makeOrder( std::move( cart ) );
    order->getCart().checkout();
    cookAllPizzas( * order );

    ASSERT_EQ( order->getStatus(), OrderStatus::Ready4Delivery );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, Status_AfterStartedDelivery_Delivering ) 
{
    auto order = makeOrder();
    order->getCart().checkout();
    cookAllPizzas( * order );
    order->onStartedDelivery();

    ASSERT_EQ( order->getStatus(), OrderStatus::Delivering );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, Status_AfterDelivery_Delivered ) 
{
    auto order = makeOrder();
    order->getCart().checkout();
    cookAllPizzas( * order );
    order->onStartedDelivery();
    order->getPaymentPlan().markPayed();
    order->onDelivered();

    ASSERT_EQ( order->getStatus(), OrderStatus::Delivered );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, StartOrFinishDelivery_WhenNewOrder_Forbidden ) 
{
    auto order = makeOrder();
    ASSERT_THROW( order->onStartedDelivery(), std::runtime_error );
    ASSERT_THROW( order->onDelivered(), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, StartOrFinishDelivery_WhenRegisteredOrder_Forbidden ) 
{
    auto order = makeOrder();
    order->getCart().checkout();
    ASSERT_THROW( order->onStartedDelivery(), std::runtime_error );
    ASSERT_THROW( order->onDelivered(), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, StartOrFinishDelivery_WhenStillCookingOrder_Forbidden ) 
{
    std::unique_ptr< OrderCart > cart( new OrderCart() );
    cart->addItem( makeSomeItem( 2 ) );
    auto order = makeOrder( std::move( cart ) );
    order->getCart().checkout();
    cookPizza( order->getPizza( 0 ) );

    ASSERT_THROW( order->onStartedDelivery(), std::runtime_error );
    ASSERT_THROW( order->onDelivered(), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, StartDelivery_WhenAlreadyStarted_Forbidden ) 
{
    auto order = makeOrder();
    order->getCart().checkout();
    cookAllPizzas( * order );
    order->onStartedDelivery();

    ASSERT_THROW( order->onStartedDelivery(), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, StartDelivery_WhenDelivered_Forbidden ) 
{
    auto order = makeOrder();
    order->getCart().checkout();
    cookAllPizzas( * order );
    order->onStartedDelivery();
    order->getPaymentPlan().markPayed();
    order->onDelivered();

    ASSERT_THROW( order->onStartedDelivery(), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, FinishDelivery_WhenDeliveryNotStarted_Forbidden ) 
{
    auto order = makeOrder();
    order->getCart().checkout();
    cookAllPizzas( * order );
    order->getPaymentPlan().markPayed();

    ASSERT_THROW( order->onDelivered(), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, FinishDelivery_WhenAlreadyDelivered_Forbidden ) 
{
    auto order = makeOrder();
    order->getCart().checkout();
    cookAllPizzas( * order );
    order->onStartedDelivery();
    order->getPaymentPlan().markPayed();
    order->onDelivered();

    ASSERT_THROW( order->onDelivered(), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, Cancel_NewOrder_Passes ) 
{
    auto order = makeOrder();
    order->cancel();
    ASSERT_EQ( order->getStatus(), OrderStatus::Cancelled );
    ASSERT_EQ( order->getPizzasCount(), 0 );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, Cancel_RegisteredOrder_Passes ) 
{
    std::unique_ptr< OrderCart > cart( new OrderCart() );
    cart->addItem( makeSomeItem( 2 ) );
    auto order = makeOrder( std::move( cart ) );
    order->getCart().checkout();
    
    order->cancel();

    ASSERT_EQ( order->getStatus(), OrderStatus::Cancelled );
    ASSERT_EQ( order->getPizzasCount(), 2 );
    ASSERT_EQ( order->getPizza( 0 ).getCookingStatus(), CookingStatus::Cancelled );
    ASSERT_EQ( order->getPizza( 1 ).getCookingStatus(), CookingStatus::Cancelled );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, Cancel_CookingOrder_PassesAndCancelsStartedPizzas ) 
{
    std::unique_ptr< OrderCart > cart( new OrderCart() );
    cart->addItem( makeSomeItem( 3 ) );
    auto order = makeOrder( std::move( cart ) );
    order->getCart().checkout();

    order->getPizza( 0 ).onCookingStarted();
    order->getPizza( 0 ).onCookingFinished();
    order->getPizza( 1 ).onCookingStarted();
    
    order->cancel();

    ASSERT_EQ( order->getStatus(), OrderStatus::Cancelled );
    ASSERT_EQ( order->getPizzasCount(), 3 );
    ASSERT_EQ( order->getPizza( 0 ).getCookingStatus(), CookingStatus::Finished );
    ASSERT_EQ( order->getPizza( 1 ).getCookingStatus(), CookingStatus::Cancelled );
    ASSERT_EQ( order->getPizza( 2 ).getCookingStatus(), CookingStatus::Cancelled );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, Cancel_ReadyOrder_PassesButNotTouchesPizzas ) 
{
    std::unique_ptr< OrderCart > cart( new OrderCart() );
    cart->addItem( makeSomeItem( 2 ) );
    auto order = makeOrder( std::move( cart ) );
    order->getCart().checkout();
    cookAllPizzas( * order );
    order->cancel();

    ASSERT_EQ( order->getStatus(), OrderStatus::Cancelled );
    ASSERT_EQ( order->getPizzasCount(), 2 );
    ASSERT_EQ( order->getPizza( 0 ).getCookingStatus(), CookingStatus::Finished );
    ASSERT_EQ( order->getPizza( 1 ).getCookingStatus(), CookingStatus::Finished );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, Cancel_OrderThatLeftKitchen_Forbidden ) 
{
    std::unique_ptr< OrderCart > cart( new OrderCart() );
    cart->addItem( makeSomeItem( 2 ) );
    auto order = makeOrder( std::move( cart ) );
    order->getCart().checkout();
    cookAllPizzas( * order );
                
    order->onStartedDelivery();

    ASSERT_THROW( order->cancel(), std::runtime_error );

    order->getPaymentPlan().markPayed();
    order->onDelivered();

    ASSERT_THROW( order->cancel(), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, Cancel_AlreadyCancelledOrder_Forbidden ) 
{
    auto order = makeOrder();
    order->cancel();

    ASSERT_THROW( order->cancel(), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, Payment_CashNotCollectedOnDelivery_Forbidden ) 
{
    auto order = makeOrder();
    order->getCart().checkout();
    cookAllPizzas( * order );
    order->onStartedDelivery();

    ASSERT_THROW( order->onDelivered(), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, Payment_WhenPrepaymentExpected_CannotCheckoutWithoutPayment ) 
{
    auto order = makeOrder( makeWireTransferPlan() );
    ASSERT_THROW( order->getCart().checkout(), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( OrderTests, Payment_WhenPrepaymentExpected_CheckoutSucceedsWhenPayed ) 
{
    auto order = makeOrder( makeWireTransferPlan()  );
    order->getPaymentPlan().markPayed();
    ASSERT_NO_THROW( order->getCart().checkout() );
}


/*=============================================================================*/ 

} // namespace Test
} // namespace Pizzario

/*=============================================================================*/ 
