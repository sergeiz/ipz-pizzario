/** (C) 2015, Sergei Zaychenko, KNURE */

#include "model/pizza.hpp"
#include "model/pizza_kind.hpp"

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include <memory>

/*=============================================================================*/ 

namespace Pizzario {
namespace Test {

/*=============================================================================*/


class PizzaTests
    :    public testing::Test
{
protected:

    PizzaTests ()
        :    m_kind( new PizzaKind( "Carbonara" ) )
    {
    }

    std::unique_ptr< PizzaKind > m_kind;
};


/*-----------------------------------------------------------------------------*/


class MockPizzaEventsListener : public Pizza::IPizzaEventsListener
{
public:
    MOCK_METHOD0( onPizzaCookingStarted,  void () );
    MOCK_METHOD0( onPizzaCookingFinished, void () );
};


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaTests, Constructor_ProjectsFields_Correctly ) 
{
    Pizza p( * m_kind, PizzaSize::Small );

    ASSERT_EQ( & p.getKind(), m_kind.get() );
    ASSERT_EQ( p.getSize(), PizzaSize::Small );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaTests, Constructor_Initially_NotStarted ) 
{
    Pizza p( * m_kind, PizzaSize::Small );
    ASSERT_EQ( p.getCookingStatus(), CookingStatus::NotStarted );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaTests, CookingStarted_FromInitialState_Passes ) 
{
    Pizza p( * m_kind, PizzaSize::Small );

    ASSERT_NO_THROW( p.onCookingStarted() );
    ASSERT_EQ( p.getCookingStatus(), CookingStatus::Started );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaTests, CookingStarted_AlreadyStarted_Fails ) 
{
    Pizza p( * m_kind, PizzaSize::Small );

    ASSERT_NO_THROW( p.onCookingStarted() );
    ASSERT_THROW( p.onCookingStarted(), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaTests, CookingStarted_AfterFinish_Fails ) 
{
    Pizza p( * m_kind, PizzaSize::Small );

    p.onCookingStarted();
    p.onCookingFinished();
       
    ASSERT_THROW( p.onCookingStarted(), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaTests, CookingStarted_AfterCancelled_Fails ) 
{
    Pizza p( * m_kind, PizzaSize::Small );

    p.onCookingStarted();
    p.onCookingCancelled();
       
    ASSERT_THROW( p.onCookingStarted(), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaTests, CookingStarted_SendsEvent ) 
{
    Pizza p( * m_kind, PizzaSize::Small );

    MockPizzaEventsListener mockListener;
    p.addEventsListener( mockListener );

    EXPECT_CALL( mockListener, onPizzaCookingStarted() ).Times( 1 );

    p.onCookingStarted();
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaTests, CookingCancelled_FromStarted_Passes ) 
{
    Pizza p( * m_kind, PizzaSize::Small );

    p.onCookingStarted();
    p.onCookingCancelled();
       
    ASSERT_EQ( p.getCookingStatus(), CookingStatus::Cancelled );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaTests, CookingCancelled_FromInitial_Passes ) 
{
    Pizza p( * m_kind, PizzaSize::Small );

    p.onCookingCancelled();
       
    ASSERT_EQ( p.getCookingStatus(), CookingStatus::Cancelled );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaTests, CookingCancelled_AlreadyCancelled_Fails ) 
{
    Pizza p( * m_kind, PizzaSize::Small );

    p.onCookingCancelled();
       
    ASSERT_THROW( p.onCookingCancelled(), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaTests, CookingCancelled_AfterFinish_Fails ) 
{
    Pizza p( * m_kind, PizzaSize::Small );

    p.onCookingStarted();
    p.onCookingFinished();
       
    ASSERT_THROW( p.onCookingCancelled(), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaTests, CookingFinished_FromInitial_Fails ) 
{
    Pizza p( * m_kind, PizzaSize::Small );

    ASSERT_THROW( p.onCookingFinished(), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaTests, CookingFinished_FromStarted_Passes ) 
{
    Pizza p( * m_kind, PizzaSize::Small );
    p.onCookingStarted();
    p.onCookingFinished();
    ASSERT_EQ( p.getCookingStatus(), CookingStatus::Finished );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaTests, CookingFinished_AlreadyFinished_Fails ) 
{
    Pizza p( * m_kind, PizzaSize::Small );
    p.onCookingStarted();
    p.onCookingFinished();
    ASSERT_THROW( p.onCookingFinished(), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaTests, CookingFinished_WasCancelled_Fails ) 
{
    Pizza p( * m_kind, PizzaSize::Small );
    p.onCookingStarted();
    p.onCookingCancelled();
    ASSERT_THROW( p.onCookingFinished(), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST_F ( PizzaTests, CookingFinished_SendsEvent ) 
{
    Pizza p( * m_kind, PizzaSize::Small );

    MockPizzaEventsListener mockListener;
    p.addEventsListener( mockListener );

    EXPECT_CALL( mockListener, onPizzaCookingStarted() ).Times( 1 );
    EXPECT_CALL( mockListener, onPizzaCookingFinished() ).Times( 1 );

    p.onCookingStarted();
    p.onCookingFinished();
}


/*-----------------------------------------------------------------------------*/

/*=============================================================================*/ 

} // namespace Test
} // namespace Pizzario

/*=============================================================================*/ 
