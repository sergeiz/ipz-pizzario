/** (C) 2015, Sergei Zaychenko, KNURE */

#include "model/cash_payment_plan.hpp"
#include "gtest/gtest.h"

#include <memory>

/*=============================================================================*/ 

namespace Pizzario {
namespace Test {

/*=============================================================================*/ 


TEST ( CashPaymentPlanTests, Payed_Initially_False ) 
{
    CashPaymentPlan p;
    ASSERT_FALSE( p.wasPayed() );
}


/*-----------------------------------------------------------------------------*/


TEST ( CashPaymentPlanTests, Payed_SetPayedToTrue_Confirmed ) 
{
    CashPaymentPlan p;
    p.markPayed();
    ASSERT_TRUE( p.wasPayed() );
}


/*-----------------------------------------------------------------------------*/


TEST ( CashPaymentPlanTests, Prepayment_NotExpected ) 
{
    CashPaymentPlan p;
    ASSERT_FALSE( p.expectPrepayment() );
}


/*-----------------------------------------------------------------------------*/


TEST ( CashPaymentPlanTests, Clone_Payed_StillPayed ) 
{
    CashPaymentPlan p1;
    p1.markPayed();

    std::unique_ptr< CashPaymentPlan > p2( p1.clone() );
    ASSERT_TRUE( p2->wasPayed() );
}


/*-----------------------------------------------------------------------------*/



TEST ( CashPaymentPlanTests, Clone_Unpayed_StillUnpayed ) 
{
    CashPaymentPlan p1;
    std::unique_ptr< CashPaymentPlan > p2( p1.clone() );
    ASSERT_FALSE( p2->wasPayed() );
}


/*=============================================================================*/ 

} // namespace Test
} // namespace Pizzario

/*=============================================================================*/ 
