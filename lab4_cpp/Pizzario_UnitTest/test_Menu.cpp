/** (C) 2015, Sergei Zaychenko, KNURE */

#include "model/menu.hpp"
#include "gtest/gtest.h"
#include "gmock/gmock.h" 

/*=============================================================================*/ 

namespace Pizzario {
namespace Test {

/*=============================================================================*/ 


TEST ( MenuTest, Kinds_Initially_Empty ) 
{
    Menu menu;
    ASSERT_EQ( menu.getPizzaKindsCount(), 0 );
}


/*-----------------------------------------------------------------------------*/


TEST ( MenuTest, Kinds_AddOne_FindIt ) 
{
    Menu menu;
    PizzaKind * pCarbonara = new PizzaKind( "Carbonara" );
    menu.addPizzaKind( std::unique_ptr< PizzaKind >( pCarbonara ) );

    ASSERT_EQ( menu.getPizzaKindsCount(), 1 );
    ASSERT_EQ( menu.findPizzaKind( "Carbonara" ), pCarbonara );
}


/*-----------------------------------------------------------------------------*/


TEST ( MenuTest, Kinds_AddTwo_FindBoth ) 
{
    Menu menu;
    
    PizzaKind * pCarbonara = new PizzaKind( "Carbonara" );
    menu.addPizzaKind( std::unique_ptr< PizzaKind >( pCarbonara ) );

    PizzaKind * pMilano = new PizzaKind( "Milano" );
    menu.addPizzaKind( std::unique_ptr< PizzaKind >( pMilano ) );

    ASSERT_EQ( menu.getPizzaKindsCount(), 2 );
    ASSERT_EQ( menu.findPizzaKind( "Carbonara" ), pCarbonara );
    ASSERT_EQ( menu.findPizzaKind( "Milano" ), pMilano );
}


/*-----------------------------------------------------------------------------*/


TEST ( MenuTest, Kinds_FindMissingInEmpty_ReturnsNull ) 
{
    Menu menu;
    ASSERT_EQ( menu.findPizzaKind( "Carbonara" ), nullptr );
}


/*-----------------------------------------------------------------------------*/


TEST ( MenuTest, Kinds_FindMissingInNonEmpty_ReturnsNull ) 
{
    Menu menu;
    PizzaKind * pCarbonara = new PizzaKind( "Carbonara" );
    menu.addPizzaKind( std::unique_ptr< PizzaKind >( pCarbonara ) );

    ASSERT_EQ( menu.findPizzaKind( "Milano" ), nullptr );
}


/*-----------------------------------------------------------------------------*/


TEST ( MenuTest, Kinds_AddDuplicate_Fails ) 
{
    Menu menu;
    PizzaKind * pCarbonara = new PizzaKind( "Carbonara" );
    menu.addPizzaKind( std::unique_ptr< PizzaKind >( pCarbonara ) );

    ASSERT_THROW( 
            menu.addPizzaKind( std::unique_ptr< PizzaKind >( new PizzaKind( "Carbonara" ) ) )
        ,    std::runtime_error 
    );
}


/*-----------------------------------------------------------------------------*/


TEST ( MenuTest, Kinds_DeleteSingle_MakesEmpty ) 
{
    Menu menu;
    menu.addPizzaKind( std::unique_ptr< PizzaKind >( new PizzaKind( "Carbonara" ) ) );
    menu.deletePizzaKind( "Carbonara" );

    ASSERT_EQ( menu.getPizzaKindsCount(), 0 );
}


/*-----------------------------------------------------------------------------*/


TEST ( MenuTest, Kinds_DeleteOneOfTwo_LeavesOther ) 
{
    Menu menu;
    menu.addPizzaKind( std::unique_ptr< PizzaKind >( new PizzaKind( "Carbonara" ) ) );
    menu.addPizzaKind( std::unique_ptr< PizzaKind >( new PizzaKind( "Milano" ) ) );
    menu.deletePizzaKind( "Carbonara" );

    ASSERT_EQ( menu.getPizzaKindsCount(), 1 );
    ASSERT_EQ( * menu.pizzaKindNamesBegin(), "Milano" );
}


/*-----------------------------------------------------------------------------*/


TEST ( MenuTest, Kinds_DeleteMissingInEmpty_Forbidden ) 
{
    Menu menu;
    ASSERT_THROW( menu.deletePizzaKind( "Carbonara" ), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST ( MenuTest, Kinds_DeleteMissingInNonEmpty_Forbidden ) 
{
    Menu menu;
    menu.addPizzaKind( std::unique_ptr< PizzaKind >( new PizzaKind( "Carbonara" ) ) );
    ASSERT_THROW( menu.deletePizzaKind( "Milano" ), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST ( MenuTest, Rename_TheOnly_AccessibleUnderNewName ) 
{
    Menu menu;
    
    PizzaKind * pCarbonara = new PizzaKind( "carbonara" );
    menu.addPizzaKind( std::unique_ptr< PizzaKind >( pCarbonara ) );

    pCarbonara->rename( "Carbonara" );

    ASSERT_EQ( menu.getPizzaKindsCount(), 1 );
    ASSERT_EQ( menu.findPizzaKind( "Carbonara" ), pCarbonara );
}


/*-----------------------------------------------------------------------------*/


TEST ( MenuTest, Rename_OneOfTwo_AccessibleUnderNewName_SecondUntouched ) 
{
    Menu menu;

     PizzaKind * pCarbonara = new PizzaKind( "carbonara" );
    menu.addPizzaKind( std::unique_ptr< PizzaKind >( pCarbonara ) );

    PizzaKind * pMilano = new PizzaKind( "Milano" );
    menu.addPizzaKind( std::unique_ptr< PizzaKind >( pMilano ) );

    pCarbonara->rename( "Carbonara" );

    ASSERT_EQ( menu.getPizzaKindsCount(), 2 );
    ASSERT_EQ( menu.findPizzaKind( "Carbonara" ), pCarbonara );
    ASSERT_EQ( menu.findPizzaKind( "Milano" ), pMilano );
}


/*-----------------------------------------------------------------------------*/


TEST ( MenuTest, Rename_ToExisting_Forbidden ) 
{
    Menu menu;
    
    PizzaKind * pCarbonara = new PizzaKind( "Carbonara" );
    menu.addPizzaKind( std::unique_ptr< PizzaKind >( pCarbonara ) );

    PizzaKind * pMilano = new PizzaKind( "Milano" );
    menu.addPizzaKind( std::unique_ptr< PizzaKind >( pMilano ) );

    ASSERT_THROW( pCarbonara->rename( "Milano" ), std::runtime_error );
}


/*-----------------------------------------------------------------------------*/


TEST ( MenuTest, OrderItem_Test_ExpectedResult ) 
{
    PizzaKind * pCarbonara = new PizzaKind( "Carbonara" );
    pCarbonara->updatePrice( PizzaSize::Small, Money( 3, 00 ) );

    Menu menu;
    menu.addPizzaKind( std::unique_ptr< PizzaKind >( pCarbonara ) );

    OrderItem item = menu.makeOrderItem( "Carbonara", PizzaSize::Small, 1 );

    ASSERT_EQ( & item.getKind(), pCarbonara );
    ASSERT_EQ( item.getQuantity(), 1 );
    ASSERT_EQ( item.getSize(), PizzaSize::Small );
    ASSERT_EQ( item.getFixedPrice(), Money( 3, 00 ) );
}


/*-----------------------------------------------------------------------------*/


TEST ( MenuTest, OrderItem_UnknownType_Fails ) 
{
    Menu menu;

    ASSERT_THROW(
            menu.makeOrderItem( "Carbonara", PizzaSize::Small, 1 )
        ,    std::runtime_error
    );
}


/*-----------------------------------------------------------------------------*/





/*=============================================================================*/ 

} // namespace Test
} // namespace Pizzario

/*=============================================================================*/ 
