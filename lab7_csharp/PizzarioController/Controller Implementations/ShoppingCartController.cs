﻿using System;
using System.Collections.Generic;
using System.Linq;

using Pizzario.Controller.ViewModels;
using Pizzario.Orm;


namespace Pizzario.Controller
{
    class ShoppingCartController : BasicController, IShoppingCartController
    {
        public ShoppingCartController ()
        {
            this.cartRepository  = RepositoryFactory.MakeShoppingCartRepository( GetDBContext() );
            this.menuRepository  = RepositoryFactory.MakeMenuRepository( GetDBContext() );
            this.orderRepository = RepositoryFactory.MakeOrderRepository( GetDBContext() );
        }


        public CartView ViewCart ( int cartID )
        {
            Model.ShoppingCart cart = ResolveCart( cartID );
            return ViewModelsFactory.BuildCartView( cart );
        }


        public int NewCart ()
        {
            Model.ShoppingCart cart = new Model.ShoppingCart();
            cartRepository.Add( cart );
            cartRepository.Commit();
            return cart.ShoppingCartId;
        }


        public int CloneCartOfPastOrder ( int orderId )
        {
            Model.Order order = ResolveOrder( orderId );
            
            Model.ShoppingCart cart = new Model.ShoppingCart();
            foreach ( Model.OrderItem item in cart.Items )
                cart.AddItem( new Model.OrderItem( item ) );
            
            cartRepository.Add( cart );
            cartRepository.Commit();
            return cart.ShoppingCartId;
        }


        public void SetCartItem ( int cartID, string pizzaKindName, string pizzaSize, int quantity )
        {
            Model.ShoppingCart cart = ResolveCart( cartID );
            Model.PizzaKind kind = ResolvePizzaKind( pizzaKindName );
            Model.PizzaSize size = ControllerUtils.DecodePizzaSize( pizzaSize );

            int itemIndex = cart.FindItemIndex( kind, size );
            if ( itemIndex == -1 )
                cart.AddItem( kind.MakeOrderItem( size, quantity ) );

            else
                cart.Items[ itemIndex ].SetQuantity( quantity );

            cartRepository.Commit();
        }


        public void RemoveCartItem ( int cartID, string pizzaKindName, string pizzaSize )
        {
            Model.ShoppingCart cart = ResolveCart( cartID );
            Model.PizzaKind kind = ResolvePizzaKind( pizzaKindName );
            Model.PizzaSize size = ControllerUtils.DecodePizzaSize( pizzaSize );

            int itemIndex = cart.FindItemIndex( kind, size );
            if ( itemIndex == -1 )
                throw new Exception( "Item not found in cart" );

            if ( ! cart.Modifiable )
                throw new Exception( "Unmodifiable cart" );

            cartRepository.RemoveItem( cart.Items[ itemIndex ] );
            cartRepository.Commit();
        }


        public void ClearCart ( int cartID )
        {
            Model.ShoppingCart cart = ResolveCart( cartID );
            if ( !cart.Modifiable )
                throw new Exception( "Unmodifiable cart" );

            cartRepository.RemoveItems( cart.Items );
            cartRepository.Commit();
        }


        public void LockCart ( int cartID )
        {
            Model.ShoppingCart cart = ResolveCart( cartID );
            cart.Lock();
            cartRepository.Commit();
        }


        public void DeleteCart ( int cartID )
        {
            Model.ShoppingCart cart = ResolveCart( cartID );
            cartRepository.Delete( cart );
            cartRepository.Commit();
        }


        private Model.ShoppingCart ResolveCart ( int cartID )
        {
            return ControllerUtils.ResolveObjectById( cartRepository, cartID );
        }


        private Model.Order ResolveOrder ( int orderID )
        {
            return ControllerUtils.ResolveObjectById( orderRepository, orderID );
        }
        

        private Model.PizzaKind ResolvePizzaKind ( string kindName )
        {
            Model.PizzaKind pizzaKind = menuRepository.FindByName( kindName );
            if ( pizzaKind == null )
                throw new Exception( "Pizza kind '" + kindName + "' is unknown" );
            return pizzaKind;
        }


        private IShoppingCartRepository cartRepository;
        private IMenuRepository menuRepository;
        private IOrderRepository orderRepository;
    }
}
