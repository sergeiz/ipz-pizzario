﻿using System;
using System.Linq;
using System.Collections.Generic;

using Pizzario.Controller.Communications;
using Pizzario.Controller.ViewModels;
using Pizzario.Orm;
using Pizzario.Infrastructure;


namespace Pizzario.Controller
{
    class OrderController : BasicController, IOrderController
    {
        public OrderController ()
        {
            this.orderRepository     = RepositoryFactory.MakeOrderRepository( GetDBContext() );
            this.cartRepository      = RepositoryFactory.MakeShoppingCartRepository( GetDBContext() );
            this.pizzaRepository     = RepositoryFactory.MakePizzaRepository( GetDBContext() );
            this.refundingRepository = RepositoryFactory.MakeRefundingRequestRepository( GetDBContext() );
            this.customerRepository  = RepositoryFactory.MakeCustomerRepository( GetDBContext() );

            this.emailComposer = new EmailComposer( InfrastructureFactory.MakeEmailAgent() );
            this.smsComposer   = new SmsComposer( InfrastructureFactory.MakeSmsAgent() );
            this.paymentAgent  = InfrastructureFactory.MakePaymentAgent();
        }


        public OrderView ViewOrderContent ( int orderID )
        {
            Model.Order order = ResolveOrder( orderID );
            return ViewModelsFactory.BuildOrderView( order );
        }


        public OrderProgressView ViewOrderProgress ( int orderID )
        {
            Model.Order order = ResolveOrder( orderID );
            return ViewModelsFactory.BuildOrderProgressView( 
                        order, 
                        pizzaRepository.SearchPizzasByOrderID( orderID ) 
                   );
        }


        public ICollection< OrderBriefView > ViewAllOrders ()
        {
            var query = orderRepository.LoadAll();
            return ViewModelsFactory.BuildViewModels( query, ViewModelsFactory.BuildOrderBriefView );
        }


        public ICollection< OrderBriefView > ViewOrdersByStatus ( string statusAsString )
        {
            Model.OrderStatus status = ControllerUtils.DecodeOrderStatus( statusAsString );
            var query = orderRepository.LoadOrdersOfStatus( status );
            return ViewModelsFactory.BuildViewModels( query, ViewModelsFactory.BuildOrderBriefView );
        }


        public OrdersByPizzaKindView BrowseOrderStatsByPizzaKinds ()
        {
            var reportLines = new List< OrdersByPizzaKindView.ReportLine >();

            var allItemsQuery = orderRepository.LoadAllItems();
            var stats = Model.Services.OrderStatsService.CalculateStatsByPizzaKinds( allItemsQuery );

            foreach ( var item in stats )
                reportLines.Add( new OrdersByPizzaKindView.ReportLine( item.KindName, item.TotalQuantity ) );

            return new OrdersByPizzaKindView 
            { 
                ReportLines = reportLines 
            };
        }


        public OrdersByIngredientView BrowseOrderStatsByIngredients ()
        {
            var reportLines = new List< OrdersByIngredientView.ReportLine >();

            var allItemsQuery = orderRepository.LoadAllItems();
            var stats = Model.Services.OrderStatsService.CalculateStatsByIngredients( allItemsQuery );
            foreach ( var entry in stats )
                reportLines.Add( new OrdersByIngredientView.ReportLine( entry.IngredientName, entry.TotalWeight ) );

            return new OrdersByIngredientView
            {
                ReportLines = reportLines
            };
        }


        public int NewVisitorOrder ( int cartID, 
                                     string email, 
                                     string address, 
                                     string phone )
        {
            Model.ShoppingCart cart = ResolveCart( cartID );

            Model.CustomerContact contact = new Model.CustomerContact();
            contact.Email   = email;
            contact.Address = address;
            contact.Phone   = phone;

            Model.Order order = new Model.Order( cart, contact );
            orderRepository.Add( order );
            orderRepository.Commit();

            smsComposer.SendOrderRegistered( order );
            emailComposer.SendOrderRegistered( order );

            return order.OrderId;
        }


        public int NewCustomerOrder ( int cartID, int customerID )
        {
            Model.ShoppingCart cart = ResolveCart( cartID );
            Model.Customer customer = ResolveCustomer( customerID );

            Model.CustomerContact contact = new Model.CustomerContact();
            contact.Email    = customer.Email;
            contact.Address = customer.Address;
            contact.Phone   = customer.Phone;

            Model.Order order = new Model.Order( cart, contact );
            if ( ! customer.CurrentDiscount.IsZero() )
                order.SetDiscount( new Model.Discount( customer.CurrentDiscount.Value ) );

            orderRepository.Add( order );
            orderRepository.Commit();

            customer.AddPastOrder( order );
            customerRepository.Commit();

            smsComposer.SendOrderRegistered( order );
            emailComposer.SendOrderRegistered( order );

            return order.OrderId;
        }


        public void DefineCashPaymentPlan ( int orderID )
        {
            Model.Order order = ResolveOrder( orderID );
            order.DefinePaymentPlan( new Model.CashPaymentPlan() );
            orderRepository.Commit();

            ScheduleOrderCooking( order );
        }


        public void DefineWireTransferPaymentPlan ( int orderID,
                                                    string cardCode, 
                                                    string cardHolder )
        {
            Model.Order order = ResolveOrder( orderID );

            int transactionId;
            if ( ! paymentAgent.makePaymentTransaction( order.TotalCost, cardCode, cardHolder, out transactionId ) )
                throw new Exception( "Wireless payment failed" );

            var plan = new Model.WireTransferPaymentPlan( cardCode, cardHolder );
            plan.SetPaymentTransactionId( transactionId );

            order.DefinePaymentPlan( plan );
            orderRepository.Commit();

            ScheduleOrderCooking( order );
        }


        private void ScheduleOrderCooking ( Model.Order order )
        {
            foreach ( Model.OrderItem item in order.Items )
                for ( int i = 0; i < item.Quantity; i++ )
                    pizzaRepository.Add( new Model.Pizza( order, item.Kind, item.Size ) );
            pizzaRepository.Commit();

            emailComposer.SendOrderScheduled( order );
        }


        public void ConfirmPayment ( int orderID )
        {
            Model.Order order = ResolveOrder( orderID );
            order.PayPlan.MarkPayed();
            orderRepository.Commit();
        }


        public void ConfirmDelivery ( int orderID, decimal collectedPaymentAmount )
        {
            Model.Order order = ResolveOrder( orderID );
            order.OnDelivered( collectedPaymentAmount  );
            orderRepository.Commit();

            emailComposer.SendOrderDelivered( order );
        }


        public void DeliveryStarted ( int orderID )
        {
            Model.Order order = ResolveOrder( orderID );
            order.OnStartedDelivery();
            orderRepository.Commit();
        }


        public int CancelOrder ( int orderID )
        {
            Model.Order order = ResolveOrder( orderID );
            Model.OrderStatus previousStatus = order.Status;

            order.Cancel();
            orderRepository.Commit();

            var pizzasOfOrder = pizzaRepository.SearchPizzasByOrderID( orderID );
            foreach ( Model.Pizza p in pizzasOfOrder )
                p.OnCookingCancelled();
            pizzaRepository.Commit();

            int refundingID = -1;
            if ( order.MayBeRefunded() )
            {
                Model.RefundingRequest request = new Model.RefundingRequest( order );
                refundingRepository.Add( request );
                refundingRepository.Commit();
                refundingID = request.RequestId;
            }

            smsComposer.SendOrderCancelled( order );
            emailComposer.SendOrderCancelled( order, previousStatus );

            return refundingID;
        }


        private Model.ShoppingCart ResolveCart ( int cartID )
        {
            return ControllerUtils.ResolveObjectById( cartRepository, cartID );
        }


        private Model.Customer ResolveCustomer ( int customerID )
        {
            return ControllerUtils.ResolveObjectById( customerRepository, customerID );
        }


        private Model.Order ResolveOrder ( int orderID )
        {
            return ControllerUtils.ResolveObjectById( orderRepository, orderID );
        }


        private IOrderRepository orderRepository;
        private IShoppingCartRepository cartRepository;
        private IPizzaRepository pizzaRepository;
        private IRefundingRequestRepository refundingRepository;
        private ICustomerRepository customerRepository;

        private EmailComposer emailComposer;
        private SmsComposer smsComposer;
        private IPaymentAgent paymentAgent;
    }
}
