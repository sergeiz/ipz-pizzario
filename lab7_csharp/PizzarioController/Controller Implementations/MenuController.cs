﻿using System;
using System.Collections.Generic;
using System.Linq;

using Pizzario.Controller.ViewModels;
using Pizzario.Orm;

namespace Pizzario.Controller
{
    class MenuController : BasicController, IMenuController
    {
        public MenuController ()
        {
            this.menuRepository = RepositoryFactory.MakeMenuRepository( GetDBContext() );
        }

        
        public ICollection< PizzaKindBriefView > ViewVisiblePizzaKinds ()
        {
            var query = menuRepository.LoadAllVisible();
            return ViewModelsFactory.BuildViewModels( query, ViewModelsFactory.BuildPizzaKindBriefView );
        }


        public ICollection< PizzaKindBriefView > ViewHiddenPizzaKinds ()
        {
            var query = menuRepository.LoadAllHidden();
            return ViewModelsFactory.BuildViewModels( query, ViewModelsFactory.BuildPizzaKindBriefView );
        }


        public ViewModels.PizzaKindView ViewPizzaKindDetails ( string kindName )
        {
            Model.PizzaKind pizzaKind = ResolvePizzaKind( kindName );
            return ViewModelsFactory.BuildPizzaKindView( pizzaKind );
        }


        public ICollection < PizzaKindBriefView > SuggestPizzaKindsByIngredients ( string[] includeIngredients,
                                                                                   string[] excludeIngredients )
        {
            var query = menuRepository.LoadAllVisible().AsEnumerable();

            if ( includeIngredients != null && includeIngredients.Length > 0 )
                query = query.Where( k => k.Recipe.HasAllOfIngredients( includeIngredients ) );

            if ( excludeIngredients != null && excludeIngredients.Length > 0 )
                query = query.Where( k => k.Recipe.HasNoneOfIngredients( excludeIngredients ) );

            return ViewModelsFactory.BuildViewModels( query.AsQueryable(), ViewModelsFactory.BuildPizzaKindBriefView );
        }


        public ICollection< PizzaKindBriefView > ViewSimilarPizzaKinds ( string kindName )
        {
            Model.PizzaKind pizzaKind = ResolvePizzaKind( kindName );

            var query = menuRepository.LoadAllVisible()
                .Where( k => k.Name != kindName )
                .ToList()
                .Where( k => k.Recipe.IsSimilarTo( pizzaKind.Recipe ) )
                .AsQueryable();

            return ViewModelsFactory.BuildViewModels( query, ViewModelsFactory.BuildPizzaKindBriefView );
        }


        public ViewModels.PizzaRecipeView ViewPizzaRecipe ( string kindName )
        {
            Model.PizzaKind pizzaKind = ResolvePizzaKind( kindName );

            var sizeRecipes = new List< PizzaRecipeView.RecipeOfSize >();

            foreach ( Model.PizzaSize size in Enum.GetValues( typeof( Model.PizzaSize ) ) )
            {
                var ingredientsWeight = new Dictionary< string, int >();
                foreach ( Model.Ingredient ingredient in pizzaKind.Recipe.Ingredients )
                    ingredientsWeight[ ingredient.Name ] = ingredient.GetWeight( size );

                var recipeOfSize = new PizzaRecipeView.RecipeOfSize
                {
                    Size = size.ToString(),
                    TotalWeight = pizzaKind.Recipe.GetTotalWeight( size ),
                    IngredientsWeight = ingredientsWeight
                };

                sizeRecipes.Add( recipeOfSize );
            }

            return new ViewModels.PizzaRecipeView
            {
                Name = pizzaKind.Name,
                SizeRecipes = sizeRecipes
            };
        }


        public PizzaRatingsView ViewPizzaKindsByPopularity ()
        {
            var query = menuRepository.LoadAllVisible()
                           .AsEnumerable()
                           .OrderByDescending( p => p.AverageRating );

            var lines = new List< PizzaRatingsView.RatingLine >();
            
            foreach ( var pizzaKind in query )
            {
                lines.Add(
                    new PizzaRatingsView.RatingLine 
                    {
                        PizzaKindName = pizzaKind.Name,
                        AverageRating = pizzaKind.AverageRating,
                        Votes         = pizzaKind.Votes
                    }
                );
            }

            return new PizzaRatingsView{ Lines = lines };

            // return ViewModelsFactory.BuildViewModels( query.AsQueryable(), ViewModelsFactory.BuildPizzaKindBriefView );
        }


        public void CreateNewPizzaKind ( string name, string description, string imageUrl )
        {
            if ( menuRepository.FindByName( name ) != null )
                throw new Exception( "New name is already in use" );

            Model.PizzaKind pizzaKind = new Model.PizzaKind( name );
            pizzaKind.Description = description;
            pizzaKind.ImageUrl    = imageUrl;

            menuRepository.Add( pizzaKind );
            menuRepository.Commit();
        }


        public void Rename ( string kindName, string newName )
        {
            if ( menuRepository.FindByName( newName ) != null )
                throw new Exception( "New name is already in use" );

            Model.PizzaKind pizzaKind = ResolvePizzaKind( kindName );
            pizzaKind.Name = newName;
            menuRepository.Commit();
        }


        public void Rate ( string kindName, int rating )
        {
            Model.PizzaKind pizzaKind = ResolvePizzaKind( kindName );
            pizzaKind.Rate( rating );
            menuRepository.Commit();
        }


        public void UpdateDescription ( string kindName, string description )
        {
            Model.PizzaKind pizzaKind = ResolvePizzaKind( kindName );
            pizzaKind.Description = description;
            menuRepository.Commit();
        }


        public void UpdateImageUrl ( string kindName, string url )
        {
            Model.PizzaKind pizzaKind = ResolvePizzaKind( kindName );
            pizzaKind.ImageUrl = url;
            menuRepository.Commit();
        }


        public void HidePizzaFromCustomers ( string kindName )
        {
            Model.PizzaKind pizzaKind = ResolvePizzaKind( kindName );
            pizzaKind.Hidden = true;
            menuRepository.Commit();
        }


        public void EnableShowingPizzaToCustomers ( string kindName )
        {
            Model.PizzaKind pizzaKind = ResolvePizzaKind( kindName );
            pizzaKind.Hidden = false;
            menuRepository.Commit();
        }
        

        public void UpdatePrice ( string kindName, string size, decimal price )
        {
            Model.PizzaKind pizzaKind = ResolvePizzaKind( kindName );
            pizzaKind.UpdatePrice( ControllerUtils.DecodePizzaSize( size ), price );
            menuRepository.Commit();
        }


        public void SetIngredient ( string kindName, string ingredientName, int smallestWeight )
        {
            Model.PizzaKind pizzaKind = ResolvePizzaKind( kindName );
            Model.PizzaRecipe recipe = pizzaKind.Recipe;

            Model.Ingredient ingredient;
            if ( recipe.HasIngredient( ingredientName ) )
                ingredient = pizzaKind.Recipe.FindIngredient( ingredientName );

            else
            {
                ingredient = new Model.Ingredient( ingredientName );
                pizzaKind.Recipe.AddIngredient( ingredient );
            }

            ingredient.DefineWeights( smallestWeight );

            menuRepository.Commit();
        }


        public void RemoveIngredient ( string kindName, string ingredientName )
        {
            Model.PizzaKind pizzaKind = ResolvePizzaKind( kindName );
            pizzaKind.Recipe.RemoveIngredient( ingredientName );
            menuRepository.Commit();
        }


        private Model.PizzaKind ResolvePizzaKind ( string kindName )
        {
            Model.PizzaKind pizzaKind = menuRepository.FindByName( kindName );
            if ( pizzaKind == null )
                throw new Exception( "Pizza kind '" + kindName + "' is unknown" );
            return pizzaKind;
        }


        private IMenuRepository menuRepository;
    }
}
