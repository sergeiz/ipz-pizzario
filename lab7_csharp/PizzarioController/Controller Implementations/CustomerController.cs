﻿using System;
using System.Collections.Generic;
using System.Linq;

using Pizzario.Controller.ViewModels;
using Pizzario.Orm;


namespace Pizzario.Controller
{
    class CustomerController : BasicController, ICustomerController
    {
        public CustomerController ()
        {
            this.customerRepository = RepositoryFactory.MakeCustomerRepository( GetDBContext() );
            this.orderRepository    = RepositoryFactory.MakeOrderRepository( GetDBContext() );
        }


        public ICollection< CustomerView > ViewAllCustomers ()
        {
            var query = customerRepository.LoadAll();
            return ViewModelsFactory.BuildViewModels( query, ViewModelsFactory.BuildCustomerView );
        }


        public ICollection< OrderBriefView > FindPastOrders ( int customerId )
        {
            Model.Customer customer = ResolveCustomer( customerId );
            var query = customer.PastOrders.AsQueryable();
            return ViewModelsFactory.BuildViewModels( query, ViewModelsFactory.BuildOrderBriefView );
        }


        public int NewCustomer ( string fullname, string email, string address, string phone )
        {
            if ( customerRepository.FindByEmail( email ) != null )
                throw new Exception( "Email is already in use" );

            var customer = new Model.Customer( fullname, email, address, phone );
            customerRepository.Add( customer );
            customerRepository.Commit();

            return customer.CustomerId;
        }


        public CustomerView FindCustomer ( string email )
        {
            var customer = customerRepository.FindByEmail( email );
            if ( customer == null )
                throw new Exception( "Customer not found" );

            return ViewModelsFactory.BuildCustomerView( customer );
        }


        public void ChangeName ( int customerId, string newFullName )
        {
            Model.Customer customer = ResolveCustomer( customerId );
            customer.SetFullname( newFullName );
            customerRepository.Commit();
        }


        public void ChangeEmail ( int customerId, string newEmail )
        {
            if ( customerRepository.FindByEmail( newEmail ) != null )
                throw new Exception( "New email is already in use" );

            Model.Customer customer = ResolveCustomer( customerId );
            customer.SetEmail( newEmail );
            customerRepository.Commit();
        }


        public void ChangeAddress ( int customerId, string newAddress )
        {
            Model.Customer customer = ResolveCustomer( customerId );
            customer.SetAddress( newAddress );
            customerRepository.Commit();
        }


        public void ChangePhone ( int customerId, string newPhone )
        {
            Model.Customer customer = ResolveCustomer( customerId );
            customer.SetPhone( newPhone );
            customerRepository.Commit();
        }


        public void SetDiscount ( int customerId, double discount )
        {
            var newDiscount = new Model.Discount( discount );
            Model.Customer customer = ResolveCustomer( customerId );
            customer.SetDiscount( newDiscount );
            customerRepository.Commit();
        }


        public void AttachOrder ( int customerId, int orderId )
        {
            Model.Customer customer = ResolveCustomer( customerId );
            Model.Order order = ResolveOrder( orderId );
            
            if ( customer.HasPastOrder( order ) )
                throw new Exception( "Customer already has this order" );

            customer.AddPastOrder( order );
            customerRepository.Commit();
        }


        private Model.Customer ResolveCustomer ( int customerId )
        {
            return ControllerUtils.ResolveObjectById( customerRepository, customerId );
        }


        private Model.Order ResolveOrder ( int orderId )
        {
            return ControllerUtils.ResolveObjectById( orderRepository, orderId );
        }


        private ICustomerRepository customerRepository;
        private IOrderRepository orderRepository;
    }
}
