﻿using System;
using System.Linq;
using System.Collections.Generic;

using Pizzario.Controller.ViewModels;
using Pizzario.Orm;


namespace Pizzario.Controller
{
    class PizzaController : BasicController, IPizzaController
    {
        public PizzaController ()
        {
            this.pizzaRepository = RepositoryFactory.MakePizzaRepository( GetDBContext() );
            this.orderRepository = RepositoryFactory.MakeOrderRepository( GetDBContext() );
        }


        public ICollection< PizzaView > ListPizzasBeingCooked ()
        {
            var query = pizzaRepository.SearchPizzasOfStatus( Model.CookingStatus.Started );
            return ViewModelsFactory.BuildViewModels( query, ViewModelsFactory.BuildPizzaView );
        }


        public ICollection< PizzaView > ListPizzasWaitingForCooking ()
        {
            var query = pizzaRepository.SearchPizzasOfStatus( Model.CookingStatus.NotStarted );
            return ViewModelsFactory.BuildViewModels( query, ViewModelsFactory.BuildPizzaView );
        }


        public void CookingStarted ( int pizzaID )
        {
            Model.Pizza pizza = ResolvePizza( pizzaID );
            pizza.OnCookingStarted();
            pizzaRepository.Commit();

            Model.Order order = ResolveOrder( pizza.RelatedOrder.OrderId );
            order.OnStartedCookingOneOfPizzas();
            orderRepository.Commit();
        }


        public void CookingFinished ( int pizzaID )
        {
            Model.Pizza pizza = ResolvePizza( pizzaID );
            pizza.OnCookingFinished();
            pizzaRepository.Commit();

            Model.Order order = ResolveOrder( pizza.RelatedOrder.OrderId );
            order.OnFinishedCookingOneOfPizzas();
            orderRepository.Commit();
        }


        private Model.Pizza ResolvePizza ( int pizzaID )
        {
            return ControllerUtils.ResolveObjectById( pizzaRepository, pizzaID );
        }


        private Model.Order ResolveOrder ( int orderID )
        {
            return ControllerUtils.ResolveObjectById( orderRepository, orderID );
        }


        private IPizzaRepository pizzaRepository;
        private IOrderRepository orderRepository;
    }
}
