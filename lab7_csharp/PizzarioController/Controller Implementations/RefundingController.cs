﻿using System;
using System.Collections.Generic;
using System.Linq;

using Pizzario.Controller.Communications;
using Pizzario.Controller.ViewModels;
using Pizzario.Orm;
using Pizzario.Infrastructure;


namespace Pizzario.Controller
{
    class RefundingController : BasicController, IRefundingController
    {
        public RefundingController ()
        {
            this.requestRepository = RepositoryFactory.MakeRefundingRequestRepository( GetDBContext()  );

            this.emailComposer = new EmailComposer( InfrastructureFactory.MakeEmailAgent() );
            this.paymentAgent = InfrastructureFactory.MakePaymentAgent();
        }


        public ICollection< RefundingRequestView > ViewPendingRefundingRequests ()
        {
            var query = requestRepository.SearchRequestsOfStatus( Model.RefundingStatus.Pending );
            return ViewModelsFactory.BuildViewModels( query, ViewModelsFactory.BuildRefundingRequestView );
        }


        public ICollection< RefundingRequestView > ViewConfirmedRefundingRequests ()
        {
            var query = requestRepository.SearchRequestsOfStatus( Model.RefundingStatus.Confirmed );
            return ViewModelsFactory.BuildViewModels( query, ViewModelsFactory.BuildRefundingRequestView );
        }


        public ICollection< RefundingRequestView > ViewRejectedRefundingRequests ()
        {
            var query = requestRepository.SearchRequestsOfStatus( Model.RefundingStatus.Rejected );
            return ViewModelsFactory.BuildViewModels( query, ViewModelsFactory.BuildRefundingRequestView );
        }


        public void RefundingConfirmed ( int requestID )
        {
            Model.RefundingRequest request = ResolveRequest( requestID );
            request.Confirmed();
            requestRepository.Commit();

            Model.WireTransferPaymentPlan plan = request.RelatedOrder.PayPlan as Model.WireTransferPaymentPlan;
            paymentAgent.rollbackTransaction( plan.TransactionId );

            emailComposer.SendRefundingConfirmed( request );
        }


        public void RefundingRejected ( int requestID )
        {
            Model.RefundingRequest request = ResolveRequest( requestID );
            request.Rejected();
            requestRepository.Commit();

            emailComposer.SendRefundingRejected( request );
        }


        private Model.RefundingRequest ResolveRequest ( int requestID )
        {
            return ControllerUtils.ResolveObjectById( requestRepository, requestID );
        }


        private IRefundingRequestRepository requestRepository;

        private EmailComposer emailComposer;
        private IPaymentAgent paymentAgent;
    }
}
