﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pizzario.Controller.ViewModels
{
    public class PizzaKindView : BasicViewModel< PizzaKindView >
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string[] Ingredients { get; set; }
        public IDictionary< string, decimal > Prices { get; set; }
        public double AverageRating { get; set; }
        public int RatingVotes { get; set; }
        public bool Hidden { get; set; }


        protected override IEnumerable< object >
        GetAttributesToIncludeInEqualityCheck ()
        {
            var list = new List< object > { Name, Description, ImageUrl, AverageRating, RatingVotes, Hidden };
            list.AddRange( Ingredients );
            list.AddRange( Prices.Keys );
            foreach ( decimal price in Prices.Values )
                list.Add( price );
            return list;
        }

      
        public override string ToString ()
        {
            StringBuilder b = new StringBuilder();
            b.Append( '{' ); 
            foreach ( var entry in Prices )
            {
                b.Append( entry.Key );
                b.Append( '=' );
                b.Append( entry.Value );
                b.Append( "; " );
            }
            b.Append( '}' ); 

            return string.Format(
                       "Name = {0}\nDescription = {1}\nImageUrl = {2}\nIngredients = {3}\nPrices = {4}\nAverageRating = {5}\nRatingVotes = {6}\nHidden = {7}\n",
                       Name,
                       Description,
                       ImageUrl,
                       string.Join( ", ", Ingredients ),
                       b.ToString(),
                       AverageRating,
                       RatingVotes,
                       Hidden
                   );
        }
    }
}
