﻿using System;
using System.Collections.Generic;

namespace Pizzario.Controller.ViewModels
{
    public class PizzaView : BasicViewModel< PizzaView >
    {
        public int PizzaId { get; set; }
        public int OrderId { get; set; }
        public string Kind { get; set; }
        public string Size { get; set; }
        public string CookingStatus { get; set; }


        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            return new List< object >{ PizzaId, OrderId, Kind, Size, CookingStatus };
        }


        public override string ToString ()
        {
            return string.Format(
               "Id = {0}\nOrderId = {1}\nKind = {2}\nSize = {3}\nCooking Status = {4}\n",
               PizzaId,
               OrderId,
               Kind,
               Size,
               CookingStatus
           );   
        }
    }
}
