﻿using System;
using System.Collections.Generic;

namespace Pizzario.Controller.ViewModels
{
    public class OrdersByIngredientView : BasicViewModel< OrdersByIngredientView >
    {
        public class ReportLine
        {
            public string Ingredient { get; private set; }
            public int TotalWeight { get; private set; }

            public ReportLine ( string ingredient, int totalWeight )
            {
                this.Ingredient  = ingredient;
                this.TotalWeight = totalWeight;
            }

            public override string ToString ()
            {
                return string.Format( "{0} - {1}", Ingredient, TotalWeight );
            }
        }


        public ICollection< ReportLine > ReportLines { get; set; }


        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            var list = new List<object>();
            foreach ( var line in ReportLines )
            {
                list.Add( line.Ingredient );
                list.Add( line.TotalWeight );
            }
            return list;
        }


        public override string ToString ()
        {
            return string.Join( "\n", ReportLines );
        }
    }
}
