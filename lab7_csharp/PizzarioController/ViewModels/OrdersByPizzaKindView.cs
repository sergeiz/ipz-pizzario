﻿using System;
using System.Collections.Generic;

namespace Pizzario.Controller.ViewModels
{
    public class OrdersByPizzaKindView : BasicViewModel< OrdersByPizzaKindView >
    {
        public class ReportLine
        {
            public string PizzaKind { get; private set; }
            public int TotalQuantity { get; private set; }

            public ReportLine ( string pizzaKind, int totalQuantity )
            {
                this.PizzaKind = pizzaKind;
                this.TotalQuantity = totalQuantity;
            }


            public override string ToString ()
            {
                return string.Format( "{0} - {1}", PizzaKind, TotalQuantity );
            }
        }


        public ICollection< ReportLine > ReportLines { get; set; }


        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            var list = new List<object>();
            foreach ( var line in ReportLines )
            {
                list.Add( line.PizzaKind );
                list.Add( line.TotalQuantity );
            }
            return list;
        }


        public override string ToString ()
        {
            return string.Join( "\n", ReportLines );
        }
    }
}
