﻿using System;
using System.Collections.Generic;

namespace Pizzario.Controller.ViewModels
{
    public class CartView : BasicViewModel< CartView >
    {
        public int CartId { get; set; }
        public decimal TotalCost { get; set; }
        public ICollection< ItemView > Items { get; set; }


        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            var list = new List< object > { CartId, TotalCost };
            list.AddRange( Items );
            return list;
        }


        public override string ToString ()
        {
            return string.Format(
                       "CartId = {0}\nTotal Cost = {1}\nItems:\n{2}",
                       CartId,
                       TotalCost,
                       string.Join( "\n", Items )
                   );
        }
    }
}
