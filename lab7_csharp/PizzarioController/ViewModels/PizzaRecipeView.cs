﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pizzario.Controller.ViewModels
{
    public class PizzaRecipeView : BasicViewModel< PizzaRecipeView >
    {
        public string Name { get; set; }

        public class RecipeOfSize
        {
            public string Size { get; set; }
            public int TotalWeight { get; set; }
            public IDictionary< string, int > IngredientsWeight { get; set; } 
        }

        public ICollection< RecipeOfSize > SizeRecipes { get; set; }

        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            var list = new List< object > { Name };
            foreach ( var sizeEntry in SizeRecipes )
            {
                list.Add( sizeEntry.Size );
                list.Add( sizeEntry.TotalWeight );

                foreach ( var ingredientEntry in sizeEntry.IngredientsWeight )
                {
                    list.Add( ingredientEntry.Key );
                    list.Add( ingredientEntry.Value );
                }
            }

            return list;
        }


        public override string ToString ()
        {
            StringBuilder b = new StringBuilder();
            b.Append( Name );
            b.Append( ":\n" );
            foreach ( var sizeEntry in SizeRecipes )
            {
                b.Append( "  " );
                b.Append( sizeEntry.Size );
                b.Append( " (totally " );
                b.Append( sizeEntry.TotalWeight );
                b.Append( " g) = {" );

                bool first = true;
                foreach ( var ingredientEntry in sizeEntry.IngredientsWeight )
                {
                    if ( ! first )
                        b.Append( ',' );
                    else
                        first = false;

                    b.Append( ' ' );
                    b.Append( ingredientEntry.Key );
                    b.Append( " = " );
                    b.Append( ingredientEntry.Value );
                }
                b.Append( "};\n" );
            }

            return b.ToString();
        }
    }
}
