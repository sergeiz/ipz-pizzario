﻿using System;
using System.Collections.Generic;

namespace Pizzario.Controller.ViewModels
{
    public class ItemView : BasicViewModel< ItemView >
    {
        public string PizzaKindName { get; set; }
        public string Size { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Cost { get; set; }


        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            return new List< object >{ PizzaKindName, Size, Quantity, Price, Cost };
        }


        public override string ToString ()
        {
            return string.Format(
                       "PizzaKind = {0}\nSize = {1}\nQuantity = {2}\nPrice = {3}\nCost = {4}\n",
                       PizzaKindName,
                       Size,
                       Quantity,
                       Price,
                       Cost
                   );
        }
    }
}
