﻿using System;
using System.Collections.Generic;

namespace Pizzario.Controller.ViewModels
{
    public class RefundingRequestView : BasicViewModel< RefundingRequestView >
    {
        public int RequestId { get; set; }
        public int OrderId { get; set; }
        public string Status { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }

        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            return new List< object >{ RequestId, OrderId, Status, ContactEmail, ContactPhone };
        }

        public override string ToString ()
        {
            return string.Format(
                       "RequestId = {0}\nOrderId = {1}\nStatus = {2}\nEmail = {3}\nPhone = {4}",
                       RequestId,
                       OrderId,
                       Status,
                       ContactEmail,
                       ContactPhone
                   );
        }
    }
}
