﻿using System;
using System.Collections.Generic;

namespace Pizzario.Controller.ViewModels
{
    public class CustomerView : BasicViewModel< CustomerView >
    {
        public int CustomerId { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public double Discount { get; set; }

        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            return new List< object > { CustomerId, FullName, Email, Discount };
        }

        public override string ToString ()
        {
            return string.Format(
                       "ID = {0}\nFullName = {1}\nEmail = {2}\nDiscount = {3}\n",
                       CustomerId,
                       FullName,
                       Email,
                       Discount
                   );            
        }
    }
}
