﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pizzario.Controller.ViewModels
{
    public class PizzaRatingsView : BasicViewModel< PizzaRatingsView >
    {
        public class RatingLine
        {
            public string PizzaKindName { get; set; }
            public int Votes { get; set; }
            public double AverageRating { get; set; }
        }

        public ICollection< RatingLine > Lines { get; set; }

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck ()
        {
            var list = new List< object >();
            foreach ( var line in Lines )
            {
                list.Add( line.PizzaKindName );
                list.Add( line.Votes );
                list.Add( line.AverageRating );
            }
            return list;
        }

        public override string ToString ()
        {
            StringBuilder b = new StringBuilder();

            foreach ( var line in Lines )
            {
                b.Append( line.PizzaKindName );
                b.Append( ' ' );
                b.Append( line.AverageRating );
                b.Append( " (" );
                b.Append( line.Votes );
                b.Append( " votes)\n" );
            }
            return b.ToString();
        }

    }
}