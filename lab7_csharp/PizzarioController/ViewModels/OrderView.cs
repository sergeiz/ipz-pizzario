﻿using System;
using System.Collections.Generic;

namespace Pizzario.Controller.ViewModels
{
    public class OrderView : BasicViewModel< OrderView >
    {
        public int OrderId { get; set; }
        public ICollection< ItemView > Items { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Status { get; set; }
        public decimal TotalCost { get; set; }
        public double Discount { get; set; }
        public string PaymentKind { get; set; }
        public bool Payed { get; set; }


        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            var list = new List< object >{ OrderId, Email, Address, Phone, Status, TotalCost, Discount, PaymentKind, Payed };
            list.AddRange( Items );
            return list;
        }


        public override string ToString ()
        {
            return string.Format(
                  "OrderId = {0}\nItems = {1}\nEmail = {2}\nAddress = {3}\nPhone = {4}\nStatus = {5}\nTotal Cost = {6}\nDiscount = {7}\nPayment via = {8}\nPayed = {9}\n",
                  OrderId,
                  string.Join( "\n\t", Items ),
                  Email,
                  Address,
                  Phone,
                  Status,
                  TotalCost,
                  Discount,
                  PaymentKind,
                  Payed
          );
        }
    }
}
