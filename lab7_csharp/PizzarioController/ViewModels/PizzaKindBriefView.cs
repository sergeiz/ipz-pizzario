﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Pizzario.Controller.ViewModels
{
    public class PizzaKindBriefView : BasicViewModel< PizzaKindBriefView >
    {
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public string[] KeyIngredients { get; set; }
        public decimal SelectedPrice { get; set; }


        protected override IEnumerable< object >
        GetAttributesToIncludeInEqualityCheck ()
        {
            var list = new List< object > { Name, ImageUrl, SelectedPrice };
            list.AddRange( KeyIngredients );
            return list;
        }


        public override string ToString ()
        {
             return string.Format(
                        "Name = {0}\nImage = {1}\nKeyIngredients = {2}\nSelectedPrice = {3}\n",
                        Name, 
                        ImageUrl,
                        string.Join( ", ", KeyIngredients ),
                        SelectedPrice
                    );
        }
    }
}
