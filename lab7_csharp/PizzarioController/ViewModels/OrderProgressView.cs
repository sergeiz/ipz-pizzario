﻿using System;
using System.Collections.Generic;

namespace Pizzario.Controller.ViewModels
{
    public class OrderProgressView : BasicViewModel< OrderProgressView >
    {
        public int OrderId { get; set; }
        public string Status { get; set; }
        public int Remaining { get; set; }
        public ICollection< PizzaView > Pizzas { get; set; }


        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            var list = new List< object > { OrderId, Status, Remaining };
            list.AddRange( Pizzas );
            return list;
        }


        public override string ToString ()
        {
            return string.Format(
                    "OrderId = {0}\nStatus = {1}\nRemaining = {2}\nPizzas:\n{3}",
                    OrderId,
                    Status,
                    Remaining,
                    string.Join( "\n", Pizzas )
            );
        }
    }
}
