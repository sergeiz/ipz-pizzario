﻿using System;
using System.Collections.Generic;

namespace Pizzario.Controller.ViewModels
{
    public class OrderBriefView : BasicViewModel< OrderBriefView >
    {
        public int OrderId { get; set; }
        public string Status { get; set; }
        public string CustomerPhone { get; set; }
        public decimal TotalCost { get; set; }

        protected override IEnumerable< object >
            GetAttributesToIncludeInEqualityCheck ()
        {
            return new List< object > { OrderId, Status, CustomerPhone, TotalCost };
        }


        public override string ToString ()
        {
            return string.Format(
                       "ID = {0}\nStatus = {1}\nCustomer Phone = {2}\nTotal Cost = {3}\n",
                       OrderId,
                       Status,
                       CustomerPhone,
                       TotalCost
                   );
        }
    }
}
