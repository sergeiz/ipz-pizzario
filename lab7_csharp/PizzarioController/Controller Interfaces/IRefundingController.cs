﻿using System;
using System.Collections.Generic;

using Pizzario.Controller.ViewModels;


namespace Pizzario.Controller
{
    public interface IRefundingController : IDisposable
    {
        ICollection< RefundingRequestView > ViewPendingRefundingRequests ();

        ICollection< RefundingRequestView > ViewConfirmedRefundingRequests ();

        ICollection< RefundingRequestView > ViewRejectedRefundingRequests ();

        void RefundingConfirmed ( int requestID );

        void RefundingRejected ( int requestID );
    }
}
