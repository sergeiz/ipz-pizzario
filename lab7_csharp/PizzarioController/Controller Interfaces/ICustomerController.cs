﻿using System;
using System.Collections.Generic;

using Pizzario.Controller.ViewModels;

namespace Pizzario.Controller
{
    public interface ICustomerController : IDisposable
    {
        ICollection< CustomerView > ViewAllCustomers ();

        ICollection< OrderBriefView > FindPastOrders ( int customerId );

        int NewCustomer ( string fullname, string email, string address, string phone );

        CustomerView FindCustomer ( string email );

        void ChangeName ( int customerId, string newFullName );

        void ChangeEmail ( int customerId, string newEmail );

        void ChangeAddress ( int customerId, string newAddress );

        void ChangePhone ( int customerId, string newPhone );

        void SetDiscount ( int customerId, double discount );

        void AttachOrder ( int customerId, int orderId );
    }
}
