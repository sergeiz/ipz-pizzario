﻿using System;
using System.Collections.Generic;

using Pizzario.Controller.ViewModels;

namespace Pizzario.Controller
{
    public interface IMenuController : IDisposable
    {
        ICollection< PizzaKindBriefView > ViewVisiblePizzaKinds ();

        ICollection< PizzaKindBriefView > ViewHiddenPizzaKinds ();

        PizzaKindView ViewPizzaKindDetails ( string kindName );

        ICollection< PizzaKindBriefView > SuggestPizzaKindsByIngredients ( string[] includeIngredients, 
                                                                           string[] excludeIngredients );

        ICollection< PizzaKindBriefView > ViewSimilarPizzaKinds ( string kindName );

        PizzaRecipeView ViewPizzaRecipe ( string kindName );

        PizzaRatingsView ViewPizzaKindsByPopularity ();

        void CreateNewPizzaKind ( string kindName, string description, string imageUrl );

        void Rename ( string kindName, string newName );

        void UpdateDescription ( string kindName, string description );

        void UpdateImageUrl ( string kindName, string url );

        void SetIngredient ( string kindName, string ingredientName, int smallestWeight );

        void RemoveIngredient ( string kindName, string ingredientName );

        void HidePizzaFromCustomers ( string kindName );

        void EnableShowingPizzaToCustomers ( string kindName );

        void UpdatePrice ( string kindName, string size, decimal price );

        void Rate ( string kindName, int rating );
    }
}
