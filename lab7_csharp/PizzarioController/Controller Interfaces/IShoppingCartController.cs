﻿using System;
using System.Collections.Generic;

using Pizzario.Controller.ViewModels;


namespace Pizzario.Controller
{
    public interface IShoppingCartController : IDisposable
    {
        CartView ViewCart ( int cartID );

        int NewCart ();

        int CloneCartOfPastOrder ( int orderId );

        void SetCartItem ( int cartID, string pizzaKindName, string pizzaSize, int quantity );

        void RemoveCartItem ( int cartID, string pizzaKindName, string pizzaSize );

        void ClearCart ( int cartID );

        void LockCart ( int cartID );

        void DeleteCart ( int cartID );
    }
}
