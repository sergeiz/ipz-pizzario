﻿using System;
using System.Collections.Generic;

using Pizzario.Controller.ViewModels;

namespace Pizzario.Controller
{
    public interface IOrderController : IDisposable
    {
        ICollection< OrderBriefView > ViewAllOrders ();

        ICollection< OrderBriefView > ViewOrdersByStatus ( string status );

        OrderView ViewOrderContent ( int orderID );

        OrderProgressView ViewOrderProgress ( int orderID );

        OrdersByPizzaKindView BrowseOrderStatsByPizzaKinds ();

        OrdersByIngredientView BrowseOrderStatsByIngredients ();

        int NewVisitorOrder ( int cartID,
                              string email,
                              string address,
                              string phone );

        int NewCustomerOrder ( int cartID, int customerID );

        void DefineCashPaymentPlan ( int orderID );

        void DefineWireTransferPaymentPlan ( int orderID, 
                                             string cardCode, 
                                             string cardHolder );

        void DeliveryStarted ( int orderID );

        void ConfirmDelivery ( int orderID, decimal collectedPaymentAmount );

        int CancelOrder ( int orderID );
    }
}
