﻿using System;
using System.Collections.Generic;

using Pizzario.Controller.ViewModels;

namespace Pizzario.Controller
{
    public interface IPizzaController : IDisposable
    {
        ICollection< PizzaView > ListPizzasBeingCooked ();

        ICollection< PizzaView > ListPizzasWaitingForCooking ();

        void CookingStarted ( int pizzaID );

        void CookingFinished ( int pizzaID );
    }
}
