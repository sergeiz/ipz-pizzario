﻿using System;
using System.Linq;
using System.Collections.Generic;

using Pizzario.Controller.ViewModels;

namespace Pizzario.Controller
{
    static class ViewModelsFactory
    {
        static internal CustomerView BuildCustomerView ( Model.Customer customer )
        {
            return new CustomerView
            {
                CustomerId = customer.CustomerId,
                FullName   = customer.FullName,
                Email      = customer.Email,
                Discount   = customer.CurrentDiscount.Value
            };
        }


        static internal ItemView BuildItemView ( Model.OrderItem item )
        {
            return new ItemView
            {
                PizzaKindName = item.Kind.Name,
                Size = item.Size.ToString(),
                Quantity = item.Quantity,
                Price = item.FixedPrice,
                Cost = item.Cost
            };
        }


        static internal CartView BuildCartView ( Model.ShoppingCart cart )
        {
            return new CartView
            {
                CartId = cart.ShoppingCartId,
                TotalCost = cart.Cost,
                Items = BuildViewModels( cart.Items.AsQueryable(), BuildItemView )
            };
        }


        static internal PizzaKindBriefView BuildPizzaKindBriefView ( Model.PizzaKind kind )
        {
            return new ViewModels.PizzaKindBriefView 
            {
                Name = kind.Name,
                ImageUrl = kind.ImageUrl,
                KeyIngredients = kind.Recipe.KeyIngredients(),
                SelectedPrice = kind.GetCurrentPrice( Model.PizzaSize.Small )
            };
        }


        static internal PizzaKindView BuildPizzaKindView ( Model.PizzaKind kind )
        {
            return new ViewModels.PizzaKindView
            {
                Name          = kind.Name,
                Description   = kind.Description,
                ImageUrl      = kind.ImageUrl,
                Ingredients   = DescribeIngredientNames( kind ),
                Prices        = DescribePizzaPrices( kind ),
                AverageRating = kind.AverageRating,
                RatingVotes   = kind.Votes,
                Hidden        = kind.Hidden
            };
        }


        static private IDictionary< string, decimal > DescribePizzaPrices ( Model.PizzaKind kind )
        {
            var pricesPerSize = new Dictionary< string, decimal >();

            foreach ( Model.PizzaSize size in Enum.GetValues( typeof( Model.PizzaSize ) ) )
                pricesPerSize.Add( size.ToString(), kind.GetCurrentPrice( size ) );

            return pricesPerSize;
        }


        static private string[] DescribeIngredientNames ( Model.PizzaKind kind )
        {
            return kind.Recipe.IngredientNames.ToArray();
        }


        static internal OrderView BuildOrderView ( Model.Order order )
        {
            return new OrderView
            {
                OrderId     = order.OrderId,
                Items       = BuildViewModels( order.Items.AsQueryable(), BuildItemView ),
                Email       = order.Contact.Email,
                Address     = order.Contact.Address,
                Phone       = order.Contact.Phone,
                Status      = order.Status.ToString(),
                TotalCost   = order.TotalCost,
                Discount    = order.AssignedDiscount.Value,
                PaymentKind = ( order.PayPlan == null ) ? "<undefined>" : order.PayPlan.ToString(),
                Payed       = ( order.PayPlan == null ) ? false : order.PayPlan.Payed
            };
        }


        static internal OrderProgressView BuildOrderProgressView ( Model.Order order, IQueryable< Model.Pizza > pizza )
        {
            return new OrderProgressView
            {
                OrderId   = order.OrderId,
                Status    = order.Status.ToString(),
                Remaining = order.RemainingPizzasToCook,
                Pizzas    = BuildViewModels( pizza, BuildPizzaView )
            };
        }


        static internal OrderBriefView BuildOrderBriefView ( Model.Order order )
        {
            return new OrderBriefView
            {
                OrderId       = order.OrderId,
                Status        = order.Status.ToString(),
                CustomerPhone = order.Contact.Phone,
                TotalCost     = order.TotalCost
            };
        }


        static internal PizzaView BuildPizzaView ( Model.Pizza pizza )
        {
            return new PizzaView
            {
                PizzaId = pizza.PizzaId,
                OrderId = pizza.RelatedOrder.OrderId,
                Kind    = pizza.Kind.Name,
                Size    = pizza.Size.ToString(),
                CookingStatus = pizza.Status.ToString()
            };
        }


        static internal RefundingRequestView BuildRefundingRequestView ( Model.RefundingRequest request )
        {
            return new ViewModels.RefundingRequestView
            {
                RequestId    = request.RequestId,
                OrderId      = request.RelatedOrder.OrderId,
                Status       = request.Status.ToString(),
                ContactEmail = request.RelatedOrder.Contact.Email,
                ContactPhone = request.RelatedOrder.Contact.Phone
            };
        }


        static internal ICollection< TView > BuildViewModels< TView, TEntity> (
            IQueryable< TEntity> query,
            Func< TEntity, TView > converter
        )
        {
            var results = new List< TView >();
            foreach ( TEntity entity in query )
                results.Add( converter( entity ) );
            return results;
        }

    }
}
