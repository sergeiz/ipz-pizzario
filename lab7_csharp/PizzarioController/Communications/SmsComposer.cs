﻿using System;
using System.Text;

using Pizzario.Model;
using Pizzario.Infrastructure;

namespace Pizzario.Controller.Communications
{
    class SmsComposer
    {
        public SmsComposer ( ISmsAgent smsAgent )
        {
            this.smsAgent = smsAgent;
        }


        public void SendOrderRegistered ( Order order )
        {
            var builder = new StringBuilder();
            builder.Append( "Your Pizzario order #" );
            builder.Append( order.OrderId );
            builder.Append( " was registered." );
            builder.Append( " Total amount: $" );
            builder.Append( order.TotalCost );

            smsAgent.sendSMS( order.Contact.Phone, builder.ToString() );
        }


        public void SendOrderCancelled ( Order order )
        {
            var builder = new StringBuilder();
            builder.Append( "Your Pizzario order #" );
            builder.Append( order.OrderId );
            builder.Append( " was cancelled." );
            if ( order.MayBeRefunded() )
            {
                builder.Append( " The amount of $" );
                builder.Append( order.TotalCost );
                builder.Append( " will be refunded after manual confirmation." );
            }

            smsAgent.sendSMS( order.Contact.Phone, builder.ToString() );
        }

        private ISmsAgent smsAgent;
    }
}
