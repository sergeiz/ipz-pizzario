﻿using System;
using System.IO;

using Pizzario.Controller;

namespace Pizzario.Client.Console
{
    class StatsByIngredientCommand : Command
    {
        public StatsByIngredientCommand ( TextWriter output )
            : base( "stats.ingredient", output )
        {
        }


        public override void Execute ( CommandSwitchValues values )
        {
            using ( var orderController = ControllerFactory.MakeOrderController() )
            {
                Output.WriteLine( orderController.BrowseOrderStatsByIngredients() );
            }
        }
    }
}
