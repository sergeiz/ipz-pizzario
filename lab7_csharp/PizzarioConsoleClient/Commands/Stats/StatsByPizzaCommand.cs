﻿using System;
using System.IO;

using Pizzario.Controller;

namespace Pizzario.Client.Console
{
    class StatsByPizzaCommand : Command
    {
        public StatsByPizzaCommand ( TextWriter output )
            :   base( "stats.pizza", output )
        {
        }


        public override void Execute ( CommandSwitchValues values )
        {
            using ( var orderController = ControllerFactory.MakeOrderController() )
            {
                Output.WriteLine( orderController.BrowseOrderStatsByPizzaKinds() );
            }
        }
    }
}
