﻿using System;
using System.IO;

using Pizzario.Controller;

namespace Pizzario.Client.Console
{
    class ShowPizzaCommand : PizzaCommand
    {
        public ShowPizzaCommand ( TextWriter output )
            :   base( "pizza.show", output )
        {
        }


        public override void Execute ( CommandSwitchValues values )
        {
            string kindName = GetKind( values );

            using ( var menuController = ControllerFactory.MakeMenuController() )
            {
                Output.WriteLine( menuController.ViewPizzaKindDetails( kindName ) );
            }
        }

    }
}
