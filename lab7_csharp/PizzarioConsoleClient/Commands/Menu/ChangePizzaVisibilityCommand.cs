﻿using System;
using System.IO;

using Pizzario.Controller;


namespace Pizzario.Client.Console
{
    class ChangePizzaVisibilityCommand : PizzaCommand
    {
        public ChangePizzaVisibilityCommand ( TextWriter output )
            : base( "pizza.visibility", output )
        {
            AddSwitch( new CommandSwitch( "-visible", CommandSwitch.ValueMode.ExpectSingle, false ) );
        }


        public override void Execute ( CommandSwitchValues values )
        {
            string kindName = GetKind( values );
            bool visible = values.GetSwitchAsBool( "-visible" );

            using ( var menuController = ControllerFactory.MakeMenuController() )
            {
                if ( visible )
                    menuController.EnableShowingPizzaToCustomers( kindName );
                else
                    menuController.HidePizzaFromCustomers( kindName );
            }
        }

    }
}
