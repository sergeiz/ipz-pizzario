﻿using System;
using System.IO;

using Pizzario.Controller;

namespace Pizzario.Client.Console
{
    class ShowPopularPizzasCommand : Command
    {
        public ShowPopularPizzasCommand ( TextWriter output )
            :   base( "manu.popular_pizzas", output )
        {
        }


        public override void Execute ( CommandSwitchValues values )
        {
            using ( var menuController = ControllerFactory.MakeMenuController() )
            {
                var ratingsView = menuController.ViewPizzaKindsByPopularity();
                Output.WriteLine( ratingsView );
            }
        }
    }
}
