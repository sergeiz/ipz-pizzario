﻿using System;
using System.IO;

using Pizzario.Controller;


namespace Pizzario.Client.Console
{
    class SuggestPizzaCommand : Command
    {
        public SuggestPizzaCommand ( TextWriter output )
            :    base( "menu.suggest", output )
        {
            AddSwitch( new CommandSwitch( "-include", CommandSwitch.ValueMode.ExpectMultiple, true ) );
            AddSwitch( new CommandSwitch( "-exclude", CommandSwitch.ValueMode.ExpectMultiple, true ) );
        }


        public override void Execute ( CommandSwitchValues values )
        {
            using ( var menuController = ControllerFactory.MakeMenuController() )
            {
                string includeIngredients = values.GetOptionalSwitch( "-include" );
                string excludeIngredients = values.GetOptionalSwitch( "-exclude" );

                string[] includes = ( includeIngredients != null ) ? includeIngredients.Split( ' ' ) : null;
                string[] excludes = ( excludeIngredients != null ) ? excludeIngredients.Split( ' ' ) : null;

                var pizzaKinds = menuController.SuggestPizzaKindsByIngredients( includes, excludes );
                ReportValues( pizzaKinds );
            }
        }
    }
}
