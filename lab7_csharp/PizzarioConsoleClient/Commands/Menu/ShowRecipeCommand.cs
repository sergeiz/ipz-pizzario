﻿using System;
using System.IO;

using Pizzario.Controller;

namespace Pizzario.Client.Console
{
    class ShowRecipeCommand : PizzaCommand
    {
        public ShowRecipeCommand ( TextWriter output )
            :    base( "pizza.recipe.show", output )
        {
        }


        public override void Execute ( CommandSwitchValues values )
        {
            string kindName = GetKind( values );

            using ( var menuController = ControllerFactory.MakeMenuController() )
            {
                Output.WriteLine( menuController.ViewPizzaRecipe( kindName ) );
            }
        }

    }
}
