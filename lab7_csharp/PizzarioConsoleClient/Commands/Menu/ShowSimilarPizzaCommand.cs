﻿using System;
using System.IO;

using Pizzario.Controller;

namespace Pizzario.Client.Console
{
    class ShowSimilarPizzaCommand : PizzaCommand
    {
        public ShowSimilarPizzaCommand ( TextWriter output )
            :    base( "menu.show_similar", output )
        {
        }


        public override void Execute ( CommandSwitchValues values )
        {
            string kindName = GetKind( values ); ;

            using ( var menuController = ControllerFactory.MakeMenuController() )
            {
                var pizzaKinds = menuController.ViewSimilarPizzaKinds( kindName );
                ReportValues( pizzaKinds );
            }
        }

    }
}
