﻿using System;
using System.IO;

using Pizzario.Controller;


namespace Pizzario.Client.Console
{
    class RatePizzaCommand : PizzaCommand
    {
        public RatePizzaCommand ( TextWriter output )
            : base( "pizza.rate", output )
        {
            AddSwitch( new CommandSwitch( "-rating", CommandSwitch.ValueMode.ExpectSingle, false ) );
        }


        public override void Execute ( CommandSwitchValues values )
        {
            string kindName = GetKind( values );
            int rating = values.GetSwitchAsInt( "-rating" );

            using ( var menuController = ControllerFactory.MakeMenuController() )
            {
                menuController.Rate( kindName, rating );
            }
        }
    }
}
