﻿using System;
using System.IO;

using Pizzario.Controller;


namespace Pizzario.Client.Console
{
    class CreatePizzaKindCommand : Command
    {
        public CreatePizzaKindCommand ( TextWriter output )
            :   base( "pizza.create", output )
        {
            AddSwitch( new CommandSwitch( "-name",  CommandSwitch.ValueMode.ExpectSingle, false ) );
            AddSwitch( new CommandSwitch( "-descr", CommandSwitch.ValueMode.ExpectSingle, true ) );
            AddSwitch( new CommandSwitch( "-image", CommandSwitch.ValueMode.ExpectSingle,  true ) );
        }


        public override void Execute ( CommandSwitchValues values )
        {
            string name = values.GetSwitch( "-name" );
            
            string description = values.GetOptionalSwitch( "-descr" );
            if ( description == null )
                description = "";

            string imageUrl = values.GetOptionalSwitch( "-image" );
            if ( imageUrl == null )
                imageUrl = "";

            using ( var menuController = ControllerFactory.MakeMenuController() )
            {
                menuController.CreateNewPizzaKind( name, description, imageUrl );
            }
        }
    }
}
