﻿using System;
using System.IO;

using Pizzario.Controller;


namespace Pizzario.Client.Console
{
    class UpdatePizzaCommand : PizzaCommand
    {
        public UpdatePizzaCommand ( TextWriter output )
            : base( "pizza.update", output )
        {
            AddSwitch( new CommandSwitch( "-name",  CommandSwitch.ValueMode.ExpectSingle, true ) );
            AddSwitch( new CommandSwitch( "-descr", CommandSwitch.ValueMode.ExpectSingle, true ) );
            AddSwitch( new CommandSwitch( "-image", CommandSwitch.ValueMode.ExpectSingle, true ) );
        }


        public override void Execute ( CommandSwitchValues values )
        {
            string kindName = GetKind( values );

            using ( var menuController = ControllerFactory.MakeMenuController() )
            {
                string newName = values.GetOptionalSwitch( "-name" );
                if ( newName != null )
                    menuController.Rename( kindName, newName );

                string newDescription = values.GetOptionalSwitch( "-descr" );
                if ( newDescription != null )
                    menuController.UpdateDescription( kindName, newDescription );

                string newImageUrl = values.GetOptionalSwitch( "-image" );
                if ( newImageUrl != null )
                    menuController.UpdateImageUrl( kindName, newImageUrl );
            }
        }
    }
}
