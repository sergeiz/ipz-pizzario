﻿using System;
using System.IO;

using Pizzario.Controller;


namespace Pizzario.Client.Console
{
    class ShowMenuCommand : Command
    {
        public ShowMenuCommand ( TextWriter output )
            :   base( "menu.show", output )
        {
            AddSwitch( new CommandSwitch( "-hidden", CommandSwitch.ValueMode.Unexpected, true ) );
        }

        public override void Execute ( CommandSwitchValues values )
        {
            using ( var menuController = ControllerFactory.MakeMenuController() )
            {
                bool showHidden = values.HasSwitch( "-hidden" );
                var pizzaKinds = ( showHidden ) ?
                    menuController.ViewHiddenPizzaKinds() :
                    menuController.ViewVisiblePizzaKinds();

                ReportValues( pizzaKinds );
            }
        }
    }
}
