﻿using System;
using System.IO;

namespace Pizzario.Client.Console
{
    abstract class PizzaCommand : Command
    {
        protected PizzaCommand ( string name, TextWriter output )
            :   base( name, output )
        {
            AddSwitch( new CommandSwitch( "-kind", CommandSwitch.ValueMode.ExpectSingle, false ) );
        }


        protected string GetKind ( CommandSwitchValues values )
        {
            return values.GetSwitch( "-kind" );
        }
    }
}
