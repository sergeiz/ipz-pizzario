﻿using System;
using System.IO;

using Pizzario.Controller;


namespace Pizzario.Client.Console
{
    class EditPricesCommand : PizzaCommand
    {
        public EditPricesCommand ( TextWriter output )
            : base( "pizza.setprice", output )
        {
            AddSwitch( new CommandSwitch( "-size", CommandSwitch.ValueMode.ExpectSingle,  false ) );
            AddSwitch( new CommandSwitch( "-price", CommandSwitch.ValueMode.ExpectSingle, false ) );
        }


        public override void Execute ( CommandSwitchValues values )
        {
            string kindName = GetKind( values );
            string size     = values.GetSwitch( "-size" );
            decimal price   = values.GetSwitchAsDecimal( "-price" );

            using ( var menuController = ControllerFactory.MakeMenuController() )
            {
                menuController.UpdatePrice( kindName, size, price );
            }
        }
    }
}
