﻿using System;
using System.IO;

using Pizzario.Controller;


namespace Pizzario.Client.Console
{
    class EditRecipeCommand : PizzaCommand
    {
        public EditRecipeCommand ( TextWriter output )
            :   base( "pizza.recipe.edit", output )
        {
            AddSwitch( new CommandSwitch( "-ingr",   CommandSwitch.ValueMode.ExpectSingle, false ) );
            AddSwitch( new CommandSwitch( "-action", CommandSwitch.ValueMode.ExpectSingle, false ) );
            AddSwitch( new CommandSwitch( "-weight", CommandSwitch.ValueMode.ExpectSingle, true ) );
        }


        public override void Execute ( CommandSwitchValues values )
        {
            string kindName = GetKind( values );
            string ingredientName = values.GetSwitch( "-ingr" );
            string action = values.GetSwitch( "-action" );

            using ( var menuController = ControllerFactory.MakeMenuController() )
            {
                if ( action == "add" || action == "set" )
                {
                    int weight = values.GetSwitchAsInt( "-weight" );
                    menuController.SetIngredient( kindName, ingredientName, weight );
                }

                else if ( action == "remove" )
                    menuController.RemoveIngredient( kindName, ingredientName );

                else
                    throw new Exception( "EditRecipeCommand: unrecognized action " + action );
            }
        }
    }
}
