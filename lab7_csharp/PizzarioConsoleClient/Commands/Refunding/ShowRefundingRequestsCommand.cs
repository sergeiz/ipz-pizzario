﻿using System;
using System.IO;

using Pizzario.Controller;


namespace Pizzario.Client.Console
{
    class ShowRefundingRequestsCommand : Command
    {
        public ShowRefundingRequestsCommand ( TextWriter output )
            :   base( "refunding.show", output )
        {
            AddSwitch( new CommandSwitch( "-status", CommandSwitch.ValueMode.ExpectSingle, false ) );
        }


        public override void Execute ( CommandSwitchValues values )
        {
            string status = values.GetSwitch( "-status" );

            using ( var refundingController = ControllerFactory.MakeRefundingController() )
            {
                if ( status == "pending" )
                    ReportValues( refundingController.ViewPendingRefundingRequests() );

                else if ( status == "confirmed" )
                    ReportValues( refundingController.ViewConfirmedRefundingRequests() );

                else if ( status == "rejected" )
                    ReportValues( refundingController.ViewRejectedRefundingRequests() );

                else
                    throw new Exception( "Refunding status must be 'pending, 'confirmed' or 'rejected'." );
            }
        }
    }
}
