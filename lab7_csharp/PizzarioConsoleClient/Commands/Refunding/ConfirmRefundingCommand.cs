﻿using System;
using System.IO;

using Pizzario.Controller;

namespace Pizzario.Client.Console
{
    class ConfirmRefundingCommand : Command
    {
        public ConfirmRefundingCommand ( TextWriter output )
            :   base( "refunding.confirm", output )
        {
            AddSwitch( new CommandSwitch( "-id", CommandSwitch.ValueMode.ExpectSingle, false ) );
        }


        public override void Execute ( CommandSwitchValues values )
        {
            int requestId = values.GetSwitchAsInt( "-id" );

            using ( var refundingController = ControllerFactory.MakeRefundingController() )
            {
                refundingController.RefundingConfirmed( requestId );
            }
        }
    }
}
