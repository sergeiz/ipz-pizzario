﻿using System;
using System.IO;
using System.Linq;

using Pizzario.Controller;

namespace Pizzario.Client.Console
{
    class PopFromCartCommand : Command
    {
        public PopFromCartCommand ( TextWriter output, EntityReference currentCart )
            : base( "cart.pop", output )
        {
            this.currentCart = currentCart;

            AddSwitch( new CommandSwitch( "-kind", CommandSwitch.ValueMode.ExpectSingle, false ) );
            AddSwitch( new CommandSwitch( "-size", CommandSwitch.ValueMode.ExpectSingle, false ) );
        }


        public override void Execute ( CommandSwitchValues values )
        {
            if ( ! currentCart.IsInitialized() )
                throw new Exception( "Shopping cart is empty" );

            string kind = values.GetSwitch( "-kind" );
            string size = values.GetSwitch( "-size" );

            using ( var cartController = ControllerFactory.MakeShoppingCartController() )
            {
                var cartView = cartController.ViewCart( currentCart.ObjectId );
                var cartItem = cartView.Items.Where( i => i.PizzaKindName == kind && i.Size == size ).SingleOrDefault();
                if ( cartItem == null )
                    throw new Exception( string.Format( "Item {0} of size {1} not present in cart", kind, size ) );

                int initialQuantity = cartItem.Quantity;
                if ( initialQuantity == 1 )
                    cartController.RemoveCartItem( currentCart.ObjectId, kind, size );
                else
                    cartController.SetCartItem( currentCart.ObjectId, kind, size, initialQuantity - 1 );
            }
        }


        private EntityReference currentCart;
    }
}
