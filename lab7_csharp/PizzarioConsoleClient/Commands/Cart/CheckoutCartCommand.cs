﻿using System;
using System.IO;

using Pizzario.Controller;

namespace Pizzario.Client.Console
{
    class CheckoutCartCommand : Command
    {
        public CheckoutCartCommand ( 
            TextWriter output, 
            EntityReference currentCart,
            EntityReference currentOrder,
            EntityReference currentCustomer
        )
            :    base( "cart.checkout", output )
        {
            this.currentCart     = currentCart;
            this.currentOrder    = currentOrder;
            this.currentCustomer = currentCustomer;

            AddSwitch( new CommandSwitch( "-email",   CommandSwitch.ValueMode.ExpectSingle, true ) );
            AddSwitch( new CommandSwitch( "-phone",   CommandSwitch.ValueMode.ExpectSingle, true ) );
            AddSwitch( new CommandSwitch( "-address", CommandSwitch.ValueMode.ExpectSingle, true ) );
        }


        public override void Execute ( CommandSwitchValues values )
        {
            if ( ! currentCart.IsInitialized() )
                throw new Exception( "Shopping cart is empty" );

            int cartId = currentCart.ObjectId;

            if ( currentCustomer.IsInitialized() )
            {
                LockCart();
                ExecuteCustomerOrder( cartId  );
            }
            else
            {
                string email   = values.GetSwitch( "-email" );
                string address = values.GetSwitch( "-address" );
                string phone   = values.GetSwitch( "-phone"   );

                LockCart();
                ExecuteVisitorOrder( cartId, email, address, phone );
            }
        }


        private void LockCart ()
        {
            using ( var cartController = ControllerFactory.MakeShoppingCartController() )
            {
                cartController.LockCart( currentCart.ObjectId );
                currentCart.Drop();
            }
        }


        private void ExecuteCustomerOrder ( int cartId )
        {
            using ( var orderController = ControllerFactory.MakeOrderController() )
            {
                currentOrder.Initialize(
                    orderController.NewCustomerOrder( cartId, currentCustomer.ObjectId ) 
                );
            }
        }


        private void ExecuteVisitorOrder ( int cartId, string email, string address, string phone )
        {
            using ( var orderController = ControllerFactory.MakeOrderController() )
            {
                currentOrder.Initialize(
                    orderController.NewVisitorOrder( cartId, email, address, phone )
                );
            }
        }


        private EntityReference currentCart;
        private EntityReference currentOrder;
        private EntityReference currentCustomer;
    }
}
