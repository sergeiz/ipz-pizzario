﻿using System;
using System.IO;

using Pizzario.Controller;

namespace Pizzario.Client.Console
{
    class ShowCartCommand : Command
    {
        public ShowCartCommand ( TextWriter output, EntityReference currentCart )
            : base( "cart.show", output )
        {
            this.currentCart = currentCart;
        }


        public override void Execute ( CommandSwitchValues values )
        {
            if ( !currentCart.IsInitialized() )
                throw new Exception( "Shopping cart is empty" );

            using ( var cartController = ControllerFactory.MakeShoppingCartController() )
            {
                Output.WriteLine( cartController.ViewCart( currentCart.ObjectId ) );
            }
        }


        private EntityReference currentCart;
    }
}
