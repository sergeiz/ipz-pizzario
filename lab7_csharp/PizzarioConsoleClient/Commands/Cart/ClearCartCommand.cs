﻿using System;
using System.IO;

using Pizzario.Controller;

namespace Pizzario.Client.Console
{
    class ClearCartCommand : Command
    {
        public ClearCartCommand ( TextWriter output, EntityReference currentCart )
            : base( "cart.clear", output )
        {
            this.currentCart = currentCart;
        }


        public override void Execute ( CommandSwitchValues values )
        {
            if ( !currentCart.IsInitialized() )
                throw new Exception( "Shopping cart is empty" );

            using ( var cartController = ControllerFactory.MakeShoppingCartController() )
            {
                cartController.ClearCart( currentCart.ObjectId );
            }
        }


        private EntityReference currentCart;
    }
}
