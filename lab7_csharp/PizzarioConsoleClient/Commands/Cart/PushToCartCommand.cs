﻿using System;
using System.IO;
using System.Linq;

using Pizzario.Controller;

namespace Pizzario.Client.Console
{
    class PushToCartCommand : Command
    {
        public PushToCartCommand ( TextWriter output, EntityReference currentCart )
            :   base( "cart.push", output )
        {
            this.currentCart = currentCart;

            AddSwitch( new CommandSwitch( "-kind",  CommandSwitch.ValueMode.ExpectSingle, false ) );
            AddSwitch( new CommandSwitch( "-size",  CommandSwitch.ValueMode.ExpectSingle, false ) );
        }


        public override void Execute ( CommandSwitchValues values )
        {
            string kind = values.GetSwitch( "-kind" );
            string size = values.GetSwitch( "-size" );

            using ( var cartController = ControllerFactory.MakeShoppingCartController() )
            {
                if ( ! currentCart.IsInitialized() )
                    currentCart.Initialize( cartController.NewCart() );

                int initialQuantity = 0;

                var cartView = cartController.ViewCart( currentCart.ObjectId );
                var cartItem = cartView.Items.Where( i => i.PizzaKindName == kind && i.Size == size ).SingleOrDefault();
                if ( cartItem != null )
                    initialQuantity = cartItem.Quantity;

                cartController.SetCartItem( currentCart.ObjectId, kind, size, initialQuantity + 1 );
            }
        }


        private EntityReference currentCart;
    }
}
