﻿using System;
using System.IO;

using Pizzario.Controller;


namespace Pizzario.Client.Console
{
    class DeliveryConfirmCommand : OrderCommand
    {
        public DeliveryConfirmCommand ( TextWriter output )
            :    base( "delivery.confirm", output )
        {
            AddSwitch( new CommandSwitch( "-payment", CommandSwitch.ValueMode.ExpectSingle, true ) );
        }


        public override void Execute ( CommandSwitchValues values )
        {
            int orderId = GetOrderId( values );

            decimal payment = 0.0M;
            if ( values.HasSwitch( "-payment" ) )
                payment = values.GetSwitchAsDecimal( "-payment" );

            using ( var orderController = ControllerFactory.MakeOrderController() )
            {
                orderController.ConfirmDelivery( orderId, payment );
            }
        }
    }
}
