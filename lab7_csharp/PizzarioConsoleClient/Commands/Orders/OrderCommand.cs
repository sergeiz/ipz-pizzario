﻿using System;
using System.IO;

using Pizzario.Controller;


namespace Pizzario.Client.Console
{
    abstract class OrderCommand : Command
    {
        protected OrderCommand ( string name, TextWriter output )
            :   base( name, output )
        {
            AddSwitch( new CommandSwitch( "-id", CommandSwitch.ValueMode.ExpectSingle, false ) );
        }


        protected int GetOrderId ( CommandSwitchValues values )
        {
            return values.GetSwitchAsInt( "-id" );
        }
    }
}
