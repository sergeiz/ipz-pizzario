﻿using System;
using System.IO;

using Pizzario.Controller;


namespace Pizzario.Client.Console
{
    class ClonePastOrderCommand : Command
    {
        public ClonePastOrderCommand ( TextWriter output, EntityReference currentCart )
            : base( "order.clone", output )
        {
            this.currentCart = currentCart;

            AddSwitch( new CommandSwitch( "-order_id", CommandSwitch.ValueMode.ExpectSingle, false ) );
        }


        public override void Execute ( CommandSwitchValues values )
        {
            int orderId = values.GetSwitchAsInt( "-order_id" );

            using ( var cartController = ControllerFactory.MakeShoppingCartController() )
            {
                int newCartId = cartController.CloneCartOfPastOrder( orderId );
                if ( currentCart.IsInitialized() )
                    cartController.DeleteCart( currentCart.ObjectId );

                currentCart.Initialize( newCartId );
            }
        }

        private EntityReference currentCart;
    }
}
