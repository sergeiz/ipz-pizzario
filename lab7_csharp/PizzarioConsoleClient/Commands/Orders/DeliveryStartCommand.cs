﻿using System;
using System.IO;

using Pizzario.Controller;


namespace Pizzario.Client.Console
{
    class DeliveryStartCommand : OrderCommand
    {
        public DeliveryStartCommand ( TextWriter output )
            : base( "delivery.start", output )
        {
        }


        public override void Execute ( CommandSwitchValues values )
        {
            int orderId = GetOrderId( values );

            using ( var orderController = ControllerFactory.MakeOrderController() )
            {
                orderController.DeliveryStarted( orderId );
            }
        }
    }
}
