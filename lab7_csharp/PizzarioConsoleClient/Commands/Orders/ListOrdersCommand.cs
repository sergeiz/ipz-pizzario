﻿using System;
using System.IO;

using Pizzario.Controller;


namespace Pizzario.Client.Console
{
    class ListOrdersCommand : Command
    {
        public ListOrdersCommand ( TextWriter output )
            :   base( "order.list", output )
        {
            AddSwitch( new CommandSwitch( "-status", CommandSwitch.ValueMode.ExpectSingle, true ) );
        }


        public override void Execute ( CommandSwitchValues values )
        {
            string status = values.GetOptionalSwitch( "-status" );

            using ( var orderController = ControllerFactory.MakeOrderController() )
            {
                if ( status == null )
                    ReportValues( orderController.ViewAllOrders() );
                else
                    ReportValues( orderController.ViewOrdersByStatus( status ) );
            }
        }
    }
}
