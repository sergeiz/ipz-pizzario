﻿using System;
using System.IO;

using Pizzario.Controller;


namespace Pizzario.Client.Console
{
    class OrderProgressCommand : OrderCommand
    {
        public OrderProgressCommand ( TextWriter output )
            : base( "order.progress", output )
        {
        }


        public override void Execute ( CommandSwitchValues values )
        {
            int orderId = GetOrderId( values );

            using ( var orderController = ControllerFactory.MakeOrderController() )
            {
                Output.WriteLine( orderController.ViewOrderProgress( orderId ) );
            }
        }
    }
}
