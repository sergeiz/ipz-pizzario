﻿using System;
using System.IO;

using Pizzario.Controller;


namespace Pizzario.Client.Console
{
    class CancelOrderCommand : OrderCommand
    {
        public CancelOrderCommand ( TextWriter output, EntityReference currentOrder )
            : base( "order.cancel", output )
        {
            this.currentOrder = currentOrder;
        }


        public override void Execute ( CommandSwitchValues values )
        {
            int orderId = GetOrderId( values );

            using ( var orderController = ControllerFactory.MakeOrderController() )
            {
                orderController.CancelOrder( orderId );

                if ( currentOrder.IsInitialized() )
                    currentOrder.Drop();
            }
        }


        private EntityReference currentOrder;
    }
}
