﻿using System;
using System.IO;

using Pizzario.Controller;

namespace Pizzario.Client.Console
{
    class ShowOrderCommand : OrderCommand
    {
        public ShowOrderCommand ( TextWriter output )
            :   base( "order.show", output )
        {
        }


        public override void Execute ( CommandSwitchValues values )
        {
            int orderId = GetOrderId( values );

            using ( var orderController = ControllerFactory.MakeOrderController() )
            {
                Output.WriteLine( orderController.ViewOrderContent( orderId ) );
            }
        }
    }
}
