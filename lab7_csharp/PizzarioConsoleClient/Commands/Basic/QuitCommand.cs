﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Pizzario.Client.Console
{
    class QuitCommand : Command
    {
        public QuitCommand ( TextWriter output )
            :   base( "quit", output )
        {
        }


        public override void Execute ( CommandSwitchValues values )
        {
            Output.WriteLine( "Good Bye!" );
            Environment.Exit( 0 );
        }
    }
}
