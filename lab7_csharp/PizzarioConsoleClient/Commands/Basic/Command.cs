﻿using System;
using System.Collections.Generic;
using System.IO;


namespace Pizzario.Client.Console
{
    abstract class Command
    {
        public string Name { get; private set; }


        protected Command ( string name, TextWriter output )
        {
            if ( name.Length == 0 )
                throw new Exception( "Command: empty name not allowed" );

            this.Name = name;
            this.output = output;

            this.switches = new Dictionary< string, CommandSwitch >();
        }


        public IEnumerable< string > SwitchNames
        {
            get { return switches.Keys; }
        }


        protected TextWriter Output
        {
            get
            {
                return output;
            }
        }


        protected void AddSwitch ( CommandSwitch sw )
        {
            if ( switches.ContainsKey( sw.Name ) )
                throw new Exception( "Command: duplicate switch" );

            switches.Add( sw.Name, sw );
        }


        public CommandSwitch FindSwitch ( string switchName )
        {
            CommandSwitch sw;
            if ( switches.TryGetValue( switchName, out sw ) )
                return sw;
            else
                return null;
        }

        
        protected void ReportValues< T > ( ICollection< T > values )
        {
            foreach ( var value in values )
                Output.WriteLine( value );

            Output.WriteLine( string.Format( "{0} total record(s).", values.Count ) );
        }


        abstract public void Execute ( CommandSwitchValues values );


        private TextWriter output;

        private IDictionary< string, CommandSwitch > switches;
    }
}
