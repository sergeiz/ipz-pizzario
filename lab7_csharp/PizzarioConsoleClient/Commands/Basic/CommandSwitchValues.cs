﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Pizzario.Client.Console
{
    class CommandSwitchValues
    {
        public CommandSwitchValues ()
        {
            this.values = new Dictionary<string, string>();
        }


        public bool HasSwitch ( string switchName )
        {
            return values.ContainsKey( switchName );
        }


        public void SetSwitch ( string switchName, string switchValue )
        {
            values.Add( switchName, switchValue );
        }


        public void AppendSwitch ( string switchName, string switchValue )
        {
            string existingValue;
            if ( values.TryGetValue( switchName, out existingValue ) )
            {
                string extendedValue = existingValue + ' ' + switchValue;
                values.Remove( switchName );
                values.Add( switchName, extendedValue );
            }
            else
                values.Add( switchName, switchValue );
        }


        public string GetSwitch ( string switchName )
        {
            string switchValue;
            if ( values.TryGetValue( switchName, out switchValue ) )
                return switchValue;

            else
                throw new Exception( "Switch " + switchName + " was not specified." );
        }


        public int GetSwitchAsInt ( string switchName )
        {
            string switchValue = GetSwitch( switchName );

            int result;
            if ( Int32.TryParse( switchValue, out result ) )
                return result;

            throw new Exception( "Switch " + switchName + " must have an integer value." );
        }


        public double GetSwitchAsDouble ( string switchName )
        {
            string switchValue = GetSwitch( switchName );

            double result;
            if ( Double.TryParse( switchValue, out result ) )
                return result;

            throw new Exception( "Switch " + switchName + " must have a real value." );
        }


        public decimal GetSwitchAsDecimal ( string switchName )
        {
            string switchValue = GetSwitch( switchName );

            decimal result;
            if ( Decimal.TryParse( switchValue, out result ) )
                return result;

            throw new Exception( "Switch " + switchName + " must have a decimal value." );
        }


        public bool GetSwitchAsBool ( string switchName )
        {
            string switchValue = GetSwitch( switchName );

            bool result;
            if ( Boolean.TryParse( switchValue, out result ) )
                return result;

            throw new Exception( "Switch " + switchName + " must have a value 'true' or 'false'." );
        }


        public string GetOptionalSwitch ( string switchName )
        {
            string switchValue;
            if ( values.TryGetValue( switchName, out switchValue ) )
                return switchValue;

            else
                return null;
        }


        private IDictionary< string, string > values;
    }
}
