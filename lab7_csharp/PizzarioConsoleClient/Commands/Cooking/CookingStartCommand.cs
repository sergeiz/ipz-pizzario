﻿using System;
using System.IO;

using Pizzario.Controller;

namespace Pizzario.Client.Console
{
    class CookingStartCommand : Command
    {
        public CookingStartCommand ( TextWriter output )
            :   base( "cooking.start", output )
        {
            AddSwitch( new CommandSwitch( "-pizza_id", CommandSwitch.ValueMode.ExpectSingle, false ) );
        }


        public override void Execute ( CommandSwitchValues values )
        {
            int pizzaId = values.GetSwitchAsInt( "-pizza_id" );

            using ( var pizzaController = ControllerFactory.MakePizzaController() )
            {
                pizzaController.CookingStarted( pizzaId );
            }
        }
    }
}
