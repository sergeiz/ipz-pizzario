﻿using System;
using System.IO;

using Pizzario.Controller;

namespace Pizzario.Client.Console
{
    class CookingFinishCommand : Command
    {
        public CookingFinishCommand ( TextWriter output )
            : base( "cooking.finish", output )
        {
            AddSwitch( new CommandSwitch( "-pizza_id", CommandSwitch.ValueMode.ExpectSingle, false ) );
        }


        public override void Execute ( CommandSwitchValues values )
        {
            int pizzaId = values.GetSwitchAsInt( "-pizza_id" );

            using ( var pizzaController = ControllerFactory.MakePizzaController() )
            {
                pizzaController.CookingFinished( pizzaId );
            }
        }
    }
}
