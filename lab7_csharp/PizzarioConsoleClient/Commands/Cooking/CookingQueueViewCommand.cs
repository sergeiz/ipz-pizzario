﻿using System;
using System.IO;

using Pizzario.Controller;

namespace Pizzario.Client.Console
{
    class CookingQueueViewCommand : Command
    {
        public CookingQueueViewCommand ( TextWriter output )
            :   base( "cooking.viewqueue", output )
        {
            AddSwitch( new CommandSwitch( "-cooking", CommandSwitch.ValueMode.Unexpected, true ) );
            AddSwitch( new CommandSwitch( "-waiting", CommandSwitch.ValueMode.Unexpected, true ) );
        }


        public override void Execute ( CommandSwitchValues values )
        {
            bool cooking = values.HasSwitch( "-cooking" );
            bool waiting = values.HasSwitch( "-waiting" );

            if ( !cooking && !waiting )
                throw new Exception( "Either -cooking or -waiting must be specified" );

            using ( var pizzaController = ControllerFactory.MakePizzaController() )
            {
                if ( cooking )
                {
                    Output.WriteLine( "=== Pizzas Being Cooked ===" );
                    ReportValues( pizzaController.ListPizzasBeingCooked() );
                }

                if ( waiting )
                {
                    Output.WriteLine( "=== Pizzas Waiting in Cooking Queue ===" );
                    ReportValues( pizzaController.ListPizzasWaitingForCooking() );
                }
            }
        }
    }
}
