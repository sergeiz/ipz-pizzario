﻿using System;
using System.IO;

using Pizzario.Controller;

namespace Pizzario.Client.Console
{
    class UpdateCustomerCommand : CustomerCommand
    {
        public UpdateCustomerCommand ( TextWriter output, EntityReference currentCustomer )
            : base( "customer.update", output, currentCustomer )
        {
            AddSwitch( new CommandSwitch( "-name",  CommandSwitch.ValueMode.ExpectSingle, true ) );
            AddSwitch( new CommandSwitch( "-email", CommandSwitch.ValueMode.ExpectSingle, true ) );
        }


        public override void Execute ( CommandSwitchValues values )
        {
            ValidateLogged();

            string newName  = values.GetOptionalSwitch( "-name" );
            string newEmail = values.GetOptionalSwitch( "-email" );

            using ( var customerController = ControllerFactory.MakeCustomerController() )
            {
                if ( newName != null )
                    customerController.ChangeName( CurrentCustomer.ObjectId, newName );

                if ( newEmail != null )
                    customerController.ChangeEmail( CurrentCustomer.ObjectId, newEmail );
            }
        }
    }
}
