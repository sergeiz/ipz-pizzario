﻿using System;
using System.IO;

using Pizzario.Controller;

namespace Pizzario.Client.Console
{
    class CustomerLoginCommand : Command
    {
        public CustomerLoginCommand ( TextWriter output, EntityReference currentCustomer )
            :   base( "customer.login", output )
        {
            this.currentCustomer = currentCustomer;

            AddSwitch( new CommandSwitch( "-email", CommandSwitch.ValueMode.ExpectSingle, false ) );
        }


        public override void Execute ( CommandSwitchValues values )
        {
            string email = values.GetSwitch( "-email" );

            using ( var customerController = ControllerFactory.MakeCustomerController() )
            {
                var customerView = customerController.FindCustomer( email );
                Output.WriteLine( "Hello " + customerView.FullName + "!" );
                currentCustomer.Initialize( customerView.CustomerId );
            }
        }


        private EntityReference currentCustomer;
    }
}
