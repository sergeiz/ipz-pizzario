﻿using System;
using System.IO;

using Pizzario.Controller;

namespace Pizzario.Client.Console
{
    class ShowCustomersCommand : Command
    {
        public ShowCustomersCommand ( TextWriter output )
            : base( "customer.showall", output )
        {
        }


        public override void Execute ( CommandSwitchValues values )
        {
            using ( var customerController = ControllerFactory.MakeCustomerController() )
            {
                var customers = customerController.ViewAllCustomers();
                ReportValues( customers );
            }
        }
    }
}
