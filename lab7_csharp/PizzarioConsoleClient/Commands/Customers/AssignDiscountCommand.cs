﻿using System;
using System.IO;

using Pizzario.Controller;


namespace Pizzario.Client.Console
{
    class AssignDiscountCommand : Command
    {
        public AssignDiscountCommand ( TextWriter output )
            : base( "customer.setdiscount", output )
        {
            AddSwitch( new CommandSwitch( "-customer_id", CommandSwitch.ValueMode.ExpectSingle, false ) );
            AddSwitch( new CommandSwitch( "-discount", CommandSwitch.ValueMode.ExpectSingle, false ) );
        }


        public override void Execute ( CommandSwitchValues values )
        {
            int customerId  = values.GetSwitchAsInt( "-customer_id" );
            double discount = values.GetSwitchAsDouble( "-discount" );

            using ( var customerController = ControllerFactory.MakeCustomerController() )
            {
                customerController.SetDiscount( customerId, discount );
            }
        }
    }
}
