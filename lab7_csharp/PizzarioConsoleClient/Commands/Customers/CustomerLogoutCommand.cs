﻿using System;
using System.IO;

using Pizzario.Controller;

namespace Pizzario.Client.Console
{
    class CustomerLogoutCommand : Command
    {
        public CustomerLogoutCommand ( TextWriter output, EntityReference currentCustomer )
            : base( "customer.logout", output )
        {
            this.currentCustomer = currentCustomer;
        }


        public override void Execute ( CommandSwitchValues values )
        {
            if ( currentCustomer.IsInitialized() )
            {
                Output.WriteLine( "Goodbye! " );
                currentCustomer.Drop();
            }

            else
                throw new Exception( "No customer logged in" );
        }


        private EntityReference currentCustomer;
    }
}
