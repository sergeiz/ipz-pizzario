﻿using System;
using System.IO;

using Pizzario.Controller;

namespace Pizzario.Client.Console
{
    class CreateCustomerCommand : CustomerCommand
    {
        public CreateCustomerCommand ( TextWriter output, EntityReference currentCustomer )
            : base( "customer.create", output, currentCustomer )
        {
            AddSwitch( new CommandSwitch( "-name",  CommandSwitch.ValueMode.ExpectSingle, false ) );
            AddSwitch( new CommandSwitch( "-email", CommandSwitch.ValueMode.ExpectSingle, false ) );
            AddSwitch( new CommandSwitch( "-addr",  CommandSwitch.ValueMode.ExpectSingle, false ) );
            AddSwitch( new CommandSwitch( "-phone", CommandSwitch.ValueMode.ExpectSingle, false ) );
        }


        public override void Execute ( CommandSwitchValues values )
        {
            string name  = values.GetSwitch( "-name"  );
            string email = values.GetSwitch( "-email" );
            string addr  = values.GetSwitch( "-addr"  );
            string phone = values.GetSwitch( "-phone" );

            using ( var customerController = ControllerFactory.MakeCustomerController() )
            {
                int customerID = customerController.NewCustomer( name, email, addr, phone );
                Output.WriteLine( "Welcome " + name + "!" );
                CurrentCustomer.Initialize( customerID );
            }
        }
    }
}
