﻿using System;
using System.IO;

using Pizzario.Controller;

namespace Pizzario.Client.Console
{
    class AttachPastOrderCommand : CustomerCommand
    {
        public AttachPastOrderCommand ( TextWriter output, EntityReference currentCustomer )
            : base( "customer.orders.attach", output, currentCustomer )
        {
            AddSwitch( new CommandSwitch( "-order_id", CommandSwitch.ValueMode.ExpectSingle, false ) );
        }


        public override void Execute ( CommandSwitchValues values )
        {
            ValidateLogged();
            int orderId = values.GetSwitchAsInt( "-order_id" );

            using ( var customerController = ControllerFactory.MakeCustomerController() )
            {
                customerController.AttachOrder( CurrentCustomer.ObjectId, orderId );
            }
        }
    }
}
