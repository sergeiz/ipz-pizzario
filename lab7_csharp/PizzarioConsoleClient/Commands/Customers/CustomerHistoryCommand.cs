﻿using System;
using System.IO;

using Pizzario.Controller;

namespace Pizzario.Client.Console
{
    class CustomerHistoryCommand : CustomerCommand
    {
        public CustomerHistoryCommand ( TextWriter output, EntityReference currentCustomer )
            :   base( "customer.history", output, currentCustomer )
        {
        }


        public override void Execute ( CommandSwitchValues values )
        {
            ValidateLogged();

            using ( var customerController = ControllerFactory.MakeCustomerController() )
            {
                var pastOrders = customerController.FindPastOrders( CurrentCustomer.ObjectId );
                ReportValues( pastOrders );
            }
        }
    }
}
