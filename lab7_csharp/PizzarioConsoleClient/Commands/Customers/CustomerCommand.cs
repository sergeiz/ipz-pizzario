﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Pizzario.Client.Console
{
    abstract class CustomerCommand : Command
    {
        protected CustomerCommand ( string name, TextWriter output, EntityReference currentCustomer )
            :   base( name, output )
        {
            this.CurrentCustomer = currentCustomer;
        }


        protected void ValidateLogged ()
        {
            if ( ! CurrentCustomer.IsInitialized() )
                throw new Exception( "No customer logged in" );
        }

        protected EntityReference CurrentCustomer { get; private set; }
    }
}
