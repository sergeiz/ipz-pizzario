﻿using System;
using System.IO;

using Pizzario.Controller;


namespace Pizzario.Client.Console
{
    class PayWithCardCommand : Command
    {
        public PayWithCardCommand ( TextWriter output, EntityReference currentOrder )
            : base( "pay.card", output )
        {
            AddSwitch( new CommandSwitch( "-code",  CommandSwitch.ValueMode.ExpectSingle, false ) );
            AddSwitch( new CommandSwitch( "-holder", CommandSwitch.ValueMode.ExpectSingle, false ) );

            this.currentOrder = currentOrder;
        }


        public override void Execute ( CommandSwitchValues values )
        {
            if ( ! currentOrder.IsInitialized() )
                throw new Exception( "No open order to pay for..." );

            string cardCode = values.GetSwitch( "-code" );
            string cardHolder = values.GetSwitch( "-holder" );

            using ( var orderController = ControllerFactory.MakeOrderController() )
            {
                orderController.DefineWireTransferPaymentPlan( currentOrder.ObjectId, cardCode, cardHolder );
                currentOrder.Drop();
            }
        }


        private EntityReference currentOrder;
    }
}
