﻿using System;
using System.IO;

using Pizzario.Controller;


namespace Pizzario.Client.Console
{
    class PayWithCashCommand : Command
    {
        public PayWithCashCommand ( TextWriter output, EntityReference currentOrder )
            : base( "pay.cash", output )
        {
            this.currentOrder = currentOrder;
        }


        public override void Execute ( CommandSwitchValues values )
        {
            if ( ! currentOrder.IsInitialized() )
                throw new Exception( "No open order to pay for..." );

            using ( var orderController = ControllerFactory.MakeOrderController() )
            {
                orderController.DefineCashPaymentPlan( currentOrder.ObjectId );
                currentOrder.Drop();
            }
        }


        private EntityReference currentOrder;
    }
}
