﻿using System;
using System.IO;

using Pizzario.Controller;


namespace Pizzario.Client.Console
{
    public class Program
    {
        public static void Main ( string[] args )
        {
            GenerateInitialContent();
            RunCommandProcessingLoop( System.Console.In, System.Console.Out );
        }


        private static void GenerateInitialContent ()
        {
            using ( var menuController = ControllerFactory.MakeMenuController() )
            {
                AddCarbonaraToMenu( menuController );
                AddMilanoToMenu( menuController );
                AddRegginoToMenu( menuController );
            }

            using ( var customerController = ControllerFactory.MakeCustomerController() )
            {
                customerController.NewCustomer( 
                    "Ivan Ivanov", 
                    "ivanov@nure.ua",
                    "Lenin av. 14",
                    "057-702-13-26"
                );
            }
        }


        private static void AddCarbonaraToMenu ( IMenuController menuController )
        {
            menuController.CreateNewPizzaKind(
                "Carbonara",
                "Carbonara Description",
                "http://pizzario.com.ua/images/carbonara.jpg"
            );

            menuController.SetIngredient( "Carbonara", "Cheese", 100 );
            menuController.SetIngredient( "Carbonara", "Olive Oil", 30 );
            menuController.SetIngredient( "Carbonara", "Mushrooms", 30 );

            menuController.UpdatePrice( "Carbonara", "Small", 3.00M );
            menuController.UpdatePrice( "Carbonara", "Medium", 5.00M );
            menuController.UpdatePrice( "Carbonara", "Large", 7.00M );

            menuController.EnableShowingPizzaToCustomers( "Carbonara" );
        }


        private static void AddMilanoToMenu ( IMenuController menuController )
        {
            menuController.CreateNewPizzaKind(
               "Milano",
               "Milano Description",
               "http://pizzario.com.ua/images/milano.jpg"
            );

            menuController.SetIngredient( "Milano", "Ham", 100 );
            menuController.SetIngredient( "Milano", "Mushrooms", 50 );
            menuController.SetIngredient( "Milano", "Tomatoes", 50 );

            menuController.UpdatePrice( "Milano", "Small", 2.50M );
            menuController.UpdatePrice( "Milano", "Medium", 4.15M );
            menuController.UpdatePrice( "Milano", "Large", 5.80M );

            menuController.EnableShowingPizzaToCustomers( "Milano" );
        }


        private static void AddRegginoToMenu ( IMenuController menuController )
        {
            menuController.CreateNewPizzaKind(
               "Reggino",
               "Reggion Description",
               "http://pizzario.com.ua/images/reggino.jpg"
            );

            menuController.SetIngredient( "Reggino", "Ham", 100 );
            menuController.SetIngredient( "Reggino", "Chicken", 80 );
            menuController.SetIngredient( "Reggino", "Olives", 50 );

            // Note: no prices, not added to visible area
        }


        private static void RunCommandProcessingLoop ( TextReader input, TextWriter output )
        {
            CommandHandler commandHandler = new CommandHandler();

            EntityReference currentCart     = new EntityReference();
            EntityReference currentOrder    = new EntityReference();
            EntityReference currentCustomer = new EntityReference();

            InitCommands( commandHandler, output, currentCart, currentOrder, currentCustomer );

            while ( true )
            {
                output.Write( "> " );
                string command = input.ReadLine();

                try
                {
                    commandHandler.ProcessCommandLine( command );
                }
                catch ( Exception e )
                {
                    output.WriteLine( 
                        string.Format( 
                            "Error: {0}\n{1}\n", 
                            e.Message, 
                            ( e.InnerException != null ) ? e.InnerException.Message : "" 
                        )
                    );
                }
            }
        }


        private static void InitCommands (
            CommandHandler handler, 
            TextWriter output, 
            EntityReference currentCart,
            EntityReference currentOrder,
            EntityReference currentCustomer
        )
        {
            handler.RegisterCommand( new HelpCommand( handler, output ) );
            handler.RegisterCommand( new QuitCommand( output ) );

            handler.RegisterCommand( new ShowMenuCommand( output ) );
            handler.RegisterCommand( new ShowPizzaCommand( output ) );
            handler.RegisterCommand( new ShowSimilarPizzaCommand( output ) );
            handler.RegisterCommand( new SuggestPizzaCommand( output ) );
            handler.RegisterCommand( new ShowRecipeCommand( output ) );
            handler.RegisterCommand( new ShowPopularPizzasCommand( output ) );
            handler.RegisterCommand( new CreatePizzaKindCommand( output ) );
            handler.RegisterCommand( new UpdatePizzaCommand( output ) );
            handler.RegisterCommand( new EditRecipeCommand( output ) );
            handler.RegisterCommand( new EditPricesCommand( output ) );
            handler.RegisterCommand( new ChangePizzaVisibilityCommand( output ) );
            handler.RegisterCommand( new RatePizzaCommand( output ) );

            handler.RegisterCommand( new ShowCustomersCommand( output ) );
            handler.RegisterCommand( new CustomerLoginCommand( output, currentCustomer ) );
            handler.RegisterCommand( new CustomerLogoutCommand( output, currentCustomer ) );
            handler.RegisterCommand( new CreateCustomerCommand( output, currentCustomer ) );
            handler.RegisterCommand( new UpdateCustomerCommand( output, currentCustomer ) );
            handler.RegisterCommand( new CustomerHistoryCommand( output, currentCustomer ) );
            handler.RegisterCommand( new AttachPastOrderCommand( output, currentCustomer ) );
            handler.RegisterCommand( new AssignDiscountCommand( output ) );

            handler.RegisterCommand( new ShowCartCommand( output, currentCart ) );
            handler.RegisterCommand( new PushToCartCommand( output, currentCart ) );
            handler.RegisterCommand( new PopFromCartCommand( output, currentCart ) );
            handler.RegisterCommand( new ClearCartCommand( output, currentCart ) );
            handler.RegisterCommand( new CheckoutCartCommand( output, currentCart, currentOrder, currentCustomer ) );
            handler.RegisterCommand( new ClonePastOrderCommand( output, currentCart ) );

            handler.RegisterCommand( new PayWithCashCommand( output, currentOrder ) );
            handler.RegisterCommand( new PayWithCardCommand( output, currentOrder ) );
            handler.RegisterCommand( new ListOrdersCommand( output ) );
            handler.RegisterCommand( new ShowOrderCommand( output ) );
            handler.RegisterCommand( new OrderProgressCommand( output ) );
            handler.RegisterCommand( new CancelOrderCommand( output, currentOrder ) );
            handler.RegisterCommand( new DeliveryStartCommand( output ) );
            handler.RegisterCommand( new DeliveryConfirmCommand( output ) );

            handler.RegisterCommand( new StatsByPizzaCommand( output ) );
            handler.RegisterCommand( new StatsByIngredientCommand( output ) );

            handler.RegisterCommand( new CookingQueueViewCommand( output ) );
            handler.RegisterCommand( new CookingStartCommand( output ) );
            handler.RegisterCommand( new CookingFinishCommand( output ) );

            handler.RegisterCommand( new ShowRefundingRequestsCommand( output ) );
            handler.RegisterCommand( new ConfirmRefundingCommand( output ) );
            handler.RegisterCommand( new RejectRefundingCommand( output ) );
        }
    }
}
