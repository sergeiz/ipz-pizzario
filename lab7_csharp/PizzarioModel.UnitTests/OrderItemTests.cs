﻿using Pizzario.Model;

using NUnit.Framework;
using System;

namespace Pizzario.Model.UnitTests
{
    [ TestFixture ]
    public class OrderItemTests
    {

        [ Test ]
        public void Constructor_ProjectsFields_Correctly ()
        {
            PizzaKind kind = makePizzaKind();
            OrderItem item = new OrderItem( kind, PizzaSize.Small, 3.0M, 2 );

            Assert.AreSame( item.Kind, kind );
            Assert.AreEqual( item.Size, PizzaSize.Small );
            Assert.AreEqual( item.FixedPrice, 3.0M );
            Assert.AreEqual( item.Quantity, 2 );
        }


        [ Test ]
        public void CopyConstructor_PropertiesCopiedCorrectly ()
        {
            PizzaKind kind = makePizzaKind();
            OrderItem item = new OrderItem( kind, PizzaSize.Small, 3.0M, 2 );
            OrderItem itemCopy = new OrderItem( item );

            Assert.AreSame ( itemCopy.Kind,       item.Kind       );
            Assert.AreEqual( itemCopy.Size,       item.Size       );
            Assert.AreEqual( itemCopy.FixedPrice, item.FixedPrice );
            Assert.AreEqual( itemCopy.Quantity,   item.Quantity   );

        }


        [ Test ]
        public void Constructor_NegativePrice_Forbidden ()
        {
            Assert.Throws< Exception >( () => new OrderItem( makePizzaKind(), PizzaSize.Small, -0.01M, 1 ) );
            Assert.DoesNotThrow( () => new OrderItem( makePizzaKind(), PizzaSize.Small, 0.00M, 1 ) );
        }


        [ Test ]
        public void Constructor_NonPositiveQuantity_Forbidden ()
        {
            Assert.Throws< Exception >( () => new OrderItem( makePizzaKind(), PizzaSize.Small, 0.01M, -1 ) );
            Assert.Throws< Exception >( () => new OrderItem( makePizzaKind(), PizzaSize.Small, 0.01M, 0 ) );
        }


        [ Test ]
        public void Cost_SingleItem_MatchesPrice ()
        {
            OrderItem item = new OrderItem( makePizzaKind(), PizzaSize.Small, 3.0M, 1 );

            Assert.AreEqual( item.Cost, item.FixedPrice );
        }


        [ Test ]
        public void Cost_NItems_MultipliesPrice ()
        {
            OrderItem item = new OrderItem( makePizzaKind(), PizzaSize.Small, 3.0M, 8 );

            Assert.AreEqual( item.Cost, item.FixedPrice * 8 );
        }


        [ Test ]
        public void Quantity_Update_Good ()
        {
            OrderItem item = new OrderItem( makePizzaKind(), PizzaSize.Small, 3.0M, 3 );
            item.SetQuantity( 5 );

            Assert.AreEqual( item.Quantity, 5 );
        }


        [ Test ]
        public void Quantity_Update_Bad ()
        {
            OrderItem item = new OrderItem( makePizzaKind(), PizzaSize.Small, 3.0M, 3 );
            Assert.Throws< Exception >( () => item.SetQuantity( 0  ) );
            Assert.Throws< Exception >( () => item.SetQuantity( -1 ) );
        }


        private PizzaKind makePizzaKind ()
        {
            return new PizzaKind( "Carbonara" );
        }
    }
}
