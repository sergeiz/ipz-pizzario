﻿using Pizzario.Model;

using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace Pizzario.Model.UnitTests
{
    [ TestFixture ]
    public class OrderTests
    {

        [ Test ]
        public void Constructor_ProjectsProperties_Correctly ()
        {
            ShoppingCart cart = new ShoppingCart();
            cart.AddItem( makeSomeItem() );
            cart.Lock();

            CustomerContact contact = new CustomerContact();
            Order o = new Order( cart, contact );

            Assert.AreEqual( o.BasicCost, cart.Cost );
            Assert.AreSame( o.Contact, contact );
        }


        [ Test ]
        public void Constructor_FromUnlockedCart_Forbidden ()
        {
            ShoppingCart cart = new ShoppingCart();
            cart.AddItem( makeSomeItem() );

            Assert.Throws< Exception >( () => makeOrder( cart ) );
        }


        [ Test ]
        public void Constructor_WithLockedCart_Passes ()
        {
            ShoppingCart cart = new ShoppingCart();
            cart.AddItem( makeSomeItem() );
            cart.Lock();

            Assert.DoesNotThrow( () => makeOrder( cart ) );
        }


        [ Test ]
        public void Constructor_PizzasCounterInitially_AsSpecifiedInCart ()
        {
            ShoppingCart cart = new ShoppingCart();
            cart.AddItem( makeSomeItem( 3 ) );
            cart.Lock();

            Order o = makeOrder( cart );

            Assert.AreEqual( o.RemainingPizzasToCook, 3 );
        }


        [ Test ]
        public void TotalCost_WithoutDiscount_MatchesCartCost ()
        {
            Order o = makeOrder();
            Assert.AreEqual( o.TotalCost, o.BasicCost );
        }


        [ Test ]
        public void TotalCost_WithDiscount_ReducesCartCost ()
        {
            Order o = makeOrder();
            o.SetDiscount( new Discount( 0.5 ) );

            Assert.AreEqual( o.TotalCost, o.BasicCost / 2 );
        }


        [ Test ]
        public void TotalCost_WithFullDiscount_MakesOrderFree ()
        {
            Order o = makeOrder();
            o.SetDiscount( new Discount( 1.0 ) );

            Assert.AreEqual( o.TotalCost, 0 );
        }


        [ Test ]
        public void Discount_Initially_Zero ()
        {
            Order o = makeOrder();

            Assert.AreEqual( o.AssignedDiscount.Value, 0.0 );
        }


        [ Test ]
        public void Discount_SetValid_ReadBack ()
        {
            Discount d = new Discount( 0.5 );
            Order o = makeOrder();
            o.SetDiscount( d );

            Assert.AreSame( o.AssignedDiscount, d );
        }


        [ Test ]
        public void Discount_SetNull_Forbidden ()
        {
            Order o = makeOrder();
            Assert.Throws< Exception >( () => o.SetDiscount( null ) );
        }


        [ Test ]
        public void Pizzas_ForSingleItem_SingleRemaining ()
        {
            ShoppingCart cart = new ShoppingCart();
            cart.AddItem( makeSomeItem( 1 ) );
            cart.Lock();
            Order o = makeOrder( cart );

            Assert.AreEqual( o.RemainingPizzasToCook, 1 );
        }


        [ Test ]
        public void Pizzas_ForDoubleItem_TwoRemaining ()
        {
            ShoppingCart cart = new ShoppingCart();
            cart.AddItem( makeSomeItem( 2 ) );
            cart.Lock();
            Order o = makeOrder( cart );

            Assert.AreEqual( o.RemainingPizzasToCook, 2 );
        }


        [ Test ]
        public void Pizzas_ForTwoSingleItems_TwoRemaining ()
        {
            ShoppingCart cart = new ShoppingCart();
            cart.AddItem( makeSomeItem( 1 ) );
            cart.AddItem( makeSomeOtherItem( 1 ) );
            cart.Lock();
            Order o = makeOrder( cart );

            Assert.AreEqual( o.RemainingPizzasToCook, 2 );
        }


        [ Test ]
        public void Pizzas_ForOneSingleDoubleOther_ThreeRemaining ()
        {
            ShoppingCart cart = new ShoppingCart();
            cart.AddItem( makeSomeItem( 1 ) );
            cart.AddItem( makeSomeOtherItem( 2 ) );
            cart.Lock();
            Order o = makeOrder( cart );

            Assert.AreEqual( o.RemainingPizzasToCook, 3 );
        }


        [ Test ]
        public void Status_Initially_Registered ()
        {
            Order o = makeOrder();
            Assert.AreEqual( o.Status, OrderStatus.Registered );
        }


        [ Test ]
        public void Status_DefinePlan_PaymentPlanDefined ()
        {
            Order o = makeOrder();
            o.DefinePaymentPlan( new CashPaymentPlan() );
            Assert.AreEqual( o.Status, OrderStatus.Waiting );
        }


        [ Test ]
        public void Status_AfterSinglePizzaReady_Ready4Delivery ()
        {
            Order o = makeOrderAndPaymentPlan();
            CookAllPizzas( o );

            Assert.AreEqual( o.Status, OrderStatus.Ready );
        }


        [ Test ]
        public void Status_AfterSinglePizzaStarted_Cooking ()
        {
            Order o = makeOrderAndPaymentPlan();
            int nRemaining = o.RemainingPizzasToCook;

            o.OnStartedCookingOneOfPizzas();

            Assert.AreEqual( o.Status, OrderStatus.Cooking );
            Assert.AreEqual( o.RemainingPizzasToCook, nRemaining );
        }


        [ Test ]
        public void Status_AfterOneOfTwoStarted_Cooking ()
        {
            ShoppingCart cart = new ShoppingCart();
            cart.AddItem( makeSomeItem( 2 ) );
            cart.Lock();
            Order o = makeOrderAndPaymentPlan( cart );
            int nRemaining = o.RemainingPizzasToCook;

            o.OnStartedCookingOneOfPizzas();

            Assert.AreEqual( o.Status, OrderStatus.Cooking );
            Assert.AreEqual( o.RemainingPizzasToCook, nRemaining );
        }


        [ Test ]
        public void Status_AfterOneOfTwoReady_Cooking()
        {
            ShoppingCart cart = new ShoppingCart();
            cart.AddItem( makeSomeItem( 2 ) );
            cart.Lock();
            Order o = makeOrderAndPaymentPlan( cart );

            o.OnStartedCookingOneOfPizzas();
            o.OnFinishedCookingOneOfPizzas();

            Assert.AreEqual( o.Status, OrderStatus.Cooking );
            Assert.AreEqual( o.RemainingPizzasToCook, 1 );
        }


        [ Test ]
        public void Status_AfterBothReady_Ready4Delivery ()
        {
            ShoppingCart cart = new ShoppingCart();
            cart.AddItem( makeSomeItem( 2 ) );
            cart.Lock();
            Order o = makeOrderAndPaymentPlan( cart );

            o.OnStartedCookingOneOfPizzas();
            o.OnFinishedCookingOneOfPizzas();
            o.OnStartedCookingOneOfPizzas();
            o.OnFinishedCookingOneOfPizzas();

            Assert.AreEqual( o.Status, OrderStatus.Ready );
            Assert.AreEqual( o.RemainingPizzasToCook, 0 );
        }


        [ Test ]
        public void Status_AfterStartedDelivery_Delivering ()
        {
            Order o = makeOrderAndPaymentPlan();
            CookAllPizzas( o );

            o.OnStartedDelivery();

            Assert.AreEqual( o.Status, OrderStatus.Delivering );
        }


        [ Test ]
        public void Status_AfterDelivery_Delivered ()
        {
            Order o = makeOrderAndPaymentPlan();
            CookAllPizzas( o );
            o.OnStartedDelivery();

            o.OnDelivered( o.TotalCost );

            Assert.AreEqual( o.Status, OrderStatus.Delivered );
        }


        [ Test ]
        public void StartOrFinishDelivery_WhenRegisteredOrder_Forbidden ()
        {
            Order o = makeOrder();

            Assert.Throws< Exception >( () => o.OnStartedDelivery() );
            Assert.Throws< Exception >( () => o.OnDelivered( o.TotalCost ) );
        }


        [ Test ]
        public void StartOrFinishDelivery_WhenPaymentPlanDefined_Forbidden ()
        {
            Order o = makeOrderAndPaymentPlan();

            Assert.Throws< Exception >( () => o.OnStartedDelivery() );
            Assert.Throws< Exception >( () => o.OnDelivered( o.TotalCost ) );
        }


        [ Test ]
        public void StartOrFinishDelivery_WhenStillCookingOrder_Forbidden ()
        {
            ShoppingCart cart = new ShoppingCart();
            cart.AddItem( makeSomeItem( 2 ) );
            cart.Lock();
            Order o = makeOrderAndPaymentPlan( cart );

            o.OnStartedCookingOneOfPizzas();

            Assert.Throws< Exception >( () => o.OnStartedDelivery() );
            Assert.Throws< Exception >( () => o.OnDelivered( o.TotalCost ) );
        }


        [ Test ]
        public void StartDelivery_WhenAlreadyStarted_Forbidden ()
        {
            Order o = makeOrderAndPaymentPlan();
            CookAllPizzas( o );
            o.OnStartedDelivery();

            Assert.Throws< Exception >( () => o.OnStartedDelivery() );
        }


        [ Test ]
        public void StartDelivery_WhenDelivered_Forbidden ()
        {
            Order o = makeOrderAndPaymentPlan();
            CookAllPizzas( o );
            o.OnStartedDelivery();
            o.OnDelivered( o.TotalCost );

            Assert.Throws< Exception >( () => o.OnStartedDelivery() );
        }


        [ Test ]
        public void FinishDelivery_WhenDeliveryNotStarted_Forbidden ()
        {
            Order o = makeOrderAndPaymentPlan();
            CookAllPizzas( o );

            Assert.Throws< Exception >( () => o.OnDelivered( o.TotalCost ) );
        }


        [ Test ]
        public void FinishDelivery_WhenAlreadyDelivered_Forbidden ()
        {
            Order o = makeOrderAndPaymentPlan();
            CookAllPizzas( o );
            o.OnStartedDelivery();

            o.OnDelivered( o.TotalCost );

            Assert.Throws< Exception >( () => o.OnDelivered( o.TotalCost ) );
        }


        [ Test ]
        public void Cancel_RegisteredOrder_Passes ()
        {
            ShoppingCart cart = new ShoppingCart();
            cart.AddItem( makeSomeItem( 2 ) );
            cart.Lock();
            Order o = makeOrder( cart );

            o.Cancel();

            Assert.AreEqual( o.Status, OrderStatus.Cancelled );
            Assert.AreEqual( o.RemainingPizzasToCook, 2 );
        }


        [ Test ]
        public void Cancel_PaymentPlannedOrder_Passes ()
        {
            ShoppingCart cart = new ShoppingCart();
            cart.AddItem( makeSomeItem( 2 ) );
            cart.Lock();
            Order o = makeOrderAndPaymentPlan( cart );

            o.Cancel();

            Assert.AreEqual( o.Status, OrderStatus.Cancelled );
            Assert.AreEqual( o.RemainingPizzasToCook, 2 );
        }


        [ Test ]
        public void Cancel_CookingOrder_Passes_NoEffectOnCounter ()
        {
            ShoppingCart cart = new ShoppingCart();
            cart.AddItem( makeSomeItem( 3 ) );
            cart.Lock();
            Order o = makeOrderAndPaymentPlan( cart );

            o.OnStartedCookingOneOfPizzas();
            o.OnStartedCookingOneOfPizzas();
            o.OnFinishedCookingOneOfPizzas();

            o.Cancel();

            Assert.AreEqual( o.Status, OrderStatus.Cancelled );
            Assert.AreEqual( o.RemainingPizzasToCook, 2 );
        }


        [ Test ]
        public void Cancel_ReadyOrder_Passes_NoEffectOnCounter ()
        {
            ShoppingCart cart = new ShoppingCart();
            cart.AddItem( makeSomeItem( 2 ) );
            cart.Lock();
            Order o = makeOrderAndPaymentPlan( cart );
            CookAllPizzas( o );

            o.Cancel();

            Assert.AreEqual( o.Status, OrderStatus.Cancelled );
            Assert.AreEqual( o.RemainingPizzasToCook, 0 );
        }


        [ Test ]
        public void Cancel_OrderThatLeftKitchen_Forbidden ()
        {
            Order o = makeOrderAndPaymentPlan();
            CookAllPizzas( o );
            
            o.OnStartedDelivery();

            Assert.Throws< Exception >( () => o.Cancel() );

            o.OnDelivered( o.TotalCost );

            Assert.Throws< Exception >( () => o.Cancel() );
        }


        [ Test ]
        public void Cancel_AlreadyCancelledOrder_Forbidden ()
        {
            Order o = makeOrder();
            o.Cancel();

            Assert.Throws< Exception >( () => o.Cancel() );
        }


        [ Test ]
        public void Payment_CashNotCollectedOnDelivery_Forbidden ()
        {
            Order o = makeOrderAndPaymentPlan();
            CookAllPizzas( o );
            o.OnStartedDelivery();

            Assert.Throws< Exception >( () => o.OnDelivered( 0.0M ) );
            Assert.Throws< Exception >( () => o.OnDelivered( -1.0M ) );
            Assert.Throws< Exception >( () => o.OnDelivered( o.TotalCost * 0.99M ) );
        }


        [ Test ]
        public void Payment_CashCollectedOnDeliveryExceeded_Ok ()
        {
            Order o = makeOrderAndPaymentPlan();
            CookAllPizzas( o );
            o.OnStartedDelivery();
            Assert.DoesNotThrow( ()=> o.OnDelivered( o.TotalCost * 1.01M ) );
        }


        [ Test ]
        public void Payment_WhenPrepaymentExpected_CannotDefinePlanWithoutPayment ()
        {
            Order order = makeOrder();
            PaymentPlan plan = makeWireTransferPlan();
            Assert.Throws <Exception >( () => order.DefinePaymentPlan( plan ) );
        }

        
        [ Test ]
        public void Payment_WhenPrepaymentExpected_DefinePlanSucceedsWhenPayed ()
        {
            Order order = makeOrder();
            PaymentPlan plan = makeWireTransferPlan();
            plan.MarkPayed();
            Assert.DoesNotThrow( () => order.DefinePaymentPlan( plan ) );
        }


        [ Test ]
        public void Payment_WhenPrepaymentUnexpected_DefinePlanSucceedsAnyway ()
        {
            Order order1 = makeOrder();
            Order order2 = makeOrder();

            PaymentPlan plan1 = new CashPaymentPlan();
            PaymentPlan plan2 = new CashPaymentPlan();
            plan1.MarkPayed();

            Assert.DoesNotThrow( () => order1.DefinePaymentPlan( plan1 ) );
            Assert.DoesNotThrow( () => order2.DefinePaymentPlan( plan2 ) );
        }


        [ Test ]
        public void Refundability_Okay_ForCancelledWithRefundablePayment ()
        {
            Order order = makeOrder();
            PaymentPlan plan = makeWireTransferPlan();
            plan.MarkPayed();
            order.DefinePaymentPlan( plan );

            order.Cancel();

            Assert.True( order.MayBeRefunded() );
        }


        [ Test ]
        public void Refundability_NotPossible_ForRegisteredOrder ()
        {
            Order order = makeOrder();
            Assert.True( ! order.MayBeRefunded() );
        }


        [ Test ]
        public void Refundability_NotPossible_ForOrderWithDefinedPaymentPlan ()
        {
            Order order = makeOrderAndPaymentPlan();
            Assert.True( ! order.MayBeRefunded() );
        }


        [ Test ]
        public void Refundability_NotPossible_ForOrderBeingCooked ()
        {
            Order order = makeOrderAndPaymentPlan();
            order.OnStartedCookingOneOfPizzas();

            Assert.True( ! order.MayBeRefunded() );
        }


        [ Test ]
        public void Refundability_NotPossible_ForOrderReady4Devliery ()
        {
            Order order = makeOrderAndPaymentPlan();
            CookAllPizzas( order );

            Assert.True( ! order.MayBeRefunded() );
        }


        [ Test ]
        public void Refundability_NotPossible_ForOrderWithDeliveryInProgress ()
        {
            Order order = makeOrderAndPaymentPlan();
            CookAllPizzas( order );
            order.OnStartedDelivery();

            Assert.True( ! order.MayBeRefunded() );
        }


        [ Test ]
        public void Refundability_NotPossible_ForDeliveredOrder ()
        {
            Order order = makeOrderAndPaymentPlan();
            CookAllPizzas( order );
            order.OnStartedDelivery();
            order.OnDelivered( order.TotalCost );

            Assert.True( ! order.MayBeRefunded() );
        }


        [ Test ]
        public void Refundability_NotPossible_ForCashOrderEvenIfPayed ()
        {
            Order order = makeOrder();
            order.DefinePaymentPlan( new CashPaymentPlan() );
            order.PayPlan.MarkPayed();
            order.Cancel();

            Assert.True( ! order.MayBeRefunded() );
        }


        private static Order makeOrder ()
        {
            ShoppingCart cart = new ShoppingCart();
            cart.AddItem( makeSomeItem() );
            cart.Lock();

            return makeOrder( cart );
        }


        private static Order makeOrder ( ShoppingCart cart )
        {
            return new Order( cart, new CustomerContact() );
        }


        private static Order makeOrderAndPaymentPlan ()
        {
            Order order = makeOrder();
            order.DefinePaymentPlan( new CashPaymentPlan() );
            return order;
        }


        private static Order makeOrderAndPaymentPlan ( ShoppingCart cart )
        {
            Order order = makeOrder( cart );
            order.DefinePaymentPlan( new CashPaymentPlan() );
            return order;
        }


        private static OrderItem makeSomeItem ( int quantity = 1 )
        {
            return new OrderItem( new PizzaKind( "some" ), PizzaSize.Small, 1.0M, quantity );
        }


        private static OrderItem makeSomeOtherItem ( int quantity = 1 )
        {
            return new OrderItem( new PizzaKind( "other" ), PizzaSize.Medium, 2.0M, quantity );
        }


        private static PaymentPlan makeWireTransferPlan ()
        {
            return new WireTransferPaymentPlan( "1234 5678 1234 5678", "Ivan Ivanov" );
        }


        private static void CookAllPizzas ( Order o  )
        {
            while ( o.RemainingPizzasToCook > 0 )
            {
                o.OnStartedCookingOneOfPizzas();
                o.OnFinishedCookingOneOfPizzas();
            }
        }
    }
}
