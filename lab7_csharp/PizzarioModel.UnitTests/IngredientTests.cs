﻿using Pizzario.Model;

using NUnit.Framework;
using System;

namespace Pizzario.Model.UnitTests
{
    [ TestFixture ]
    public class IngredientTests
    {
        [ Test ]
        public void Constructor_ReturnsGivenName ()
        {
            Ingredient ing = new Ingredient( "Meat" );
            Assert.AreEqual( ing.Name, "Meat" );
        }


        [ Test ]
        public void Constructor_RejectsEmptyName ()
        {
            Assert.Throws< Exception >( () => new Ingredient( "" ) );
        }


        [ Test ]
        public void Weights_DefaultZero_AllSizes ()
        {
            Ingredient ing = new Ingredient( "Meat" );

            Assert.AreEqual( ing.GetWeight( PizzaSize.Small  ), 0 );
            Assert.AreEqual( ing.GetWeight( PizzaSize.Medium ), 0 );
            Assert.AreEqual( ing.GetWeight( PizzaSize.Large  ), 0 );
        }


        [ Test ]
        public void Weights_AssignSmallest_OthersProportional ()
        {
            Ingredient ing = new Ingredient( "Meat" );
            ing.DefineWeights( 10 );

            Assert.AreEqual( ing.GetWeight( PizzaSize.Small  ), 10 );
            Assert.AreEqual( ing.GetWeight( PizzaSize.Medium ), 20 );
            Assert.AreEqual( ing.GetWeight( PizzaSize.Large  ), 30 );
        }


        [ Test ]
        public void Weights_AssignNegative_Forbidden ()
        {
            Ingredient ing = new Ingredient( "Meat" );
            
            Assert.Throws< Exception >( () => ing.DefineWeights( -1 ) );
        }


        [ Test ]
        public void Weights_AssignZero_OK ()
        {
            Ingredient ing = new Ingredient( "Meat" );
            ing.DefineWeights( 0 );

            Assert.AreEqual( ing.GetWeight( PizzaSize.Small  ), 0 );
            Assert.AreEqual( ing.GetWeight( PizzaSize.Medium ), 0 );
            Assert.AreEqual( ing.GetWeight( PizzaSize.Large  ), 0 );
        }
    }
}
