﻿using Pizzario.Model;

using NUnit.Framework;
using System;

namespace Pizzario.Model.UnitTests
{
    [ TestFixture ]
    public class PizzaRecipeTests
    {
        [ Test ]
        public void Ingredients_Initially_None ()
        {
            PizzaRecipe recipe = new PizzaRecipe();

            CollectionAssert.IsEmpty( recipe.Ingredients );
        }
        

        [ Test ]
        public void Ingredients_AskMissingInEmpty ()
        {
            PizzaRecipe recipe = new PizzaRecipe();

            CollectionAssert.IsEmpty( recipe.Ingredients );
            Assert.False( recipe.HasIngredient( "Cheese" ) );
            Assert.IsNull( recipe.FindIngredient( "Cheese" ) );
        }


        [ Test ]
        public void Ingredients_InsertOne_ReturnWhenQueried ()
        {
            PizzaRecipe recipe = new PizzaRecipe();
            Ingredient ing = new Ingredient( "Cheese" );
            recipe.AddIngredient( ing );

            CollectionAssert.AreEqual( recipe.Ingredients, new Ingredient[] { ing } );
            Assert.True( recipe.HasIngredient( "Cheese" ) );
            Assert.AreSame( recipe.FindIngredient( "Cheese" ), ing );
        }


        [ Test ]
        public void Ingredients_AskMissingInNonEmpty ()
        {
            PizzaRecipe recipe = new PizzaRecipe();
            recipe.AddIngredient( new Ingredient( "Cheese" ) );

            Assert.False( recipe.HasIngredient( "Ham" ) );
            Assert.IsNull( recipe.FindIngredient( "Ham" ) );
        }



        [ Test ]
        public void Ingredients_Add_Two_QueryEach ()
        {
            PizzaRecipe recipe = new PizzaRecipe();
            Ingredient ing1 = new Ingredient( "Cheese" );
            Ingredient ing2 = new Ingredient( "Ham" );
            recipe.AddIngredient( ing1 );
            recipe.AddIngredient( ing2 );

            CollectionAssert.AreEquivalent( recipe.Ingredients, new Ingredient[] { ing1, ing2 } );
            Assert.True( recipe.HasIngredient( "Cheese" ) );
            Assert.True( recipe.HasIngredient( "Ham" ) );
            Assert.AreSame( recipe.FindIngredient( "Cheese" ), ing1 );
            Assert.AreSame( recipe.FindIngredient( "Ham" ), ing2 );
        }


        [ Test ]
        public void Ingredients_Add_Duplicate_Forbidden ()
        {
            PizzaRecipe recipe = new PizzaRecipe();
            recipe.AddIngredient( new Ingredient( "Cheese" ) );

            Assert.Throws< Exception >( () => recipe.AddIngredient( new Ingredient( "Cheese" ) ) );
        }


        [ Test ]
        public void Ingredients_Remove_Single_MakesEmpty ()
        {
            PizzaRecipe recipe = new PizzaRecipe();
            recipe.AddIngredient( new Ingredient( "Cheese" ) );
            recipe.RemoveIngredient( "Cheese" );

            CollectionAssert.IsEmpty( recipe.Ingredients );
        }


        [ Test ]
        public void Ingredients_Remove_OneOfTwo_LeavesSecond ()
        {
            PizzaRecipe recipe = new PizzaRecipe();
            Ingredient ing1 = new Ingredient( "Cheese" );
            Ingredient ing2 = new Ingredient( "Ham" );
            recipe.AddIngredient( ing1 );
            recipe.AddIngredient( ing2 );
            recipe.RemoveIngredient( "Cheese" );

            CollectionAssert.AreEqual( recipe.Ingredients, new Ingredient[] { ing2 } );
            Assert.True( recipe.HasIngredient( "Ham" ) );
            Assert.False( recipe.HasIngredient( "Cheese" ) );
        }


        [ Test ]
        public void Ingredients_Remove_MissingFromEmpty_Forbidden ()
        {
            PizzaRecipe recipe = new PizzaRecipe();

            Assert.Throws< Exception >( () => recipe.RemoveIngredient( "Cheese" ) );
        }


        [ Test ]
        public void Ingredients_Remove_MissingFromNonEmpty_Forbidden ()
        {
            PizzaRecipe recipe = new PizzaRecipe();
            recipe.AddIngredient( new Ingredient( "Ham" ) );

            Assert.Throws< Exception >( () => recipe.RemoveIngredient( "Cheese" ) );
        }


        [ Test ]
        public void TotalWeight_OfEmpty_Zero ()
        {
            PizzaRecipe recipe = new PizzaRecipe();

            Assert.AreEqual( recipe.GetTotalWeight( PizzaSize.Small  ), 0.0 );
            Assert.AreEqual( recipe.GetTotalWeight( PizzaSize.Medium ), 0.0 );
            Assert.AreEqual( recipe.GetTotalWeight( PizzaSize.Large  ), 0.0 );
        }


        [ Test ]
        public void TotalWeight_OneIngredient_MatchesIngredient ()
        {
            PizzaRecipe recipe = new PizzaRecipe();
            Ingredient ing = new Ingredient( "Cheese" );
            ing.DefineWeights( 100 );
            recipe.AddIngredient( ing );

            Assert.AreEqual( recipe.GetTotalWeight( PizzaSize.Small  ), ing.GetWeight( PizzaSize.Small  ) );
            Assert.AreEqual( recipe.GetTotalWeight( PizzaSize.Medium ), ing.GetWeight( PizzaSize.Medium ) );
            Assert.AreEqual( recipe.GetTotalWeight( PizzaSize.Large  ), ing.GetWeight( PizzaSize.Large  ) );
        }


        [ Test ]
        public void TotalWeight_TwoIngredients_MatchesSum ()
        {
            PizzaRecipe recipe = new PizzaRecipe();
            Ingredient ing1 = new Ingredient( "Cheese" );
            Ingredient ing2 = new Ingredient( "Ham" );
            ing1.DefineWeights( 100 );
            ing2.DefineWeights(  40 );
            recipe.AddIngredient( ing1 );
            recipe.AddIngredient( ing2 );

            Assert.AreEqual( 
                recipe.GetTotalWeight( PizzaSize.Small  ), 
                ing1.GetWeight( PizzaSize.Small  ) + ing2.GetWeight( PizzaSize.Small ) 
            );

            Assert.AreEqual(
                recipe.GetTotalWeight( PizzaSize.Medium ), 
                ing1.GetWeight( PizzaSize.Medium ) + ing2.GetWeight( PizzaSize.Medium )
            );

            Assert.AreEqual( 
                recipe.GetTotalWeight( PizzaSize.Large  ), 
                ing1.GetWeight( PizzaSize.Large  ) + ing2.GetWeight( PizzaSize.Large )
            );
        }


        [ Test ]
        public void KeyIngredients_WhenNoIngredients_Empty ()
        {
            PizzaRecipe recipe = new PizzaRecipe();

            CollectionAssert.IsEmpty( recipe.KeyIngredients() );
        }


        [ Test ]
        public void KeyIngredients_WhenSingle_IsReturned ()
        {
            PizzaRecipe recipe = new PizzaRecipe();
            Ingredient ing = new Ingredient( "Cheese" );
            recipe.AddIngredient( ing );

            CollectionAssert.AreEqual( recipe.KeyIngredients(), new string[] { "Cheese" } );
        }


        [ Test ]
        public void KeyIngredients_WhenNumberMatchesActual_AllReturned ()
        {
            Ingredient ing1 = new Ingredient( "Cheese" );
            Ingredient ing2 = new Ingredient( "Ham" );

            PizzaRecipe recipe = new PizzaRecipe();
            recipe.AddIngredient( ing1 );
            recipe.AddIngredient( ing2 );

            CollectionAssert.AreEquivalent( recipe.KeyIngredients( 2 ), new string[] { "Cheese", "Ham" } );
        }


        [ Test ]
        public void KeyIngredients_WhenNumberSmallerThanActual_AnalyzeWeights_PickLargest ()
        {
            Ingredient ing1 = new Ingredient( "Cheese" );
            ing1.DefineWeights( 50 );
            Ingredient ing2 = new Ingredient( "Ham" );
            ing2.DefineWeights( 20 );
            Ingredient ing3 = new Ingredient( "Mushrooms" );
            ing3.DefineWeights( 40 );

            PizzaRecipe recipe = new PizzaRecipe();
            recipe.AddIngredient( ing1 );
            recipe.AddIngredient( ing2 );
            recipe.AddIngredient( ing3 );

            CollectionAssert.AreEquivalent( recipe.KeyIngredients( 2 ), new string[] { "Cheese", "Mushrooms" } );
        }


        [ Test ]
        public void KeyIngredients_EqualWeights_ReturnByAlphabet ()
        {
            Ingredient ing1 = new Ingredient( "Ham" );
            ing1.DefineWeights( 50 );
            Ingredient ing2 = new Ingredient( "Cheese" );
            ing2.DefineWeights( 50 );

            PizzaRecipe recipe = new PizzaRecipe();
            recipe.AddIngredient( ing1 );
            recipe.AddIngredient( ing2 );

            CollectionAssert.AreEquivalent( recipe.KeyIngredients( 1 ), new string[] { "Cheese" } );
        }


        [ Test ]
        public void KeyIngredients_ComplexWeights_ReturnWeightThanAlphabet ()
        {
            Ingredient ing1 = new Ingredient( "Ham" );
            ing1.DefineWeights( 50 );
            Ingredient ing2 = new Ingredient( "Cheese" );
            ing2.DefineWeights( 50 );
            Ingredient ing3 = new Ingredient( "Mushrooms" );
            ing3.DefineWeights( 100 );

            PizzaRecipe recipe = new PizzaRecipe();
            recipe.AddIngredient( ing1 );
            recipe.AddIngredient( ing2 );
            recipe.AddIngredient( ing3 );

            CollectionAssert.AreEquivalent( recipe.KeyIngredients( 2 ), new string[] { "Cheese", "Mushrooms" } );
        }


        [ Test ]
        public void KeyIngredients_ZeroOrNegativeCount_Rejected ()
        {
            PizzaRecipe recipe = new PizzaRecipe();
            Assert.Throws< Exception >( () => recipe.KeyIngredients(  0 ) );
            Assert.Throws< Exception >( () => recipe.KeyIngredients( -1 ) );
        }

        
        [ Test ]
        public void HasAll_EmptyRecipe_ReturnsFalse ()
        {
            PizzaRecipe recipe = new PizzaRecipe();

            Assert.False( recipe.HasAllOfIngredients( new string[] { "Cheese", "Ham" } ) );
        }
        

        [ Test ]
        public void HasAll_FullMatch_ReturnsTrue ()
        {
            PizzaRecipe recipe = new PizzaRecipe();
            recipe.AddIngredient( new Ingredient( "Cheese" ) );
            recipe.AddIngredient( new Ingredient( "Ham" ) );

            Assert.True( recipe.HasAllOfIngredients( new string[] { "Cheese", "Ham" } ) );
        }


        [ Test ]
        public void HasAll_PartialMatch_ReturnsFalse ()
        {
            PizzaRecipe recipe = new PizzaRecipe();
            recipe.AddIngredient( new Ingredient( "Cheese" ) );
            recipe.AddIngredient( new Ingredient( "Ham" ) );

            Assert.False( recipe.HasAllOfIngredients( new string[] { "Cheese", "Mushrooms" } ) );
        }


        [ Test ]
        public void HasAll_FullMismatch_ReturnsFalse ()
        {
            PizzaRecipe recipe = new PizzaRecipe();
            recipe.AddIngredient( new Ingredient( "Cheese" ) );
            recipe.AddIngredient( new Ingredient( "Ham" ) );

            Assert.False( recipe.HasAllOfIngredients( new string[] { "Tomatoes", "Mushrooms" } ) );
        }


        [ Test ]
        public void HasAll_EmptyQuery_Forbidden ()
        {
            PizzaRecipe recipe = new PizzaRecipe();

            Assert.Throws< Exception >( () => recipe.HasAllOfIngredients( new string[] {} ) );
        }


        [ Test ]
        public void HasNone_EmptyRecipe_ReturnsTrue ()
        {
            PizzaRecipe recipe = new PizzaRecipe();

            Assert.True( recipe.HasNoneOfIngredients( new string[] { "Cheese", "Ham" } ) );
        }


        [ Test ]
        public void HasNone_FullMatch_ReturnsFalse ()
        {
            PizzaRecipe recipe = new PizzaRecipe();
            recipe.AddIngredient( new Ingredient( "Cheese" ) );
            recipe.AddIngredient( new Ingredient( "Ham" ) );

            Assert.False( recipe.HasNoneOfIngredients( new string[] { "Cheese", "Ham" } ) );
        }
        

        [ Test ]
        public void HasNone_PartialMatch_ReturnsFalse ()
        {
            PizzaRecipe recipe = new PizzaRecipe();
            recipe.AddIngredient( new Ingredient( "Cheese" ) );
            recipe.AddIngredient( new Ingredient( "Ham" ) );

            Assert.False( recipe.HasNoneOfIngredients( new string[] { "Mushrooms", "Ham" } ) );
        }
        

        [ Test ]
        public void HasNone_FullMismatch_ReturnsTrue ()
        {
            PizzaRecipe recipe = new PizzaRecipe();
            recipe.AddIngredient( new Ingredient( "Cheese" ) );
            recipe.AddIngredient( new Ingredient( "Ham" ) );

            Assert.True( recipe.HasNoneOfIngredients( new string[] { "Mushrooms", "Tomatoes" } ) );
        }


        [ Test ]
        public void HasNone_EmptyQuery_Forbidden ()
        {
            PizzaRecipe recipe = new PizzaRecipe();

            Assert.Throws< Exception >( () => recipe.HasNoneOfIngredients( new string[] {} ) );
        }


        [ Test ]
        public void Similar_MatchTwoEmpty_ReturnsTrue ()
        {
            PizzaRecipe recipe1 = new PizzaRecipe();
            PizzaRecipe recipe2 = new PizzaRecipe();

            Assert.True( recipe1.IsSimilarTo( recipe2 ) );
            Assert.True( recipe2.IsSimilarTo( recipe1 ) );
        }


        [ Test ]
        public void Similar_MatchToEmpty_OrAgainstEmpty_ReturnsFalse ()
        {
            PizzaRecipe recipe1 = new PizzaRecipe();
            recipe1.AddIngredient( new Ingredient( "Cheese" ) );
            recipe1.AddIngredient( new Ingredient( "Ham" ) );

            PizzaRecipe recipe2 = new PizzaRecipe();

            Assert.False( recipe1.IsSimilarTo( recipe2 ) );
            Assert.False( recipe2.IsSimilarTo( recipe1 ) );
        }


        [ Test ]
        public void Similar_MatchTwoIdentical_ReturnsTrue ()
        {
            PizzaRecipe recipe1 = new PizzaRecipe();
            recipe1.AddIngredient( new Ingredient( "Cheese" ) );
            recipe1.AddIngredient( new Ingredient( "Ham" ) );

            PizzaRecipe recipe2 = new PizzaRecipe();
            recipe2.AddIngredient( new Ingredient( "Cheese" ) );
            recipe2.AddIngredient( new Ingredient( "Ham" ) );

            Assert.True( recipe1.IsSimilarTo( recipe2 ) );
            Assert.True( recipe2.IsSimilarTo( recipe1 ) );
        }


        [ Test ]
        public void Similar_MatchWithTwoIngredientsOneCommon_ReturnsTrue ()
        {
            PizzaRecipe recipe1 = new PizzaRecipe();
            recipe1.AddIngredient( new Ingredient( "Cheese" ) );
            recipe1.AddIngredient( new Ingredient( "Ham" ) );

            PizzaRecipe recipe2 = new PizzaRecipe();
            recipe2.AddIngredient( new Ingredient( "Mushrooms" ) );
            recipe2.AddIngredient( new Ingredient( "Ham" ) );

            Assert.True( recipe1.IsSimilarTo( recipe2 ) );
            Assert.True( recipe2.IsSimilarTo( recipe1 ) );
        }


        [ Test ]
        public void Similar_MatchWithThreeIngredientsOneCommon_ReturnsFalse ()
        {
            PizzaRecipe recipe1 = new PizzaRecipe();
            recipe1.AddIngredient( new Ingredient( "Cheese" ) );
            recipe1.AddIngredient( new Ingredient( "Ham" ) );
            recipe1.AddIngredient( new Ingredient( "Mushrooms" ) );

            PizzaRecipe recipe2 = new PizzaRecipe();
            recipe2.AddIngredient( new Ingredient( "Tomatoes" ) );
            recipe2.AddIngredient( new Ingredient( "Ham" ) );
            recipe2.AddIngredient( new Ingredient( "Sausage" ) );

            Assert.False( recipe1.IsSimilarTo( recipe2 ) );
            Assert.False( recipe2.IsSimilarTo( recipe1 ) );
        }


        [ Test ]
        public void Similar_MatchWithThreeIngredientsTwoCommon_ReturnsTrue ()
        {
            PizzaRecipe recipe1 = new PizzaRecipe();
            recipe1.AddIngredient( new Ingredient( "Cheese" ) );
            recipe1.AddIngredient( new Ingredient( "Ham" ) );
            recipe1.AddIngredient( new Ingredient( "Mushrooms" ) );

            PizzaRecipe recipe2 = new PizzaRecipe();
            recipe2.AddIngredient( new Ingredient( "Tomatoes" ) );
            recipe2.AddIngredient( new Ingredient( "Ham" ) );
            recipe2.AddIngredient( new Ingredient( "Cheese" ) );

            Assert.True( recipe1.IsSimilarTo( recipe2 ) );
            Assert.True( recipe2.IsSimilarTo( recipe1 ) );
        }


        [ Test ]
        public void Similar_MatchWithUnequalIngredientsCount_ReturnsDependingOnSecondCount ()
        {
            PizzaRecipe recipe1 = new PizzaRecipe();
            recipe1.AddIngredient( new Ingredient( "Cheese" ) );
            recipe1.AddIngredient( new Ingredient( "Ham" ) );
            recipe1.AddIngredient( new Ingredient( "Mushrooms" ) );

            PizzaRecipe recipe2 = new PizzaRecipe();
            recipe2.AddIngredient( new Ingredient( "Tomatoes" ) );
            recipe2.AddIngredient( new Ingredient( "Ham" ) );

            Assert.True( recipe1.IsSimilarTo( recipe2 ) ); // Ham common, 1 of 2 matching
            Assert.False( recipe2.IsSimilarTo( recipe1 ) ); // Ham common, only 1 of 3 matching
        }
    }
}
