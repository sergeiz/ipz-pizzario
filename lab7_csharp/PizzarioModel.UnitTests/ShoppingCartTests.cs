﻿using Pizzario.Model;

using NUnit.Framework;
using System;


namespace Pizzario.Model.UnitTests
{
    [ TestFixture ]
    public class ShoppingCartTests
    {

        [ Test ]
        public void Items_None_Initially ()
        {
            ShoppingCart cart = new ShoppingCart();

            CollectionAssert.IsEmpty( cart.Items );
        }


        [ Test ]
        public void Add_One_ListOne ()
        {
            ShoppingCart cart = new ShoppingCart();
            OrderItem item = makeSmallItem();
            cart.AddItem( item );

            CollectionAssert.AreEqual( cart.Items, new OrderItem[] { item } );
        }


        [ Test ]
        public void Add_Two_ListTwo ()
        {
            ShoppingCart cart = new ShoppingCart();
            OrderItem item1 = makeSmallItem();
            OrderItem item2 = makeMediumItem();
            cart.AddItem( item1 );
            cart.AddItem( item2 );

            CollectionAssert.AreEqual( cart.Items, new OrderItem[] { item1, item2 } );
        }


        [ Test ]
        public void Add_TwoSameKindSize_Fails ()
        {
            ShoppingCart cart = new ShoppingCart();
            PizzaKind kind = makePizzaKind();
            cart.AddItem( new OrderItem( kind, PizzaSize.Small, 3.0M, 1 ) );
            
            Assert.Throws< Exception >( () => cart.AddItem( new OrderItem( kind, PizzaSize.Small, 3.0M, 1 ) ) );
        }


        [ Test ]
        public void Add_TwoSameKindDifferentSize_Passes ()
        {
            ShoppingCart cart = new ShoppingCart();
            PizzaKind kind = makePizzaKind();
            OrderItem itemS = new OrderItem( kind, PizzaSize.Small,  3.0M, 1 );
            OrderItem itemM = new OrderItem( kind, PizzaSize.Medium, 3.0M, 1 );
            cart.AddItem( itemS );
            cart.AddItem( itemM );

            CollectionAssert.AreEqual( cart.Items, new OrderItem[] { itemS, itemM } );
        }


        [ Test ]
        public void Add_TwoDifferentKindSameSize_Passes ()
        {
            ShoppingCart cart = new ShoppingCart();
            PizzaKind kind1 = makePizzaKind();
            PizzaKind kind2 = makeAnotherPizzaKind();
            OrderItem item1 = new OrderItem( kind1, PizzaSize.Small, 3.0M, 1 );
            OrderItem item2 = new OrderItem( kind2, PizzaSize.Small, 3.0M, 1 );
            cart.AddItem( item1 );
            cart.AddItem( item2 );

            CollectionAssert.AreEqual( cart.Items, new OrderItem[] { item1, item2 } );
        }


        [ Test ]
        public void Update_OnEmpty_Fails ()
        {
            ShoppingCart cart = new ShoppingCart();
            Assert.Throws< ArgumentOutOfRangeException >( () => cart.UpdateItem( 0, makeSmallItem() ) );
        }


        [ Test ]
        public void Update_OnNonEmptyGoodIndex_Passes ()
        {
            ShoppingCart cart = new ShoppingCart();
            cart.AddItem( makeSmallItem() );
            OrderItem newItem = makeMediumItem();
            cart.UpdateItem( 0, newItem );

            CollectionAssert.AreEqual( cart.Items, new OrderItem[] { newItem } );
        }


        [ Test ]
        public void Update_OnNonEmptyBadIndex_Fails ()
        {
            ShoppingCart cart = new ShoppingCart();
            cart.AddItem( makeSmallItem() );
            
            Assert.Throws< ArgumentOutOfRangeException >( () => cart.UpdateItem( 1, makeMediumItem() ) );
            Assert.Throws< ArgumentOutOfRangeException >( () => cart.UpdateItem( -1, makeMediumItem() ) );
        }

        [ Test ]
        public void Drop_Empty_Fails ()
        {
            ShoppingCart cart = new ShoppingCart();

            Assert.Throws< ArgumentOutOfRangeException >( () => cart.DropItem( 0 ) );
        }


        [ Test ]
        public void Drop_NonEmptyGoodIndex_Passes ()
        {
            ShoppingCart cart = new ShoppingCart();
            cart.AddItem( makeSmallItem() );
            cart.DropItem( 0 );

            CollectionAssert.IsEmpty( cart.Items );
        }


        [ Test ]
        public void Drop_NonEmptyBadIndex_Fails ()
        {
            ShoppingCart cart = new ShoppingCart();
            cart.AddItem( makeSmallItem() );

            Assert.Throws< ArgumentOutOfRangeException >( () => cart.DropItem( 1 ) );
            Assert.Throws< ArgumentOutOfRangeException >( () => cart.DropItem( -1 ) );
        }


        [ Test ]
        public void Clear_Empty_Passes ()
        {
            ShoppingCart cart = new ShoppingCart();
            cart.ClearItems();

            CollectionAssert.IsEmpty( cart.Items );
        }


        [ Test ]
        public void Clear_NonEmpty_Passes ()
        {
            ShoppingCart cart = new ShoppingCart();
            cart.AddItem( makeSmallItem() );
            cart.AddItem( makeMediumItem() );
            cart.ClearItems();

            CollectionAssert.IsEmpty( cart.Items );
        }


        [ Test ]
        public void Cost_Empty_Zero ()
        {
            ShoppingCart cart = new ShoppingCart();

            Assert.AreEqual( cart.Cost, 0.0M );
        }


        [ Test ]
        public void Cost_SingleItem_MatchesItem ()
        {
            ShoppingCart cart = new ShoppingCart();
            OrderItem item = makeSmallItem();
            cart.AddItem( item );

            Assert.AreEqual( cart.Cost, item.Cost );
        }


        [ Test ]
        public void Cost_TwoItems_MatchesSumItems ()
        {
            ShoppingCart cart = new ShoppingCart();
            OrderItem item1 = makeSmallItem();
            OrderItem item2 = makeMediumItem();
            cart.AddItem( item1 );
            cart.AddItem( item2 );

            Assert.AreEqual( cart.Cost, item1.Cost + item2.Cost );
        }


        [ Test ]
        public void Modifiable_IsTrue_Initially ()
        {
            ShoppingCart cart = new ShoppingCart();
            Assert.True( cart.Modifiable );
        }


        [ Test ]
        public void Modifiable_AfterLock_IsFalse ()
        {
            ShoppingCart cart = new ShoppingCart();
            cart.AddItem( makeSmallItem() );
            cart.Lock();

            Assert.False( cart.Modifiable );
        }


        [ Test ]
        public void Lock_Empty_Fails ()
        {
            ShoppingCart cart = new ShoppingCart();

            Assert.Throws< Exception >( () => cart.Lock() );
        }


        [ Test ]
        public void Unmodifiable_ReadingItems_Passes ()
        {
            ShoppingCart cart = new ShoppingCart();
            OrderItem item1 = makeSmallItem();
            OrderItem item2 = makeMediumItem();
            cart.AddItem( item1 );
            cart.AddItem( item2 );

            cart.Lock();

            CollectionAssert.AreEqual( cart.Items, new OrderItem[] { item1, item2 } );
        }


        [ Test ]
        public void Unmodifiable_AddItem_Forbidden ()
        {
            ShoppingCart cart = new ShoppingCart();
            cart.AddItem( makeSmallItem() );
            cart.Lock();

            Assert.Throws< Exception >( () => cart.AddItem( makeMediumItem() ) );
        }


        [ Test ]
        public void Unmodifiable_UpdateItem_Forbidden ()
        {
            ShoppingCart cart = new ShoppingCart();
            cart.AddItem( makeSmallItem() );
            cart.Lock();

            Assert.Throws< Exception >( () => cart.UpdateItem( 0, makeMediumItem() ) );
        }


        [ Test ]
        public void Unmodifiable_DropItem_Forbidden ()
        {
            ShoppingCart cart = new ShoppingCart();
            cart.AddItem( makeSmallItem() );
            cart.Lock();

            Assert.Throws< Exception >( () => cart.DropItem( 0 ) );
        }


        [ Test ]
        public void Unmodifiable_ClearItems_Forbidden ()
        {
            ShoppingCart cart = new ShoppingCart();
            cart.AddItem( makeSmallItem() );
            cart.Lock();

            Assert.Throws< Exception >( () => cart.ClearItems() );
        }


        private PizzaKind makePizzaKind ()
        {
            return new PizzaKind( "Carbonara" );
        }


        private PizzaKind makeAnotherPizzaKind ()
        {
            return new PizzaKind( "Milano" );
        }


        private OrderItem makeSmallItem ()
        {
            return new OrderItem( makePizzaKind(), PizzaSize.Small, 3.0M, 1 );
        }


        private OrderItem makeMediumItem ()
        {
            return new OrderItem( makeAnotherPizzaKind(), PizzaSize.Medium, 5.0M, 1 );
        }
    }
}
