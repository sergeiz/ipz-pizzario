﻿using Pizzario.Model;

using NUnit.Framework;
using System;

namespace Pizzario.Model.UnitTests
{
    [ TestFixture ]
    public class DiscountTests
    {
        [ Test ]
        public void Constructor_ZeroDiscount_ByDefault ()
        {
            Discount d = new Discount();

            Assert.AreEqual( d.Value, 0.0 );
        }


        [ Test ]
        public void Constructor_ConcreteDiscount_ReturnedBack ()
        {
            Discount d = new Discount( 0.5 );

            Assert.AreEqual( d.Value, 0.5 );
        }


        [ Test ]
        public void Constructor_BorderRanges_Accepted ()
        {
            Discount d1 = new Discount( 0.0 );
            Discount d2 = new Discount( 1.0 );

            Assert.AreEqual( d1.Value, 0.0 );
            Assert.AreEqual( d2.Value, 1.0 );
        }


        [ Test ]
        public void Constructor_InvalidRanges ()
        {
            Assert.Throws< Exception >( () => new Discount( -0.001 ) );
            Assert.Throws< Exception >( () => new Discount( 1.001 ) );
        }


        [ Test ]
        public void IsZero_TrueForZeroOnly ()
        {
            Discount d1 = new Discount( 0.0 );
            Discount d2 = new Discount( 0.001 );
            Discount d3 = new Discount( 1.0 );

            Assert.True( d1.IsZero() );
            Assert.False( d2.IsZero() );
            Assert.False( d3.IsZero() );
        }
    }
}
