﻿using Pizzario.Model;

using NUnit.Framework;
using System;

namespace Pizzario.Model.UnitTests
{
    [ TestFixture ]
    public class CustomerTests
    {
        [ Test ]
        public void Constructor_CopiesProperties_Correctly ()
        {
            Customer c = new Customer( "Ivan Ivanov", "ivanov@nure.ua", "Lenin av. 14", "0571234567" );

            Assert.AreEqual( c.FullName, "Ivan Ivanov" );
            Assert.AreEqual( c.Email,    "ivanov@nure.ua" );
            Assert.AreEqual( c.Address,  "Lenin av. 14" );
            Assert.AreEqual( c.Phone,    "0571234567" );
        }


        [ Test ]
        public void Constructor_RejectsEmptyName ()
        {
            Assert.Throws< Exception >( () => new Customer( "", "ivanov@nure.ua", "Lenin av. 14", "0571234567" ) );
        }


        [ Test ]
        public void Constructor_RejectsEmptyEmail ()
        {
            Assert.Throws< Exception >( () => new Customer( "Ivan Ivanov", "", "Lenin av. 14", "0571234567" ) );
        }


        [ Test ]
        public void Constructor_RejectsEmptyAddress ()
        {
            Assert.Throws< Exception >( () => new Customer( "Ivan Ivanov", "ivanov@nure.ua", "", "0571234567" ) );
        }


        [ Test ]
        public void Constructor_RejectsEmptyPhone ()
        {
            Assert.Throws< Exception >( () => new Customer( "Ivan Ivanov", "ivanov@nure.ua", "Lenin av. 14", "" ) );
        }



        [ Test ]
        public void SetFullname_UpdatesProperty ()
        {
            Customer c = makeCustomer();
            c.SetFullname( "Ivan Petrov" );

            Assert.AreEqual( c.FullName, "Ivan Petrov" );
        }


        [ Test ]
        public void SetFullname_ToSame_Ignored ()
        {
            Customer c = makeCustomer();
            Assert.DoesNotThrow( () => c.SetFullname( c.FullName ) );
        }


        [ Test ]
        public void SetFullname_ToEmpty_Forbidden ()
        {
            Customer c = makeCustomer();
            Assert.Throws< Exception >( () => c.SetFullname( "" ) );
        }


        [ Test ]
        public void SetEmail_UpdatesProperty ()
        {
            Customer c = makeCustomer();
            c.SetEmail( "hellraiser@nure.ua" );

            Assert.AreEqual( c.Email, "hellraiser@nure.ua" );
        }


        [ Test ]
        public void SetEmail_ToSame_Ignored ()
        {
            Customer c = makeCustomer(); 
            Assert.DoesNotThrow( () => c.SetEmail( c.Email ) );
        }


        [ Test ]
        public void SetEmail_ToEmpty_Forbidden ()
        {
            Customer c = makeCustomer(); 
            Assert.Throws< Exception >( () => c.SetEmail( "" ) );
        }


        [ Test ]
        public void SetAddress_UpdatesProperty ()
        {
            Customer c = makeCustomer();
            c.SetAddress( "Sumskaya str. 1" );

            Assert.AreEqual( c.Address, "Sumskaya str. 1" );
        }


        [ Test ]
        public void SetAddress_ToSame_Ignored ()
        {
            Customer c = makeCustomer();
            Assert.DoesNotThrow( () => c.SetAddress( c.Address ) );
        }


        [ Test ]
        public void SetAddress_ToEmpty_Forbidden ()
        {
            Customer c = makeCustomer();
            Assert.Throws< Exception >( () => c.SetAddress( "" ) );
        }


        [ Test ]
        public void SetPhone_UpdatesProperty ()
        {
            Customer c = makeCustomer();
            c.SetPhone( "12345" );

            Assert.AreEqual( c.Phone, "12345" );
        }


        [ Test ]
        public void SetPhone_ToSame_Ignored ()
        {
            Customer c = makeCustomer();
            Assert.DoesNotThrow( () => c.SetPhone( c.Phone ) );
        }


        [ Test ]
        public void SetPhone_ToEmpty_Forbidden ()
        {
            Customer c = makeCustomer();
            Assert.Throws< Exception >( () => c.SetPhone( "" ) );
        }


        [ Test ]
        public void Discount_InitiallyZero ()
        {
            Customer c = makeCustomer(); 
            Assert.True( c.CurrentDiscount.IsZero() );
        }


        [ Test ]
        public void Discount_SetConcrete_UpdatesProperty ()
        {
            Customer c = makeCustomer(); 

            Discount d = new Discount( 0.5 );
            c.SetDiscount( d );

            Assert.AreSame( d, c.CurrentDiscount );
        }


        [ Test ]
        public void Orders_Empty_Initially ()
        {
            Customer c = makeCustomer(); 
            CollectionAssert.IsEmpty( c.PastOrders );
        }


        [ Test ]
        public void Orders_AddOrder_ScanSuccess ()
        {
            Customer c = makeCustomer(); 
            Order o = makeOrder( "Carbonara" );
            c.AddPastOrder( o );

            CollectionAssert.AreEqual( c.PastOrders, new Order[] { o } );
        }


        [ Test ]
        public void Orders_AddTwoOrders_SequenceNotBroken ()
        {
            Customer c = makeCustomer(); 
            Order o1 = makeOrder( "Carbonara" );
            Order o2 = makeOrder( "Milano" );
            c.AddPastOrder( o1 );
            c.AddPastOrder( o2 );

            CollectionAssert.AreEqual( c.PastOrders, new Order[] { o1, o2 } );
        }


        [ Test ]
        public void Orders_AddSame_Ignored ()
        {
            Customer c = makeCustomer(); 
            Order o = makeOrder( "Carbonara" );
            c.AddPastOrder( o );
            c.AddPastOrder( o );

            CollectionAssert.AreEqual( c.PastOrders, new Order[] { o } );
        }


        [ Test ]
        public void Orders_HasOrder_FindsExisting ()
        {
            Customer c = makeCustomer(); 
            Order o = makeOrder( "Carbonara" );
            c.AddPastOrder( o );

            Assert.True( c.HasPastOrder( o ) );
        }


        [ Test ]
        public void Orders_HasOrder_NullWhenEmpty ()
        {
            Customer c = makeCustomer(); 
            Order o = makeOrder( "Carbonara" );
            Assert.False( c.HasPastOrder( o ) );
        }


        [ Test ]
        public void Orders_HasOrder_NullWhenHavingDifferentEvenIfSimilar ()
        {
            Customer c = makeCustomer(); 
            Order o1 = makeOrder( "Carbonara" );
            Order o2 = makeOrder( "Carbonara" );
            c.AddPastOrder( o1 );

            Assert.False( c.HasPastOrder( o2 ) );
        }


        private Customer makeCustomer ()
        {
            return new Customer( "Ivan Ivanov", "ivanov@nure.ua", "Lenin av. 14", "0571234567" );
        }


        private Order makeOrder ( string pizzaKind )
        {
            ShoppingCart cart = new ShoppingCart();
            cart.AddItem( new OrderItem( new PizzaKind( pizzaKind ), PizzaSize.Small, 1.0M, 1 ) );
            cart.Lock();

            Order order = new Order( cart, new CustomerContact() );
            return order;
        }
    }
}
