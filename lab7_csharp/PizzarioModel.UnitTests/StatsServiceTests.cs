﻿using Pizzario.Model;
using Pizzario.Model.Services;

using NUnit.Framework;
using System;
using System.Linq;

namespace Pizzario.Model.UnitTests
{
    [ TestFixture ]
    public class StatsServiceTests
    {
        [ Test ]
        public void StatsByKind_NoData_NoStats ()
        {
            OrderItem[] items = new OrderItem[] { };
            var stats = OrderStatsService.CalculateStatsByPizzaKinds( items.AsQueryable() );

            Assert.False( stats.Any() );
        }


        [ Test ]
        public void StatsByKind_SingleItem_SingleStatsRecord ()
        {
            OrderItem[] items = new OrderItem[] {
                new OrderItem( new PizzaKind( "Carbonara" ), PizzaSize.Small, 1.0M, 1 ) 
            };

            var stats = OrderStatsService.CalculateStatsByPizzaKinds( items.AsQueryable() ).ToArray();

            CollectionAssert.AreEqual( 
                stats,
                new object [] { 
                    new OrderStatsService.PizzaKindQuantityRecord {
                        KindName = "Carbonara",
                        TotalQuantity = 1
                    }
                }
            );
        }


        [ Test ]
        public void StatsByKind_SingleItemTwoUnits_SingleStatsRecordDoubledData ()
        {
            OrderItem[] items = new OrderItem[] { 
                new OrderItem( new PizzaKind( "Carbonara" ), PizzaSize.Small, 1.0M, 2 ) 
            };

            var stats = OrderStatsService.CalculateStatsByPizzaKinds( items.AsQueryable() ).ToArray();

            CollectionAssert.AreEqual( 
                stats,
                new object [] { 
                    new OrderStatsService.PizzaKindQuantityRecord {
                        KindName = "Carbonara",
                        TotalQuantity = 2
                    }
                }
            );
        }


        [ Test ]
        public void StatsByKind_TwoSimpleItems_CombinedData ()
        {
            var query = new OrderItem[] {
                new OrderItem( new PizzaKind( "Carbonara" ), PizzaSize.Small, 1.0M, 1 ),
                new OrderItem( new PizzaKind( "Milano" ),    PizzaSize.Small, 1.0M, 1 ) 
            };

            var stats = OrderStatsService.CalculateStatsByPizzaKinds( query.AsQueryable() ).ToArray();

            CollectionAssert.AreEqual( 
                stats,
                new object [] { 

                    new OrderStatsService.PizzaKindQuantityRecord {
                        KindName = "Carbonara",
                        TotalQuantity = 1
                    },

                    new OrderStatsService.PizzaKindQuantityRecord {
                        KindName = "Milano",
                        TotalQuantity = 1
                    }
                }
            );
        }


        [ Test ]
        public void StatsByKind_TwoIntersectingItems_SingleRecord ()
        {
            PizzaKind carbonara = new PizzaKind( "Carbonara" );

            var query = new OrderItem[] {
                new OrderItem( carbonara, PizzaSize.Small, 1.0M, 2 ),
                new OrderItem( carbonara, PizzaSize.Large, 1.0M, 4 ) 
            };

            var stats = OrderStatsService.CalculateStatsByPizzaKinds( query.AsQueryable() ).ToArray();

            CollectionAssert.AreEqual( 
                stats,
                new object [] { 
                    new OrderStatsService.PizzaKindQuantityRecord {
                        KindName = "Carbonara",
                        TotalQuantity = 6
                    }
                }
            );
        }


        [ Test ]
        public void StatsByKind_PartiallyIntersectingItems_SummarizedRecords_OrderedByQuantityThenAlphabet ()
        {
            PizzaKind carbonara = new PizzaKind( "Carbonara" );
            PizzaKind milano    = new PizzaKind( "Milano" );
            PizzaKind reggina   = new PizzaKind( "Reggina" );

            var query = new OrderItem[] {
                new OrderItem( carbonara, PizzaSize.Small,  1.0M, 2 ),
                new OrderItem( milano,    PizzaSize.Large,  1.0M, 1 ),
                new OrderItem( carbonara, PizzaSize.Large,  1.0M, 1 ),
                new OrderItem( reggina,   PizzaSize.Medium, 1.0M, 3 ), 
                new OrderItem( milano,    PizzaSize.Small,  1.0M, 4 )
            };

            var stats = OrderStatsService.CalculateStatsByPizzaKinds( query.AsQueryable() ).ToArray();

            CollectionAssert.AreEqual( 
                stats,
                new object [] { 
                    new OrderStatsService.PizzaKindQuantityRecord {
                        KindName = "Milano",
                        TotalQuantity = 5
                    },
                    new OrderStatsService.PizzaKindQuantityRecord {
                        KindName = "Carbonara",
                        TotalQuantity = 3
                    },
                    new OrderStatsService.PizzaKindQuantityRecord {
                        KindName = "Reggina",
                        TotalQuantity = 3
                    }
               }
            );
        }


        [ Test ]
        public void StatsByIngredient_NoData_NoStats ()
        {
            OrderItem[] items = new OrderItem[] { };
            var stats = OrderStatsService.CalculateStatsByIngredients( items.AsQueryable() );

            Assert.False( stats.Any() );
        }


        [ Test ]
        public void StatsByIngredient_OneSmallItemOneIngredient_OneRecord ()
        {
            Ingredient ing = new Ingredient( "Cheese" );
            ing.DefineWeights( 100 );

            PizzaKind carbonara = new PizzaKind( "Carbonara" );
            carbonara.Recipe.AddIngredient( ing );

            OrderItem[] items = new OrderItem[] {
                new OrderItem( carbonara, PizzaSize.Small, 1.0M, 1 ) 
            };

            var stats = OrderStatsService.CalculateStatsByIngredients( items.AsQueryable() ).ToArray();

            CollectionAssert.AreEqual( 
                stats,
                new object [] { 
                    new OrderStatsService.IngredientUsageRecord {
                        IngredientName = "Cheese",
                        TotalWeight = 100
                    }
                }
            );
        }
        

        [ Test ]
        public void StatsByIngredient_OneLargeItemOneIngredient_OneRecordTrippledWeight ()
        {
            Ingredient ing = new Ingredient( "Cheese" );
            ing.DefineWeights( 100 );

            PizzaKind carbonara = new PizzaKind( "Carbonara" );
            carbonara.Recipe.AddIngredient( ing );

            OrderItem[] items = new OrderItem[] {
                new OrderItem( carbonara, PizzaSize.Large, 1.0M, 1 ) 
            };

            var stats = OrderStatsService.CalculateStatsByIngredients( items.AsQueryable() ).ToArray();

            CollectionAssert.AreEqual( 
                stats,
                new object [] { 
                    new OrderStatsService.IngredientUsageRecord {
                        IngredientName = "Cheese",
                        TotalWeight = 300
                    }
                }
            );
        }


        [ Test ]
        public void StatsByIngredient_TwoSmallItemOneIngredient_OneRecordDoubleWeight ()
        {
            Ingredient ing = new Ingredient( "Cheese" );
            ing.DefineWeights( 100 );

            PizzaKind carbonara = new PizzaKind( "Carbonara" );
            carbonara.Recipe.AddIngredient( ing );

            OrderItem[] items = new OrderItem[] {
                new OrderItem( carbonara, PizzaSize.Small, 1.0M, 2 ) 
            };

            var stats = OrderStatsService.CalculateStatsByIngredients( items.AsQueryable() ).ToArray();

            CollectionAssert.AreEqual(
                stats,
                new object[] { 
                    new OrderStatsService.IngredientUsageRecord {
                        IngredientName = "Cheese",
                        TotalWeight = 200
                    }
                }
            );
        }


        [ Test ]
        public void StatsByIngredient_DifferentItems_NoCommonIngredients_SeparateReports ()
        {
            Ingredient ing1 = new Ingredient( "Cheese" );
            ing1.DefineWeights( 100 );

            Ingredient ing2 = new Ingredient( "Ham" );
            ing2.DefineWeights( 50 );

            Ingredient ing3 = new Ingredient( "Mushrooms" );
            ing3.DefineWeights( 70 );

            PizzaKind carbonara = new PizzaKind( "Carbonara" );
            carbonara.Recipe.AddIngredient( ing1 );
            carbonara.Recipe.AddIngredient( ing2 );

            PizzaKind milano = new PizzaKind( "Milano" );
            milano.Recipe.AddIngredient( ing3 );

            OrderItem[] items = new OrderItem[] {
                new OrderItem( carbonara, PizzaSize.Small, 1.0M, 1 ), 
                new OrderItem( milano,    PizzaSize.Small, 1.0M, 1 )
            };

            var stats = OrderStatsService.CalculateStatsByIngredients( items.AsQueryable() ).ToArray();

            CollectionAssert.AreEqual(
                stats,
                new object[] { 
                    new OrderStatsService.IngredientUsageRecord {
                        IngredientName = "Cheese",
                        TotalWeight = 100
                    },
                    new OrderStatsService.IngredientUsageRecord {
                        IngredientName = "Mushrooms",
                        TotalWeight = 70
                    },
                    new OrderStatsService.IngredientUsageRecord {
                        IngredientName = "Ham",
                        TotalWeight = 50
                    }
                }
            );
        }


        [ Test ]
        public void StatsByIngredient_DifferentItems_HavingCommonIngredients_JointReports ()
        {
            Ingredient ing1 = new Ingredient( "Cheese" );
            ing1.DefineWeights( 100 );

            Ingredient ing2 = new Ingredient( "Ham" );
            ing2.DefineWeights( 50 );

            Ingredient ing3 = new Ingredient( "Mushrooms" );
            ing3.DefineWeights( 70 );

            Ingredient ing4 = new Ingredient( "Cheese" );
            ing4.DefineWeights( 80 );

            Ingredient ing5 = new Ingredient( "Ham" );
            ing5.DefineWeights( 90 );

            Ingredient ing6 = new Ingredient( "Tomatoes" );
            ing6.DefineWeights( 35 );

            PizzaKind carbonara = new PizzaKind( "Carbonara" );
            carbonara.Recipe.AddIngredient( ing1 );
            carbonara.Recipe.AddIngredient( ing2 );

            PizzaKind milano = new PizzaKind( "Milano" );
            milano.Recipe.AddIngredient( ing3 );
            milano.Recipe.AddIngredient( ing4 );

            PizzaKind reggina = new PizzaKind( "Reggina" );
            reggina.Recipe.AddIngredient( ing5 );
            reggina.Recipe.AddIngredient( ing6 );

            OrderItem[] items = new OrderItem[] {
                new OrderItem( carbonara, PizzaSize.Small, 1.0M, 1 ), 
                new OrderItem( milano,    PizzaSize.Small, 1.0M, 1 ),
                new OrderItem( reggina,   PizzaSize.Small, 1.0M, 1 ),
            };

            var stats = OrderStatsService.CalculateStatsByIngredients( items.AsQueryable() ).ToArray();

            CollectionAssert.AreEqual(
                stats,
                new object[] { 
                    new OrderStatsService.IngredientUsageRecord {
                        IngredientName = "Cheese",
                        TotalWeight = 180
                    },
                    new OrderStatsService.IngredientUsageRecord {
                        IngredientName = "Ham",
                        TotalWeight = 140
                    },
                    new OrderStatsService.IngredientUsageRecord {
                        IngredientName = "Mushrooms",
                        TotalWeight = 70
                    },
                    new OrderStatsService.IngredientUsageRecord {
                        IngredientName = "Tomatoes",
                        TotalWeight = 35
                    },
                }
            );
        }


        [ Test ]
        public void StatsByIngredient_ComplexCase_DifferentSizesAndQuantities ()
        {
            Ingredient ing1 = new Ingredient( "Cheese" );
            ing1.DefineWeights( 50 );

            Ingredient ing2 = new Ingredient( "Mushrooms" );
            ing2.DefineWeights( 40 );

            Ingredient ing3 = new Ingredient( "Ham" );
            ing3.DefineWeights( 30 );

            PizzaKind carbonara = new PizzaKind( "Carbonara" );
            carbonara.Recipe.AddIngredient( ing1 );
            carbonara.Recipe.AddIngredient( ing2 );

            PizzaKind milano = new PizzaKind( "Milano" );
            milano.Recipe.AddIngredient( ing2 );
            milano.Recipe.AddIngredient( ing3 );

            PizzaKind reggina = new PizzaKind( "Reggina" );
            reggina.Recipe.AddIngredient( ing1 );
            reggina.Recipe.AddIngredient( ing3 );

            OrderItem[] items = new OrderItem[] {
                new OrderItem( carbonara, PizzaSize.Small,  1.0M, 3 ), 
                new OrderItem( milano,    PizzaSize.Medium, 1.0M, 1 ),
                new OrderItem( reggina,   PizzaSize.Large,  1.0M, 2 ),
            };


            var stats = OrderStatsService.CalculateStatsByIngredients( items.AsQueryable() ).ToArray();

            CollectionAssert.AreEqual(
                stats,
                new object[] { 
                    new OrderStatsService.IngredientUsageRecord {
                        IngredientName = "Cheese",
                        TotalWeight = 450 // carbonara-small-3 ( 50 x 1 x 3 = 150 ) + reggina-large-2 ( 50 x 3 x 2 = 300 )
                    },
                    new OrderStatsService.IngredientUsageRecord {
                        IngredientName = "Ham",
                        TotalWeight = 240 // milano-med-1 ( 30 x 2 x 1 = 60 ) + reggina-large-2 ( 30 x 3 x 2 = 180 )
                    },
                    new OrderStatsService.IngredientUsageRecord {
                        IngredientName = "Mushrooms",
                        TotalWeight = 200 // carbonara-small-3 ( 40 x 1 x 3 = 120 ) + milano-med-1 ( 40 x 2 x 1 = 80 )
                    },
                }
            );
        }
    }
}
