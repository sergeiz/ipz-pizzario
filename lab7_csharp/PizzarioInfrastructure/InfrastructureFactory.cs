﻿namespace Pizzario.Infrastructure
{
    public static class InfrastructureFactory
    {
        public static IEmailAgent MakeEmailAgent ()
        {
            return new EmailAgentFake();
        }

        public static ISmsAgent MakeSmsAgent ()
        {
            return new SmsAgentFake();
        }

        public static IPaymentAgent MakePaymentAgent ()
        {
            return new PaymentAgentFake();
        }
    }
}
