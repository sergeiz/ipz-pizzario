﻿using System;

namespace Pizzario.Infrastructure
{
    public interface ISmsAgent
    {
        void sendSMS ( string phoneNumber, string body );
    }
}
