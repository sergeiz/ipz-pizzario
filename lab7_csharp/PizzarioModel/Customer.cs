﻿using System;
using System.Collections.Generic;

namespace Pizzario.Model
{
    public class Customer
    {
        protected Customer () {}


        public Customer ( string fullname, string email, string address, string phone )
        {
            SetFullname( fullname );
            SetEmail( email );
            SetAddress( address );
            SetPhone( phone );

            this.CurrentDiscount = new Discount();
            this.PastOrders = new List< Order >();
        }


        public int CustomerId { get; set; }
        public string FullName { get; private set; }
        public string Email { get; private set; }
        public string Address { get; private set; }
        public string Phone { get; private set; }
        public Discount CurrentDiscount { get; private set; }

        public virtual ICollection< Order > PastOrders { get; private set; }


        public void SetFullname ( string fullname )
        {
            if ( fullname.Length == 0 )
                throw new Exception( "Customer.SetFullname: empty name" );

            this.FullName = fullname;
        }


        public void SetEmail ( string email )
        {
            if ( email.Length == 0 )
                throw new Exception( "Customer.SetEmail: empty email" );

            this.Email = email;
        }


        public void SetAddress ( string address )
        {
            if ( address.Length == 0 )
                throw new Exception( "Customer.SetAddress: empty address" );

            this.Address = address;
        }


        public void SetPhone ( string phone )
        {
            if ( phone.Length == 0 )
                throw new Exception( "Customer.SetPhone: empty phone" );

            this.Phone = phone;
        }


        public void SetDiscount ( Discount discount )
        {
            if ( discount == null )
                throw new Exception( "Customer.SetDiscount: null discount object" );

            this.CurrentDiscount = discount;
        }


        public void AddPastOrder ( Order order )
        {
            if ( ! HasPastOrder( order ) )
                PastOrders.Add( order );
        }


        public bool HasPastOrder ( Order order )
        {
            return PastOrders.Contains( order );
        }
    }
}
