﻿using System;

namespace Pizzario.Model
{
    public class Discount
    {
        public Discount ()
        {
            this.Value = 0.0;
        }

        public Discount ( double value )
        {
            if ( value < 0.0 || value > 1.0 )
                throw new Exception( "Discount should be between [0.0;1.0]" );

            this.Value = value;
        }

        public double Value { get; private set; }

        public bool IsZero ()
        {
            return Value == 0.0;
        }
    }
}
