﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Pizzario.Model
{
    public class PizzaRecipe
    {
        public int PizzaRecipeId { get; set; }

        public PizzaRecipe ()
        {
            this.Ingredients = new List< Ingredient >();
        }


        public virtual ICollection< Ingredient > Ingredients { get; private set; }


        public IEnumerable< string > IngredientNames 
        {
            get 
            {
                return Ingredients.Select( ing => ing.Name );
            }
        }        


        public bool HasIngredient ( string ingredientName )
        {
            return Ingredients.Where( ing => ing.Name == ingredientName ).Any();
        }


        public Ingredient FindIngredient ( string ingredientName )
        {
            return Ingredients.Where( ing => ing.Name == ingredientName ).FirstOrDefault();
        }


        public void AddIngredient ( Ingredient ingredient )
        {
            if ( HasIngredient( ingredient.Name ) )
                throw new Exception( "PizzaRecipe.AddIngredient: duplicate ingredient" );

            else
                Ingredients.Add( ingredient );
        }


        public void RemoveIngredient ( string ingredientName )
        {
            Ingredient ingredient = FindIngredient( ingredientName );
            if ( ingredient != null )
                Ingredients.Remove( ingredient );

            else
                throw new Exception( "PizzaRecipe.RemoveIngredient: missing ingredient" );
        }


        public int GetTotalWeight ( PizzaSize size )
        {
            int totalWeight = 0;
            foreach ( Ingredient ing in Ingredients )
                totalWeight += ing.GetWeight( size );

            return totalWeight;
        }

        
        public string[] KeyIngredients ( int numKeyIngredients = 3 )
        {
            if ( numKeyIngredients < 1 )
                throw new Exception( "PizzaRecipe.KeyIngredients: number of key ingredients must be at least 1" );

            int nActualIngredients = Ingredients.Count();
            if ( nActualIngredients < numKeyIngredients )
                return IngredientNames.ToArray();

            return Ingredients
                .Select( 
                    ing => new { 
                        Name = ing.Name, 
                        Weight = ing.GetWeight( PizzaSize.Small ) 
                    } 
                )
                .OrderBy( g => g.Name )
                .OrderByDescending( g => g.Weight )
                .Take( numKeyIngredients )
                .Select( g => g.Name )
                .ToArray();
        }


        public bool HasAllOfIngredients ( string[] ingredients )
        {
            if ( ingredients.Length == 0 )
                throw new Exception( "PizzaRecipe.HasAllOfIngredients: empty ingredients list" );

            foreach ( string ingredient in ingredients )
                if ( ! HasIngredient( ingredient ) )
                    return false;

            return true;
        }


        public bool HasNoneOfIngredients ( string[] ingredients )
        {
            if ( ingredients.Length == 0 )
                throw new Exception( "PizzaRecipe.HasNoneOfIngredients: empty ingredients list" );

            foreach ( string ingredient in ingredients )
                if ( HasIngredient( ingredient ) )
                    return false;

            return true;
        }


        public bool IsSimilarTo ( PizzaRecipe recipe )
        {
            int requiredMatchings = ( recipe.Ingredients.Count + 1 ) / 2;
            if ( requiredMatchings == 0 )
                return Ingredients.Count == 0;

            int actualMatchings = 0;

            foreach ( string ingredient in recipe.IngredientNames )
                if ( HasIngredient( ingredient ) )
                    ++actualMatchings;

            return actualMatchings >= requiredMatchings;
        }
    }
}
