﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Pizzario.Model
{
    public class Order
    {
        public int OrderId { get; set; }

        public virtual ICollection< OrderItem > Items { get; private set; }

        public decimal BasicCost { get; private set; }

        public virtual PaymentPlan PayPlan { get; private set; }

        public CustomerContact Contact { get; set; }

        public OrderStatus Status { get; private set; }

        public int RemainingPizzasToCook { get; private set; }

        public Discount AssignedDiscount { get; private set; }

        public decimal TotalCost
        {
            get
            {
                return ( decimal ) ( ( double ) BasicCost * ( 1.0 - AssignedDiscount.Value ) );
            }
        }


        protected Order () {}

        public Order ( ShoppingCart cart, CustomerContact contact )
        {
            if ( cart.Modifiable )
                throw new Exception( "Order: initializing with a cart being edited" );

            this.Items     = new List< OrderItem >( cart.Items );
            this.BasicCost = cart.Cost;
            this.Contact   = contact;

            this.AssignedDiscount = new Discount();

            this.Status = OrderStatus.Registered;

            this.RemainingPizzasToCook = 0;
            foreach ( OrderItem item in Items )
                this.RemainingPizzasToCook += item.Quantity;
        }


        public void SetDiscount ( Discount discount )
        {
            if ( discount == null )
                throw new Exception( "Order.SetDiscount: null discount object" );

            this.AssignedDiscount = discount;
        }


        public void DefinePaymentPlan ( PaymentPlan plan )
        {
            if ( Status != OrderStatus.Registered )
                throw new Exception( "Order.DefinePaymentPlan - can only run in Registered state" );

            if ( plan.ExpectPrepayment() && ! plan.Payed )
                throw new Exception( "Order: pre-payment unavailable" );

            this.PayPlan = plan;

            Status = OrderStatus.Waiting;
        }


        public void Cancel ()
        {
	        switch ( Status )
	        {
		        case OrderStatus.Registered:
                case OrderStatus.Waiting:
		        case OrderStatus.Cooking:
		        case OrderStatus.Ready:
			        break;

		        default:
			        throw new Exception( "Order.Cancel - cannot cancel in current order state" );
	        }

	        Status = OrderStatus.Cancelled;
        }


        public void OnStartedCookingOneOfPizzas ()
        {
            if ( Status == OrderStatus.Waiting )
                Status = OrderStatus.Cooking;

            else if ( Status != OrderStatus.Cooking )
                throw new Exception( "Order.OnStartedCookingOneOfPizzas - can only happen in PaymentPlanDefined & Cooking states" );
        }


        public void OnFinishedCookingOneOfPizzas ()
        {
            if ( Status == OrderStatus.Cooking )
            {
                -- RemainingPizzasToCook;
                if ( RemainingPizzasToCook == 0 )
                    Status = OrderStatus.Ready;
            }
            else
                throw new Exception( "Order.OnFinishedCookingOneOfPizzas - can only happen in Cooking state" );
        }


        public void OnStartedDelivery ()
        {
            if ( Status != OrderStatus.Ready )
                throw new Exception( "Order.OnStartedDelivery - can only happen in Ready4Delivery state" );

            Status = OrderStatus.Delivering;
        }


        public void OnDelivered ( decimal collectedPaymentAmount )
        {
            if ( Status != OrderStatus.Delivering )
                throw new Exception( "Order.OnDelivered - can only happen in Delivering state" );

            if ( ! PayPlan.ExpectPrepayment() && ! PayPlan.Payed )
            {
                if ( collectedPaymentAmount < TotalCost )
                    throw new Exception( "Order.OnDelivered - post-payment should not be greater than collected amount" );

                PayPlan.MarkPayed();
            }
            else if ( collectedPaymentAmount > 0 )
                throw new Exception( "Order.OnDelivered - collecting payment after delivery unexpected" );

            Status = OrderStatus.Delivered;
        }


        public bool MayBeRefunded ()
        {
            return Status == OrderStatus.Cancelled &&
                   PayPlan.IsRefundable();
        }
    }
}
