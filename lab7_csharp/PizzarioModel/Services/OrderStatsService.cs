﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Pizzario.Model.Services
{
    public static class OrderStatsService
    {
        public class PizzaKindQuantityRecord
        {
            public string KindName { get; set; }
            public int TotalQuantity { get; set; }

            public override bool Equals ( object obj )
            {
                if ( obj != null && ( obj is PizzaKindQuantityRecord ) )
                {
                    PizzaKindQuantityRecord rec = obj as PizzaKindQuantityRecord;
                    return KindName == rec.KindName && TotalQuantity == rec.TotalQuantity;
                }
                else
                    return false;
            }

            public override int GetHashCode ()
            {
                return KindName.GetHashCode() ^ TotalQuantity.GetHashCode();
            }

            public override string ToString ()
            {
                return string.Format( "{0} - {1}", KindName, TotalQuantity );
            }
        }

        public class IngredientUsageRecord
        {
            public string IngredientName { get; set; }
            public int TotalWeight { get; set; }

            public override bool Equals ( object obj )
            {
                if ( obj != null && ( obj is IngredientUsageRecord ) )
                {
                    IngredientUsageRecord rec = obj as IngredientUsageRecord;
                    return IngredientName == rec.IngredientName && TotalWeight == rec.TotalWeight;
                }
                else
                    return false;
            }

            public override int GetHashCode ()
            {
                return IngredientName.GetHashCode() ^ TotalWeight.GetHashCode();
            }

            public override string ToString ()
            {
                return string.Format( "{0} - {1}", IngredientName, TotalWeight );
            }
        }


        public static IQueryable< PizzaKindQuantityRecord > 
            CalculateStatsByPizzaKinds ( IQueryable< OrderItem >  orderItems )
        {
            var stats = orderItems
                            .GroupBy( i => i.Kind )
                            .Select(
                                g => new PizzaKindQuantityRecord 
                                    { 
                                        KindName = g.Key.Name, 
                                        TotalQuantity = g.Sum( i => i.Quantity ) 
                                    } 
                            )
                            .OrderByDescending( g => g.TotalQuantity );

            return stats;
        }


        public static IQueryable< IngredientUsageRecord > 
            CalculateStatsByIngredients ( IQueryable< OrderItem >  orderItems )
        {
            var ingredientEntries = orderItems
                                .GroupBy( i => new { Kind = i.Kind, Size = i.Size } )
                                .Select(
                                    g1 => new
                                    {
                                        Kind = g1.Key.Kind,
                                        Size = g1.Key.Size,
                                        TotalQuantity = g1.Sum( i => i.Quantity )
                                    }
                                )
                                .SelectMany(
                                    g1 => g1.Kind.Recipe.Ingredients,
                                    ( g1, i ) => new
                                    {
                                        Recipe = g1.Kind.Recipe,
                                        Size = g1.Size,
                                        TotalQuantity = g1.TotalQuantity,
                                        Ingr = i
                                    }
                                )
                                .GroupBy( g2 => g2.Ingr.Name )
                                .AsEnumerable();

            var stats = ingredientEntries
                                .Select( g2 =>
                                    new IngredientUsageRecord
                                    {
                                        IngredientName = g2.Key,
                                        TotalWeight = g2.Sum( x => x.Recipe.FindIngredient( g2.Key )
                                                                  .GetWeight( x.Size ) * x.TotalQuantity )
                                    }
                                )
                                .OrderBy( y => y.IngredientName )
                                .OrderByDescending( y => y.TotalWeight )
                                ;

            return stats.AsQueryable();
        }
    }
}
