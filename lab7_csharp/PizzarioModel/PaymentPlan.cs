﻿namespace Pizzario.Model
{
    public abstract class PaymentPlan
    {
        public int PaymentPlanId { get; set; }

        public bool Payed { get; private set; }

        protected PaymentPlan ()
        {
            this.Payed = false;
        }

        public void MarkPayed ()
        {
            this.Payed = true;
        }

        public abstract bool ExpectPrepayment ();

        public abstract bool IsRefundable ();
    }
}
