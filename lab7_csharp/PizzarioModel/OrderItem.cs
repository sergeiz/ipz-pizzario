﻿using System;

namespace Pizzario.Model
{
    public class OrderItem
    {
        public int OrderItemId { get; set; }

        public virtual PizzaKind Kind { get; private set; }

        public PizzaSize Size { get; private set; }

        public decimal FixedPrice { get; private set; }

        public int Quantity { get; private set; }

        public decimal Cost
        {
            get
            {
                return FixedPrice * Quantity;
            }
        }


        protected OrderItem () {}


        public OrderItem ( PizzaKind kind, PizzaSize size, decimal fixedPrice, int quantity )
        {
            if ( fixedPrice < 0 )
                throw new Exception( "OrderItem: negative price" );

            SetQuantity( quantity );
 
            this.Kind       = kind;
            this.Size       = size;
            this.FixedPrice = fixedPrice;
        }


        public OrderItem ( OrderItem item )
        {
            this.Kind       = item.Kind;
            this.Size       = item.Size;
            this.FixedPrice = item.FixedPrice;
            this.Quantity   = item.Quantity;
        }


        public void SetQuantity ( int quantity )
        {
            if ( quantity <= 0 )
                throw new Exception( "OrderItem: non-positive quantity" );

            this.Quantity = quantity;
        }
    }
}
