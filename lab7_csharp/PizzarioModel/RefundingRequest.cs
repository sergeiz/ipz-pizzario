﻿using System;

namespace Pizzario.Model
{
    public class RefundingRequest
    {
        public int RequestId { get; set; }

        public virtual Order RelatedOrder { get; private set; }

        public RefundingStatus Status { get; private set; }


        protected RefundingRequest () {}


        public RefundingRequest ( Order order )
        {
            this.RelatedOrder = order;
            this.Status = Status;
        }


        public void Confirmed ()
        {
            if ( Status == RefundingStatus.Pending )
                Status = RefundingStatus.Confirmed;

            else
                throw new Exception( "RefundingRequest: confirmation possible in Pending state only" );
        }


        public void Rejected ()
        {
            if ( Status == RefundingStatus.Pending )
                Status = RefundingStatus.Rejected;

            else
                throw new Exception( "RefundingRequest: rejection possible in Pending state only" );
        }
    }
}
