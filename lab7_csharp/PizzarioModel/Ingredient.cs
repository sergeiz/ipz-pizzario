﻿using System;
using System.Text;

namespace Pizzario.Model
{
    public class Ingredient
    {
        public int IngredientId { get; set; }

        public string Name { get; private set; }


        protected Ingredient () {}


        public Ingredient ( string name )
        {
            if ( name.Length == 0 )
                throw new Exception( "Ingredient: empty name" );

            this.Name = name;
        }


        public int GetWeight ( PizzaSize size )
        {
            return weightPerSize[ ( int ) size ];
        }


        public void DefineWeights ( int smallestWeight )
        {
            if ( smallestWeight < 0 )
                throw new Exception( "Ingredient.SetWeight: negative weight not allowed" );

            for ( int i = 0; i < weightPerSize.Length; i++ )
                weightPerSize[ i ] = smallestWeight * ( i + 1 );
        }


        private int[] weightPerSize = new int[ Enum.GetValues( typeof( PizzaSize ) ).Length ];

        public string WeightEncoded
        {
            get
            {
                StringBuilder builder = new StringBuilder();
                for ( int i = 0; i < weightPerSize.Length; i++ )
                {
                    builder.Append( weightPerSize[ i ] );
                    builder.Append( ' ' );
                }

                return builder.ToString();
            }

            private set
            {
                string trimmedValue = value.TrimEnd();
                string[] parts = trimmedValue.Split( ' ' );
                for ( int i = 0; i < parts.Length; i++ )
                    weightPerSize[ i ] = int.Parse( parts[ i ] );
            }
        }
    }
}
