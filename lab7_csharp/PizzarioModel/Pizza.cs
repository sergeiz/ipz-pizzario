﻿using System;

namespace Pizzario.Model
{
    public class Pizza
    {
        public int PizzaId { get; set; }

        public virtual Order RelatedOrder { get; private set; }

        public virtual PizzaKind Kind { get; private set; }

        public PizzaSize Size { get; private set; }

        public CookingStatus Status { get; private set; }


        protected Pizza () {}


        public Pizza ( Order order, PizzaKind kind, PizzaSize size )
        {
            this.RelatedOrder = order;
            this.Kind = kind;
            this.Size = size;
            this.Status = CookingStatus.NotStarted;
        }

        public void OnCookingStarted ()
        {
            if ( Status == CookingStatus.NotStarted )
                Status = CookingStatus.Started;

            else
                throw new Exception( "Pizza.OnCookingStarted: may only start cooking if not started yet" );
        }


        public void OnCookingCancelled ()
        {
	        if ( Status == CookingStatus.Started || Status == CookingStatus.NotStarted )
                Status = CookingStatus.Cancelled;

            else
                throw new Exception( "Pizza.OnCookingCancelled: may only abort unfinished pizza" );
        }


        public void OnCookingFinished ()
        {
            if ( Status == CookingStatus.Started )
                Status = CookingStatus.Finished;

            else
                throw new Exception( "Pizza.OnCookingFinished: may only finish cooking if started" );
        }
    }
}
