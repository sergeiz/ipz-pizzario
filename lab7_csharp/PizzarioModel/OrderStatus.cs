﻿namespace Pizzario.Model
{
    public enum OrderStatus
    {
        Registered,
        Waiting,
        Cooking,
        Ready,
        Delivering,
        Delivered,
        Cancelled
    }
}
