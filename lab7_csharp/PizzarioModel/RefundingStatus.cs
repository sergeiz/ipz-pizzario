﻿namespace Pizzario.Model
{
    public enum RefundingStatus
    {
        Pending,
        Confirmed,
        Rejected
    }
}
