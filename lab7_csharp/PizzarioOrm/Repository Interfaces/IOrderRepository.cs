﻿using System;

using Pizzario.Model;
using System.Linq;

namespace Pizzario.Orm
{
    public interface IOrderRepository : IRepository< Order >
    {
        IQueryable< Order > LoadOrdersOfStatus ( OrderStatus status );

        IQueryable< OrderItem > LoadAllItems ();
    }
}
