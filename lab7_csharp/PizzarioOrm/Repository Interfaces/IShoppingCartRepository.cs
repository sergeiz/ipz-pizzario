﻿using System;
using System.Collections.Generic;

using Pizzario.Model;

namespace Pizzario.Orm
{
    public interface IShoppingCartRepository : IRepository< ShoppingCart >
    {
        void RemoveItem ( OrderItem item );
        void RemoveItems ( IEnumerable< OrderItem > items );
    }
}
