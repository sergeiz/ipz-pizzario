﻿using System;
using System.Linq;

namespace Pizzario.Orm
{
    public interface IRepository< T > where T : class
    {
        T Load ( int id );

        IQueryable< T > LoadAll ();

        void Add ( T t );

        void Delete ( T t );

        void Commit ();
    }
}
