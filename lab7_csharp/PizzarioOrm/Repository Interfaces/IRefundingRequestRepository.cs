﻿using System;
using System.Linq;

using Pizzario.Model;

namespace Pizzario.Orm
{
    public interface IRefundingRequestRepository : IRepository< RefundingRequest >
    {
        IQueryable< RefundingRequest > SearchRequestsOfStatus ( RefundingStatus status );
    }
}
