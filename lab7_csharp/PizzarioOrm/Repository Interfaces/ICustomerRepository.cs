﻿using System;

using Pizzario.Model;

namespace Pizzario.Orm
{
    public interface ICustomerRepository : IRepository< Customer >
    {
        Customer FindByEmail ( string email );
    }
}
