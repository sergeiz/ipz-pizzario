﻿using Pizzario.Model;

using System.Data.Entity;

namespace Pizzario.Orm
{
    public class PizzarioDbFacade : DbContext
    {
        static PizzarioDbFacade ()
        {
            Database.SetInitializer(
                new DropCreateDatabaseAlways< PizzarioDbFacade >()
            );
        }

        public DbSet< PizzaKind > PizzaKinds { get; set; }
        public DbSet< ShoppingCart > ShoppingCarts { get; set; }
        public DbSet< Pizza > Pizzas { get; set; }
        public DbSet< Order > Orders { get; set; }
        public DbSet< OrderItem > OrderItems { get; set; }
        public DbSet< RefundingRequest > RefundingRequests { get; set; }
        public DbSet<Customer> Customers { get; set; }

        protected override void OnModelCreating ( DbModelBuilder modelBuilder )
        {
            modelBuilder.Configurations.Add( new PizzaKindConfiguration() );
            modelBuilder.Configurations.Add( new PizzaRecipeConfiguration() );
            modelBuilder.Configurations.Add( new OrderItemConfiguration() );
            modelBuilder.Configurations.Add( new ShoppingCartConfiguration() );
            modelBuilder.Configurations.Add( new PizzaConfiguration() );
            modelBuilder.Configurations.Add( new PaymentPlanConfiguration() );
            modelBuilder.Configurations.Add( new OrderConfiguration() );
            modelBuilder.Configurations.Add( new RefundingRequestConfiguration() );
            modelBuilder.Configurations.Add( new IngredientConfiguration() );
            modelBuilder.Configurations.Add( new CustomerConfiguration() );

            modelBuilder.Configurations.Add( new CustomerContactConfiguration() );
            modelBuilder.Configurations.Add( new DiscountConfiguration() );
        }
    }
}
