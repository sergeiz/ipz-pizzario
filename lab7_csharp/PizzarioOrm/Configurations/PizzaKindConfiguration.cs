﻿using Pizzario.Model;

using System.Data.Entity.ModelConfiguration;

namespace Pizzario.Orm
{
    class PizzaKindConfiguration : EntityTypeConfiguration< PizzaKind >
    {
        public PizzaKindConfiguration ()
        {
            HasKey( k => k.PizzaKindId );
            Property( k => k.Name ).IsRequired();
            Property( k => k.PricesEncoded ).IsRequired();
            HasRequired( k => k.Recipe );
        }
    }
}
