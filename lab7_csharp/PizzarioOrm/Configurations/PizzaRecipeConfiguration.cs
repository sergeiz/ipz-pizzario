﻿using Pizzario.Model;

using System.Data.Entity.ModelConfiguration;

namespace Pizzario.Orm
{
    class PizzaRecipeConfiguration : EntityTypeConfiguration< PizzaRecipe >
    {
        public PizzaRecipeConfiguration ()
        {
            HasKey( r => r.PizzaRecipeId );
            HasMany( r => r.Ingredients ).WithRequired();
        }
    }
}
