﻿using Pizzario.Model;

using System.Data.Entity.ModelConfiguration;

namespace Pizzario.Orm
{
    class CustomerConfiguration : EntityTypeConfiguration< Customer >
    {
        public CustomerConfiguration ()
        {
            HasKey(c => c.CustomerId );
            HasMany< Order >( c => c.PastOrders ).WithOptional();
        }
    }
}
