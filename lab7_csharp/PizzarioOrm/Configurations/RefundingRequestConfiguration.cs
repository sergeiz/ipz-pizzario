﻿using Pizzario.Model;

using System.Data.Entity.ModelConfiguration;

namespace Pizzario.Orm
{
    class RefundingRequestConfiguration : EntityTypeConfiguration<RefundingRequest>
    {
        public RefundingRequestConfiguration ()
        {
            HasKey( r => r.RequestId );
            HasRequired( r => r.RelatedOrder );
        }
    }
}
