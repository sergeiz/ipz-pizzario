﻿using Pizzario.Model;

using System.Data.Entity.ModelConfiguration;

namespace Pizzario.Orm
{
    class DiscountConfiguration : ComplexTypeConfiguration< Discount >
    {
        public DiscountConfiguration ()
        {
            Property( d => d.Value ).IsRequired();
        }
    }
}
