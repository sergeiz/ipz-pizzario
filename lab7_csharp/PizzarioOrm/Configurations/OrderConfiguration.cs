﻿using Pizzario.Model;

using System.Data.Entity.ModelConfiguration;

namespace Pizzario.Orm
{
    class OrderConfiguration : EntityTypeConfiguration< Order >
    {
        public OrderConfiguration ()
        {
            HasKey( o => o.OrderId );
            HasMany< OrderItem >( o => o.Items ).WithOptional();
            HasOptional( o => o.PayPlan );
        }
    }
}
