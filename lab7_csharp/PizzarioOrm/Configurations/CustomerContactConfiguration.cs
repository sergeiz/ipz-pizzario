﻿using Pizzario.Model;

using System.Data.Entity.ModelConfiguration;

namespace Pizzario.Orm
{
    class CustomerContactConfiguration : ComplexTypeConfiguration< CustomerContact >
    {
        public CustomerContactConfiguration ()
        {
            Property( c => c.Email ).IsRequired();
            Property( c => c.Address ).IsRequired();
            Property( c => c.Phone ).IsRequired();
        }
    }
}
