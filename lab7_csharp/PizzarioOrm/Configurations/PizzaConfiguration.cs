﻿using Pizzario.Model;

using System.Data.Entity.ModelConfiguration;

namespace Pizzario.Orm
{
    class PizzaConfiguration : EntityTypeConfiguration< Pizza >
    {
        public PizzaConfiguration ()
        {
            HasKey( p => p.PizzaId );
            HasRequired( p => p.RelatedOrder );
            HasRequired( p => p.Kind );
        }
    }
}
