﻿using Pizzario.Model;

using System.Data.Entity.ModelConfiguration;

namespace Pizzario.Orm
{
    class IngredientConfiguration : EntityTypeConfiguration< Ingredient >
    {
        public IngredientConfiguration ()
        {
            HasKey( i => i.IngredientId );
            Property( i => i.WeightEncoded ).IsRequired();
        }
    }
}
