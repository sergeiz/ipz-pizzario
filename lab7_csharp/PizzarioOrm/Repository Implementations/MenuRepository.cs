﻿using System;
using System.Linq;

using Pizzario.Model;

namespace Pizzario.Orm
{
    class MenuRepository : BasicRepository< PizzaKind >, IMenuRepository
    {
        public MenuRepository ( PizzarioDbFacade dbContext )
            :   base( dbContext, dbContext.PizzaKinds )
        {
        }
        
        public PizzaKind FindByName ( string name )
        {
            var db = GetDBContext();
            return db.PizzaKinds.Where( pk => pk.Name == name ).SingleOrDefault();
        }

        public IQueryable< PizzaKind > LoadAllVisible ()
        {
            var db = GetDBContext();
            return db.PizzaKinds.Where( k => ! k.Hidden );
        }

        public IQueryable< PizzaKind > LoadAllHidden ()
        {
            var db = GetDBContext();
            return db.PizzaKinds.Where( k => k.Hidden );
        }
    }
}
