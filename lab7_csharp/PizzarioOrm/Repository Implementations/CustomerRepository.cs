﻿using System;
using System.Linq;

using Pizzario.Model;

namespace Pizzario.Orm
{
    class CustomerRepository : BasicRepository< Customer >, ICustomerRepository
    {
        public CustomerRepository ( PizzarioDbFacade dbContext )
            : base( dbContext, dbContext.Customers )
        {
        }

        public Customer FindByEmail ( string email )
        {
            var db = GetDBContext();
            return db.Customers.Where( c => c.Email == email ).FirstOrDefault();
        }
    }
}
