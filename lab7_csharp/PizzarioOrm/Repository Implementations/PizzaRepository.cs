﻿using System;
using System.Linq;

using Pizzario.Model;


namespace Pizzario.Orm
{
    class PizzaRepository : BasicRepository< Pizza >, IPizzaRepository
    {
        public PizzaRepository ( PizzarioDbFacade dbContext )
            :   base( dbContext, dbContext.Pizzas )
        {
        }


        public IQueryable< Pizza > SearchPizzasByOrderID ( int orderID )
        {
            var db = GetDBContext();
            return db.Pizzas.Where( p => p.RelatedOrder.OrderId == orderID );
        }


        public IQueryable< Pizza > SearchPizzasOfStatus ( CookingStatus status )
        {
            var db = GetDBContext();
            return db.Pizzas.Where( p => p.Status == status );
        }
    }
}
