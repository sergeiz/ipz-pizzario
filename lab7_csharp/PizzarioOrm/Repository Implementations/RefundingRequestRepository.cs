﻿using System;
using System.Linq;

using Pizzario.Model;


namespace Pizzario.Orm
{
    class RefundingRequestRepository : BasicRepository< RefundingRequest >, 
                                       IRefundingRequestRepository
    {
        public RefundingRequestRepository ( PizzarioDbFacade dbContext )
            :   base( dbContext, dbContext.RefundingRequests )
        {
        }

        public IQueryable< RefundingRequest > SearchRequestsOfStatus ( RefundingStatus status )
        {
            var dbContext = GetDBContext();
            return dbContext.RefundingRequests.Where( r => r.Status == status );
        }

    }
}
