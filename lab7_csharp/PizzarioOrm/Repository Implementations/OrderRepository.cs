﻿using System;

using Pizzario.Model;
using System.Linq;


namespace Pizzario.Orm
{
    class OrderRepository : BasicRepository< Order >, IOrderRepository
    {
        public OrderRepository ( PizzarioDbFacade dbContext )
            :   base( dbContext, dbContext.Orders )
        {
        }


        public IQueryable< Order > LoadOrdersOfStatus ( OrderStatus status )
        {
            var dbContext = GetDBContext();
            return dbContext.Orders.Where( o => o.Status == status );
        }


        public IQueryable< OrderItem > LoadAllItems ()
        {
            var dbContext = GetDBContext();
            return dbContext.OrderItems;
        }
    }
}
