﻿using System;
using System.Collections.Generic;
using System.Linq;

using Pizzario.Model;

namespace Pizzario.Orm
{
    class ShoppingCartRepository : BasicRepository< ShoppingCart >, 
                                   IShoppingCartRepository
    {
        public ShoppingCartRepository ( PizzarioDbFacade dbContext )
            :   base( dbContext, dbContext.ShoppingCarts )
        {
        }


        public void RemoveItem ( OrderItem item )
        {
            var dbContext = GetDBContext();
            dbContext.OrderItems.Remove( item );
        }


        public void RemoveItems ( IEnumerable< OrderItem > items )
        {
            var dbContext = GetDBContext();
            dbContext.OrderItems.RemoveRange( items );
        }
    }
}
