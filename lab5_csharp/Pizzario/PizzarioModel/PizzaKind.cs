﻿using System;
using System.Text;

namespace Pizzario.Model
{
    public class PizzaKind
    {
        public const int MaximumVote = 5;

        public int PizzaKindId { get; set; }

        public string Name
        {
            get { return name; }

            set
            {
                if ( value == null || value.Length == 0 )
                    throw new Exception( "PizzaKind: empty name" );

                if ( BeforeNameChanged != null && name != null && name != value )
                    BeforeNameChanged( this, new BeforeNameChangedEventArgs( name, value ) );

                name = value;
            }
        }


        public string Description { get; set; }
        
        public string ImageUrl { get; set; }
        
        public int Votes { get; private set; }

        public int TotalRating { get; private set; }

        public bool Hidden { get; set; }

        public PizzaRecipe Recipe { get; private set; }

        public double AverageRating
        {
            get
            {
                if ( Votes != 0 )
                    return ( double ) ( TotalRating ) / Votes;
                else
                    return 0.0;
            }
        }


        private PizzaKind () { }


        public PizzaKind ( string name )
        {
            this.Name = name;
            this.Recipe = new PizzaRecipe();

            this.Description = "";
            this.ImageUrl = "";
            this.Votes = 0;
            this.TotalRating = 0;
            this.Hidden = false;
        }


        public void Rate ( int rating )
        {
            if ( rating <= 0 || rating > MaximumVote )
                throw new System.Exception( "PizzaKind.Rate: expecting rating within [1;" + MaximumVote + "] range" );

            else
            {
                this.TotalRating += rating;
                this.Votes++;
            }
        }


        public void SetupRatingStats ( int votes, int totalRating )
        {
            if ( votes < 0 )
                throw new System.Exception( "PizzaKind.SetupRatingStats: negative votes" );

            else if ( votes == 0 && totalRating != 0 )
                throw new System.Exception( "PizzaKind.SetupRatingStats: zero votes can only give zero ratings" );

            else if ( votes > 0 && totalRating < votes )
                throw new System.Exception( "PizzaKind.SetupRatingStats: total rating may not be smaller than number of votes" );

            else if ( votes > 0 && totalRating > ( votes * MaximumVote ) )
                throw new System.Exception( "PizzaKind.SetupRatingStats: total rating may not exceed maximum from all votes" );

            this.Votes = votes;
            this.TotalRating = totalRating;
        }


        public decimal GetCurrentPrice ( PizzaSize size )
        {
            return priceBySize[ ( int ) size ];
        }

        public void UpdatePrice ( PizzaSize size, decimal price )
        {
            if ( price < 0 )
                throw new System.Exception( "PizzaKind.UpdatePrice: price cannot be negative" );

            priceBySize[ ( int ) size ] = price;
        }


        public class BeforeNameChangedEventArgs : EventArgs
        {
            public String OldName { get; private set; }
            public String NewName { get; private set; }

            public BeforeNameChangedEventArgs ( string oldName, string newName )
            {
                this.OldName = oldName;
                this.NewName = newName;
            }
        }

        public delegate void BeforeNameChangedHandler ( object sender, BeforeNameChangedEventArgs e );

        public event BeforeNameChangedHandler BeforeNameChanged;


        private string name;

        private decimal[] priceBySize = new decimal[ Enum.GetNames( typeof( PizzaSize ) ).Length ];

        public String PricesEncoded
        {
            get
            {
                StringBuilder builder = new StringBuilder();
                for ( int i = 0; i < priceBySize.Length; i++ )
                {
                    if ( i != 0 )
                        builder.Append( ' ' );
                    builder.Append( priceBySize[ i ] );
                }

                return builder.ToString();
            }
            private set
            {
                string[] parts = value.Split( ' ' );
                for ( int i = 0; i < parts.Length; i++ )
                    priceBySize[ i ] = decimal.Parse( parts[ i ] );
            }
        }
    }
}
