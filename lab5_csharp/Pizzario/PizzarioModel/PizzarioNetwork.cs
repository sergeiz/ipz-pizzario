﻿using System;
using System.Collections.Generic;

namespace Pizzario.Model
{
    public class PizzarioNetwork
    {
        public Menu getMenu ()
        {
            return menu;
        }

        public IList< Order > Orders 
        { 
            get
            {
                return orders;
            }
        }


        public Order newOrder ( DeliveryContact contact, PaymentPlan payPlan )
        {
            Order o = new Order( orders.Count, new OrderCart(), contact, payPlan );
            orders.Add( o );
            return o;
        }


        private Menu menu = new Menu();
        private List< Order > orders = new List< Order >();
    }
}
