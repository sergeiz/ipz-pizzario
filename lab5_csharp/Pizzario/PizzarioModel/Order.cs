﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Pizzario.Model
{
    public class Order
    {
        public int OrderId { get; set; }

        public OrderCart Cart { get; private set; }

        public PaymentPlan PayPlan { get; private set; }

        public DeliveryContact Contact { get; set; }

        public OrderStatus Status { get; private set; }

        public ICollection< Pizza > Pizzas 
        { 
            get { return pizzas; }
        }

        public double Discount
        {
            get
            {
                return discount;
            }
            set
            {
                if ( value < 0.0 || value > 1.0 )
                    throw new System.Exception( "Order: discount should be between [0.0;1.0]" );

                this.discount = value;
            }
        }

        public decimal TotalCost
        {
            get
            {
                return ( decimal ) ( ( double ) Cart.Cost * ( 1.0 - Discount ) );
            }
        }


        private Order () {}

        public Order ( int id, OrderCart cart, DeliveryContact contact, PaymentPlan payPlan )
        {
            if ( ! cart.Modifiable )
                throw new System.Exception( "Order: initializing with cart that is already checked out" );

            this.OrderId = id;
            this.Cart    = cart;
            this.Contact = contact;
            this.PayPlan = payPlan;

            this.Status   = OrderStatus.New;

            this.Cart.CheckoutEvent += ( object sender, EventArgs args ) => HandleCheckout();
        }


        public int GetPizzasCount ()
        {
            return pizzas.Count;
        }


        public Pizza GetPizza ( int index )
        {
            return pizzas[ index ];
        }


        public void Cancel ()
        {
	        switch ( Status )
	        {
		        case OrderStatus.New:
		        case OrderStatus.Registered:
		        case OrderStatus.Cooking:
		        case OrderStatus.Ready4Delivery:
			        break;

		        default:
			        throw new System.Exception( "Order.Cancel - cannot cancel in current order state" );
	        }

	        Status = OrderStatus.Cancelled;

            foreach ( Pizza p in pizzas )
                if ( p.Status != CookingStatus.Finished )
                    p.OnCookingCancelled();
        }


        public void OnStartedDelivery ()
        {
            if ( Status != OrderStatus.Ready4Delivery )
                throw new System.Exception( "Order.OnStartedDelivery - can only happen in Ready4Delivery state" );

            Status = OrderStatus.Delivering;
        }


        public void OnDelivered ()
        {
            if ( Status != OrderStatus.Delivering )
                throw new System.Exception( "Order.OnDelivered - can only happen in Delivering state" );

            if ( ! PayPlan.ExpectPrepayment() && ! PayPlan.Payed )
                throw new System.Exception( "Order.OnDelivered - post-payment should have been collected by now" );

            Status = OrderStatus.Delivered;
        }

        
        private void HandleCheckout ()
        {
            if ( Status != OrderStatus.New )
                throw new System.Exception( "Order.HandleCheckout - Can only process new order!" );

            if ( PayPlan.ExpectPrepayment() && !PayPlan.Payed )
                throw new System.Exception( "Order.HandleCheckout - pre-payment unavailable" );

            Status = OrderStatus.Registered;
            
            int nItems = Cart.GetItemsCount();
            for ( int i = 0; i < nItems; i++ )
            {
                OrderItem item = Cart.GetItem( i );
                for ( int k = 0; k < item.Quantity; k++ )
                {
                    Pizza p = new Pizza( item.Kind, item.Size );
                    pizzas.Add( p );

                    p.CookingStarted += ( object sender, EventArgs args ) => HandlePizzaCookingStarted();
                    p.CookingFinished += ( object sender, EventArgs args ) => HandlePizzaReady();
                }
            }
        }


        private void HandlePizzaCookingStarted ()
        {
            if ( Status == OrderStatus.Registered )
                Status = OrderStatus.Cooking;

            else if ( Status != OrderStatus.Cooking )
                throw new System.Exception( "Order.HandlePizzaCookingStarted - can only happen in Registered & Cooking states" );
        }


        private void HandlePizzaReady ()
        {
            if ( Status != OrderStatus.Cooking )
                throw new System.Exception( "Order.HandlePizzaReady - can only happen in Cooking state" );

            if ( Pizzas.All( ( Pizza p ) => p.Status == CookingStatus.Finished ) )
                Status = OrderStatus.Ready4Delivery;
        }


        private List< Pizza > pizzas = new List< Pizza >();
        private double discount = 0.0;
    }
}
