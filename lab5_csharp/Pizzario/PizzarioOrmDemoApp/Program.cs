﻿using Pizzario.Model;
using Pizzario.Model.Test;
using Pizzario.Report;
using Pizzario.Orm;

using System;
using System.IO;
using System.Text;

namespace Pizzario.Orm.DemoApp
{
    public class Program
    {
        public static void Main ( string[] args )
        {
            TestModelGenerator testModelGenerator = new TestModelGenerator();
            PizzarioNetwork network1 = testModelGenerator.generate();

            PizzarioNetworkOrm.Save( network1 );

            PizzarioNetwork network2 = PizzarioNetworkOrm.Restore();

            compareNetworks( network1, network2 );
        }


        private static void compareNetworks ( PizzarioNetwork network1, PizzarioNetwork network2 )
        {
            StringWriter writer1 = new StringWriter( new StringBuilder() );
            var reportGenerator1 = new ReportGenerator( writer1, network1 );
            reportGenerator1.generate();

            StringWriter writer2 = new StringWriter( new StringBuilder() );
            var reportGenerator2 = new ReportGenerator( writer2, network2 );
            reportGenerator2.generate();

            String report1Content = writer1.ToString();
            String report2Content = writer2.ToString();

            Console.WriteLine( report2Content );
            Console.WriteLine( report1Content.Equals( report2Content ) ? "PASSED: Models match" : "FAILED: Models mismatch" );
        }
    }
}
