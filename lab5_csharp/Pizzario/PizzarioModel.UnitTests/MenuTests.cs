﻿using Pizzario.Model;

using NUnit.Framework;
using System;

namespace Pizzario.Model.UnitTests
{
    [ TestFixture ]
    public class MenuTests
    {
        [ Test ]
        public void Kinds_Initially_Empty ()
        {
            Menu menu = new Menu();

            Assert.AreEqual( menu.GetPizzaKindsCount(), 0 );
        }


        [ Test ]
        public void Kinds_AddOne_FindIt ()
        {
            Menu menu = new Menu();
            PizzaKind carbonara = new PizzaKind( "Carbonara" );
            menu.AddPizzaKind( carbonara );

            Assert.AreEqual( menu.GetPizzaKindsCount(), 1 );
            Assert.AreSame( menu.FindPizzaKind( "Carbonara" ), carbonara );
        }


        [ Test ]
        public void Kinds_AddTwo_FindBoth ()
        {
            Menu menu = new Menu();
            
            PizzaKind carbonara = new PizzaKind( "Carbonara" );
            PizzaKind milano = new PizzaKind( "Milano" );
            menu.AddPizzaKind( carbonara );
            menu.AddPizzaKind( milano );

            Assert.AreEqual( menu.GetPizzaKindsCount(), 2 );
            Assert.AreSame( menu.FindPizzaKind( "Carbonara" ), carbonara );
            Assert.AreSame( menu.FindPizzaKind( "Milano" ), milano );
        }


        [ Test ]
        public void Kinds_FindMissingInEmpty_ReturnsNull ()
        {
            Menu menu = new Menu();

            Assert.IsNull( menu.FindPizzaKind( "Carbonara" ) );
        }


        [ Test ]
        public void Kinds_FindMissingInNonEmpty_ReturnsNull ()
        {
            Menu menu = new Menu();
            menu.AddPizzaKind( new PizzaKind( "Carbonara" ) );

            Assert.IsNull( menu.FindPizzaKind( "Milano" ) );
        }


        [ Test ]
        public void Kinds_AddDuplicate_Fails ()
        {
            Menu menu = new Menu();
            menu.AddPizzaKind( new PizzaKind( "Carbonara" ) );

            Assert.Throws< Exception >( () => menu.AddPizzaKind( new PizzaKind( "Carbonara" ) ) );
        }


        [ Test ]
        public void Kinds_DeleteSingle_MakesEmpty ()
        {
            Menu menu = new Menu();
            menu.AddPizzaKind( new PizzaKind( "Carbonara" ) );
            menu.DeletePizzaKind( "Carbonara" );

            Assert.AreEqual( menu.GetPizzaKindsCount(), 0 );
        }


        [ Test ]
        public void Kinds_DeleteOneOfTwo_LeavesOther ()
        {
            Menu menu = new Menu();
            menu.AddPizzaKind( new PizzaKind( "Carbonara" ) );
            menu.AddPizzaKind( new PizzaKind( "Milano" ) );
            menu.DeletePizzaKind( "Carbonara" );

            Assert.AreEqual( menu.GetPizzaKindsCount(), 1 );

            var pizzaKindsEnumerator = menu.PizzaKinds.GetEnumerator();
            pizzaKindsEnumerator.MoveNext();
            Assert.AreEqual( pizzaKindsEnumerator.Current.Name, "Milano" );
        }


        [ Test ]
        public void Kinds_DeleteMissingInEmpty_Forbidden ()
        {
            Menu menu = new Menu();

            Assert.Throws< Exception >( () => menu.DeletePizzaKind( "Milano" ) );
        }


        [ Test ]
        public void Kinds_DeleteMissingInNonEmpty_Forbidden ()
        {
            Menu menu = new Menu();
            menu.AddPizzaKind( new PizzaKind( "Carbonara" ) );

            Assert.Throws< Exception >( () => menu.DeletePizzaKind( "Milano" ) );
        }


        [ Test ]
        public void Rename_TheOnly_AccessibleUnderNewName ()
        {
            Menu menu = new Menu();
            PizzaKind carbonara = new PizzaKind( "carbonara" );
            menu.AddPizzaKind( carbonara );

            carbonara.Name = "Carbonara";

            Assert.AreSame( menu.FindPizzaKind( "Carbonara" ), carbonara );
            Assert.AreEqual( menu.GetPizzaKindsCount(), 1 );
        }


        [ Test ]
        public void Rename_OneOfTwo_AccessibleUnderNewName_SecondUntouched ()
        {
            Menu menu = new Menu();
            PizzaKind carbonara = new PizzaKind( "carbonara" );
            PizzaKind milano = new PizzaKind( "Milano" );
            menu.AddPizzaKind( carbonara );
            menu.AddPizzaKind( milano );

            carbonara.Name = "Carbonara";

            Assert.AreSame( menu.FindPizzaKind( "Carbonara" ), carbonara );
            Assert.AreSame( menu.FindPizzaKind( "Milano" ), milano );
            Assert.AreEqual( menu.GetPizzaKindsCount(), 2 );
        }


        [ Test ]
        public void Rename_ToExisting_Forbidden ()
        {
            Menu menu = new Menu();
            PizzaKind carbonara = new PizzaKind( "Carbonara" );
            PizzaKind milano = new PizzaKind( "Milano" );
            menu.AddPizzaKind( carbonara );
            menu.AddPizzaKind( milano );

            Assert.Throws< Exception >( () => carbonara.Name = "Milano" );
        }


        [ Test ]
        public void Rename_Deleted_UnaffectsMenu ()
        {
            Menu menu = new Menu();
            PizzaKind carbonara = new PizzaKind( "Carbonara" );
            menu.AddPizzaKind( carbonara );
            menu.DeletePizzaKind( "Carbonara" );

            Assert.AreEqual( menu.GetPizzaKindsCount(), 0 );
            Assert.DoesNotThrow( () => carbonara.Name = "New Carbonara" );
        }


        [ Test ]
        public void OrderItem_Test_ExpectedResult ()
        {
            PizzaKind carbonara = new PizzaKind( "Carbonara" );
            carbonara.UpdatePrice( PizzaSize.Small, 3.0M );

            Menu menu = new Menu();
            menu.AddPizzaKind( carbonara );

            OrderItem item = menu.MakeOrderItem( "Carbonara", PizzaSize.Small, 1 );
            Assert.AreSame( item.Kind, carbonara );
            Assert.AreEqual( item.Quantity, 1 );
            Assert.AreEqual( item.Size, PizzaSize.Small );
            Assert.AreEqual( item.FixedPrice, 3.0M );
        }
    }
}
