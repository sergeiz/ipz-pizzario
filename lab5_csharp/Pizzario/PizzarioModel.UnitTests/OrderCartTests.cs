﻿using Pizzario.Model;

using NUnit.Framework;
using NSubstitute;
using System;


namespace Pizzario.Model.UnitTests
{
    [ TestFixture ]
    public class OrderCartTests
    {

        [ Test ]
        public void Items_None_Initially ()
        {
            OrderCart cart = new OrderCart();

            Assert.AreEqual( cart.GetItemsCount(), 0 );
        }


        [ Test ]
        public void Add_One_ListOne ()
        {
            OrderCart cart = new OrderCart();
            OrderItem item = makeSmallItem();
            cart.AddItem( item );

            Assert.AreEqual( cart.GetItemsCount(), 1 );
            Assert.AreSame( cart.GetItem( 0 ), item );
        }


        [ Test ]
        public void Add_Two_ListTwo ()
        {
            OrderCart cart = new OrderCart();
            OrderItem item1 = makeSmallItem();
            OrderItem item2 = makeMediumItem();
            cart.AddItem( item1 );
            cart.AddItem( item2 );

            Assert.AreEqual( cart.GetItemsCount(), 2 );
            Assert.AreSame( cart.GetItem( 0 ), item1 );
            Assert.AreSame( cart.GetItem( 1 ), item2 );
        }


        [ Test ]
        public void Add_TwoSameKindSize_Fails ()
        {
            OrderCart cart = new OrderCart();
            PizzaKind kind = makePizzaKind();
            cart.AddItem( new OrderItem( kind, PizzaSize.Small, 3.0M, 1 ) );
            
            Assert.Throws< Exception >( () => cart.AddItem( new OrderItem( kind, PizzaSize.Small, 3.0M, 1 ) ) );
        }


        [ Test ]
        public void Add_TwoSameKindDifferentSize_Passes ()
        {
            OrderCart cart = new OrderCart();
            PizzaKind kind = makePizzaKind();
            OrderItem itemS = new OrderItem( kind, PizzaSize.Small,  3.0M, 1 );
            OrderItem itemM = new OrderItem( kind, PizzaSize.Medium, 3.0M, 1 );
            cart.AddItem( itemS );
            cart.AddItem( itemM );

            Assert.AreEqual( cart.GetItemsCount(), 2 );
            Assert.AreSame( cart.GetItem( 0 ), itemS );
            Assert.AreSame( cart.GetItem( 1 ), itemM );
        }


        [ Test ]
        public void Add_TwoDifferentKindSameSize_Passes ()
        {
            OrderCart cart = new OrderCart();
            PizzaKind kind1 = makePizzaKind();
            PizzaKind kind2 = makeAnotherPizzaKind();
            OrderItem item1 = new OrderItem( kind1, PizzaSize.Small, 3.0M, 1 );
            OrderItem item2 = new OrderItem( kind2, PizzaSize.Small, 3.0M, 1 );
            cart.AddItem( item1 );
            cart.AddItem( item2 );

            Assert.AreEqual( cart.GetItemsCount(), 2 );
            Assert.AreSame( cart.GetItem( 0 ), item1 );
            Assert.AreSame( cart.GetItem( 1 ), item2 );
        }


        [ Test ]
        public void Update_OnEmpty_Fails ()
        {
            OrderCart cart = new OrderCart();
            Assert.Throws< ArgumentOutOfRangeException >( () => cart.UpdateItem( 0, makeSmallItem() ) );
        }


        [ Test ]
        public void Update_OnNonEmptyGoodIndex_Passes ()
        {
            OrderCart cart = new OrderCart();
            cart.AddItem( makeSmallItem() );
            OrderItem newItem = makeMediumItem();
            cart.UpdateItem( 0, newItem );

            Assert.AreEqual( cart.GetItemsCount(), 1 );
            Assert.AreSame( cart.GetItem( 0 ), newItem );
        }


        [ Test ]
        public void Update_OnNonEmptyBadIndex_Fails ()
        {
            OrderCart cart = new OrderCart();
            cart.AddItem( makeSmallItem() );
            
            Assert.Throws< ArgumentOutOfRangeException >( () => cart.UpdateItem( 1, makeMediumItem() ) );
            Assert.Throws< ArgumentOutOfRangeException >( () => cart.UpdateItem( -1, makeMediumItem() ) );
        }

        [ Test ]
        public void Drop_Empty_Fails ()
        {
            OrderCart cart = new OrderCart();

            Assert.Throws< ArgumentOutOfRangeException >( () => cart.DropItem( 0 ) );
        }


        [ Test ]
        public void Drop_NonEmptyGoodIndex_Passes ()
        {
            OrderCart cart = new OrderCart();
            cart.AddItem( makeSmallItem() );
            cart.DropItem( 0 );

            Assert.AreEqual( cart.GetItemsCount(), 0 );
        }


        [ Test ]
        public void Drop_NonEmptyBadIndex_Fails ()
        {
            OrderCart cart = new OrderCart();
            cart.AddItem( makeSmallItem() );

            Assert.Throws< ArgumentOutOfRangeException >( () => cart.DropItem( 1 ) );
            Assert.Throws< ArgumentOutOfRangeException >( () => cart.DropItem( -1 ) );
        }


        [ Test ]
        public void Clear_Empty_Passes ()
        {
            OrderCart cart = new OrderCart();
            cart.ClearItems();
            Assert.AreEqual( cart.GetItemsCount(), 0 );
        }


        [ Test ]
        public void Clear_NonEmpty_Passes ()
        {
            OrderCart cart = new OrderCart();
            cart.AddItem( makeSmallItem() );
            cart.AddItem( makeMediumItem() );
            cart.ClearItems();
            Assert.AreEqual( cart.GetItemsCount(), 0 );
        }


        [ Test ]
        public void Cost_Empty_Zero ()
        {
            OrderCart cart = new OrderCart();

            Assert.AreEqual( cart.Cost, 0.0M );
        }


        [ Test ]
        public void Cost_SingleItem_MatchesItem ()
        {
            OrderCart cart = new OrderCart();
            cart.AddItem( makeSmallItem() );

            Assert.AreEqual( cart.Cost, cart.GetItem( 0 ).Cost );
        }


        [ Test ]
        public void Cost_TwoItems_MatchesSumItems ()
        {
            OrderCart cart = new OrderCart();
            cart.AddItem( makeSmallItem() );
            cart.AddItem( makeMediumItem() );

            Assert.AreEqual( cart.Cost, cart.GetItem( 0 ).Cost + cart.GetItem( 1 ).Cost );
        }


        [ Test ]
        public void Modifiable_IsTrue_Initially ()
        {
            OrderCart cart = new OrderCart();
            Assert.True( cart.Modifiable );
        }


        [ Test ]
        public void Modifiable_AfterCheckout_IsFalse ()
        {
            OrderCart cart = new OrderCart();
            cart.AddItem( makeSmallItem() );
            cart.Checkout();

            Assert.False( cart.Modifiable );
        }


        [ Test ]
        public void Checkout_Empty_Fails ()
        {
            OrderCart cart = new OrderCart();

            Assert.Throws< Exception >( () => cart.Checkout() );
        }


        [ Test ]
        public void Checkout_SendsEvent ()
        {
            OrderCart cart = new OrderCart();
            cart.AddItem( makeSmallItem() );

            var mockHandler = Substitute.For< EventHandler >();
            cart.CheckoutEvent += mockHandler;

            cart.Checkout();

            mockHandler.Received( 1 ).Invoke( cart, EventArgs.Empty );
        }


        [ Test ]
        public void Unmodifiable_ReadingItems_Passes ()
        {
            OrderCart cart = new OrderCart();
            OrderItem item1 = makeSmallItem();
            OrderItem item2 = makeMediumItem();
            cart.AddItem( item1 );
            cart.AddItem( item2 );

            cart.Checkout();

            Assert.AreEqual( cart.GetItemsCount(), 2 );
            Assert.AreSame( cart.GetItem( 0 ), item1 );
            Assert.AreSame( cart.GetItem( 1 ), item2 );
        }


        [ Test ]
        public void Unmodifiable_AddItem_Forbidden ()
        {
            OrderCart cart = new OrderCart();
            cart.AddItem( makeSmallItem() );
            cart.Checkout();

            Assert.Throws< Exception >( () => cart.AddItem( makeMediumItem() ) );
        }


        [ Test ]
        public void Unmodifiable_UpdateItem_Forbidden ()
        {
            OrderCart cart = new OrderCart();
            cart.AddItem( makeSmallItem() );
            cart.Checkout();

            Assert.Throws< Exception >( () => cart.UpdateItem( 0, makeMediumItem() ) );
        }


        [ Test ]
        public void Unmodifiable_DropItem_Forbidden ()
        {
            OrderCart cart = new OrderCart();
            cart.AddItem( makeSmallItem() );
            cart.Checkout();

            Assert.Throws< Exception >( () => cart.DropItem( 0 ) );
        }


        [ Test ]
        public void Unmodifiable_ClearItems_Forbidden ()
        {
            OrderCart cart = new OrderCart();
            cart.AddItem( makeSmallItem() );
            cart.Checkout();

            Assert.Throws< Exception >( () => cart.ClearItems() );
        }


        private PizzaKind makePizzaKind ()
        {
            return new PizzaKind( "Carbonara" );
        }


        private PizzaKind makeAnotherPizzaKind ()
        {
            return new PizzaKind( "Milano" );
        }


        private OrderItem makeSmallItem ()
        {
            return new OrderItem( makePizzaKind(), PizzaSize.Small, 3.0M, 1 );
        }


        private OrderItem makeMediumItem ()
        {
            return new OrderItem( makeAnotherPizzaKind(), PizzaSize.Medium, 5.0M, 1 );
        }
    }
}
