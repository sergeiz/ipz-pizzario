﻿using Pizzario.Model;

using NUnit.Framework;
using System;


namespace Pizzario.Model.UnitTests
{
    [ TestFixture ]
    public class CashPaymentPlanTests
    {
        [ Test ]
        public void Payed_Initially_False ()
        {
            PaymentPlan p = new CashPaymentPlan();
            Assert.False( p.Payed );
        }


        [ Test ]
        public void Payed_SetPayedToTrue_Confirmed ()
        {
            PaymentPlan p = new CashPaymentPlan();
            p.MarkPayed();
            Assert.True( p.Payed );
        }


        [ Test ]
        public void Prepayment_NotExpected ()
        {
            PaymentPlan p = new CashPaymentPlan();
            Assert.False( p.ExpectPrepayment() );
        }


        [ Test ]
        public void Clone_Payed_StillPayed ()
        {
            PaymentPlan p1 = new CashPaymentPlan();
            p1.MarkPayed();
            PaymentPlan p2 = p1.Clone();

            Assert.True( p2.Payed );
        }


        [ Test ]
        public void Clone_Unpayed_StillUnpayed ()
        {
            PaymentPlan p1 = new CashPaymentPlan();
            PaymentPlan p2 = p1.Clone();

            Assert.False( p2.Payed );
        }
    }
}
