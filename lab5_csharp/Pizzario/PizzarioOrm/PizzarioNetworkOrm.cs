﻿using Pizzario.Model;

using System.Linq;
using System.Data.Entity;

namespace Pizzario.Orm
{
    public static class PizzarioNetworkOrm
    {
        public static void Save ( PizzarioNetwork network )
        {
            using ( var context = new PizzarioDbContext() )
            {
                context.Database.Delete();
                context.PizzaKinds.AddRange( network.getMenu().PizzaKinds );
                context.Orders.AddRange( network.Orders );
                context.SaveChanges();
            }
        }

        public static PizzarioNetwork Restore ()
        {
            PizzarioNetwork network = new PizzarioNetwork();

            using ( var context = new PizzarioDbContext() )
            {
                var pizzaKindsQuery = context.PizzaKinds.Include( k => k.Recipe );
                foreach ( PizzaKind kind in pizzaKindsQuery )
                    network.getMenu().AddPizzaKind( kind );

                var ordersQuery =
                    context.Orders
                        .Include( o => o.Cart.Items )
                        .Include( o => o.Pizzas )
                        .Include( o => o.PayPlan );

                foreach ( Order o in ordersQuery )
                    network.Orders.Add( o );
            }

            return network;
        }
    }
}
