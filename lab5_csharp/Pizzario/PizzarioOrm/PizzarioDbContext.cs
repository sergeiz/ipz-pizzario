﻿using Pizzario.Model;

using System.Data.Entity;

namespace Pizzario.Orm
{
    public class PizzarioDbContext : DbContext
    {
        static PizzarioDbContext ()
        {
            Database.SetInitializer(
                new DropCreateDatabaseIfModelChanges< PizzarioDbContext >()
            );
        }

        public DbSet< PizzaKind > PizzaKinds { get; set; }
        public DbSet< Order > Orders { get; set; }

        protected override void OnModelCreating ( DbModelBuilder modelBuilder )
        {
            modelBuilder.Configurations.Add( new PizzaKindConfiguration() );
            modelBuilder.Configurations.Add( new PizzaRecipeConfiguration() );
            modelBuilder.Configurations.Add( new OrderItemConfiguration() );
            modelBuilder.Configurations.Add( new OrderCartConfiguration() );
            modelBuilder.Configurations.Add( new PizzaConfiguration() );
            modelBuilder.Configurations.Add( new PaymentPlanConfiguration() );
            modelBuilder.Configurations.Add( new OrderConfiguration() );

            modelBuilder.Configurations.Add( new DeliveryContactConfiguration() );
        }
    }
}
