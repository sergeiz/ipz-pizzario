﻿using Pizzario.Model;

using System.Data.Entity.ModelConfiguration;

namespace Pizzario.Orm
{
    class OrderCartConfiguration : EntityTypeConfiguration< OrderCart >
    {
        public OrderCartConfiguration ()
        {
            HasKey( c => c.OrderCartId );
            HasMany< OrderItem >( c => c.Items ).WithRequired();
        }
    }
}
