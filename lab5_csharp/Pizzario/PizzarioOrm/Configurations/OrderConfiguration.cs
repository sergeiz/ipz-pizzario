﻿using Pizzario.Model;

using System.Data.Entity.ModelConfiguration;

namespace Pizzario.Orm
{
    class OrderConfiguration : EntityTypeConfiguration< Order >
    {
        public OrderConfiguration ()
        {
            HasKey( o => o.OrderId );
            HasMany< Pizza >( o => o.Pizzas ).WithRequired();
            HasRequired( o => o.Cart );
            HasRequired( o => o.PayPlan );
        }
    }
}
