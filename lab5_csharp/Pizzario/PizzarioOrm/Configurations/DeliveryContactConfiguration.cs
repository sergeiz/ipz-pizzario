﻿using Pizzario.Model;

using System.Data.Entity.ModelConfiguration;

namespace Pizzario.Orm
{
    class DeliveryContactConfiguration : ComplexTypeConfiguration< DeliveryContact >
    {
        public DeliveryContactConfiguration ()
        {
            Property( c => c.Address ).IsRequired();
            Property( c => c.Phone ).IsRequired();
            Property( c => c.Comment ).HasMaxLength( 200 );
        }
    }
}
