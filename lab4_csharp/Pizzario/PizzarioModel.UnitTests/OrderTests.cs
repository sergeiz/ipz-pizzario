﻿using Pizzario.Model;

using NUnit.Framework;
using NSubstitute;
using System;
using System.Collections.Generic;

namespace Pizzario.Model.UnitTests
{
    [ TestFixture ]
    public class OrderTests
    {

        [ Test ]
        public void Constructor_ProjectsProperties_Correctly ()
        {
            OrderCart cart = new OrderCart();
            DeliveryContact contact = new DeliveryContact();
            PaymentPlan payPlan = new CashPaymentPlan();

            Order o = new Order( 1, cart, contact, payPlan );

            Assert.AreEqual( o.OrderId, 1 );
            Assert.AreSame( o.Cart, cart );
            Assert.AreSame( o.Contact, contact );
            Assert.AreSame( o.PayPlan, payPlan );
        }


        [ Test ]
        public void Constructor_FromUnmodifiableCart_Forbidden ()
        {
            OrderCart cart = new OrderCart();
            cart.AddItem( makeSomeItem() );
            cart.Checkout();

            Assert.Throws< Exception >( () => makeOrder( cart ) );
        }


        [ Test ]
        public void Constructor_NoPizzasInitially_EvenWhenCartHasItems ()
        {
            OrderCart cart = new OrderCart();
            cart.AddItem( makeSomeItem() );

            Order o = makeOrder( cart );

            Assert.AreEqual( o.GetPizzasCount(), 0 );
        }


        [ Test ]
        public void TotalCost_WithoutDiscount_MatchesCartCost ()
        {
            Order o = makeOrder();
            o.Discount = 0.0;

            Assert.AreEqual( o.TotalCost, o.Cart.Cost );
        }


        [ Test ]
        public void TotalCost_WithDiscount_ReducesCartCost ()
        {
            Order o = makeOrder();
            o.Discount = 0.5;

            Assert.AreEqual( o.TotalCost, o.Cart.Cost / 2 );
        }


        [ Test ]
        public void TotalCost_WithFullDiscount_MakesOrderFree ()
        {
            Order o = makeOrder();
            o.Discount = 1.0;

            Assert.AreEqual( o.TotalCost, 0 );
        }


        [ Test ]
        public void Discount_Initially_Zero ()
        {
            Order o = makeOrder();

            Assert.AreEqual( o.Discount, 0.0 );
        }


        [ Test ]
        public void Discount_SetValid_ReadBack ()
        {
            Order o = makeOrder();
            o.Discount = 0.2;

            Assert.AreEqual( o.Discount, 0.2 );
        }


        [ Test ]
        public void Discount_SetNegative_Forbidden ()
        {
            Order o = makeOrder();
            
            Assert.Throws< Exception >( () => o.Discount = -0.01 );
        }


        [ Test ]
        public void Discount_SetMoreThan100Percent_Forbidden ()
        {
            Order o = makeOrder();

            Assert.Throws< Exception >( () => o.Discount = 1.01 );
        }


        [ Test ]
        public void Pizzas_ForSingleItem_SingleMatchingPizza ()
        {
            OrderCart cart = new OrderCart();
            cart.AddItem( makeSomeItem( 1 ) );
            Order o = makeOrder( cart );
            cart.Checkout();

            Assert.AreEqual( o.GetPizzasCount(), 1 );
            Assert.AreSame( o.GetPizza( 0 ).Kind, cart.GetItem( 0 ).Kind );
            Assert.AreEqual( o.GetPizza( 0 ).Size, cart.GetItem( 0 ).Size );
        }


        [ Test ]
        public void Pizzas_ForDoubleItem_TwoMatchingPizzas ()
        {
            OrderCart cart = new OrderCart();
            cart.AddItem( makeSomeItem( 2 ) );
            Order o = makeOrder( cart );
            cart.Checkout();

            Assert.AreEqual( o.GetPizzasCount(), 2 );
            Assert.AreSame(  o.GetPizza( 0 ).Kind, cart.GetItem( 0 ).Kind );
            Assert.AreSame(  o.GetPizza( 1 ).Kind, cart.GetItem( 0 ).Kind );
            Assert.AreEqual( o.GetPizza( 0 ).Size, cart.GetItem( 0 ).Size );
            Assert.AreEqual( o.GetPizza( 1 ).Size, cart.GetItem( 0 ).Size );
        }


        [ Test ]
        public void Pizzas_ForTwoSingleItems_TwoDifferentMatchingPizzas ()
        {
            OrderCart cart = new OrderCart();
            cart.AddItem( makeSomeItem( 1 ) );
            cart.AddItem( makeSomeOtherItem( 1 ) );
            Order o = makeOrder( cart );
            cart.Checkout();

            Assert.AreEqual( o.GetPizzasCount(), 2 );
            Assert.AreSame( o.GetPizza( 0 ).Kind, cart.GetItem( 0 ).Kind );
            Assert.AreSame( o.GetPizza( 1 ).Kind, cart.GetItem( 1 ).Kind );
            Assert.AreEqual( o.GetPizza( 0 ).Size, cart.GetItem( 0 ).Size );
            Assert.AreEqual( o.GetPizza( 1 ).Size, cart.GetItem( 1 ).Size );
        }


        [ Test ]
        public void Pizzas_ForOneSingleDoubleOther_ThreeMatchingPizzas ()
        {
            OrderCart cart = new OrderCart();
            cart.AddItem( makeSomeItem( 1 ) );
            cart.AddItem( makeSomeOtherItem( 2 ) );
            Order o = makeOrder( cart );
            cart.Checkout();

            Assert.AreEqual( o.GetPizzasCount(), 3 );
            Assert.AreSame( o.GetPizza( 0 ).Kind, cart.GetItem( 0 ).Kind );
            Assert.AreSame( o.GetPizza( 1 ).Kind, cart.GetItem( 1 ).Kind );
            Assert.AreSame( o.GetPizza( 2 ).Kind, cart.GetItem( 1 ).Kind );
            Assert.AreEqual( o.GetPizza( 0 ).Size, cart.GetItem( 0 ).Size );
            Assert.AreEqual( o.GetPizza( 1 ).Size, cart.GetItem( 1 ).Size );
            Assert.AreEqual( o.GetPizza( 2 ).Size, cart.GetItem( 1 ).Size );
        }


        [ Test ]
        public void Status_Initially_New ()
        {
            Order o = makeOrder();
            Assert.AreEqual( o.Status, OrderStatus.New );
        }


        [ Test ]
        public void Status_AfterCheckout_Registered ()
        {
            Order o = makeOrder();
            o.Cart.Checkout();

            Assert.AreEqual( o.Status, OrderStatus.Registered );
        }


        [ Test ]
        public void Status_AfterSinglePizzaReady_Ready4Delivery ()
        {
            Order o = makeOrder();
            o.Cart.Checkout();
            CookAllPizzas( o );

            Assert.AreEqual( o.Status, OrderStatus.Ready4Delivery );
        }


        [ Test ]
        public void Status_AfterSinglePizzaStarted_Cooking ()
        {
            Order o = makeOrder();
            o.Cart.Checkout();

            Pizza p = o.GetPizza( 0 );
            p.OnCookingStarted();

            Assert.AreEqual( o.Status, OrderStatus.Cooking );
        }


        [ Test ]
        public void Status_AfterOneOfTwoStarted_Cooking ()
        {
            OrderCart cart = new OrderCart();
            cart.AddItem( makeSomeItem( 2 ) );
            Order o = makeOrder( cart );
            cart.Checkout();

            Pizza p = o.GetPizza( 0 );
            p.OnCookingStarted();

            Assert.AreEqual( o.Status, OrderStatus.Cooking );
        }


        [ Test ]
        public void Status_AfterOneOfTwoReady_Cooking()
        {
            OrderCart cart = new OrderCart();
            cart.AddItem( makeSomeItem( 2 ) );
            Order o = makeOrder( cart );
            cart.Checkout();

            CookPizza( o.GetPizza( 0 ) );

            Assert.AreEqual( o.Status, OrderStatus.Cooking );
        }


        [ Test ]
        public void Status_AfterBothReady_Ready4Delivery ()
        {
            OrderCart cart = new OrderCart();
            cart.AddItem( makeSomeItem( 2 ) );
            Order o = makeOrder( cart );
            cart.Checkout();

            CookAllPizzas( o );

            Assert.AreEqual( o.Status, OrderStatus.Ready4Delivery );
        }


        [ Test ]
        public void Status_AfterStartedDelivery_Delivering ()
        {
            Order o = makeOrder();
            o.Cart.Checkout();
            CookAllPizzas( o );

            o.OnStartedDelivery();

            Assert.AreEqual( o.Status, OrderStatus.Delivering );
        }


        [ Test ]
        public void Status_AfterDelivery_Delivered ()
        {
            Order o = makeOrder();
            o.Cart.Checkout();
            CookAllPizzas( o );
            o.OnStartedDelivery();
            o.PayPlan.MarkPayed();

            o.OnDelivered();

            Assert.AreEqual( o.Status, OrderStatus.Delivered );
        }


        [ Test ]
        public void StartOrFinishDelivery_WhenNewOrder_Forbidden ()
        {
            Order o = makeOrder();

            Assert.Throws< Exception >( () => o.OnStartedDelivery() );
            Assert.Throws< Exception >( () => o.OnDelivered() );
        }


        [ Test ]
        public void StartOrFinishDelivery_WhenRegisteredOrder_Forbidden ()
        {
            Order o = makeOrder();
            o.Cart.Checkout();

            Assert.Throws< Exception >( () => o.OnStartedDelivery() );
            Assert.Throws< Exception >( () => o.OnDelivered() );
        }


        [ Test ]
        public void StartOrFinishDelivery_WhenStillCookingOrder_Forbidden ()
        {
            OrderCart cart = new OrderCart();
            cart.AddItem( makeSomeItem( 2 ) );
            Order o = makeOrder( cart );
            o.Cart.Checkout();
            CookPizza( o.GetPizza( 0 ) );

            Assert.Throws< Exception >( () => o.OnStartedDelivery() );
            Assert.Throws< Exception >( () => o.OnDelivered() );
        }


        [ Test ]
        public void StartDelivery_WhenAlreadyStarted_Forbidden ()
        {
            Order o = makeOrder();
            o.Cart.Checkout();
            CookAllPizzas( o );
            o.OnStartedDelivery();

            Assert.Throws< Exception >( () => o.OnStartedDelivery() );
        }


        [ Test ]
        public void StartDelivery_WhenDelivered_Forbidden ()
        {
            Order o = makeOrder();
            o.Cart.Checkout();
            CookAllPizzas( o );
            o.OnStartedDelivery();
            o.PayPlan.MarkPayed();
            o.OnDelivered();

            Assert.Throws< Exception >( () => o.OnStartedDelivery() );
        }


        [ Test ]
        public void FinishDelivery_WhenDeliveryNotStarted_Forbidden ()
        {
            Order o = makeOrder();
            o.Cart.Checkout();
            CookAllPizzas( o );
            o.PayPlan.MarkPayed();

            Assert.Throws< Exception >( () => o.OnDelivered() );
        }


        [ Test ]
        public void FinishDelivery_WhenAlreadyDelivered_Forbidden ()
        {
            Order o = makeOrder();
            o.Cart.Checkout();
            CookAllPizzas( o );
            o.OnStartedDelivery();
            o.PayPlan.MarkPayed();

            o.OnDelivered();

            Assert.Throws< Exception >( () => o.OnDelivered() );
        }


        [ Test ]
        public void Cancel_NewOrder_Passes ()
        {
            Order o = makeOrder();
            o.Cancel();

            Assert.AreEqual( o.Status, OrderStatus.Cancelled );
            Assert.AreEqual( o.GetPizzasCount(), 0 );
        }


        [ Test ]
        public void Cancel_RegisteredOrder_Passes ()
        {
            OrderCart cart = new OrderCart();
            cart.AddItem( makeSomeItem( 2 ) );
            Order o = makeOrder( cart );
            cart.Checkout();

            o.Cancel();

            Assert.AreEqual( o.Status, OrderStatus.Cancelled );
            Assert.AreEqual( o.GetPizzasCount(), 2 );
            Assert.AreEqual( o.GetPizza( 0 ).Status, CookingStatus.Cancelled );
            Assert.AreEqual( o.GetPizza( 1 ).Status, CookingStatus.Cancelled );
        }


        [ Test ]
        public void Cancel_CookingOrder_PassesAndCancelsStartedPizzas ()
        {
            OrderCart cart = new OrderCart();
            cart.AddItem( makeSomeItem( 3 ) );
            Order o = makeOrder( cart );
            cart.Checkout();

            o.GetPizza( 0 ).OnCookingStarted();
            o.GetPizza( 0 ).OnCookingFinished();
            o.GetPizza( 1 ).OnCookingStarted();

            o.Cancel();

            Assert.AreEqual( o.Status, OrderStatus.Cancelled );
            Assert.AreEqual( o.GetPizzasCount(), 3 );
            Assert.AreEqual( o.GetPizza( 0 ).Status, CookingStatus.Finished );
            Assert.AreEqual( o.GetPizza( 1 ).Status, CookingStatus.Cancelled );
            Assert.AreEqual( o.GetPizza( 2 ).Status, CookingStatus.Cancelled );
        }


        [ Test ]
        public void Cancel_ReadyOrder_PassesButNotTouchesPizzas ()
        {
            OrderCart cart = new OrderCart();
            cart.AddItem( makeSomeItem( 2 ) );
            Order o = makeOrder( cart );
            cart.Checkout();
            CookAllPizzas( o );

            o.Cancel();

            Assert.AreEqual( o.Status, OrderStatus.Cancelled );
            Assert.AreEqual( o.GetPizzasCount(), 2 );
            Assert.AreEqual( o.GetPizza( 0 ).Status, CookingStatus.Finished );
            Assert.AreEqual( o.GetPizza( 1 ).Status, CookingStatus.Finished );
        }


        [ Test ]
        public void Cancel_OrderThatLeftKitchen_Forbidden ()
        {
            Order o = makeOrder();
            o.Cart.Checkout();
            CookAllPizzas( o );
            
            o.OnStartedDelivery();

            Assert.Throws< Exception >( () => o.Cancel() );

            o.PayPlan.MarkPayed();

            o.OnDelivered();

            Assert.Throws< Exception >( () => o.Cancel() );
        }


        [ Test ]
        public void Cancel_AlreadyCancelledOrder_Forbidden ()
        {
            Order o = makeOrder();
            o.Cancel();

            Assert.Throws< Exception >( () => o.Cancel() );
        }


        [ Test ]
        public void Payment_CashNotCollectedOnDelivery_Forbidden ()
        {
            Order o = makeOrder();
            o.Cart.Checkout();
            CookAllPizzas( o );
            o.OnStartedDelivery();

            Assert.Throws< Exception >( () => o.OnDelivered() );
        }


        [ Test ]
        public void Payment_WhenPrepaymentExpected_CannotCheckoutWithoutPayment ()
        {
            Order o = makeOrder( makeWireTransferPlan() );
            
            Assert.Throws< Exception >( () => o.Cart.Checkout() );
        }

        
        [ Test ]
        public void Payment_WhenPrepaymentExpected_CheckoutSucceedsWhenPayed ()
        {
            PaymentPlan plan = makeWireTransferPlan();
            Order o = makeOrder( plan );
            plan.MarkPayed();
            Assert.DoesNotThrow( () => o.Cart.Checkout() );
        }


        private static Order makeOrder ()
        {
            OrderCart cart = new OrderCart();
            cart.AddItem( makeSomeItem() );

            return makeOrder( cart );
        }


        private static Order makeOrder ( OrderCart cart )
        {
            return new Order( 1, cart, new DeliveryContact(), new CashPaymentPlan() );
        }


        private static Order makeOrder ( PaymentPlan plan )
        {
            OrderCart cart = new OrderCart();
            cart.AddItem( makeSomeItem() );
            return new Order( 1, cart, new DeliveryContact(), plan );
        }


        private static OrderItem makeSomeItem ( int quantity = 1 )
        {
            return new OrderItem( new PizzaKind( "some" ), PizzaSize.Small, 1.0M, quantity );
        }


        private static OrderItem makeSomeOtherItem ( int quantity = 1 )
        {
            return new OrderItem( new PizzaKind( "other" ), PizzaSize.Medium, 2.0M, quantity );
        }


        private static PaymentPlan makeWireTransferPlan ()
        {
            return new WireTransferPaymentPlan( "1234 5678 1234 5678", "Ivan Ivanov" );
        }


        private static void CookPizza ( Pizza p )
        {
            p.OnCookingStarted();
            p.OnCookingFinished();
        }


        private static void CookAllPizzas ( Order o  )
        {
            foreach ( Pizza p in o.Pizzas )
                CookPizza( p );
        }
    }
}
