﻿using Pizzario.Model;

using NUnit.Framework;
using System;


namespace Pizzario.Model.UnitTests
{
    [ TestFixture ]
    public class WireTransferPaymentPlanTests
    {
        [ Test ]
        public void Constructor_ProjectsFields_Correctly ()
        {
            WireTransferPaymentPlan plan = new WireTransferPaymentPlan( "123", "Ivanov" );

            Assert.AreEqual( plan.CardCode, "123" );
            Assert.AreEqual( plan.CardHolder, "Ivanov" );
        }


        [ Test ]
        public void Payed_Initially_False ()
        {
            PaymentPlan p = makeWireTransferPlan();
            Assert.False( p.Payed );
        }


        [ Test ]
        public void Payed_SetPayedToTrue_Confirmed ()
        {
            PaymentPlan p = makeWireTransferPlan();
            p.MarkPayed();
            Assert.True( p.Payed );
        }


        [ Test ]
        public void Prepayment_Expected ()
        {
            PaymentPlan p = makeWireTransferPlan();
            Assert.True( p.ExpectPrepayment() );
        }


        [ Test ]
        public void Clone_Payed_StillPayed ()
        {
            PaymentPlan p1 = makeWireTransferPlan();
            p1.MarkPayed();
            PaymentPlan p2 = p1.Clone();

            Assert.True( p2.Payed );
        }


        [ Test ]
        public void Clone_Unpayed_StillUnpayed ()
        {
            PaymentPlan p1 = makeWireTransferPlan();
            PaymentPlan p2 = p1.Clone();

            Assert.False( p2.Payed );
        }

        [ Test ]
        public void Clone_CopiesFields_Correctly ()
        {
            WireTransferPaymentPlan plan = new WireTransferPaymentPlan( "123", "Ivanov" );
            WireTransferPaymentPlan plan2 = plan.Clone() as WireTransferPaymentPlan;

            Assert.AreEqual( plan2.CardCode, "123" );
            Assert.AreEqual( plan2.CardHolder, "Ivanov" );
        }


        private static WireTransferPaymentPlan makeWireTransferPlan ()
        {
            return new WireTransferPaymentPlan( "1234 5678 1234 5678", "Ivan Ivanov" );
        }

    }
}
