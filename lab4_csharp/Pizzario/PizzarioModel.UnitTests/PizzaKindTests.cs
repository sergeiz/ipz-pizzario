﻿using Pizzario.Model;

using NUnit.Framework;
using NSubstitute;
using System;


namespace Pizzario.Model.UnitTests
{
    [ TestFixture ]
    class PizzaKindTests
    {
        [ Test ]
        public void Constructor_ReturnsExpectedDefaults ()
        {
            PizzaKind kind = makeKind();

            Assert.AreEqual( kind.Name, "Carbonara" );
            Assert.IsEmpty( kind.Description );
            Assert.IsEmpty( kind.ImageUrl );
            Assert.AreEqual( kind.Votes, 0 );
            Assert.AreEqual( kind.TotalRating, 0 );
            Assert.False( kind.Hidden );
            Assert.NotNull( kind.Recipe );
        }


        [ Test ]
        public void Name_ConstructingWithEmpty_Forbidden ()
        {
            Assert.Throws< Exception >( () => new PizzaKind( null ) );
            Assert.Throws< Exception >( () => new PizzaKind( "" ) );
        }


        [ Test ]
        public void Name_AssigningToEmpty_Forbidden ()
        {
            PizzaKind kind = makeKind();
            Assert.Throws< Exception >( () => kind.Name = null );
            Assert.Throws< Exception >( () => kind.Name = "" );
        }


        [ Test ]
        public void Name_Reassignment_SendsEvent ()
        {
            PizzaKind kind = new PizzaKind( "Carbonara" );

            var handlerMock = Substitute.For< PizzaKind.BeforeNameChangedHandler >();
            kind.BeforeNameChanged += handlerMock;

            kind.Name = "Milano";

            handlerMock.Received( 1 )
                .Invoke(
                    Arg.Is( kind ),
                    Arg.Is( ( PizzaKind.BeforeNameChangedEventArgs e ) => e.OldName == "Carbonara" && e.NewName == "Milano" )
                )
                ;
        }


        [ Test ]
        public void Name_ReassignmentWithoutListener_PassesSilently ()
        {
            PizzaKind kind = new PizzaKind( "Carbonara" );
            kind.Name = "New Carbonara";

            Assert.AreEqual( kind.Name, "New Carbonara" );
        }


        [ Test ]
        public void Name_ReassignmentToSame_DoesNotSendEvent ()
        {
            PizzaKind kind = new PizzaKind( "Carbonara" );

            var handlerMock = Substitute.For<PizzaKind.BeforeNameChangedHandler>();
            kind.BeforeNameChanged += handlerMock;

            kind.Name = "Carbonara";

            handlerMock.DidNotReceiveWithAnyArgs().Invoke( null, null );
        }


        [ Test ]
        public void Rating_WhenNoVotes_Zero ()
        {
            PizzaKind kind = makeKind();

            Assert.AreEqual( kind.Votes, 0 );
            Assert.AreEqual( kind.TotalRating, 0 );
            Assert.AreEqual( kind.AverageRating, 0 );
        }


        [ Test ]
        public void Rating_WhenSingleVote_ReturnsIt ()
        {
            PizzaKind kind = makeKind();
            kind.Rate( 3 );

            Assert.AreEqual( kind.Votes, 1 );
            Assert.AreEqual( kind.TotalRating, 3 );
            Assert.AreEqual( kind.AverageRating, 3 );
        }


        [ Test ]
        public void Rating_WhenTwoVotes_ReturnsAverage ()
        {
            PizzaKind kind = makeKind();
            kind.Rate( 3 );
            kind.Rate( 5 );

            Assert.AreEqual( kind.Votes, 2 );
            Assert.AreEqual( kind.TotalRating, 8 );
            Assert.AreEqual( kind.AverageRating, 4 );
        }


        [ Test ]
        public void Rating_WhenThreeVotes_ReturnsAverage ()
        {
            PizzaKind kind = makeKind();
            kind.Rate( 1 );
            kind.Rate( 3 );
            kind.Rate( 5 );

            Assert.AreEqual( kind.Votes, 3 );
            Assert.AreEqual( kind.TotalRating, 9 );
            Assert.AreEqual( kind.AverageRating, 3 );
        }


        [ Test ]
        public void Rating_ZeroVotes_Forbidden ()
        {
            PizzaKind kind = makeKind();

            Assert.Throws< Exception >( () => kind.Rate( 0 ) );
        }


        [ Test ]
        public void Rating_NegativeVotes_Forbidden ()
        {
            PizzaKind kind = makeKind();

            Assert.Throws< Exception >( () => kind.Rate( -1 ) );
        }


        [ Test ]
        public void Rating_VotesBeyondMaximum_Forbidden ()
        {
            PizzaKind kind = makeKind();
            kind.Rate( PizzaKind.MaximumVote );

            Assert.Throws< Exception >( () => kind.Rate( PizzaKind.MaximumVote + 1 ) );
        }


        [ Test ]
        public void RatingSetup_VotesStartFromConcrete_ReturnsSame ()
        {
            PizzaKind kind = makeKind();
            kind.SetupRatingStats( 5, 25 );

            Assert.AreEqual( kind.Votes, 5 );
            Assert.AreEqual( kind.TotalRating, 25 );
            Assert.AreEqual( kind.AverageRating, 5 );
        }

        
        [ Test ]
        public void RatingSetup_VotesBeforeSetup_GetOverwritten ()
        {
            PizzaKind kind = makeKind();
            kind.Rate( 5 );
            kind.SetupRatingStats( 5, 25 );

            Assert.AreEqual( kind.Votes, 5 );
            Assert.AreEqual( kind.TotalRating, 25 );
            Assert.AreEqual( kind.AverageRating, 5 );
        }


        [ Test ]
        public void RatingSetup_VotesAfterSetup_AppendToSetup ()
        {
            PizzaKind kind = makeKind();
            kind.SetupRatingStats( 5, 25 );
            kind.Rate( 5 );

            Assert.AreEqual( kind.Votes, 6 );
            Assert.AreEqual( kind.TotalRating, 30 );
            Assert.AreEqual( kind.AverageRating, 5 );
        }


        [ Test ]
        public void RatingSetup_NegativeVotesSetup_Forbidden ()
        {
            PizzaKind kind = makeKind();

            Assert.Throws< Exception >( () => kind.SetupRatingStats( -1, 0 ) );
        }


        [ Test ]
        public void RatingSetup_ZeroVotes_IsFineWhenZeroTotal ()
        {
            PizzaKind kind = makeKind();

            Assert.DoesNotThrow( () => kind.SetupRatingStats( 0, 0 ) );
        }


        [ Test ]
        public void RatingSetup_ZeroVotes_IsNotFineWithNonZeroTotal ()
        {
            PizzaKind kind = makeKind();

            Assert.Throws< Exception >( () => kind.SetupRatingStats( 0, 1 ) );
            Assert.Throws< Exception >( () => kind.SetupRatingStats( 0, -1 ) );
        }


        [ Test ]
        public void RatingSetup_TotalSmallerThanVotes_NotAllowed ()
        {
            PizzaKind kind = makeKind();
            Assert.DoesNotThrow( () => kind.SetupRatingStats( 5, 5 ) );
            Assert.Throws< Exception >( () => kind.SetupRatingStats( 5, 4 ) );
            Assert.Throws< Exception >( () => kind.SetupRatingStats( 1, 0 ) );
        }


        [ Test ]
        public void RatingSetup_TotalLargeThanMaxVotes_NotAllowed ()
        {
            PizzaKind kind = makeKind();
            Assert.DoesNotThrow( () => kind.SetupRatingStats( 5, 25 ) );
            Assert.Throws< Exception >( () => kind.SetupRatingStats( 5, 5 * PizzaKind.MaximumVote + 1 ) );
            Assert.Throws< Exception >( () => kind.SetupRatingStats( 1, PizzaKind.MaximumVote + 1 ) );
        }


        [ Test ]
        public void Prices_Initially_Zeroes ()
        {
            PizzaKind kind = makeKind();

            Assert.AreEqual( kind.GetCurrentPrice( PizzaSize.Small ),   0 );
            Assert.AreEqual( kind.GetCurrentPrice( PizzaSize.Medium ),  0 );
            Assert.AreEqual( kind.GetCurrentPrice( PizzaSize.Large ),   0 );
        }


        [ Test ]
        public void Prices_UpdateEach_ConfirmAssignedValues ()
        {
            PizzaKind kind = makeKind();
            kind.UpdatePrice( PizzaSize.Small, 3.0M );
            kind.UpdatePrice( PizzaSize.Medium, 5.0M );
            kind.UpdatePrice( PizzaSize.Large, 7.0M );

            Assert.AreEqual( kind.GetCurrentPrice( PizzaSize.Small ), 3.0M );
            Assert.AreEqual( kind.GetCurrentPrice( PizzaSize.Medium ), 5.0M );
            Assert.AreEqual( kind.GetCurrentPrice( PizzaSize.Large ), 7.0M );
        }


        [ Test ]
        public void Prices_UpdateSameTypeTwice_SavesSecondPrice ()
        {
            PizzaKind kind = makeKind();
            kind.UpdatePrice( PizzaSize.Small, 3.0M );
            kind.UpdatePrice( PizzaSize.Small, 3.5M );

            Assert.AreEqual( kind.GetCurrentPrice( PizzaSize.Small ), 3.5M );
        }


        [ Test ]
        public void Prices_UpdateToZero_Allowed ()
        {
            PizzaKind kind = makeKind();
            kind.UpdatePrice( PizzaSize.Small, 0.0M );

            Assert.AreEqual( kind.GetCurrentPrice( PizzaSize.Small ), 0.0M );
        }


        [ Test ]
        public void Prices_UpdateToNegative_Forbidden ()
        {
            PizzaKind kind = makeKind();
            
            Assert.Throws< Exception >( () => kind.UpdatePrice( PizzaSize.Small, -0.01M ) );
        }


        private PizzaKind makeKind ()
        {
            return new PizzaKind( "Carbonara" );
        }

    }
}
