﻿namespace Pizzario.Model
{
    public enum OrderStatus
    {
        New,
        Registered,
        Cooking,
        Ready4Delivery,
        Delivering,
        Delivered,
        Cancelled
    }
}
