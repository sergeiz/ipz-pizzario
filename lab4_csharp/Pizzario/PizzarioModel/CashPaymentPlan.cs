﻿namespace Pizzario.Model
{
    public class CashPaymentPlan : PaymentPlan
    {
        public override bool ExpectPrepayment ()
        {
            return false;
        }

        public override string ToString()
        {
            return "Cash payment on delivery";
        }

        public override PaymentPlan Clone ()
        {
            CashPaymentPlan copy = new CashPaymentPlan();
            if ( Payed )
                copy.MarkPayed();
            return copy;
        }
    }
}
