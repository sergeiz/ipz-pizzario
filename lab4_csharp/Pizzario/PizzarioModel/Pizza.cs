﻿using System;

namespace Pizzario.Model
{
    public class Pizza
    {
        public PizzaKind Kind { get; private set; }

        public PizzaSize Size { get; private set; }

        public CookingStatus Status { get; private set; }

        public event EventHandler CookingStarted;

        public event EventHandler CookingFinished;


        public Pizza ( PizzaKind kind, PizzaSize size )
        {
            this.Kind = kind;
            this.Size = size;
            this.Status = CookingStatus.NotStarted;
        }

        public void OnCookingStarted ()
        {
            if ( Status == CookingStatus.NotStarted )
            {
                Status = CookingStatus.Started;

                if ( CookingStarted != null )
                    CookingStarted( this, EventArgs.Empty );
            }
            else
                throw new System.Exception( "Pizza.OnCookingStarted: may only start cooking if not started yet" );
        }


        public void OnCookingCancelled ()
        {
	        if ( Status == CookingStatus.Started || Status == CookingStatus.NotStarted )
                Status = CookingStatus.Cancelled;

            else
                throw new System.Exception( "Pizza.OnCookingCancelled: may only abort unfinished pizza" );
        }


        public void OnCookingFinished ()
        {
            if ( Status == CookingStatus.Started )
            {
                Status = CookingStatus.Finished;

                if ( CookingFinished != null )
                    CookingFinished( this, EventArgs.Empty );
            }
            else
                throw new System.Exception( "Pizza.OnCookingFinished: may only finish cooking if started" );
        }
    }
}
