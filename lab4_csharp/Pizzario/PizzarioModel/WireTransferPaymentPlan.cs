﻿namespace Pizzario.Model
{
    public class WireTransferPaymentPlan : PaymentPlan
    {
        public string CardCode { get; private set; }

        public string CardHolder { get; private set; }

        public WireTransferPaymentPlan ( string cardCode, string cardHolder )
        {
            this.CardCode = cardCode;
            this.CardHolder = cardHolder;
        }

        public override bool ExpectPrepayment ()
        {
            return true;
        }

        public override string ToString ()
        {
            return "Wire transfer";
        }

        public override PaymentPlan Clone ()
        {
            WireTransferPaymentPlan copy = new WireTransferPaymentPlan( CardCode, CardHolder );
            if ( Payed )
                copy.MarkPayed();
            return copy;
        }
    }
}
