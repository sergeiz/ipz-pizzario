﻿using System;
using System.Collections.Generic;

namespace Pizzario.Model
{
    public class OrderCart
    {
        public IEnumerable< OrderItem > Items
        {
            get { return items; }
        }


        public bool Modifiable
        {
            get { return modifiable; }
        }


        public decimal Cost
        {
            get
            {
                decimal totalCost = 0;
                items.ForEach( ( OrderItem i ) => totalCost += i.Cost);
                return totalCost;
            }
        }


        public int GetItemsCount ()
        {
            return items.Count;
        }

        public OrderItem GetItem ( int index )
        {
            return items[ index ];
        }

        public void AddItem ( OrderItem item )
        {
            if ( ! modifiable )
                throw new System.Exception( "OrderCart.AddItem: unmodifiable cart" );

            foreach ( OrderItem i in items )
            {
                if ( i.Kind == item.Kind && i.Size == item.Size )
                    throw new System.Exception( "OrderCart.AddItem: duplicate kind-size pair added" );
            }

            items.Add( item );
        }

        public void UpdateItem ( int index, OrderItem item )
        {
            if ( ! modifiable )
                throw new System.Exception( "OrderCart.UpdateItem: unmodifiable cart" );

            items[ index ] = item;
        }

        public void DropItem ( int index )
        {
            if ( ! modifiable )
                throw new System.Exception( "OrderCart.DropItem: unmodifiable cart" );

            items.RemoveAt( index );
        }

        public void ClearItems ()
        {
            if ( ! modifiable )
                throw new System.Exception( "OrderCart.ClearItems: unmodifiable cart" );

            items.Clear();
        }


        public void Checkout ()
        {
            if ( items.Count == 0 )
                throw new System.Exception( "OrderCart.Checkout: checkout of empty cart" );

            modifiable = false;

            if ( CheckoutEvent != null )
                CheckoutEvent( this, EventArgs.Empty );
        }


        public event EventHandler CheckoutEvent;


        private List< OrderItem > items = new List< OrderItem >();
        private bool modifiable = true;
    }
}
