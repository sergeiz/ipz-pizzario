﻿namespace Pizzario.Model
{
    public abstract class PaymentPlan
    {
        public bool Payed { get; private set; }

        protected PaymentPlan ()
        {
            this.Payed = false;
        }

        public void MarkPayed ()
        {
            this.Payed = true;
        }

        public abstract PaymentPlan Clone ();

        public abstract bool ExpectPrepayment ();
    }
}
