﻿using System.Collections.Generic;

namespace Pizzario.Model
{
    public class Menu
    {
        public IEnumerable< string > PizzaKindNames
        {
            get { return pizzaKindsByName.Keys; }
        }

        public IEnumerable< PizzaKind > PizzaKinds
        {
            get { return pizzaKindsByName.Values; }
        }


        public int GetPizzaKindsCount ()
        {
            return pizzaKindsByName.Count;
        }

        public PizzaKind FindPizzaKind ( string name )
        {
            PizzaKind result;
            if ( pizzaKindsByName.TryGetValue( name, out result ) )
                return result;
            else
                return null;
        }

        public void AddPizzaKind ( PizzaKind kind )
        {
           	if ( FindPizzaKind( kind.Name ) != null )
                throw new System.Exception( "Menu.AddPizzaKind: duplicate kind" );

            pizzaKindsByName[ kind.Name ] = kind;

            kind.BeforeNameChanged += HandlePizzaRename;
        }

        public void DeletePizzaKind ( string name )
        {
            PizzaKind kind = FindPizzaKind( name );
            if ( kind == null )
                throw new System.Exception( "Menu.DeletePizzaKind: unregistered kind" );

            pizzaKindsByName.Remove( name );

            kind.BeforeNameChanged -= HandlePizzaRename;
        }

        private void HandlePizzaRename ( object sender, PizzaKind.BeforeNameChangedEventArgs args )
        {
            PizzaKind kind = FindPizzaKind( args.OldName );
            if ( kind == null )
                throw new System.Exception( "Menu.HandlePizzaRename: unregistered kind" );

            if ( args.OldName == args.NewName )
                return;

            if ( FindPizzaKind( args.NewName ) != null )
                throw new System.Exception( "Menu.HandlePizzaRename: target name already reserved" );

            pizzaKindsByName.Remove( args.OldName );
            pizzaKindsByName[ args.NewName ] = kind;
        }

        public OrderItem MakeOrderItem ( string name, PizzaSize size, int quantity )
        {
            PizzaKind kind = FindPizzaKind( name );
            if ( kind == null )
                throw new System.Exception( "Menu.MakeOrderItem: unregistered kind" );

            return new OrderItem( kind, size, kind.GetCurrentPrice( size ), quantity );
        }

        private Dictionary< string, PizzaKind > pizzaKindsByName = new Dictionary< string, PizzaKind  >();
    }
}
