﻿using Pizzario.Model;

namespace Pizzario.Model.Test
{
    public class TestModelGenerator
    {
        public PizzarioNetwork generate ()
        {
            PizzarioNetwork network = new PizzarioNetwork();
            fillPizzaMenu( network.getMenu() );
            fillOrders( network, network.getMenu() );
            return network;
        }

        private void fillPizzaMenu ( Menu m )
        {
            ////

            PizzaKind carbonara = new PizzaKind( "Carbonara" );
            m.AddPizzaKind( carbonara );

            carbonara.Description = "One of the most popular pizza recipes in the world";
            carbonara.SetupRatingStats( 170, 812 );
            carbonara.ImageUrl = "http://pizzario.com.ua/images/carbonara.jpg";

            carbonara.Recipe.AddIngredient( "Cheese", 100 );
            carbonara.Recipe.AddIngredient( "Olive Oil", 30 );

            carbonara.UpdatePrice( PizzaSize.Small, 3.00M );
            carbonara.UpdatePrice( PizzaSize.Medium, 5.00M );
            carbonara.UpdatePrice( PizzaSize.Large, 7.00M );

            ////

            PizzaKind milano = new PizzaKind( "Milano" );
            m.AddPizzaKind( milano );

            milano.Description = "Classic recipe with ham, mushrooms and tomatoes";
            milano.SetupRatingStats( 102, 415 );
            milano.ImageUrl = "http://pizzario.com.ua/images/milano.jpg";

            milano.Recipe.AddIngredient( "Ham", 100 );
            milano.Recipe.AddIngredient( "Mushrooms", 50 );
            milano.Recipe.AddIngredient( "Tomatoes", 50 );

            milano.UpdatePrice( PizzaSize.Small, 2.50M );
            milano.UpdatePrice( PizzaSize.Medium, 4.15M );
            milano.UpdatePrice( PizzaSize.Large, 5.80M );
        }

        private void fillOrders ( PizzarioNetwork network, Menu m )
        {
            PizzaKind carbonara = m.FindPizzaKind( "Carbonara" );
            PizzaKind milano = m.FindPizzaKind( "Milano" );

            Order order1 = network.newOrder(
                    new DeliveryContact()
                    { Address = "Lenin av. 14, room 320", 
                      Phone = "702-13-26", 
                      Comment = "APVT Department" }
                , new CashPaymentPlan()
            );

            order1.Cart.AddItem( m.MakeOrderItem( "Carbonara", PizzaSize.Medium, 2 ) );
            order1.Cart.AddItem( m.MakeOrderItem( "Milano", PizzaSize.Large, 1 ) );
            order1.Discount = 0.025;
            order1.Cart.Checkout();


            Order order2 = network.newOrder(
                    new DeliveryContact()
                    { Address = "23rd August Str 2, ap. 2", 
                      Phone = "333-33-33", 
                      Comment = "Knock 3 times" }
                , new CashPaymentPlan()
            );

            order2.Cart.AddItem( m.MakeOrderItem( "Carbonara", PizzaSize.Small, 1 ) );
        }

    }
}
