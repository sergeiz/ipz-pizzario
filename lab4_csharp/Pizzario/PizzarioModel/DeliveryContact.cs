﻿namespace Pizzario.Model
{
    public class DeliveryContact
    {
        public string Address { get; set; }

        public string Phone { get; set; }

        public string Comment { get; set; }
    }
}
