﻿namespace Pizzario.Model
{
    public class OrderItem
    {
        public PizzaKind Kind { get; private set; }

        public PizzaSize Size { get; private set; }

        public decimal FixedPrice { get; private set; }

        public int Quantity { get; private set; }

        public decimal Cost
        {
            get
            {
                return FixedPrice * Quantity;
            }
        }


        public OrderItem ( PizzaKind kind, PizzaSize size, decimal fixedPrice, int quantity )
        {
            if ( fixedPrice < 0 )
                throw new System.Exception( "OrderItem: negative price" );

            if ( quantity <= 0 )
                throw new System.Exception( "OrderItem: non-positive quantity" );

            this.Kind = kind;
            this.Size = size;
            this.FixedPrice = fixedPrice;
            this.Quantity = quantity;
        }
    }
}
