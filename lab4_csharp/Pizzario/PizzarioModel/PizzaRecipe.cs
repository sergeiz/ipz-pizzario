﻿using System.Collections.Generic;

namespace Pizzario.Model
{
    public class PizzaRecipe
    {
        public IEnumerable< string > Ingredients
        {
            get { return ingredientsWeight.Keys; }
        }

        public int GetIngredientsCount ()
        {
            return ingredientsWeight.Count;
        }

        public int GetIngredientWeight ( string ingredient )
        {
            int result;
            if ( ingredientsWeight.TryGetValue( ingredient, out result ) )
                return result;

            else
                return 0;
        }

        public void AddIngredient ( string ingredient, int weight )
        {
            if ( ingredientsWeight.ContainsKey( ingredient ) )
                throw new System.Exception( "PizzaKind.AddIngredient: duplicate ingredient" );

            else
                ingredientsWeight.Add(ingredient, weight);
        }

        public void UpdateIngredient ( string ingredient, int weight )
        {
            if ( ingredientsWeight.ContainsKey( ingredient ) )
                ingredientsWeight[ ingredient ] = weight;

            else
                throw new System.Exception( "PizzaKind.UpdateIngredient: missing ingredient" );
        }

        public void RemoveIngredient ( string ingredient )
        {
            if ( ingredientsWeight.ContainsKey( ingredient ) )
                ingredientsWeight.Remove( ingredient );

            else
                throw new System.Exception( "PizzaKind.RemoveIngredient: missing ingredient" );
        }

        private Dictionary< string, int > ingredientsWeight = new Dictionary< string, int >();
    }
}
