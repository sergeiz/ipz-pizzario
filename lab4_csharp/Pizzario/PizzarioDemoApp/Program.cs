﻿using Pizzario.Model;
using Pizzario.Model.Test;
using Pizzario.Report;

using System;

namespace Pizzario.DemoApp
{
    class Program
    {
        public static void Main ( string[] args )
        {
            Program p = new Program();
            p.fillTestModel();
            p.generateModelReport();
        }

        private void fillTestModel ()
        {
            TestModelGenerator generator = new TestModelGenerator();
            network = generator.generate();
        }

        private void generateModelReport ()
        {
            ReportGenerator generator = new ReportGenerator( Console.Out, network );
            generator.generate();
        }
        
        private PizzarioNetwork network = new PizzarioNetwork();
    }
}
