class PizzaSize:
    SMALL = 0
    MEDIUM = 1
    LARGE = 2
    
    Total = 3
    
    status_as_string = [ "SMALL", "MEDIUM", "LARGE" ]
    
    @staticmethod
    def to_string( size ):
        return PizzaSize.status_as_string[ size ]

    @staticmethod 
    def is_valid( size ):
        return size in range( PizzaSize.SMALL , PizzaSize.LARGE + 1 )
    
    @staticmethod 
    def validate( size ):
        if PizzaSize.is_valid( size ):
            pass
        else:
            raise Exception( "Invalid PizzaSize code" )
        
        