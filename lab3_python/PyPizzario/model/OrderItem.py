class OrderItem(object):
    
    def __init__(self, kind, size, current_price, quantity):
        
        if ( current_price < 0 ):
            raise Exception( "OrderItem: negative price" )

        if ( quantity <= 0 ):
            raise Exception( "OrderItem: non-positive quantity" )
        
        self.kind = kind
        self.size = size
        self.fixed_price = current_price
        self.quantity = quantity
        
        
    def cost( self ):
        return self.fixed_price * self.quantity
