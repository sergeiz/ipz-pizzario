from model.PaymentPlan import PaymentPlan

class CashPaymentPlan( PaymentPlan ):
    
    def __init__( self ):
        PaymentPlan.__init__( self )
    
    def expect_prepayment( self ):
        return False
    
    def clone( self ):
        copy = CashPaymentPlan()
        if ( self.payed ):
            copy.mark_payed()
        return copy
