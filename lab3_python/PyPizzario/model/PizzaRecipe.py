class PizzaRecipe:
    
    def __init__( self ):
        self.ingredients_weight = dict()
        
    def browse_ingredients( self ):
        return iter( self.ingredients_weight )
    
    def get_ingredients_count( self ):
        return len(self.ingredients_weight)
    
    def get_ingredient_weight( self , ingredient ):
        i = self.ingredients_weight.get( ingredient )
        if i != None:
            return i
        else:
            return 0
       
    def add_ingredient( self, ingredient, weight ):
        if self.ingredients_weight.get( ingredient ) != None:
            raise Exception( "PizzaKind.add_ingredient: duplicate ingredient" )
        self.ingredients_weight[ ingredient ] = weight
        
    def update_ingredient( self , ingredient, weight ):
        if self.ingredients_weight.get( ingredient ) == None:
            raise Exception( "PizzaKind.add_ingredient: missing ingredient" )
        self.ingredients_weight[ ingredient ] = weight
        
    def remove_ingredient( self , ingredient ):
        if self.ingredients_weight.get( ingredient ) == None:
            raise Exception( "PizzaKind.add_ingredient: missing ingredient" )
        self.ingredients_weight.pop( ingredient )
        