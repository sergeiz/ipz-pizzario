class CookingStatus:
    NOT_STARTED = 0
    STARTED = 1
    FINISHED = 2
    CANCELLED = 3
    
    status_as_string = [ "NOT_STARTED", "STARTED", "FINISHED" , "CANCELLED" ]
    
    @staticmethod
    def to_string( status ):
        return CookingStatus.status_as_string[ status ]
        
             
    @staticmethod 
    def is_valid( status ):
        return status in range( CookingStatus.NOT_STARTED , CookingStatus.CANCELLED + 1 )
    
    @staticmethod 
    def validate( status ):
        if CookingStatus.is_valid( status ):
            pass
        else:
            raise Exception( "Invalid CookingStatus code" )
