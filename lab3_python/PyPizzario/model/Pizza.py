from model.CookingStatus import CookingStatus
from model.Events import Event

class Pizza:
    
    def __init__( self , kind , size ):
        self.kind = kind
        self.size = size
        self.status = CookingStatus.NOT_STARTED
        
        self.cooking_started = Event()
        self.cooking_finished = Event()
        
        
    def on_cooking_started( self ):
        if self.status == CookingStatus.NOT_STARTED:
            self.status = CookingStatus.STARTED
            self.cooking_started()
        else:
            raise Exception( "Pizza.OnCookingStarted: may only start cooking if not started yet" )
        
        
    def on_cooking_cancelled( self ):
        if ( self.status == CookingStatus.STARTED or self.status == CookingStatus.NOT_STARTED ):
            self.status = CookingStatus.CANCELLED
        else:
            raise Exception( "Pizza.OnCookingCancelled: may only abort unfinished pizza" )
        
    
    def on_cooking_finished( self ):
        if self.status == CookingStatus.STARTED:
            self.status = CookingStatus.FINISHED
            self.cooking_finished()
        else:
            raise Exception( "Pizza.OnCookingFinished: may only finish cooking if started" )
