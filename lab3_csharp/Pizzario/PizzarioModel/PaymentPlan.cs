﻿namespace Pizzario.Model
{
    public abstract class PaymentPlan
    {
        public abstract PaymentPlan clone ();
        public abstract bool expectPrepayment ();
        public abstract bool wasPayed ();
    }
}
