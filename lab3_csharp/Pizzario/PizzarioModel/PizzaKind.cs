﻿using System;

namespace Pizzario.Model
{
    public class PizzaKind
    {
        public int PizzaKindId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
        
        public string ImageUrl { get; set; }
        
        public int Votes { get; private set; }

        public int TotalRating { get; private set; }

        public bool Hidden { get; set; }

        public PizzaRecipe Recipe { get; private set; }


        public PizzaKind ( string name )
        {
            this.Recipe = new PizzaRecipe();

            this.Name = name;
            this.Description = "";
            this.ImageUrl = "";
            this.Votes = 0;
            this.TotalRating = 0;
            this.Hidden = false;
        }

        public double getAverageRating ()
        {
            if ( Votes != 0 )
                return ( double ) ( TotalRating ) / Votes;
	        else
		        return 0.0;
        }
	
        public void rate ( int rating )
        {
            if ( rating <= 0 || rating > 5 )
                throw new System.Exception( "PizzaKind.rate: expecting rating within [1;5] range" );

            else
            {
                this.TotalRating += rating;
                this.Votes++;
            }
        }


        public void setupRatingStats ( int votes, int totalRating )
        {
            this.Votes = votes;
            this.TotalRating = totalRating;
        }

        public decimal getCurrentPrice ( PizzaSize size )
        {
            return priceBySize[ ( int ) size ];
        }

        public void updatePrice ( PizzaSize size, decimal price )
        {
            priceBySize[ ( int ) size ] = price;
        }

        private decimal[] priceBySize = new decimal[ Enum.GetNames( typeof( PizzaSize ) ).Length ];
    }
}
