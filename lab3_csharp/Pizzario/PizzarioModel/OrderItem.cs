﻿namespace Pizzario.Model
{
    public class OrderItem
    {
        public PizzaKind Kind { get; private set; }

        public PizzaSize Size { get; private set; }

        public decimal FixedPrice { get; private set; }

        public int Quantity { get; private set; }


        public OrderItem ( PizzaKind kind, PizzaSize size, decimal fixedPrice, int quantity )
        {
            this.Kind = kind;
            this.Size = size;
            this.FixedPrice = fixedPrice;
            this.Quantity = quantity;
        }

        public decimal getCost ()
        {
            return FixedPrice * Quantity;
        }
    }
}
