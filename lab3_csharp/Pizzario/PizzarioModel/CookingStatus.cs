﻿namespace Pizzario.Model
{
    public enum CookingStatus
    {
        NotStarted,
        Started,
        Finished,
        Cancelled
    }
}
