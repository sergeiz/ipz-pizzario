﻿using System;
using System.Collections.Generic;

namespace Pizzario.Model
{
    public class PizzarioNetwork
    {
        public Menu getMenu ()
        {
            return menu;
        }

        public int getOrdersCount ()
        {
            return orders.Count;
        }

        public IEnumerable< Order > Orders
        {
            get {  return orders; }
        }

        public Order newOrder ( DeliveryContact contact, PaymentPlan payPlan )
        {
            Order o = new Order(orders.Count, contact, payPlan );
            orders.Add( o );
            return o;
        }

        public void pushOrder ( Order order )
        {
            orders.Add( order );
        }

        private Menu menu = new Menu();
        private List< Order > orders = new List< Order >();
    }
}
