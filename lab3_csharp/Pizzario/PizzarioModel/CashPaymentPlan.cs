﻿namespace Pizzario.Model
{
    public class CashPaymentPlan : PaymentPlan
    {
        public bool Payed { get; private set; }

		
        public CashPaymentPlan ()
        {
            this.Payed = false;
        }

        public override bool wasPayed ()
        {
            return Payed;
        }

        public void setPayed ( bool payed)
        {
            this.Payed = payed;
        }

        public override bool expectPrepayment ()
        {
            return false;
        }

        public override string ToString()
        {
            return "Cash payment on delivery";
        }

        public override PaymentPlan clone ()
        {
            CashPaymentPlan copy = new CashPaymentPlan();
            copy.setPayed( wasPayed() );
            return copy;
        }
    }
}
