﻿using Pizzario.Model;
using System;
using System.Collections.Generic;
using System.IO;

namespace Pizzario.Report
{
    public class ReportGenerator
    {
        public ReportGenerator ( TextWriter output, PizzarioNetwork network )
        {
            this.output = output;
            this.network = network;
        }

        public void generate ()
        {
            showMenu( network.getMenu() );
            showOrders( network.Orders );
        }

        private void showMenu ( Menu m )
        {
            output.WriteLine(" ==== Menu ==== ");

            foreach ( string name in m.PizzaKindNames )
            {
                PizzaKind kind = m.findPizzaKind( name );
                output.WriteLine();
                output.WriteLine( "Pizza \"" + name + "\":" );
                output.WriteLine( "\tDescription: " + kind.Description );
                output.WriteLine( "\tImage URL: " + kind.ImageUrl );

                showPizzaRatings( kind );
                showPizzaIngredients( kind );
                showPizzaPrices( kind );
            }

            output.WriteLine();
            output.WriteLine(" ==== End Menu ==== ");
        }

        private void showPizzaRatings ( PizzaKind kind )
        {
            output.Write( "\tRating: " );
            output.Write( kind.getAverageRating() );
            output.WriteLine( " ( " + kind.Votes + " votes)" );
        }

        private void showPizzaIngredients ( PizzaKind kind )
        {
            PizzaRecipe recipe = kind.Recipe;

            bool first = true;

            output.Write( "\tIngredients: " );

            foreach ( string ingredient in recipe.Ingredients )
            {
                if ( ! first )
                    output.Write( ", " );
                else
                    first = false;

                output.Write( recipe.getIngredientWeight( ingredient ) );
                output.Write( "g " );
                output.Write( ingredient );
            }

            output.WriteLine();
        }

        private void showPizzaPrices ( PizzaKind kind )
        {
            output.Write( "\tPrices: " );
            output.Write( kind.getCurrentPrice( PizzaSize.Small ) );
            output.Write( " small, " );
            output.Write( kind.getCurrentPrice( PizzaSize.Medium ) );
            output.Write( " medium, " );
            output.Write( kind.getCurrentPrice( PizzaSize.Large ) );
            output.WriteLine( " large" );
        }

        private void showOrders ( IEnumerable< Order > orders )
        {
            output.WriteLine( " ==== Orders ==== " );

            foreach ( Order order in orders )
                showOrder( order );

            output.WriteLine();
            output.WriteLine( " ==== End Orders ==== " );
        }


        private void showOrder ( Order order )
        {
            output.WriteLine();
            output.Write( "Order #" );
            output.Write( order.OrderId );
            output.WriteLine( ":" );

            output.Write( "\tStatus: " );
            output.WriteLine( Enum.GetNames( typeof( OrderStatus ) )[ ( int ) order.Status ] );

            output.Write( "\tTotal cost: " );
            output.WriteLine( order.getTotalCost() );

            output.Write( "\tPayment plan: " );
            output.WriteLine( order.PayPlan.ToString() );

            output.Write( "\tDiscount: " );
            output.Write( order.Discount * 100.0 );
            output.WriteLine( '%' );

            showDeliveryContact( order.Contact );
            showCart( order.Cart );
            showOrderedPizzas( order );
        }


        private void showDeliveryContact ( DeliveryContact contact )
        {
            output.Write( "\tContact: " );
            output.WriteLine( contact.Address );

            output.Write( "\tPhone: " );
            output.WriteLine( contact.Phone );

            output.Write( "\tComment: " );
            output.WriteLine( contact.Comment );
        }


        private void showCart ( OrderCart cart )
        {
            output.WriteLine( "\tCart:" );

            int nItems = cart.getItemsCount();
            for ( int i = 0; i < nItems; i++ )
            {
                output.Write( "\t  " );
                output.Write( i + 1 );
                output.Write( ") " );

                OrderItem item = cart.getItem( i );

                output.Write( item.Kind.Name );
                output.Write( ' ' );
                output.Write( Enum.GetNames( typeof( PizzaSize ) )[ ( int ) item.Size ] );
                output.Write( ", price " );
                output.Write( item.FixedPrice );
                output.Write( ", quantity " );
                output.Write( item.Quantity );
                output.Write( ", cost " );
                output.WriteLine( item.getCost() );
            }
        }


        private void showOrderedPizzas ( Order order )
        {
            if ( order.getPizzasCount() == 0 )
                return;

            output.WriteLine( "\tPizzas:" );

            int nPizzas = order.getPizzasCount();
            for ( int i = 0; i < nPizzas; i++ )
            {
                output.Write( "\t  " );
                output.Write( i + 1 );
                output.Write( ") Pizza " );

                Pizza p = order.getPizza( i );
                output.Write( p.Kind.Name );
                output.Write( ' ' );
                output.Write( Enum.GetNames( typeof( PizzaSize ) )[ ( int ) p.Size ] );
                output.Write( ", cooking status: " );
                output.WriteLine( Enum.GetNames( typeof( CookingStatus ) )[ ( int ) p.Status ] );
            }
        }
    
        private PizzarioNetwork network;
        private TextWriter output;
    }
}
