﻿using System.Collections.Generic;

namespace Pizzario.Model
{
    public class Menu
    {
        public IEnumerable< string > PizzaKindNames
        {
            get { return pizzaKindsByName.Keys; }
        }

        public IEnumerable< PizzaKind > PizzaKinds
        {
            get { return pizzaKindsByName.Values; }
        }


        public int getPizzaKindsCount ()
        {
            return pizzaKindsByName.Count;
        }

        public PizzaKind findPizzaKind ( string name )
        {
            PizzaKind result;
            if ( pizzaKindsByName.TryGetValue( name, out result ) )
                return result;
            else
                return null;
        }

        public void addPizzaKind ( PizzaKind kind )
        {
           	if ( findPizzaKind( kind.Name ) != null )
        		throw new System.Exception( "Menu.addPizzaKind: duplicate kind" );

            pizzaKindsByName[ kind.Name ] = kind;
        }

        public void deletePizzaKind ( string name )
        {
            PizzaKind kind = findPizzaKind( name );
            if ( kind != null )
                pizzaKindsByName.Remove( name );
        }
        
        public void renamePizzaKind ( string name, string newName )
        {
            PizzaKind kind = findPizzaKind( name );
            if ( kind == null )
                throw new System.Exception( "Menu.renamePizzaKind: unregistered kind" );

            if ( name == newName )
                return;

            pizzaKindsByName.Remove( name );
            kind.Name = newName;
            pizzaKindsByName[ newName ] = kind;
        }

        public OrderItem makeOrderItem ( PizzaKind kind, PizzaSize size, int quantity )
        {
            return new OrderItem( kind, size, kind.getCurrentPrice( size ), quantity );
        }

        private Dictionary< string, PizzaKind > pizzaKindsByName = new Dictionary< string, PizzaKind  >();
    }
}
