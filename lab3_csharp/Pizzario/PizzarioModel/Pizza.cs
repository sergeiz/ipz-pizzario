﻿namespace Pizzario.Model
{
    public class Pizza
    {
        public PizzaKind Kind { get; private set; }

        public PizzaSize Size { get; private set; }

        public Order ParentOrder { get; private set; }

        public CookingStatus Status { get; private set; }


        public Pizza ( PizzaKind kind, PizzaSize size, Order parentOrder )
        {
            this.Kind = kind;
            this.Size = size;
            this.ParentOrder = parentOrder;
            this.Status = CookingStatus.NotStarted;
        }

        public void onCookingStarted ()
        {
            if ( Status == CookingStatus.NotStarted )
            {
                Status = CookingStatus.Started;
                ParentOrder.onPizzaCookingStarted();
            }
            else
                throw new System.Exception( "Pizza.onCookingStarted: may only start cooking if not started yet" );
        }


        public void onCookingCancelled ()
        {
	        if ( Status == CookingStatus.Started || Status == CookingStatus.NotStarted )
                Status = CookingStatus.Cancelled;

            else
                throw new System.Exception( "Pizza.onCookingCancelled: may only abort unfinished pizza" );
        }


        public void onCookingFinished ()
        {
            if ( Status == CookingStatus.Started )
            {
                Status = CookingStatus.Finished;
                ParentOrder.onPizzaReady();
            }
            else
                throw new System.Exception( "Pizza.onCookingFinished: may only finish cooking if started" );
        }
    }
}
