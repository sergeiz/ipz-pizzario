﻿using System.Collections.Generic;

namespace Pizzario.Model
{
    public class OrderCart
    {
        public IList< OrderItem > Items 
        {
            get
            {
                return items;
            }
            private set
            {
                items.Clear();
                items.AddRange( value );
            }
        }


        public OrderCart ()
        {
            this.items = new List< OrderItem >();
        }

        public int getItemsCount ()
        {
            return items.Count;
        }

        public OrderItem getItem ( int index )
        {
            return items[ index ];
        }

        public void addItem ( OrderItem item )
        {
            items.Add( item );
        }

        public void updateItem ( int index, OrderItem item )
        {
            items[ index ] = item;
        }

        public void dropItem ( int index )
        {
            items.RemoveAt( index );
        }

        public void clearItems ()
        {
            items.Clear();
        }

        public decimal getCost ()
        {
            decimal totalCost = 0;
            items.ForEach( ( OrderItem i ) => totalCost += i.getCost() );
            return totalCost;
        }


        private List< OrderItem > items;
    }
}
