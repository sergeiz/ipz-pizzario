﻿using Pizzario.Model;

namespace Pizzario.Model.Test
{
    public class TestModelGenerator
    {
        public PizzarioNetwork generate ()
        {
            PizzarioNetwork network = new PizzarioNetwork();
            fillPizzaMenu( network.getMenu() );
            fillOrders( network, network.getMenu() );
            return network;
        }

        private void fillPizzaMenu ( Menu m )
        {
            ////

            PizzaKind carbonara = new PizzaKind( "Carbonara" );
            m.addPizzaKind( carbonara );

            carbonara.Description = "One of the most popular pizza recipes in the world";
            carbonara.setupRatingStats( 170, 812 );
            carbonara.ImageUrl = "http://pizzario.com.ua/images/carbonara.jpg";

            carbonara.Recipe.addIngredient( "Cheese", 100 );
            carbonara.Recipe.addIngredient( "Olive Oil", 30 );

            carbonara.updatePrice( PizzaSize.Small, 3.00M );
            carbonara.updatePrice( PizzaSize.Medium, 5.00M );
            carbonara.updatePrice( PizzaSize.Large, 7.00M );

            ////

            PizzaKind milano = new PizzaKind( "Milano" );
            m.addPizzaKind( milano );

            milano.Description = "Classic recipe with ham, mushrooms and tomatoes";
            milano.setupRatingStats( 102, 415 );
            milano.ImageUrl = "http://pizzario.com.ua/images/milano.jpg";

            milano.Recipe.addIngredient( "Ham", 100 );
            milano.Recipe.addIngredient( "Mushrooms", 50 );
            milano.Recipe.addIngredient( "Tomatoes", 50 );

            milano.updatePrice( PizzaSize.Small, 2.50M );
            milano.updatePrice( PizzaSize.Medium, 4.15M );
            milano.updatePrice( PizzaSize.Large, 5.80M );
        }

        private void fillOrders ( PizzarioNetwork network, Menu m )
        {
            PizzaKind carbonara = m.findPizzaKind( "Carbonara" );
            PizzaKind milano = m.findPizzaKind( "Milano" );

            Order order1 = network.newOrder(
                    new DeliveryContact()
                    { Address = "Lenin av. 14, room 320", 
                      Phone = "702-13-26", 
                      Comment = "APVT Department" }
                , new CashPaymentPlan()
            );

            order1.Cart.addItem( m.makeOrderItem( carbonara, PizzaSize.Medium, 2 ) );
            order1.Cart.addItem( m.makeOrderItem( milano, PizzaSize.Large, 1 ) );
            order1.setDiscount( 0.025 );
            order1.process();


            Order order2 = network.newOrder(
                    new DeliveryContact()
                    { Address = "23rd August Str 2, ap. 2", 
                      Phone = "333-33-33", 
                      Comment = "Knock 3 times" }
                , new CashPaymentPlan()
            );

            order2.Cart.addItem( m.makeOrderItem( carbonara, PizzaSize.Small, 1 ) );
        }

    }
}
