﻿using System.Collections.Generic;

namespace Pizzario.Model
{
    public class PizzaRecipe
    {
        public IEnumerable< string > Ingredients
        {
            get { return ingredientsWeight.Keys; }
        }

        public int getIngredientsCount ()
        {
            return ingredientsWeight.Count;
        }

        public int getIngredientWeight ( string ingredient )
        {
            int result;
            if ( ingredientsWeight.TryGetValue( ingredient, out result ) )
                return result;

            else
                return 0;
        }

        public void addIngredient ( string ingredient, int weight )
        {
            if ( ingredientsWeight.ContainsKey( ingredient ) )
                throw new System.Exception( "PizzaKind.addIngredient: duplicate ingredient" );

            else
                ingredientsWeight.Add(ingredient, weight);
        }

        public void updateIngredient ( string ingredient, int weight )
        {
            if ( ingredientsWeight.ContainsKey( ingredient ) )
                ingredientsWeight[ ingredient ] = weight;

            else
                throw new System.Exception( "PizzaKind.updateIngredient: missing ingredient" );
        }

        public void removeIngredient ( string ingredient )
        {
            if ( ingredientsWeight.ContainsKey( ingredient ) )
                ingredientsWeight.Remove( ingredient );

            else
                throw new System.Exception( "PizzaKind.updateIngredient: missing ingredient" );
        }

        private Dictionary< string, int > ingredientsWeight = new Dictionary< string, int >();
    }
}
