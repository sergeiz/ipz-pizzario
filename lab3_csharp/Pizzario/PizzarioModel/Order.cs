﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Pizzario.Model
{
    public class Order
    {
        public int OrderId { get; private set; }

        public OrderCart Cart { get; private set; }

        public PaymentPlan PayPlan { get; private set; }

        public DeliveryContact Contact { get; set; }

        public OrderStatus Status { get; private set; }

        public double Discount { get; private set; }

        public IList< Pizza > Pizzas { get; private set; }


        public Order ( int id, DeliveryContact contact, PaymentPlan payPlan )
        {
            this.OrderId = id;
            this.Contact = contact;
            this.PayPlan = payPlan;

            this.Cart = new OrderCart();
            this.Status = OrderStatus.New;
            this.Discount = 0.0;
            this.Pizzas = new List< Pizza >();
        }

        public int getPizzasCount ()
        {
            return Pizzas.Count;
        }

        public Pizza getPizza ( int index )
        {
            return Pizzas[ index ];
        }

        public decimal getTotalCost ()
        {
            decimal cost = Cart.getCost();
            return ( decimal )( ( double ) cost * ( 1.0 - Discount ) );
        } 

        public void setDiscount ( double discount )
        {
            if ( discount < 0.0 || discount > 1.0 )
                throw new System.Exception( "Order.setDisconut - invalid percentage value" );

            this.Discount = discount;
        }

        public void process ()
        {
           	if ( Status != OrderStatus.New )
		        throw new System.Exception( "Order.process - Can only process new order!" );

            Status = OrderStatus.Registered;

            int nItems = Cart.getItemsCount();
            for ( int i = 0; i < nItems; i++ )
            {
                OrderItem item = Cart.getItem( i );
                for ( int k = 0; k < item.Quantity; k++ )
                    Pizzas.Add( new Pizza( item.Kind, item.Size, this ) );
            }
        }

        public void cancel ()
        {
	        switch ( Status )
	        {
		        case OrderStatus.New:
		        case OrderStatus.Registered:
		        case OrderStatus.Cooking:
		        case OrderStatus.Ready4Delivery:
		        case OrderStatus.Delivering:
			        break;

		        default:
			        throw new System.Exception( "Order::cancel - cannot cancel in current order state" );
	        }

	        Status = OrderStatus.Cancelled;
        }


        public void onPizzaCookingStarted ()
        {
            if ( Status == OrderStatus.Registered )
                Status = OrderStatus.Cooking;

            else if ( Status != OrderStatus.Cooking )
                throw new System.Exception( "Order.onPizzaCookingStarted - can only happen in Registered & Cooking states" );
        }


        public void onPizzaReady ()
        {
            if ( Status != OrderStatus.Cooking )
                throw new System.Exception( "Order.onPizzaReady - can only happen in Cooking state" );

            if ( Pizzas.All( ( Pizza p ) => p.Status == CookingStatus.Finished ) )
	    	    Status = OrderStatus.Ready4Delivery;
        }


        public void onStartedDelivery ()
        {
            if ( Status != OrderStatus.Ready4Delivery )
                throw new System.Exception("Order.onStartedDelivery - can only happen in Ready4Delivery state");

            Status = OrderStatus.Delivering;
        }


        public void onDelivered ()
        {
            if ( Status != OrderStatus.Delivering )
		        throw new System.Exception( "Order.onDelivered - can only happen in Delivering state" );

            Status = OrderStatus.Delivered;
        }
    }
}
