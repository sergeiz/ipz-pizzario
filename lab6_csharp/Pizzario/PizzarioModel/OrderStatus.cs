﻿namespace Pizzario.Model
{
    public enum OrderStatus
    {
        Registered,
        PaymentPlanDefined,
        Cooking,
        Ready4Delivery,
        Delivering,
        Delivered,
        Cancelled
    }
}
