﻿using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Pizzario.Model
{
    public class PizzaRecipe
    {
        public int PizzaRecipeId { get; set; }

        public IEnumerable< string > Ingredients
        {
            get { return ingredientsWeight.Keys; }
        }

        public int GetIngredientsCount ()
        {
            return ingredientsWeight.Count;
        }

        public int GetIngredientWeight ( string ingredient )
        {
            int result;
            if ( ingredientsWeight.TryGetValue( ingredient, out result ) )
                return result;

            else
                return 0;
        }


        public bool HasAllOfIngredients ( string[] ingredients )
        {
            foreach ( string ingredient in ingredients )
                if ( ! ingredientsWeight.Keys.Contains( ingredient ) )
                    return false;

            return true;
        }


        public bool HasNoneOfIngredients ( string[] ingredients )
        {
            foreach ( string ingredient in ingredients )
                if ( ingredientsWeight.Keys.Contains( ingredient ) )
                    return false;

            return true;
        }


        public void AddIngredient ( string ingredient, int weight )
        {
            if ( ingredientsWeight.ContainsKey( ingredient ) )
                throw new System.Exception( "PizzaKind.AddIngredient: duplicate ingredient" );

            else
                ingredientsWeight.Add(ingredient, weight);
        }

        public void UpdateIngredient ( string ingredient, int weight )
        {
            if ( ingredientsWeight.ContainsKey( ingredient ) )
                ingredientsWeight[ ingredient ] = weight;

            else
                throw new System.Exception( "PizzaKind.UpdateIngredient: missing ingredient" );
        }

        public void RemoveIngredient ( string ingredient )
        {
            if ( ingredientsWeight.ContainsKey( ingredient ) )
                ingredientsWeight.Remove( ingredient );

            else
                throw new System.Exception( "PizzaKind.RemoveIngredient: missing ingredient" );
        }

        private Dictionary< string, int > ingredientsWeight = new Dictionary< string, int >();

        public string IngredientsWeightEncoded
        {
            get
            {
                StringBuilder builder = new StringBuilder();
                foreach ( var kv in ingredientsWeight )
                {
                    builder.Append( kv.Key );
                    builder.Append( '\n' );
                    builder.Append( kv.Value );
                    builder.Append( '\n' );
                }

                return builder.ToString();
            }
            private set
            {
                ingredientsWeight.Clear();

                string trimmed = value.TrimEnd();
                if ( trimmed.Length > 0 )
                {
                    string[] parts = trimmed.Split( '\n' );
                    for ( int i = 0; i < parts.Length; i += 2 )
                        ingredientsWeight.Add( parts[ i ], int.Parse( parts[ i + 1 ] ) );
                }
            }
        }
    }
}
