﻿namespace Pizzario.Model
{
    public enum PizzaSize
    {
        Small,
        Medium,
        Large
    }
}
