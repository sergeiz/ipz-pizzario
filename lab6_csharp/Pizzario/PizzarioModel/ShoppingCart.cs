﻿using System;
using System.Collections.Generic;

namespace Pizzario.Model
{
    public class ShoppingCart
    {
        public int ShoppingCartId { get; set; }

        public virtual IList< OrderItem > Items { get; private set; }

        public bool Modifiable { get; private set; }
        
        public decimal Cost
        {
            get
            {
                decimal totalCost = 0;
                for ( int i = 0; i < Items.Count; i++ )
                    totalCost += Items[ i ].Cost;
                return totalCost;
            }
        }


        public ShoppingCart ()
        {
            this.Items = new List< OrderItem >();
            this.Modifiable = true;
        }


        public int GetItemsCount ()
        {
            return Items.Count;
        }


        public OrderItem GetItem ( int index )
        {
            return Items[ index ];
        }


        public int FindItemIndex ( PizzaKind kind, PizzaSize size )
        {
            for ( int i = 0; i < GetItemsCount(); i++ )
            {
                OrderItem item = GetItem( i );
                if ( item.Kind == kind && item.Size == size )
                    return i;
            }

            return -1;
        }


        public void AddItem ( OrderItem item )
        {
            if ( ! Modifiable )
                throw new System.Exception( "ShoppingCart.AddItem: unmodifiable cart" );

            int existingItemIndex = FindItemIndex( item.Kind, item.Size );
            if ( existingItemIndex != -1 )
                throw new System.Exception( "ShoppingCart.AddItem: duplicate kind-size pair added" );

            Items.Add( item );
        }


        public void UpdateItem ( int index, OrderItem item )
        {
            if ( ! Modifiable )
                throw new System.Exception( "ShoppingCart.UpdateItem: unmodifiable cart" );

            Items[ index ] = item;
        }


        public void DropItem ( int index )
        {
            if ( ! Modifiable )
                throw new System.Exception( "ShoppingCart.DropItem: unmodifiable cart" );

            Items.RemoveAt( index );
        }


        public void ClearItems ()
        {
            if ( ! Modifiable )
                throw new System.Exception( "ShoppingCart.ClearItems: unmodifiable cart" );

            Items.Clear();
        }


        public void Lock ()
        {
            if ( Items.Count == 0 )
                throw new System.Exception( "ShoppingCart.Lock: locking empty cart" );

            Modifiable = false;
        }
    }
}
