﻿using System;

namespace Pizzario.Model
{
    public class RefundingRequest
    {
        public int RequestId { get; set; }

        public virtual Order RelatedOrder { get; private set; }

        public RefundingStatus Status { get; private set; }


        protected RefundingRequest () {}


        public RefundingRequest ( Order order )
        {
            this.RelatedOrder = order;
            this.Status = Status;
        }


        public void Confirmed ()
        {
            if ( Status == RefundingStatus.Requested )
                Status = RefundingStatus.Confirmed;

            else
                throw new System.Exception( "RefundingRequest: confirmation possible in Requested state only" );
        }


        public void Rejected ()
        {
            if ( Status == RefundingStatus.Requested )
                Status = RefundingStatus.Rejected;

            else
                throw new System.Exception( "RefundingRequest: rejection possible in Requested state only" );
        }
    }
}
