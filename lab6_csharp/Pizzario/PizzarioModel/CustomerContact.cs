﻿namespace Pizzario.Model
{
    public class CustomerContact
    {
        public string Email { get; set; }

        public string Address { get; set; }

        public string Phone { get; set; }

        public string Comment { get; set; }
    }
}
