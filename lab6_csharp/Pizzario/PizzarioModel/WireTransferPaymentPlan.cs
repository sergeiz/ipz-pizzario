﻿namespace Pizzario.Model
{
    public class WireTransferPaymentPlan : PaymentPlan
    {
        public string CardCode { get; private set; }

        public string CardHolder { get; private set; }

        public int TransactionId { get; set; }

        private WireTransferPaymentPlan () { }

        public WireTransferPaymentPlan ( string cardCode, string cardHolder )
        {
            this.CardCode = cardCode;
            this.CardHolder = cardHolder;
            this.TransactionId = -1;
        }

        public void SetPaymentTransactionId ( int transactionId )
        {
            this.TransactionId = transactionId;
            MarkPayed();
        }

        public override bool ExpectPrepayment ()
        {
            return true;
        }

        public override bool IsRefundable ()
        {
            return true;
        }

        public override string ToString ()
        {
            return "Wire transfer";
        }
    }
}
