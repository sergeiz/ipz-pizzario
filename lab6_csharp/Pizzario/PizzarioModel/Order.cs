﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Pizzario.Model
{
    public class Order
    {
        public int OrderId { get; set; }

        public virtual ICollection< OrderItem > Items { get; private set; }

        public decimal BasicCost { get; private set; }

        public virtual PaymentPlan PayPlan { get; private set; }

        public virtual CustomerContact Contact { get; set; }

        public OrderStatus Status { get; private set; }

        public int RemainingPizzasToCook { get; private set; }

        public double Discount
        {
            get
            {
                return discount;
            }
            set
            {
                if ( value < 0.0 || value > 1.0 )
                    throw new System.Exception( "Order: discount should be between [0.0;1.0]" );

                this.discount = value;
            }
        }

        public decimal TotalCost
        {
            get
            {
                return ( decimal ) ( ( double ) BasicCost * ( 1.0 - Discount ) );
            }
        }


        protected Order () {}

        public Order ( ShoppingCart cart, CustomerContact contact )
        {
            if ( cart.Modifiable )
                throw new System.Exception( "Order: initializing with a cart being edited" );

            this.Items     = new List< OrderItem >( cart.Items );
            this.BasicCost = cart.Cost;
            this.Contact   = contact;

            this.Status = OrderStatus.Registered;

            this.RemainingPizzasToCook = 0;
            foreach ( OrderItem item in Items )
                this.RemainingPizzasToCook += item.Quantity;
        }


        public void DefinePaymentPlan ( PaymentPlan plan )
        {
            if ( Status != OrderStatus.Registered )
                throw new System.Exception( "Order.DefinePaymentPlan - can only run in Registered state" );

            if ( plan.ExpectPrepayment() && ! plan.Payed )
                throw new System.Exception( "Order: pre-payment unavailable" );

            this.PayPlan = plan;

            Status = OrderStatus.PaymentPlanDefined;
        }


        public void Cancel ()
        {
	        switch ( Status )
	        {
		        case OrderStatus.Registered:
                case OrderStatus.PaymentPlanDefined:
		        case OrderStatus.Cooking:
		        case OrderStatus.Ready4Delivery:
			        break;

		        default:
			        throw new System.Exception( "Order.Cancel - cannot cancel in current order state" );
	        }

	        Status = OrderStatus.Cancelled;
        }


        public void OnStartedCookingOneOfPizzas ()
        {
            if ( Status == OrderStatus.PaymentPlanDefined )
                Status = OrderStatus.Cooking;

            else if ( Status != OrderStatus.Cooking )
                throw new System.Exception( "Order.OnStartedCookingOneOfPizzas - can only happen in PaymentPlanDefined & Cooking states" );
        }


        public void OnFinishedCookingOneOfPizzas ()
        {
            if ( Status == OrderStatus.Cooking )
            {
                -- RemainingPizzasToCook;
                if ( RemainingPizzasToCook == 0 )
                    Status = OrderStatus.Ready4Delivery;
            }
            else
                throw new System.Exception( "Order.OnFinishedCookingOneOfPizzas - can only happen in Cooking state" );
        }


        public void OnStartedDelivery ()
        {
            if ( Status != OrderStatus.Ready4Delivery )
                throw new System.Exception( "Order.OnStartedDelivery - can only happen in Ready4Delivery state" );

            Status = OrderStatus.Delivering;
        }


        public void OnDelivered ()
        {
            if ( Status != OrderStatus.Delivering )
                throw new System.Exception( "Order.OnDelivered - can only happen in Delivering state" );

            if ( ! PayPlan.ExpectPrepayment() && ! PayPlan.Payed )
                throw new System.Exception( "Order.OnDelivered - post-payment should have been collected by now" );

            Status = OrderStatus.Delivered;
        }


        public bool MayBeRefunded ()
        {
            return Status == OrderStatus.Cancelled &&
                   PayPlan.IsRefundable();
        }


        private double discount = 0.0;
    }
}
