﻿namespace Pizzario.Model
{
    public class CashPaymentPlan : PaymentPlan
    {
        public override bool ExpectPrepayment ()
        {
            return false;
        }

        public override bool IsRefundable ()
        {
            return false;
        }

        public override string ToString()
        {
            return "Cash payment on delivery";
        }
    }
}
