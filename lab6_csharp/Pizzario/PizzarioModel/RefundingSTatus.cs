﻿namespace Pizzario.Model
{
    public enum RefundingStatus
    {
        Requested,
        Confirmed,
        Rejected
    }
}
