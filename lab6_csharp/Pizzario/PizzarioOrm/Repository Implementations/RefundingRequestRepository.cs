﻿using System;

using Pizzario.Model;


namespace Pizzario.Orm
{
    class RefundingRequestRepository : BasicRepository< RefundingRequest >, 
                                       IRefundingRequestRepository
    {
        public RefundingRequestRepository ( PizzarioDbFacade dbContext )
            :   base( dbContext, dbContext.RefundingRequests )
        {
        }
    }
}
