﻿using System;
using System.Linq;

using Pizzario.Model;

namespace Pizzario.Orm
{
    class ShoppingCartRepository : BasicRepository< ShoppingCart >, 
                                   IShoppingCartRepository
    {
        public ShoppingCartRepository ( PizzarioDbFacade dbContext )
            :   base( dbContext, dbContext.ShoppingCarts )
        {
        }
    }
}
