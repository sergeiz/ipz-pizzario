﻿using System;

using Pizzario.Model;


namespace Pizzario.Orm
{
    class OrderRepository : BasicRepository< Order >, IOrderRepository
    {
        public OrderRepository ( PizzarioDbFacade dbContext )
            :   base( dbContext, dbContext.Orders )
        {
        }
    }
}
