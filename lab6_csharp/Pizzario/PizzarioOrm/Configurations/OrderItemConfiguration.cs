﻿using Pizzario.Model;

using System.Data.Entity.ModelConfiguration;

namespace Pizzario.Orm
{
    class OrderItemConfiguration : EntityTypeConfiguration< OrderItem >
    {
        public OrderItemConfiguration ()
        {
            HasKey( i => i.OrderItemId );
            HasRequired( i => i.Kind );
        }
    }
}
