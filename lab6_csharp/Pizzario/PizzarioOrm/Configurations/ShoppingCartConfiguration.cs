﻿using Pizzario.Model;

using System.Data.Entity.ModelConfiguration;

namespace Pizzario.Orm
{
    class ShoppingCartConfiguration : EntityTypeConfiguration< ShoppingCart >
    {
        public ShoppingCartConfiguration ()
        {
            HasKey( c => c.ShoppingCartId );
            HasMany< OrderItem >( c => c.Items ).WithRequired();
        }
    }
}
