﻿using Pizzario.Model;

using System.Data.Entity.ModelConfiguration;

namespace Pizzario.Orm
{
    class PaymentPlanConfiguration : EntityTypeConfiguration< PaymentPlan >
    {
        public PaymentPlanConfiguration ()
        {
            HasKey( p => p.PaymentPlanId );
        }
    }
}
