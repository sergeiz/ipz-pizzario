﻿using System;
using System.Linq;
using Pizzario.Model;

namespace Pizzario.Orm
{
    public interface IMenuRepository : IRepository< PizzaKind >
    {
        PizzaKind FindByName ( string name );
    }
}
