﻿using System;

using Pizzario.Model;

namespace Pizzario.Orm
{
    public interface IShoppingCartRepository : IRepository< ShoppingCart >
    {
    }
}
