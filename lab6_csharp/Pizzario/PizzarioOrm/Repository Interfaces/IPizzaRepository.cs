﻿using System;
using System.Linq;

using Pizzario.Model;

namespace Pizzario.Orm
{
    public interface IPizzaRepository : IRepository< Pizza >
    {
        IQueryable< Pizza > SearchPizzasByOrderID ( int orderID );
    }
}
