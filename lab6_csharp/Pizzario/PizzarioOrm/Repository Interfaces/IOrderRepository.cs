﻿using System;

using Pizzario.Model;

namespace Pizzario.Orm
{
    public interface IOrderRepository : IRepository< Order >
    {
    }
}
