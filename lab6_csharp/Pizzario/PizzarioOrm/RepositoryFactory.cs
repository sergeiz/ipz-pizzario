﻿namespace Pizzario.Orm
{
    public static class RepositoryFactory
    {
        public static IMenuRepository MakeMenuRepository ( PizzarioDbFacade dbContext )
        {
            return new MenuRepository( dbContext );
        }

        public static IShoppingCartRepository MakeShoppingCartRepository ( PizzarioDbFacade dbContext )
        {
            return new ShoppingCartRepository( dbContext );
        }

        public static IOrderRepository MakeOrderRepository ( PizzarioDbFacade dbContext )
        {
            return new OrderRepository( dbContext );
        }

        public static IPizzaRepository MakePizzaRepository ( PizzarioDbFacade dbContext )
        {
            return new PizzaRepository( dbContext );
        }

        public static IRefundingRequestRepository MakeRefundingRequestRepository ( PizzarioDbFacade dbContext )
        {
            return new RefundingRequestRepository( dbContext );
        }
    }
}
