﻿using Pizzario.Model;

using NUnit.Framework;
using System;

namespace Pizzario.Model.UnitTests
{
    [ TestFixture ]
    public class PizzaRecipeTests
    {
        [ Test ]
        public void Ingredients_Initially_None ()
        {
            PizzaRecipe recipe = new PizzaRecipe();

            Assert.AreEqual( recipe.GetIngredientsCount(), 0 );
        }


        [ Test ]
        public void Ingredients_AskMissingInEmpty_ReturnsZero ()
        {
            PizzaRecipe recipe = new PizzaRecipe();

            Assert.AreEqual( recipe.GetIngredientWeight( "cheese" ), 0 );
        }


        [ Test ]
        public void Ingredients_InsertOne_ReturnWeight ()
        {
            PizzaRecipe recipe = new PizzaRecipe();
            recipe.AddIngredient( "cheese", 10 );

            Assert.AreEqual( recipe.GetIngredientsCount(), 1 );
            Assert.AreEqual( recipe.GetIngredientWeight( "cheese" ), 10 );
        }
        

        [ Test ]
        public void Ingredients_AskMissingInNonEmpty_ReturnsZero ()
        {
            PizzaRecipe recipe = new PizzaRecipe();
            recipe.AddIngredient( "cheese", 10 );

            Assert.AreEqual( recipe.GetIngredientWeight( "ham" ), 0 );
        }


        [ Test ]
        public void Ingredients_InsertTwo_ReturnEachWeight ()
        {
            PizzaRecipe recipe = new PizzaRecipe();
            recipe.AddIngredient( "cheese", 10 );
            recipe.AddIngredient( "ham", 20 );

            Assert.AreEqual( recipe.GetIngredientsCount(), 2 );
            Assert.AreEqual( recipe.GetIngredientWeight( "cheese" ), 10 );
            Assert.AreEqual( recipe.GetIngredientWeight( "ham" ), 20 );
        }


        [ Test ]
        public void Ingredients_InsertDuplicate_Forbidden ()
        {
            PizzaRecipe recipe = new PizzaRecipe();
            recipe.AddIngredient( "cheese", 10 );

            Assert.Throws< Exception>( () => recipe.AddIngredient( "cheese", 20 ) );
        }

        [ Test ]
        public void Ingredients_InsertAndUpdate_ReturnUpdatedWeight ()
        {
            PizzaRecipe recipe = new PizzaRecipe();
            recipe.AddIngredient( "cheese", 10 );
            recipe.UpdateIngredient( "cheese", 20 );

            Assert.AreEqual( recipe.GetIngredientWeight( "cheese" ), 20 );
        }


        [ Test ]
        public void Ingredients_UpdatingOneOfTwo_DoesNotAlterSecond ()
        {
            PizzaRecipe recipe = new PizzaRecipe();
            recipe.AddIngredient( "cheese", 10 );
            recipe.AddIngredient( "ham", 20 );
            recipe.UpdateIngredient( "cheese", 30 );

            Assert.AreEqual( recipe.GetIngredientWeight( "cheese" ), 30 );
            Assert.AreEqual( recipe.GetIngredientWeight( "ham" ), 20 );
        }


        [ Test ]
        public void Ingredients_DeleteSingle_MakesEmpty ()
        {
            PizzaRecipe recipe = new PizzaRecipe();
            recipe.AddIngredient( "cheese", 10 );
            recipe.RemoveIngredient( "cheese" );

            Assert.AreEqual( recipe.GetIngredientsCount(), 0 );
        }


        [ Test ]
        public void Ingredients_DeleteOneOfTwo_LeavesSecond ()
        {
            PizzaRecipe recipe = new PizzaRecipe();
            recipe.AddIngredient( "cheese", 10 );
            recipe.AddIngredient( "ham", 20 );
            recipe.RemoveIngredient( "cheese" );

            Assert.AreEqual( recipe.GetIngredientsCount(), 1 );
            Assert.AreEqual( recipe.GetIngredientWeight( "ham" ), 20 );
            Assert.AreEqual( recipe.GetIngredientWeight( "cheese" ), 0 );
        }


        [ Test ]
        public void Ingredients_DeleteMissingFromEmpty_Forbidden ()
        {
            PizzaRecipe recipe = new PizzaRecipe();
            
            Assert.Throws< Exception >( () => recipe.RemoveIngredient( "cheese" ) );
        }


        [ Test ]
        public void Ingredients_DeleteMissingFromNonEmpty_Forbidden ()
        {
            PizzaRecipe recipe = new PizzaRecipe();
            recipe.AddIngredient( "ham", 50 );

            Assert.Throws<Exception>( () => recipe.RemoveIngredient( "cheese" ) );
        }
    }
}
