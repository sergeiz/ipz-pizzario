﻿using Pizzario.Model;

using NUnit.Framework;
using System;

namespace Pizzario.Model.UnitTests
{
    [ TestFixture ]
    public class RefundingRequestTests
    {
        [ Test ]
        public void Constructor_ProjectsOrderCorrectly ()
        {
            Order order = makeOrder();
            RefundingRequest request = new RefundingRequest( order );

            Assert.AreSame( request.RelatedOrder, order );
        }


        [ Test ]
        public void Status_RequestedInitally ()
        {
            RefundingRequest request = new RefundingRequest( makeOrder() );
            Assert.AreEqual( request.Status, RefundingStatus.Requested );
        }


        [ Test ]
        public void Status_ConfirmationOfRequested_Ok ()
        {
            RefundingRequest request = new RefundingRequest( makeOrder() );
            request.Confirmed();
            Assert.AreEqual( request.Status, RefundingStatus.Confirmed );
        }


        [ Test ]
        public void Status_ConfirmationOfConfirmed_Fails ()
        {
            RefundingRequest request = new RefundingRequest( makeOrder() );
            request.Confirmed();
            Assert.Throws< Exception >( () => request.Confirmed() );
        }
        

        [ Test ]
        public void Status_ConfirmationOfRejected_Fails ()
        {
            RefundingRequest request = new RefundingRequest( makeOrder() );
            request.Rejected();
            Assert.Throws< Exception >( () => request.Confirmed() );
        }


        [ Test ]
        public void Status_RejectionOfRequested_Ok ()
        {
            RefundingRequest request = new RefundingRequest( makeOrder() );
            request.Rejected();
            Assert.AreEqual( request.Status, RefundingStatus.Rejected );
        }
        

        [ Test ]
        public void Status_RejectionOfConfirmed_Fails ()
        {
            RefundingRequest request = new RefundingRequest( makeOrder() );
            request.Confirmed();
            Assert.Throws< Exception >( () => request.Rejected() );
        }
        

        [ Test ]
        public void Status_RejectionOfRejected_Fails ()
        {
            RefundingRequest request = new RefundingRequest( makeOrder() );
            request.Rejected();
            Assert.Throws< Exception >( () => request.Rejected() );
        }


        private Order makeOrder ()
        {
            ShoppingCart cart = new ShoppingCart();
            cart.AddItem( new OrderItem( new PizzaKind( "AAA" ), PizzaSize.Small, 1.0M, 1 ) );
            cart.Lock();

            return new Order( cart, new CustomerContact() );
        }
    }
}
