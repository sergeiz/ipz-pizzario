﻿using Pizzario.Model;

using NUnit.Framework;
using System;


namespace Pizzario.Model.UnitTests
{
    [ TestFixture ]
    public class CashPaymentPlanTests
    {
        [ Test ]
        public void Payed_Initially_False ()
        {
            PaymentPlan p = new CashPaymentPlan();
            Assert.False( p.Payed );
        }


        [ Test ]
        public void Payed_SetPayedToTrue_Confirmed ()
        {
            PaymentPlan p = new CashPaymentPlan();
            p.MarkPayed();
            Assert.True( p.Payed );
        }


        [ Test ]
        public void Prepayment_NotExpected ()
        {
            PaymentPlan p = new CashPaymentPlan();
            Assert.False( p.ExpectPrepayment() );
        }


        [ Test ]
        public void Refundability_NotExpected ()
        {
            PaymentPlan p = new CashPaymentPlan();
            Assert.False( p.IsRefundable() );
        }
    }
}
