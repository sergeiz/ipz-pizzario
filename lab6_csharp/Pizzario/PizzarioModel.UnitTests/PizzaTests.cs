﻿using Pizzario.Model;

using NUnit.Framework;
using System;


namespace Pizzario.Model.UnitTests
{
    [ TestFixture ]
    public class PizzaTests
    {

        [ Test ]
        public void Constructor_ProjectsFields_Correctly ()
        {
            PizzaKind kind = new PizzaKind( "Carbonara" );
            Order o = makeOrder( kind );
            Pizza p = new Pizza( o, kind, PizzaSize.Small );

            Assert.AreSame( p.RelatedOrder, o );
            Assert.AreSame( p.Kind, kind );
            Assert.AreEqual( p.Size, PizzaSize.Small );
        }


        [ Test ]
        public void Constructor_Initially_NotStarted ()
        {
            Pizza p = makeSimplePizza();

            Assert.AreEqual( p.Status, CookingStatus.NotStarted );
        }


        [ Test ]
        public void CookingStarted_FromInitialState_Passes ()
        {
            Pizza p = makeSimplePizza();

            Assert.DoesNotThrow( () => p.OnCookingStarted() );
            Assert.AreEqual( p.Status, CookingStatus.Started );
        }


        [ Test ]
        public void CookingStarted_AlreadyStarted_Fails ()
        {
            Pizza p = makeSimplePizza();
            p.OnCookingStarted();
            
            Assert.Throws< Exception >( () => p.OnCookingStarted() );
        }


        [ Test ]
        public void CookingStarted_AfterFinish_Fails ()
        {
            Pizza p = makeSimplePizza();
            p.OnCookingStarted();
            p.OnCookingFinished();
            
            Assert.Throws< Exception >( () => p.OnCookingStarted() );
        }

        
        [ Test ]
        public void CookingStarted_AfterCancelled_Fails ()
        {
            Pizza p = makeSimplePizza();
            p.OnCookingStarted();
            p.OnCookingCancelled();
            
            Assert.Throws< Exception >( () => p.OnCookingStarted() );
        }


        [ Test ]
        public void CookingCancelled_FromStarted_Passes ()
        {
            Pizza p = makeSimplePizza();
            p.OnCookingStarted();
            p.OnCookingCancelled();

            Assert.AreEqual( p.Status, CookingStatus.Cancelled );
        }


        [ Test ]
        public void CookingCancelled_FromInitial_Passes ()
        {
            Pizza p = makeSimplePizza();
            p.OnCookingCancelled();

            Assert.AreEqual( p.Status, CookingStatus.Cancelled );
        }


        [ Test ]
        public void CookingCancelled_AlreadyCancelled_Fails ()
        {
            Pizza p = makeSimplePizza();
            p.OnCookingStarted();
            p.OnCookingCancelled();

            Assert.Throws< Exception >( () => p.OnCookingCancelled() );
        }


        [ Test ]
        public void CookingCancelled_AfterFinish_Fails ()
        {
            Pizza p = makeSimplePizza();
            p.OnCookingStarted();
            p.OnCookingFinished();
            
            Assert.Throws< Exception >( () => p.OnCookingCancelled() );
        }


        [ Test ]
        public void CookingFinished_FromInitial_Fails ()
        {
            Pizza p = makeSimplePizza();

            Assert.Throws< Exception >( () => p.OnCookingFinished() );
        }


        [ Test ]
        public void CookingFinished_FromStarted_Passes ()
        {
            Pizza p = makeSimplePizza();
            p.OnCookingStarted();
            p.OnCookingFinished();

            Assert.AreEqual( p.Status, CookingStatus.Finished );
        }


        [ Test ]
        public void CookingFinished_AlreadyFinished_Fails ()
        {
            Pizza p = makeSimplePizza();
            p.OnCookingStarted();
            p.OnCookingFinished();

            Assert.Throws< Exception >( () => p.OnCookingFinished() );
        }


        [ Test ]
        public void CookingFinished_WasCancelled_Fails ()
        {
            Pizza p = makeSimplePizza();
            p.OnCookingStarted();
            p.OnCookingCancelled();

            Assert.Throws<Exception>( () => p.OnCookingFinished() );
        }


        private static Pizza makeSimplePizza ()
        {
            PizzaKind kind = new PizzaKind( "some" );
            return new Pizza( makeOrder( kind ), kind, PizzaSize.Small );
        }


        private static Order makeOrder ( PizzaKind kind )
        {
            ShoppingCart cart = new ShoppingCart();
            cart.AddItem( new OrderItem( kind, PizzaSize.Small, 1.0M, 1 ) );
            cart.Lock();

            return new Order( cart, new CustomerContact() );
        }

    }
}
