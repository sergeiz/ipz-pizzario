﻿using Pizzario.Model;

using NUnit.Framework;
using System;


namespace Pizzario.Model.UnitTests
{
    [ TestFixture ]
    public class WireTransferPaymentPlanTests
    {
        [ Test ]
        public void Constructor_ProjectsFields_Correctly ()
        {
            var plan = new WireTransferPaymentPlan( "123", "Ivanov" );

            Assert.AreEqual( plan.CardCode, "123" );
            Assert.AreEqual( plan.CardHolder, "Ivanov" );
        }


        [ Test ]
        public void Payed_Initially_False ()
        {
            var p = makeWireTransferPlan();
            Assert.False( p.Payed );
        }


        [ Test ]
        public void Payed_SetPayedToTrue_Confirmed ()
        {
            var p = makeWireTransferPlan();
            p.MarkPayed();
            Assert.True( p.Payed );
        }


        [ Test ]
        public void Prepayment_Expected ()
        {
            var p = makeWireTransferPlan();
            Assert.True( p.ExpectPrepayment() );
        }


        [ Test ]
        public void Refundability_ExpectedIfPayed ()
        {
            var p = makeWireTransferPlan();
            p.MarkPayed();

            Assert.True( p.IsRefundable() );
        }


        [ Test ]
        public void Refundability_UnexpectedUnlessPayed ()
        {
            var p = makeWireTransferPlan();
            Assert.False( ! p.IsRefundable() );
        }


        [ Test ]
        public void TransactionId_Undefined_ByDefault ()
        {
            var p = makeWireTransferPlan();
            Assert.AreEqual( p.TransactionId, -1 );
        }


        [ Test ]
        public void TransactionId_CorrectlySaved ()
        {
            var p = makeWireTransferPlan();
            p.SetPaymentTransactionId( 12345 );
            Assert.AreEqual( p.TransactionId, 12345 );
        }


        [ Test ]
        public void TransactionId_MakesOrderPayed ()
        {
            var p = makeWireTransferPlan();
            p.SetPaymentTransactionId( 12345 );
            Assert.True( p.Payed );
        }


        private static WireTransferPaymentPlan makeWireTransferPlan ()
        {
            return new WireTransferPaymentPlan( "1234 5678 1234 5678", "Ivan Ivanov" );
        }
    }
}
