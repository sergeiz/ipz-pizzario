﻿using System;
using System.Text;

using Pizzario.Model;
using Pizzario.Infrastructure;

namespace Pizzario.Controller.Communications
{
    class EmailComposer
    {
        public EmailComposer ( IEmailAgent emailAgent )
        {
            this.emailAgent = emailAgent;
        }


        public void SendOrderRegistered ( Order order )
        {
            var builder = new StringBuilder();
            builder.Append( "Your Pizzario order #" );
            builder.Append( order.OrderId );
            builder.Append( " was registered." );
            builder.Append( " Total amount: $" );
            builder.Append( order.TotalCost );

            emailAgent.sendEmail( 
                order.Contact.Email, 
                "Pizza order registered", 
                builder.ToString()
            );
        }


        public void SendOrderScheduled ( Order order )
        {
            var builder = new StringBuilder();
            builder.Append( "Order #" );
            builder.Append( order.OrderId );
            builder.Append( " was added to the cooking queue. " );

            emailAgent.sendEmail(
                OperatorEmail,
                "Pizza order scheduled",
                builder.ToString()
            );
        }


        public void SendOrderDelivered ( Order order )
        {
            var builder = new StringBuilder();
            builder.Append( "Order #" );
            builder.Append( order.OrderId );
            builder.Append( " was succesfully delivered to customer. " );

            emailAgent.sendEmail(
                OperatorEmail,
                "Pizza order delivered",
                builder.ToString()
            );
        }


        public void SendOrderCancelled ( Order order, OrderStatus previousStatus )
        {
            var builder = new StringBuilder();
            builder.Append( "Order #" );
            builder.Append( order.OrderId );
            builder.Append( " was cancelled at " );
            builder.Append( previousStatus );
            builder.Append( " state." );

            if ( order.MayBeRefunded() )
            {
                builder.Append( " The amount of $" );
                builder.Append( order.TotalCost );
                builder.Append( " will be refunded after manual confirmation." );
            }

            String messageBody = builder.ToString();

            emailAgent.sendEmail(
                order.Contact.Email,
                "Pizza order cancelled",
                messageBody
            );

            emailAgent.sendEmail(
                OperatorEmail,
                "Pizza order cancelled: make sure items removed from queues",
                messageBody
            );

            if ( order.MayBeRefunded() )
            {
                emailAgent.sendEmail(
                    AdministratorEmail,
                    "Pizza order cancelled: refunding required",
                    messageBody
                );
            }
        }


        public void SendRefundingConfirmed ( RefundingRequest request )
        {
            var builder = new StringBuilder();
            builder.Append( "The refunding for cancelled order #" );
            builder.Append( request.RelatedOrder.OrderId );
            builder.Append( " was confirmed after manual checking. " );
            builder.Append( "The refunding will happen automatically within a working day. " );

            emailAgent.sendEmail(
                request.RelatedOrder.Contact.Email,
                "Refunding confirmed",
                builder.ToString()
            );
        }


        public void SendRefundingRejected ( RefundingRequest request )
        {
            var builder = new StringBuilder();
            builder.Append( "The refunding for cancelled order #" );
            builder.Append( request.RelatedOrder.OrderId );
            builder.Append( " was rejected after manual checking." );

            emailAgent.sendEmail(
                request.RelatedOrder.Contact.Email,
                "Refunding rejected",
                builder.ToString()
            );
        }


        private IEmailAgent emailAgent;

        private static readonly string OperatorEmail = "operator@pizzario.com";
        private static readonly string AdministratorEmail = "administrator@pizzario.com";
    }
}
