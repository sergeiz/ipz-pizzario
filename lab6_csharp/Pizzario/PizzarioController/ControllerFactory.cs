﻿namespace Pizzario.Controller
{
    public static class ControllerFactory
    {
        public static IMenuController MakeMenuController ()
        {
            return new MenuController();
        }

        public static IShoppingCartController MakeShoppingCartController ()
        {
            return new ShoppingCartController();
        }

        public static IOrderController MakeOrderController ()
        {
            return new OrderController();
        }

        public static IPizzaController MakePizzaController ()
        {
            return new PizzaController();
        }

        public static IRefundingController MakeRefundingController ()
        {
            return new RefundingController();
        }
    }
}
