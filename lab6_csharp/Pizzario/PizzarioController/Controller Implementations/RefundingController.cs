﻿using System;
using System.Collections.Generic;
using System.Linq;

using Pizzario.Controller.Communications;
using Pizzario.Model;
using Pizzario.Orm;
using Pizzario.Infrastructure;

namespace Pizzario.Controller
{
    class RefundingController : BasicController, IRefundingController
    {
        public RefundingController ()
        {
            this.requestRepository = RepositoryFactory.MakeRefundingRequestRepository( GetDBContext()  );

            this.emailComposer = new EmailComposer( InfrastructureFactory.MakeEmailAgent() );
            this.paymentAgent = InfrastructureFactory.MakePaymentAgent();
        }


        public RefundingRequest[] GetAllRefundingRequests ()
        {
            return requestRepository.LoadAll().ToArray();
        }


        public void RefundingConfirmed ( int requestID )
        {
            RefundingRequest request = ResolveRequest( requestID );
            request.Confirmed();
            requestRepository.Commit();

            WireTransferPaymentPlan plan = request.RelatedOrder.PayPlan as WireTransferPaymentPlan;
            paymentAgent.rollbackTransaction( plan.TransactionId );

            emailComposer.SendRefundingConfirmed( request );
        }


        public void RefundingRejected ( int requestID )
        {
            RefundingRequest request = ResolveRequest( requestID );
            request.Rejected();
            requestRepository.Commit();

            emailComposer.SendRefundingRejected( request );
        }


        private RefundingRequest ResolveRequest ( int requestID )
        {
            return ControllerUtils.ResolveObjectById( requestRepository, requestID );
        }


        private IRefundingRequestRepository requestRepository;

        private EmailComposer emailComposer;
        private IPaymentAgent paymentAgent;
    }
}
