﻿using System;
using System.Collections.Generic;
using System.Linq;

using Pizzario.Orm;

namespace Pizzario.Controller
{
    class ShoppingCartController : BasicController, IShoppingCartController
    {
        public ShoppingCartController ()
        {
            this.cartRepository = RepositoryFactory.MakeShoppingCartRepository( GetDBContext() );
            this.menuRepository = RepositoryFactory.MakeMenuRepository( GetDBContext() );
        }


        public int NewCart ()
        {
            Model.ShoppingCart cart = new Model.ShoppingCart();
            cartRepository.Add( cart );
            cartRepository.Commit();
            return cart.ShoppingCartId;
        }


        public void SetCartItem ( int cartID, string pizzaKindName, string pizzaSize, int quantity )
        {
            Model.ShoppingCart cart = ResolveCart( cartID );
            Model.PizzaKind kind = ResolvePizzaKind( pizzaKindName );
            Model.PizzaSize size = ControllerUtils.DecodePizzaSize( pizzaSize );

            int itemIndex = cart.FindItemIndex( kind, size );
            if ( itemIndex == -1 )
                cart.AddItem( kind.MakeOrderItem( size, quantity ) );

            else
                cart.UpdateItem( itemIndex, kind.MakeOrderItem( size, quantity ) );

            cartRepository.Commit();
        }


        public void RemoveCartItem ( int cartID, string pizzaKindName, string pizzaSize )
        {
            Model.ShoppingCart cart = ResolveCart( cartID );
            Model.PizzaKind kind = ResolvePizzaKind( pizzaKindName );
            Model.PizzaSize size = ControllerUtils.DecodePizzaSize( pizzaSize );

            int itemIndex = cart.FindItemIndex( kind, size );
            if ( itemIndex == -1 )
                throw new SystemException( "Item not found in cart" );

            cart.DropItem( itemIndex );

            cartRepository.Commit();
        }


        public void ClearCart ( int cartID )
        {
            Model.ShoppingCart cart = ResolveCart( cartID );
            cart.ClearItems();
            cartRepository.Commit();
        }


        public void LockCart ( int cartID )
        {
            Model.ShoppingCart cart = ResolveCart( cartID );
            cart.Lock();
            cartRepository.Commit();
        }


        private Model.ShoppingCart ResolveCart ( int cartID )
        {
            return ControllerUtils.ResolveObjectById( cartRepository, cartID );
        }
        

        private Model.PizzaKind ResolvePizzaKind ( string kindName )
        {
            Model.PizzaKind pizzaKind = menuRepository.FindByName( kindName );
            if ( pizzaKind == null )
                throw new System.Exception( "Pizza kind '" + kindName + "' is unknown" );
            return pizzaKind;
        }


        private IShoppingCartRepository cartRepository;
        private IMenuRepository menuRepository;
    }
}
