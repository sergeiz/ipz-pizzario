﻿using System;
using System.Collections.Generic;
using System.Linq;
using Pizzario.Model;
using Pizzario.Orm;

namespace Pizzario.Controller
{
    class MenuController : BasicController, IMenuController
    {
        public MenuController ()
        {
            this.menuRepository = RepositoryFactory.MakeMenuRepository( GetDBContext() );
        }


        public PizzaKind[] GetAllKinds ( bool withHidden = false )
        {
            var kinds = menuRepository.LoadAll();
            return ( withHidden ? kinds : WithoutHidden( kinds ) ).ToArray();
        }


        public void CreateNewPizzaKind ( string name, string description, string imageUrl )
        {
            if ( menuRepository.FindByName( name ) != null )
                throw new SystemException( "New name is already in use" );

            Model.PizzaKind pizzaKind = new Model.PizzaKind( name );
            pizzaKind.Description = description;
            pizzaKind.ImageUrl    = imageUrl;

            menuRepository.Add( pizzaKind );
            menuRepository.Commit();
        }


        public void Rename ( string kindName, string newName )
        {
            if ( menuRepository.FindByName( newName ) != null )
                throw new SystemException( "New name is already in use" );

            Model.PizzaKind pizzaKind = ResolvePizzaKind( kindName );
            pizzaKind.Name = newName;
            menuRepository.Commit();
        }


        public void RateRecipe ( string kindName, int rating )
        {
            Model.PizzaKind pizzaKind = ResolvePizzaKind( kindName );
            pizzaKind.Rate( rating );
            menuRepository.Commit();
        }


        public void UpdateDescription ( string kindName, string description )
        {
            Model.PizzaKind pizzaKind = ResolvePizzaKind( kindName );
            pizzaKind.Description = description;
            menuRepository.Commit();
        }


        public void UpdateImageUrl ( string kindName, string url )
        {
            Model.PizzaKind pizzaKind = ResolvePizzaKind( kindName );
            pizzaKind.ImageUrl = url;
            menuRepository.Commit();
        }


        public void HidePizzaFromCustomers ( string kindName )
        {
            Model.PizzaKind pizzaKind = ResolvePizzaKind( kindName );
            pizzaKind.Hidden = true;
            menuRepository.Commit();
        }


        public void EnableShowingPizzaToCustomers ( string kindName )
        {
            Model.PizzaKind pizzaKind = ResolvePizzaKind( kindName );
            pizzaKind.Hidden = false;
            menuRepository.Commit();
        }
        

        public void UpdatePrice ( string kindName, string size, decimal price )
        {
            Model.PizzaKind pizzaKind = ResolvePizzaKind( kindName );
            pizzaKind.UpdatePrice( ControllerUtils.DecodePizzaSize( size ), price );
            menuRepository.Commit();
        }


        public void SetIngredient ( string kindName, string ingredient, int weight )
        {
            Model.PizzaKind pizzaKind = ResolvePizzaKind( kindName );
            if ( pizzaKind.Recipe.Ingredients.Contains( ingredient ) )
                pizzaKind.Recipe.UpdateIngredient( ingredient, weight );
            else
                pizzaKind.Recipe.AddIngredient( ingredient, weight );

            menuRepository.Commit();
        }


        public void RemoveIngredient ( string kindName, string ingredient )
        {
            Model.PizzaKind pizzaKind = ResolvePizzaKind( kindName );
            pizzaKind.Recipe.RemoveIngredient( ingredient );
            menuRepository.Commit();
        }


        private IQueryable< Model.PizzaKind > WithoutHidden ( IQueryable< Model.PizzaKind > kinds )
        {
            return kinds.Where( k => ! k.Hidden );
        }


        private IDictionary< string, decimal > DescribePizzaPrices ( Model.PizzaKind kind )
        {
            var pricesPerSize = new Dictionary<string, decimal>();

            foreach ( Model.PizzaSize size in Enum.GetValues( typeof( Model.PizzaSize ) ) )
                pricesPerSize.Add( size.ToString(), kind.GetCurrentPrice( size ) );

            return pricesPerSize;
        }


        private IDictionary< string, double > DescribeIngredients ( Model.PizzaRecipe recipe )
        {
            var ingredientsWeight = new Dictionary< string, double >();

            foreach ( string ingredient in recipe.Ingredients )
                ingredientsWeight.Add( ingredient, recipe.GetIngredientWeight( ingredient ) );

            return ingredientsWeight;
        }


        private Model.PizzaKind ResolvePizzaKind ( string kindName )
        {
            Model.PizzaKind pizzaKind = menuRepository.FindByName( kindName );
            if ( pizzaKind == null )
                throw new System.Exception( "Pizza kind '" + kindName + "' is unknown" );
            return pizzaKind;
        }


        private IMenuRepository menuRepository;
    }
}
