﻿using System;

using Pizzario.Orm;

namespace Pizzario.Controller
{
    class BasicController
    {
        protected PizzarioDbFacade GetDBContext ()
        {
            if ( dbContext == null )
                dbContext = new PizzarioDbFacade();

            return dbContext;
        }


        public void Dispose ()
        {
            Dispose( true );
            GC.SuppressFinalize( this );
        }


        protected virtual void Dispose ( bool disposing )
        {
            if ( disposing && dbContext != null )
                dbContext.Dispose();
        }

        private PizzarioDbFacade dbContext;
    }
}
