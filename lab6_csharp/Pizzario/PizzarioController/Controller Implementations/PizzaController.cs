﻿using System;
using System.Linq;

using Pizzario.Model;
using Pizzario.Orm;


namespace Pizzario.Controller
{
    class PizzaController : BasicController, IPizzaController
    {
        public PizzaController ()
        {
            this.pizzaRepository = RepositoryFactory.MakePizzaRepository( GetDBContext() );
            this.orderRepository = RepositoryFactory.MakeOrderRepository( GetDBContext() );
        }


        public Pizza[] GetAllPizzas ()
        {
            return pizzaRepository.LoadAll().ToArray();
        }


        public Pizza[] SearchPizzasOfOrder ( int orderID )
        {
            return pizzaRepository.SearchPizzasByOrderID( orderID ).ToArray();
        }


        public void CookingStarted ( int pizzaID )
        {
            Pizza pizza = ResolvePizza( pizzaID );
            pizza.OnCookingStarted();
            pizzaRepository.Commit();

            Order order = ResolveOrder( pizza.RelatedOrder.OrderId );
            order.OnStartedCookingOneOfPizzas();
            orderRepository.Commit();
        }


        public void CookingFinished ( int pizzaID )
        {
            Pizza pizza = ResolvePizza( pizzaID );
            pizza.OnCookingFinished();
            pizzaRepository.Commit();

            Order order = ResolveOrder( pizza.RelatedOrder.OrderId );
            order.OnFinishedCookingOneOfPizzas();
            orderRepository.Commit();
        }

        
        private Pizza ResolvePizza ( int pizzaID )
        {
            return ControllerUtils.ResolveObjectById( pizzaRepository, pizzaID );
        }


        private Order ResolveOrder ( int orderID )
        {
            return ControllerUtils.ResolveObjectById( orderRepository, orderID );
        }

        private IPizzaRepository pizzaRepository;
        private IOrderRepository orderRepository;
    }
}
