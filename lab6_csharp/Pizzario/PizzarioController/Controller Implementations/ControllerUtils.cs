﻿using System;

namespace Pizzario.Controller
{
    static class ControllerUtils
    {
        public static Model.PizzaSize DecodePizzaSize ( string sizeAsString )
        {
            Model.PizzaSize result;
            if ( ! Enum.TryParse< Model.PizzaSize >( sizeAsString, out result ) )
                throw new SystemException( "Unrecognized pizza size" );
            return result;
        }

        public static Model.OrderStatus DecodeOrderStatus ( string statusAsString )
        {
            Model.OrderStatus result;
            if ( ! Enum.TryParse< Model.OrderStatus >( statusAsString, out result ) )
                throw new SystemException( "Unrecognized order status" );
            return result;
        }

        public static T ResolveObjectById< T >  ( Orm.IRepository<T> repository, int objectID )
            where T : class
        {
            T result = repository.Load( objectID );
            if ( result == null )
                throw new SystemException( typeof( T ).Name + " #" + objectID + " is unresolved" );
            return result;
        }
    }
}
