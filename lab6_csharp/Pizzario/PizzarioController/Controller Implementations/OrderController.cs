﻿using System;
using System.Linq;

using Pizzario.Controller.Communications;
using Pizzario.Orm;
using Pizzario.Model;
using Pizzario.Infrastructure;

namespace Pizzario.Controller
{
    class OrderController : BasicController, IOrderController
    {
        public OrderController ()
        {
            this.orderRepository     = RepositoryFactory.MakeOrderRepository( GetDBContext() );
            this.cartRepository      = RepositoryFactory.MakeShoppingCartRepository( GetDBContext() );
            this.pizzaRepository     = RepositoryFactory.MakePizzaRepository( GetDBContext() );
            this.refundingRepository = RepositoryFactory.MakeRefundingRequestRepository( GetDBContext() );

            this.emailComposer = new EmailComposer( InfrastructureFactory.MakeEmailAgent() );
            this.smsComposer   = new SmsComposer( InfrastructureFactory.MakeSmsAgent() );
            this.paymentAgent  = InfrastructureFactory.MakePaymentAgent();
        }


        public Order[] GetAllOrders ()
        {
            return orderRepository.LoadAll().ToArray();
        }


        public int NewOrder ( int cartID, 
                              string email, 
                              string address, 
                              string phone, 
                              string comment )
        {
            ShoppingCart cart = ResolveCart( cartID );

            CustomerContact contact = new CustomerContact();
            contact.Email   = email;
            contact.Address = address;
            contact.Phone   = phone;
            contact.Comment = comment;

            Order order = new Order( cart, contact );
            orderRepository.Add( order );
            orderRepository.Commit();

            smsComposer.SendOrderRegistered( order );
            emailComposer.SendOrderRegistered( order );

            return order.OrderId;
        }


        public void DefineCashPaymentPlan ( int orderID )
        {
            Order order = ResolveOrder( orderID );
            order.DefinePaymentPlan( new CashPaymentPlan() );
            orderRepository.Commit();

            ScheduleOrderCooking( order );
        }


        public void DefineWireTransferPaymentPlan ( int orderID,
                                                    string cardCode, 
                                                    string cardHolder )
        {
            Order order = ResolveOrder( orderID );

            int transactionId;
            if ( ! paymentAgent.makePaymentTransaction( order.TotalCost, cardCode, cardHolder, out transactionId ) )
                throw new System.Exception( "Wireless payment failed" );

            WireTransferPaymentPlan plan = new WireTransferPaymentPlan( cardCode, cardHolder );
            plan.SetPaymentTransactionId( transactionId );

            order.DefinePaymentPlan( plan );
            orderRepository.Commit();

            ScheduleOrderCooking( order );
        }


        private void ScheduleOrderCooking ( Order order )
        {
            foreach ( OrderItem item in order.Items )
                for ( int i = 0; i < item.Quantity; i++ )
                    pizzaRepository.Add( new Pizza( order, item.Kind, item.Size ) );
            pizzaRepository.Commit();

            emailComposer.SendOrderScheduled( order );
        }


        public void ConfirmPayment ( int orderID )
        {
            Order order = ResolveOrder( orderID );
            order.PayPlan.MarkPayed();
            orderRepository.Commit();
        }


        public void ConfirmDelivery ( int orderID )
        {
            Order order = ResolveOrder( orderID );
            order.OnDelivered();
            orderRepository.Commit();

            emailComposer.SendOrderDelivered( order );
        }


        public void DeliveryStarted ( int orderID )
        {
            Order order = ResolveOrder( orderID );
            order.OnStartedDelivery();
            orderRepository.Commit();
        }


        public int CancelOrder ( int orderID )
        {
            Order order = ResolveOrder( orderID );
            OrderStatus previousStatus = order.Status;

            order.Cancel();
            orderRepository.Commit();

            var pizzasOfOrder = pizzaRepository.SearchPizzasByOrderID( orderID );
            foreach ( Pizza p in pizzasOfOrder )
                p.OnCookingCancelled();
            pizzaRepository.Commit();

            int refundingID = -1;
            if ( order.MayBeRefunded() )
            {
                RefundingRequest request = new RefundingRequest( order );
                refundingRepository.Add( request );
                refundingRepository.Commit();
                refundingID = request.RequestId;
            }

            smsComposer.SendOrderCancelled( order );
            emailComposer.SendOrderCancelled( order, previousStatus );

            return refundingID;
        }


        private ShoppingCart ResolveCart ( int cartID )
        {
            return ControllerUtils.ResolveObjectById( cartRepository, cartID );
        }


        private Order ResolveOrder ( int orderID )
        {
            return ControllerUtils.ResolveObjectById( orderRepository, orderID );
        }


        private IOrderRepository orderRepository;
        private IShoppingCartRepository cartRepository;
        private IPizzaRepository pizzaRepository;
        private IRefundingRequestRepository refundingRepository;

        private EmailComposer emailComposer;
        private SmsComposer smsComposer;
        private IPaymentAgent paymentAgent;
    }
}
