﻿using System;

using Pizzario.Model;

namespace Pizzario.Controller
{
    public interface IRefundingController : IDisposable
    {
        RefundingRequest[] GetAllRefundingRequests ();

        void RefundingConfirmed ( int requestID );

        void RefundingRejected ( int requestID );
    }
}
