﻿using System;


namespace Pizzario.Controller
{
    public interface IShoppingCartController : IDisposable
    {
        int NewCart ();

        void SetCartItem ( int cartID, string pizzaKindName, string pizzaSize, int quantity );

        void RemoveCartItem ( int cartID, string pizzaKindName, string pizzaSize );

        void ClearCart ( int cartID );

        void LockCart ( int cartID );
    }
}
