﻿using System;

using Pizzario.Model;


namespace Pizzario.Controller
{
    public interface IMenuController : IDisposable
    {
        PizzaKind[] GetAllKinds ( bool withHidden = false );

        void CreateNewPizzaKind ( string kindName, string description, string imageUrl );

        void Rename ( string kindName, string newName );

        void UpdateDescription ( string kindName, string description );

        void UpdateImageUrl ( string kindName, string url );

        void SetIngredient ( string kindName, string ingredient, int weight );

        void RemoveIngredient ( string kindName, string ingredient );

        void HidePizzaFromCustomers ( string kindName );

        void EnableShowingPizzaToCustomers ( string kindName );

        void UpdatePrice ( string kindName, string size, decimal price );

        void RateRecipe ( string kindName, int rating );
    }
}
