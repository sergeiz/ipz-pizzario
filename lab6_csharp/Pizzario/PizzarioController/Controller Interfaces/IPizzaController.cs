﻿using System;

using Pizzario.Model;


namespace Pizzario.Controller
{
    public interface IPizzaController : IDisposable
    {
        Pizza[] GetAllPizzas ();

        Pizza[] SearchPizzasOfOrder ( int orderID );

        void CookingStarted ( int pizzaID );

        void CookingFinished ( int pizzaID );
    }
}
