﻿using System;

using Pizzario.Model;


namespace Pizzario.Controller
{
    public interface IOrderController : IDisposable
    {
        Order[] GetAllOrders ();

        int NewOrder ( int cartID,
                       string email,
                       string address,
                       string phone,
                       string comment );

        void DefineCashPaymentPlan ( int orderID );

        void DefineWireTransferPaymentPlan ( int orderID, 
                                             string cardCode, 
                                             string cardHolder );

        void ConfirmPayment ( int orderID );

        void DeliveryStarted ( int orderID );

        void ConfirmDelivery ( int orderID );

        int CancelOrder ( int orderID );
    }
}
