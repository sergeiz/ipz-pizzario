﻿using System;
using System.IO;

using Pizzario.Model;


namespace Pizzario.TestApp
{
    class ReportGenerator
    {
        public ReportGenerator ( TextWriter output )
        {
            this.output = output;
        }


        public void DisplayMenu ( PizzaKind[] menu )
        {
            output.WriteLine( " ==== Menu ==== " );

            foreach ( PizzaKind kind in menu )
            {
                output.WriteLine();
                output.WriteLine( "Pizza \"" + kind.Name + "\":" );
                output.WriteLine( "\tDescription: " + kind.Description );
                output.WriteLine( "\tImage URL: " + kind.ImageUrl );

                ShowPizzaRatings( kind );
                ShowPizzaIngredients( kind );
                ShowPizzaPrices( kind );
            }

            output.WriteLine();
            output.WriteLine( " ==== End Menu ==== " );
        }


        private void ShowPizzaRatings ( PizzaKind kind )
        {
            output.Write( "\tRating: " );
            output.Write( kind.AverageRating );
            output.WriteLine( " ( " + kind.Votes + " votes)" );
        }


        private void ShowPizzaIngredients ( PizzaKind kind )
        {
            PizzaRecipe recipe = kind.Recipe;

            bool first = true;

            output.Write( "\tIngredients: " );

            foreach ( string ingredient in recipe.Ingredients )
            {
                if ( !first )
                    output.Write( ", " );
                else
                    first = false;

                output.Write( recipe.GetIngredientWeight( ingredient ) );
                output.Write( "g " );
                output.Write( ingredient );
            }

            output.WriteLine();
        }

        private void ShowPizzaPrices ( PizzaKind kind )
        {
            output.Write( "\tPrices: " );
            output.Write( kind.GetCurrentPrice( PizzaSize.Small ) );
            output.Write( " small, " );
            output.Write( kind.GetCurrentPrice( PizzaSize.Medium ) );
            output.Write( " medium, " );
            output.Write( kind.GetCurrentPrice( PizzaSize.Large ) );
            output.WriteLine( " large" );
        }


        public void DisplayOrders ( Order[] orders )
        {
            output.WriteLine( " ==== Orders ==== " );

            foreach ( Order o in orders )
            {
                output.WriteLine();
                ShowOrder( o );
            }

            output.WriteLine();
            output.WriteLine( " ==== End Orders ==== " );
        }


        private void ShowOrder ( Order order )
        {
            output.WriteLine();
            output.Write( "Order #" );
            output.Write( order.OrderId );
            output.WriteLine( ":" );

            output.Write( "\tStatus: " );
            output.WriteLine( Enum.GetNames( typeof( OrderStatus ) )[ ( int ) order.Status ] );

            output.Write( "\tTotal cost: " );
            output.WriteLine( order.TotalCost );

            output.Write( "\tPayment plan: " );
            output.Write( order.PayPlan.ToString() );
            output.Write( ", " );
            output.WriteLine( order.PayPlan.Payed ? "payed" : "not payed" );

            output.Write( "\tDiscount: " );
            output.Write( order.Discount * 100.0 );
            output.WriteLine( '%' );

            ShowDeliveryContact( order.Contact );
            ShowOrderedItems( order );
        }


        private void ShowDeliveryContact ( CustomerContact contact )
        {
            output.Write( "\tEmail: " );
            output.WriteLine( contact.Email );

            output.Write( "\tAddress: " );
            output.WriteLine( contact.Address );

            output.Write( "\tPhone: " );
            output.WriteLine( contact.Phone );

            output.Write( "\tComment: " );
            output.WriteLine( contact.Comment );
        }


        private void ShowOrderedItems ( Order order )
        {
            foreach ( OrderItem item in order.Items )
            {
                output.Write( "\t  " );
                output.Write( item.Kind.Name );
                output.Write( ' ' );
                output.Write( Enum.GetNames( typeof( PizzaSize ) )[ ( int ) item.Size ] );
                output.Write( ", price " );
                output.Write( item.FixedPrice );
                output.Write( ", quantity " );
                output.Write( item.Quantity );
                output.Write( ", cost " );
                output.WriteLine( item.Cost );
            }
        }


        public void DisplayPizzas ( Pizza[] pizzas )
        {
            output.WriteLine( " ==== Pizzas ==== " );
            output.WriteLine();

            foreach ( Pizza p in pizzas )
            {
                output.Write( "#" );
                output.Write( p.PizzaId );
                output.Write( ", Order #" );
                output.Write( p.RelatedOrder.OrderId );
                output.Write( ", " );
                output.Write( p.Kind.Name );
                output.Write( ' ' );
                output.Write( Enum.GetNames( typeof( PizzaSize ) )[ ( int ) p.Size ] );
                output.Write( ", cooking status: " );
                output.WriteLine( Enum.GetNames( typeof( CookingStatus ) )[ ( int ) p.Status ] );
            }

            output.WriteLine();
            output.WriteLine( " ==== End Pizzas ==== " );
        }


        public void DisplayRefundingRequests ( RefundingRequest[] requests )
        {
            output.WriteLine( " ==== Refunding Requests ==== " );
            output.WriteLine();

            foreach ( RefundingRequest r in requests )
            {
                output.Write( "#" );
                output.Write( r.RequestId );
                output.Write( ", Order #" );
                output.Write( r.RelatedOrder.OrderId );
                output.Write( ", Status: " );
                output.WriteLine( Enum.GetNames( typeof( RefundingStatus ) )[ ( int ) r.Status ] );
            }

            output.WriteLine();
            output.WriteLine( " ==== End Refunding Requests ==== " );
        }



        private TextWriter output;
    }
}
