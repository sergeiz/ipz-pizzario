﻿using System;
using System.IO;

using Pizzario.Controller;
using Pizzario.Model;

namespace Pizzario.TestApp
{
    public class Program
    {
        public static void Main ( string[] args )
        {
            FillTestMenu();
            CreateAndServeTestOrder1();
            CreateAndCancelTestOrder2();

            DisplayMenu();
            DisplayOrders();
            DisplayPizzas();
            DisplayRefundingRequests();
        }


        private static void FillTestMenu ()
        {
            using ( var menuController = ControllerFactory.MakeMenuController() )
            {
                AddCarbonaraToMenu( menuController );
                AddMilanoToMenu( menuController );
            }
        }


        private static void AddCarbonaraToMenu ( IMenuController menuController )
        {
            menuController.CreateNewPizzaKind( 
                "Carbonara",
                "One of the most popular pizza recipes in the world",
                "http://pizzario.com.ua/images/carbonara.jpg"
            );

            menuController.SetIngredient( "Carbonara", "Cheese", 100 );
            menuController.SetIngredient( "Carbonara", "Olive Oil", 30 );

            menuController.UpdatePrice( "Carbonara", "Small",  3.00M );
            menuController.UpdatePrice( "Carbonara", "Medium", 5.00M );
            menuController.UpdatePrice( "Carbonara", "Large",  7.00M );

            menuController.EnableShowingPizzaToCustomers( "Carbonara" );
        }


        private static void AddMilanoToMenu ( IMenuController menuController )
        {
            menuController.CreateNewPizzaKind(
               "Milano",
               "Classic recipe with ham, mushrooms and tomatoes",
               "http://pizzario.com.ua/images/milano.jpg"
            );

            menuController.SetIngredient( "Milano", "Ham", 100 );
            menuController.SetIngredient( "Milano", "Mushrooms", 50 );
            menuController.SetIngredient( "Milano", "Tomatoes", 50 );

            menuController.UpdatePrice( "Milano", "Small",  2.50M );
            menuController.UpdatePrice( "Milano", "Medium", 4.15M );
            menuController.UpdatePrice( "Milano", "Large",  5.80M );

            menuController.EnableShowingPizzaToCustomers( "Milano" );
        }


        private static void CreateAndServeTestOrder1 ()
        {
            int cartID = CreateTestCart1();
            int orderID = RegisterOrder( cartID );
            DefineCashPaymentPlan( orderID );
            ServeOrder( orderID );
        }


        private static void CreateAndCancelTestOrder2 ()
        {
            int cartID = CreateTestCart2();
            int orderID = RegisterOrder( cartID );
            DefineWirelessTransferPaymentPlan( orderID );
            int refundingID = CancelOrder( orderID );
            ConfirmRefunding( refundingID );
        }



        private static int CreateTestCart1 ()
        {
            using ( var cartController = ControllerFactory.MakeShoppingCartController() )
            {
                int newCartId = cartController.NewCart();
                cartController.SetCartItem( newCartId, "Carbonara", "Medium", 2 );
                cartController.SetCartItem( newCartId, "Milano", "Large", 1 );
                cartController.LockCart( newCartId );
                return newCartId;
            }
        }



        private static int CreateTestCart2 ()
        {
            using ( var cartController = ControllerFactory.MakeShoppingCartController() )
            {
                int newCartId = cartController.NewCart();
                cartController.SetCartItem( newCartId, "Carbonara", "Small", 1 );
                cartController.LockCart( newCartId );
                return newCartId;
            }
        }


        private static int RegisterOrder ( int cartID )
        {
            using ( var orderController = ControllerFactory.MakeOrderController() )
            {
                return orderController.NewOrder( 
                            cartID, 
                            "ivanov@nure.ua", 
                            "Lenin av. 14, room 320", 
                            "702-13-26", 
                            "APVT Department" 
                      );
            }
        }


        private static void DefineCashPaymentPlan ( int orderID )
        {
            using ( var orderController = ControllerFactory.MakeOrderController() )
            {
                orderController.DefineCashPaymentPlan( orderID );
            }
        }


        private static void DefineWirelessTransferPaymentPlan ( int orderID )
        {
            using ( var orderController = ControllerFactory.MakeOrderController() )
            {
                orderController.DefineWireTransferPaymentPlan( 
                    orderID, 
                    "1234 5678 0909 9090", 
                    "Ivan Ivanv" 
                );
            }
        }


        private static void ServeOrder ( int orderID )
        {
            using ( var pizzaController = ControllerFactory.MakePizzaController() )
            {
                foreach ( Pizza p in pizzaController.SearchPizzasOfOrder( orderID ) )
                    pizzaController.CookingStarted( p.PizzaId );

                foreach ( Pizza p in pizzaController.SearchPizzasOfOrder( orderID ) )
                    pizzaController.CookingFinished( p.PizzaId );
            }

            using ( var orderController = ControllerFactory.MakeOrderController() )
            {
                orderController.DeliveryStarted( orderID );

                orderController.ConfirmPayment( orderID );

                orderController.ConfirmDelivery( orderID );
            }
        }


        private static int CancelOrder ( int orderID )
        {
            using ( var orderController = ControllerFactory.MakeOrderController() )
            {
                return orderController.CancelOrder( orderID );
            }
        }


        private static void ConfirmRefunding ( int refundingID )
        {
            using ( var refundingController = ControllerFactory.MakeRefundingController() )
            {
                refundingController.RefundingConfirmed( refundingID );
            }
        }


        private static void DisplayMenu ()
        {
            using ( var menuController = ControllerFactory.MakeMenuController() )
            {
                ReportGenerator generator = new ReportGenerator( Console.Out );
                generator.DisplayMenu( menuController.GetAllKinds() );
            }
        }


        private static void DisplayOrders ()
        {
            using ( var orderController = ControllerFactory.MakeOrderController() )
            {
                ReportGenerator generator = new ReportGenerator( Console.Out );
                generator.DisplayOrders( orderController.GetAllOrders() );
            }
        }


        private static void DisplayPizzas ()
        {
            using ( var pizzaController = ControllerFactory.MakePizzaController() )
            {
                ReportGenerator generator = new ReportGenerator( Console.Out );
                generator.DisplayPizzas( pizzaController.GetAllPizzas() );
            }
        }


        private static void DisplayRefundingRequests ()
        {
            using ( var refundingController = ControllerFactory.MakeRefundingController() )
            {
                ReportGenerator generator = new ReportGenerator( Console.Out );
                generator.DisplayRefundingRequests( refundingController.GetAllRefundingRequests() );
            }
        }

    }
}
