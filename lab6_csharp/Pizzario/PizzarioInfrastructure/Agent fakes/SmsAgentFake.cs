﻿using System;

namespace Pizzario.Infrastructure
{
    class SmsAgentFake : ISmsAgent
    {
        public void sendSMS ( string phoneNumber, string body )
        {
            Console.Write( "==== SMS to " );
            Console.Write( phoneNumber );
            Console.WriteLine( "==== ");
            Console.WriteLine( body );
            Console.WriteLine( "==== End SMS ==== " );
            Console.WriteLine();
        }
    }
}
