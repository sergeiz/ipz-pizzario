﻿using System;

namespace Pizzario.Infrastructure
{
    public interface IEmailAgent
    {
        void sendEmail ( string targetAddress, string subject, string body );
    }
}
