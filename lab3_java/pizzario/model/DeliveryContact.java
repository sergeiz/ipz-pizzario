package pizzario.model;

public class DeliveryContact {
	
	public DeliveryContact( String address , String phone , String comment )
	{
		this.address = address;
		this.phone = phone;
		this.comment = comment;
	}
	
	public String getAddress()
	{
		return address;
	}
	
	public void setAddress( String address )
	{
		this.address = address;
	}
	
	public String getPhone()
	{
		return phone;
	}
	
	public void setPhone( String phone )
	{
		this.phone = phone;
	}
	
	public String getComment()
	{
		return comment;
	}
	
	public void setComment( String comment )
	{
		this.comment = comment;
	}
	
	private String address;
	private String phone;
	private String comment;
}
