package pizzario.model;

import java.math.BigDecimal;

public class PizzaKind {
	
	public PizzaKind( String name )
	{
		this.recipe = new PizzaRecipe();
		
		this.name = name;
		this.description = "";
		this.imageURL = "";
		this.votes = 0;
		this.totalRating = 0;
		this.hidden = false;
	}

	public String getName()
	{
		return name;
	}
	
	public void rename( String name )
	{
		this.name = name;
	}
	
	public String getDescription()
	{
		return description;
	}
	
	public void updateDescription( String description )
	{
		this.description = description;
	}
	
	public String getImageUrl()
	{
		return imageURL;
	}
	
	public void setImageURL( String imageURL )
	{
		this.imageURL = imageURL;
	}
	
	public int getRatingVotesCount()
	{
		return votes;
	}
	
	public int getTotalRating()
	{
		return totalRating;
	}
	
	public boolean isHidden()
	{
		return hidden;
	}
	
	public void setHidden( boolean hidden )
	{
		this.hidden = hidden;
	}
	
	public PizzaRecipe getRecipe()
	{
		return recipe;
	}
	
	public double getAverageRating()
	{
		if ( votes != 0 )
			return ( double ) ( totalRating ) / votes;
		else
			return 0.0;
	}
	
	public void rate( int rating )
	{
		if ( rating <= 0 || rating > 5 )
			throw new RuntimeException( "PizzaKind.rate: expecting rating within [1;5] range" );
		else
		{
			this.totalRating += rating;
			this.votes ++;
		}
	}
	
	public void setupRatingStats( int votes, int totalRating )
	{
		this.votes = votes;
		this.totalRating = totalRating;
	}
	
	public BigDecimal getCurrentPrice( PizzaSize size )
	{
		return priceBySize[ size.ordinal() ];
	}
	
	public void updatePrice( PizzaSize size, BigDecimal price )
	{
		priceBySize[ size.ordinal() ] = price;
	}
	
	private String name;
	private String description;
	private String imageURL;
	private int votes;
	private int totalRating;
	private boolean hidden;
	private PizzaRecipe recipe;
	private BigDecimal[] priceBySize = new BigDecimal[ PizzaSize.values().length ];
}
