package pizzario.model;

public class Pizza {
	
	public Pizza ( PizzaKind kind, PizzaSize size, Order parentOrder )
	{
		this.kind = kind;
		this.size = size;
		this.parentOrder = parentOrder;
		this.status = CookingStatus.NotStarted;
	}
	
	public PizzaKind getPizzaKind()
	{
		return kind;
	}
	
	public PizzaSize getPizzaSize()
	{
		return size;
	}
	
	public Order getParentOrder()
	{
		return parentOrder;
	}
	
	public CookingStatus getCookingStatus()
	{
		return status;
	}
	
	public void onCookingStarted()
	{
		 if ( status == CookingStatus.NotStarted )
		 {
			 status = CookingStatus.Started;
			 parentOrder.onPizzaCookingStarted();
		 }
		 else
			 throw new RuntimeException( "Pizza.onCookingStarted: may only start cooking if not started yet" );
	}
	
	public void onCookingCancelled ()
	{
		if ( status == CookingStatus.Started || status == CookingStatus.NotStarted )
			status = CookingStatus.Cancelled;

		else
			throw new RuntimeException( "Pizza.onCookingCancelled: may only abort unfinished pizza" );
	}
	
	public void onCookingFinished ()
	{
		if ( status == CookingStatus.Started )
		{
			status = CookingStatus.Finished;
			parentOrder.onPizzaReady();
		}
		else
			throw new RuntimeException( "Pizza.onCookingFinished: may only finish cooking if started" );
	}
	
	private PizzaKind kind;
	private PizzaSize size;
	private Order parentOrder;
	private CookingStatus status;
}
