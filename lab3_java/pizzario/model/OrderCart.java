package pizzario.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class OrderCart {
	
	public OrderCart()
	{
		this.items = new ArrayList< OrderItem >();
	}
	
	 public int getItemsCount()
	 {
		 return items.size();
	 }
	 
	 public OrderItem getItem( int index )
	 {
		 return items.get( index );
	 }
	 
	 public void addItem( OrderItem item )
	 {
		 items.add( item );
	 }
	 
	 public void updateItem( int index, OrderItem item )
	 {
		 items.set( index , item );
	 }
	 
	 public void dropItem ( int index )
	 {
		 items.remove( index );
	 }
	 
	 public void clearItems ()
	 {
		 items.clear();
	 }
	 
	 public BigDecimal getCost ()
	 {
		 BigDecimal totalCost = new BigDecimal( 0 );
		 for( OrderItem i : items )
			 totalCost.add(  i.getCost() );
		 return totalCost;
	 }
	
	private List< OrderItem > items;
}
