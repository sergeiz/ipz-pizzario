package pizzario.model;

public abstract class PaymentPlan {
	
	public abstract PaymentPlan clone();
	public abstract boolean expectPrepayment();
	public abstract boolean wasPayed();
}
