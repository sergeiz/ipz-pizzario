package pizzario.model;

import java.util.Map;
import java.util.TreeMap;

public class Menu {
	
	public Iterable< String > pizzaKindNames()
	{
		return pizzaKindsByName.keySet();
	}
	
	public Iterable< PizzaKind > pizzaKinds()
	{
		return pizzaKindsByName.values();
	}
	
	public int getPizzaKindsCount()
	{
		return pizzaKindsByName.size();
	}
	 
	public PizzaKind findPizzaKind( String name )
	{
		return pizzaKindsByName.get( name );
	}
	
	public void addPizzaKind( PizzaKind kind )
	{
		if ( pizzaKindsByName.containsValue( kind ) )
			throw new RuntimeException( "Menu.addPizzaKind: duplicate kind" );
		
		pizzaKindsByName.put( kind.getName() , kind );
	}
	
	 public void deletePizzaKind ( String name )
	 {
		 if ( pizzaKindsByName.containsKey( name ) )
			 pizzaKindsByName.remove( name );
	 }
	 
	 public void renamePizzaKind( String name, String newName )
	 {
		 PizzaKind kind = pizzaKindsByName.get( name );
		 if ( kind == null )
			 throw new RuntimeException( "Menu.renamePizzaKind: unregistered kind" );
		 
		 if ( name == newName )
			 return;
		 
		 pizzaKindsByName.remove( name );
		 kind.rename( newName );
		 pizzaKindsByName.put( newName , kind );			
	 }
	 
	 public OrderItem makeOrderItem ( PizzaKind kind, PizzaSize size, int quantity )
	 {
		 return new OrderItem( kind, size, kind.getCurrentPrice( size ), quantity );
	 }
	
	 private Map< String , PizzaKind > pizzaKindsByName = new TreeMap< String , PizzaKind >();
	
}
