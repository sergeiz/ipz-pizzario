package pizzario.model;

import java.util.ArrayList;
import java.util.List;

public class PizzarioNetwork {
	
	public Menu getMenu ()
	{
		return menu;
	}
	
	public int getOrdersCount ()
	{
		return orders.size();
	}
	
	public Iterable< Order > getOrders()
	{
		return orders;
	}
	
	public Order newOrder ( DeliveryContact contact, PaymentPlan payPlan )
	{
		Order o = new Order( contact, payPlan );
		orders.add( o );
		return o;
	}
	
	public void pushOrder ( Order order )
	{
		orders.add( order );
	}
	
	private Menu menu = new Menu();
	private List< Order > orders = new ArrayList< Order >();
}
