package pizzario.model.test;

import java.math.BigDecimal;
import java.math.MathContext;

import pizzario.model.*;

public class TestModelGenerator {
	
	public PizzarioNetwork generate ()
	{
		PizzarioNetwork network = new PizzarioNetwork();
		fillPizzaMenu( network.getMenu() );
		fillOrders( network, network.getMenu() );
		return network;
	}
	
	private void fillPizzaMenu ( Menu m )
	{
		////

		PizzaKind carbonara = new PizzaKind( "Carbonara" );
		m.addPizzaKind( carbonara );

		carbonara.updateDescription( "One of the most popular pizza recipes in the world" );
		carbonara.setupRatingStats( 170, 812 );
		carbonara.setImageURL( "http://pizzario.com.ua/images/carbonara.jpg" );

		carbonara.getRecipe().addIngredient( "Cheese", 100 );
		carbonara.getRecipe().addIngredient( "Olive Oil", 30 );

		carbonara.updatePrice( PizzaSize.SMALL, new BigDecimal( 3.00, moneyContext ) );
		carbonara.updatePrice( PizzaSize.MEDIUM, new BigDecimal( 5.00, moneyContext ) );
		carbonara.updatePrice( PizzaSize.LARGE, new BigDecimal( 7.00, moneyContext ) );

		////

		PizzaKind milano = new PizzaKind( "Milano" );
		m.addPizzaKind( milano );

		milano.updateDescription( "Classic recipe with ham, mushrooms and tomatoes" );
		milano.setupRatingStats( 102, 415 );
		milano.setImageURL( "http://pizzario.com.ua/images/milano.jpg" );

		milano.getRecipe().addIngredient( "Ham", 100 );
		milano.getRecipe().addIngredient( "Mushrooms", 50 );
		milano.getRecipe().addIngredient( "Tomatoes", 50 );

		milano.updatePrice( PizzaSize.SMALL, new BigDecimal( 2.50, moneyContext ) );
		milano.updatePrice( PizzaSize.MEDIUM, new BigDecimal( 4.15, moneyContext ) );
		milano.updatePrice( PizzaSize.LARGE, new BigDecimal( 5.80, moneyContext ) );
	}
	
	private void fillOrders ( PizzarioNetwork network, Menu m )
	{
		PizzaKind carbonara = m.findPizzaKind( "Carbonara" );
		PizzaKind milano = m.findPizzaKind( "Milano" );

		Order order1 = network.newOrder(
				new DeliveryContact( "Lenin av. 14, room 320" , "702-13-26" , "APVT Department" )
			  , new CashPaymentPlan()
		);

		order1.getOrderCart().addItem( m.makeOrderItem( carbonara, PizzaSize.MEDIUM, 2 ) );
		order1.getOrderCart().addItem( m.makeOrderItem( milano, PizzaSize.LARGE, 1 ) );
		order1.setDiscount( 0.025 );
		order1.process();


		Order order2 = network.newOrder(
				new DeliveryContact( "23rd August Str 2, ap. 2" , "333-33-33" , "Knock 3 times" )
			  , new CashPaymentPlan()
		);

		order2.getOrderCart().addItem( m.makeOrderItem( carbonara, PizzaSize.SMALL, 1 ) );
	}
	
	private static MathContext moneyContext = new MathContext( 3 );

}
