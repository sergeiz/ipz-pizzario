package pizzario.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Order {
	
	public Order ( DeliveryContact contact, PaymentPlan payPlan )
	{
		this.contact = contact;
		this.payPlan = payPlan;
		this.cart = new OrderCart();
		this.status = OrderStatus.New;
		this.discount = 0.0;
		this.pizzas = new ArrayList< Pizza >();
	}
	
	public OrderCart getOrderCart()
	{
		return cart;
	}
	
	public PaymentPlan getPaymentPlan()
	{
		return payPlan;
	}
	
	public DeliveryContact getDeliveryContact()
	{
		return contact;
	}
	
	public void setDeliveryContact( DeliveryContact contact )
	{
		this.contact = contact;
	}
	
	public OrderStatus getOrderStatus()
	{
		return status;
	}
	
	public double getDiscount()
	{
		return discount;
	}
	
	public Iterable< Pizza > getPizzas()
	{
		return pizzas;
	}
	
	public int getPizzasCount()
	{
		return pizzas.size();
	}
	 
	public Pizza getPizza( int index )
	{
		return pizzas.get( index );
	}
	
	public BigDecimal getTotalCost()
	{
		BigDecimal cost = cart.getCost();
		return ( cost.multiply( new BigDecimal( 1.0 - discount ) ) );
	} 
	
	public void setDiscount( double discount )
	{
		if ( discount < 0.0 || discount > 1.0 )
			throw new RuntimeException( "Order.setDisconut - invalid percentage value" );

		this.discount = discount;
	}
	
	public void process()
	{
		if ( status != OrderStatus.New )
			throw new RuntimeException( "Order.process - Can only process new order!" );
		
		status = OrderStatus.Registered;
		
		int nItems = cart.getItemsCount();
		for ( int i = 0; i < nItems; i++ )
		{
			OrderItem item = cart.getItem( i );
			for ( int k = 0; k < item.getQuantity(); k++ )
				pizzas.add( new Pizza( item.getPizzaKind(), item.getPizzaSize(), this ) );
		}
	}
	
	public void cancel ()
	{
		switch ( status )
		{
			case New:
			case Registered:
			case Cooking:
			case Ready4Delivery:
			case Delivering:
				break;

			default:
				throw new RuntimeException( "Order::cancel - cannot cancel in current order state" );
		}

		status = OrderStatus.Cancelled;
	}

	
	public void onPizzaCookingStarted() 
	{
		if ( status == OrderStatus.Registered )
			status = OrderStatus.Cooking;

		else if ( status != OrderStatus.Cooking )
			throw new RuntimeException( "Order.onPizzaCookingStarted - can only happen in Registered & Cooking states" );		
	}

	public void onPizzaReady() 
	{
		if ( status != OrderStatus.Cooking )
			throw new RuntimeException( "Order.onPizzaReady - can only happen in Cooking state" );

		for ( Pizza pizza : pizzas )
		{
			if ( pizza.getCookingStatus() != CookingStatus.Finished )
				return;
		}
		
		status = OrderStatus.Ready4Delivery;
	}
	
	public void onStartedDelivery ()
	{
		if ( status != OrderStatus.Ready4Delivery )
			throw new RuntimeException("Order.onStartedDelivery - can only happen in Ready4Delivery state");

		status = OrderStatus.Delivering;
	}
	
	public void onDelivered ()
	{
		if ( status != OrderStatus.Delivering )
			throw new RuntimeException( "Order.onDelivered - can only happen in Delivering state" );

		status = OrderStatus.Delivered;
	}
	
	private OrderCart cart;
	private PaymentPlan payPlan;
	private DeliveryContact contact;
	private OrderStatus status;
	private double discount;
	private List< Pizza > pizzas;

}
