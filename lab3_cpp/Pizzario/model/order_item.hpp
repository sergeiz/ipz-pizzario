/** (C) 2015, Sergei Zaychenko, KNURE */

#ifndef _ORDER_ITEM_HPP_
#define _ORDER_ITEM_HPP_

/*-----------------------------------------------------------------------------*/

#include "model/pizza_size.hpp"
#include "model/money.hpp"

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/

class PizzaKind;

/*-----------------------------------------------------------------------------*/


class OrderItem
{

/*-----------------------------------------------------------------------------*/

public:

/*-----------------------------------------------------------------------------*/

	OrderItem ( 
			PizzaKind const & _kind
		,	PizzaSize::Enum _size
		,	Money _fixedPrice
		,	int _quantity 
	);

	PizzaKind const & getKind () const;

	PizzaSize::Enum getSize () const;

	Money getFixedPrice () const;

	Money getCost () const;

	int getQuantity () const;

/*-----------------------------------------------------------------------------*/

private:

/*-----------------------------------------------------------------------------*/
	
	PizzaKind const * m_pKind;

	PizzaSize::Enum m_size;

	int m_quantity;

	Money m_fixedPrice;

/*-----------------------------------------------------------------------------*/

};


/*-----------------------------------------------------------------------------*/
	

inline
PizzaKind const & 
OrderItem::getKind () const
{
	return * m_pKind;

} // OrderItem::getKind


/*-----------------------------------------------------------------------------*/


inline
PizzaSize::Enum 
OrderItem::getSize () const
{
	return m_size;

} // OrderItem::getSize


/*-----------------------------------------------------------------------------*/


inline
Money 
OrderItem::getFixedPrice () const
{
	return m_fixedPrice;

} // OrderItem::getFixedPrice


/*-----------------------------------------------------------------------------*/


inline
int
OrderItem::getQuantity () const
{
	return m_quantity;

} // OrderItem::getQuantity


/*=============================================================================*/ 

} // namespace Pizzario

/*=============================================================================*/ 

#endif //  _ORDER_ITEM_HPP_
