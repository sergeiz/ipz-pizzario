/** (C) 2015, Sergei Zaychenko, KNURE */

#include "model/order_item.hpp"

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/


OrderItem::OrderItem (
		PizzaKind const & _kind
	,	PizzaSize::Enum _size
	,	Money _fixedPrice
	,	int _quantity 
)
	:	m_pKind( & _kind )
	,	m_size( _size )
	,	m_fixedPrice( _fixedPrice )
	,	m_quantity( _quantity )
{
} // OrderItem::OrderItem


/*-----------------------------------------------------------------------------*/


Money 
OrderItem::getCost () const
{
	return m_fixedPrice * m_quantity;

} // OrderItem::getCost


/*=============================================================================*/ 

} // namespace Pizzario

/*=============================================================================*/ 
