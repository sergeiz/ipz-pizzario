/** (C) 2015, Sergei Zaychenko, KNURE */

#include "model/pizza_kind.hpp"
#include "model/pizza_recipe.hpp"

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/ 


PizzaKind::PizzaKind ( std::string const & _name )
	:	m_name( _name )
	,	m_nVotes( 0 )
	,	m_nTotalRating( 0 )
	,	m_hidden( false )
{
	m_recipe.reset( new PizzaRecipe() );

} // PizzaKind::PizzaKind


/*-----------------------------------------------------------------------------*/


PizzaKind::~PizzaKind ()
{
	// Recipe is destroyed automatically

} // PizzaKind::~PizzaKind


/*-----------------------------------------------------------------------------*/


void
PizzaKind::rename ( std::string const & _newName )
{
	m_name = _newName;

} // PizzaKind::rename 


/*-----------------------------------------------------------------------------*/

	
double 
PizzaKind::getAverageRating () const
{
	if ( m_nVotes )
		return static_cast< double >( m_nTotalRating ) / m_nVotes;
	else
		return 0.0;

} // PizzaKind::getAverageRating


/*-----------------------------------------------------------------------------*/


void 
PizzaKind::rate ( int _rating )
{
	if ( _rating <= 0 || _rating > 5 )
		throw std::runtime_error( "PizzaKind::rate: expecting rating within [1;5] range" );

	m_nTotalRating += _rating;

} // PizzaKind::rate


/*-----------------------------------------------------------------------------*/


void
PizzaKind::setupRatingStats ( int _nVotes, int _nTotalRating )
{
	if ( _nVotes < 0 || _nTotalRating < _nVotes )
		throw std::runtime_error( "PizzaKind::setupRatingStats: incorrect ratings" );

	m_nVotes = _nVotes;
	m_nTotalRating = _nTotalRating;

} // PizzaKind::setupRatingStats


/*=============================================================================*/ 

} // namespace Pizzario

/*=============================================================================*/ 
