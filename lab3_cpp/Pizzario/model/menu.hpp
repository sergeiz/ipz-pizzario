/** (C) 2015, Sergei Zaychenko, KNURE */

#ifndef _MENU_HPP_
#define _MENU_HPP_

/*-----------------------------------------------------------------------------*/

#include "model/pizza_size.hpp"
#include "model/money.hpp"
#include "model/order_item.hpp"

#include "utils/iterator_utils.hpp"
#include "utils/map_key_iterator.hpp"

#include <unordered_map>
#include <vector>

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/ 

class PizzaKind;

/*-----------------------------------------------------------------------------*/

class Menu
{

/*-----------------------------------------------------------------------------*/

	typedef std::unordered_map< std::string, std::unique_ptr< PizzaKind > > PizzaKindsByName;
	typedef Utils::MapKeyIterator< PizzaKindsByName > PizzaKindsByNameKeyIterator;

/*-----------------------------------------------------------------------------*/

public:

/*-----------------------------------------------------------------------------*/
	
	typedef Utils::Iterator< PizzaKindsByNameKeyIterator > PizzaNameIterator;

/*-----------------------------------------------------------------------------*/

	Menu ();

	~Menu ();

	int getPizzaKindsCount () const;

	PizzaNameIterator pizzaKindNamesBegin () const;
	PizzaNameIterator pizzaKindNamesEnd () const;

	PizzaKind * findPizzaKind ( std::string const & _name ) const;

	void addPizzaKind ( std::unique_ptr< PizzaKind > _kind );

	void deletePizzaKind ( std::string const & _name ); 

	void renamePizzaKind ( std::string const & _name, std::string const & _newName );
	
	Money getCurrentPrice ( PizzaKind const & _kind, PizzaSize::Enum _size ) const;

	void updatePrice ( PizzaKind const & _kind, PizzaSize::Enum _size, Money _price );

	OrderItem makeOrderItem ( PizzaKind const & _kind, PizzaSize::Enum _size, int _quantity ) const;

/*-----------------------------------------------------------------------------*/

private:

/*-----------------------------------------------------------------------------*/
	
	Menu ( const Menu & );
	Menu & operator = ( const Menu & );

/*-----------------------------------------------------------------------------*/

	PizzaKindsByName m_pizzaKinds;
		
	struct PizzaPrices
	{
		Money m_prices[ PizzaSize::Last ];
	};

	std::unordered_map< PizzaKind const *, PizzaPrices > m_pizzaPrices;

/*-----------------------------------------------------------------------------*/

};


/*-----------------------------------------------------------------------------*/


inline int 
Menu::getPizzaKindsCount () const
{
	return m_pizzaKinds.size();

} // Menu::getPizzaKindsCount


/*-----------------------------------------------------------------------------*/

} // namespace Pizzario

/*=============================================================================*/ 

#endif //  _MENU_HPP_