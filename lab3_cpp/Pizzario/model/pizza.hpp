/** (C) 2015, Sergei Zaychenko, KNURE */

#ifndef _PIZZA_HPP_
#define _PIZZA_HPP_

/*-----------------------------------------------------------------------------*/

#include "model/pizza_size.hpp"
#include "model/cooking_status.hpp"

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/ 

class PizzaKind;
class Order;

/*-----------------------------------------------------------------------------*/


class Pizza
{

/*-----------------------------------------------------------------------------*/

public:

/*-----------------------------------------------------------------------------*/

	Pizza ( 
			PizzaKind const & _kind
		,	PizzaSize::Enum _size
		,	Order & _parentOrder
		,	CookingStatus::Enum _initialStatus = CookingStatus::NotStarted
	);

	const PizzaKind & getKind () const;

	PizzaSize::Enum getSize () const;

	Order & getParentOrder () const;

	CookingStatus::Enum getCookingStatus () const;

	void onCookingStarted ();

	void onCookingCancelled ();

	void onCookingFinished ();

/*-----------------------------------------------------------------------------*/

private:

/*-----------------------------------------------------------------------------*/
	
	Pizza ( const Pizza & );
	Pizza & operator = ( const Pizza & );

/*-----------------------------------------------------------------------------*/
	
	PizzaKind const & m_kind;

	PizzaSize::Enum const m_size;

	Order & m_parentOrder;

	CookingStatus::Enum m_cookingStatus;

/*-----------------------------------------------------------------------------*/

};


/*-----------------------------------------------------------------------------*/


inline
const PizzaKind & 
Pizza::getKind () const
{
	return m_kind;

} // Pizza::getKind


/*-----------------------------------------------------------------------------*/


inline
PizzaSize::Enum
Pizza::getSize () const
{
	return m_size;

} // Pizza::getSize


/*-----------------------------------------------------------------------------*/


inline
Order & 
Pizza::getParentOrder () const
{
	return m_parentOrder;

} // Pizza::getParentOrder


/*-----------------------------------------------------------------------------*/


inline CookingStatus::Enum 
Pizza::getCookingStatus () const
{
	return m_cookingStatus;

} // Pizza::getCookingStatus


/*=============================================================================*/ 

} // namespace Pizzario

/*=============================================================================*/ 

#endif //  _PIZZA_RECIPE_HPP_