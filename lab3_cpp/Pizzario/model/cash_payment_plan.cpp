/** (C) 2015, Sergei Zaychenko, KNURE */

#include "model/cash_payment_plan.hpp"

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/


CashPaymentPlan::CashPaymentPlan ( bool _payed )
	:	PaymentPlan()
	,	m_payed( _payed )
{
} // CashPaymentPlan::CashPaymentPlan


/*-----------------------------------------------------------------------------*/


PaymentPlan * 
CashPaymentPlan::clone () const
{
	return new CashPaymentPlan( m_payed );

} // CashPaymentPlan::clone


/*-----------------------------------------------------------------------------*/


std::string
CashPaymentPlan::describe () const
{
	return "Cash payment on delivery";

} // CashPaymentPlan::describe 


/*-----------------------------------------------------------------------------*/


bool 
CashPaymentPlan::expectPrepayment () const
{
	return false;

} // CashPaymentPlan::expectPrepayment 


/*-----------------------------------------------------------------------------*/


bool
CashPaymentPlan::wasPayed () const
{
	return m_payed;

} // CashPaymentPlan::wasPayed


/*=============================================================================*/ 

} // namespace Pizzario

/*=============================================================================*/ 
