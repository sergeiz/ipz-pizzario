/** (C) 2015, Sergei Zaychenko, KNURE */

#include "model/menu.hpp"
#include "model/pizza_kind.hpp"

#include <algorithm>
#include <cassert>

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/ 


Menu::Menu ()
{
} // Menu::Menu


/*-----------------------------------------------------------------------------*/


Menu::~Menu ()
{
	// Pizza kinds will be destroyed automatically

}  // Menu::~Menu


/*-----------------------------------------------------------------------------*/


PizzaKind * 
Menu::findPizzaKind ( std::string const & _name ) const
{
	auto it = m_pizzaKinds.find( _name );
	return ( it != m_pizzaKinds.end() ) ? it->second.get() : nullptr;

} // Menu::findPizzaKind


/*-----------------------------------------------------------------------------*/


Menu::PizzaNameIterator
Menu::pizzaKindNamesBegin () const
{
	return PizzaNameIterator( new PizzaKindsByNameKeyIterator( m_pizzaKinds.begin() ) );

} // Menu::pizzaKindNamesBegin


/*-----------------------------------------------------------------------------*/
	

Menu::PizzaNameIterator
Menu::pizzaKindNamesEnd () const
{
	return PizzaNameIterator( new PizzaKindsByNameKeyIterator( m_pizzaKinds.end() ) );

} // Menu::pizzaKindNamesEnd


/*-----------------------------------------------------------------------------*/
	

void 
Menu::addPizzaKind ( std::unique_ptr< PizzaKind > _kind )
{
	assert( _kind.get() );

	if ( findPizzaKind( _kind->getName() ) )
		throw std::runtime_error( "Menu::addPizzaKind: duplicate kind" );

	m_pizzaPrices[ _kind.get() ] = PizzaPrices();
	m_pizzaKinds[ _kind->getName() ] = std::move( _kind );

} // Menu::addPizzaKind


/*-----------------------------------------------------------------------------*/


void
Menu::deletePizzaKind ( std::string const & _name )
{
	auto it = m_pizzaKinds.find( _name );
	if ( it == m_pizzaKinds.end() )
		throw std::runtime_error( "Menu::removePizzaKind: unregistered kind" );


} // Menu::removePizzaKind

/*-----------------------------------------------------------------------------*/


void 
Menu::renamePizzaKind ( std::string const & _name, std::string const & _newName )
{
	auto it = m_pizzaKinds.find( _name );
	if ( it == m_pizzaKinds.end() )
		throw std::runtime_error( "Menu::renamePizzaKind: unregistered kind" );

	if ( _name == _newName )
		return;

	std::unique_ptr< PizzaKind > pizza = std::move( it->second );
	m_pizzaKinds.erase( it );

	pizza->rename( _newName );

	m_pizzaKinds[ pizza->getName() ] = std::move( pizza );

} // Menu::renamePizzaKind


/*-----------------------------------------------------------------------------*/


Money 
Menu::getCurrentPrice ( PizzaKind const & _kind, PizzaSize::Enum _size ) const
{
	auto it = m_pizzaPrices.find( & _kind );
	if ( it == m_pizzaPrices.end() )
		throw std::runtime_error( "Menu::getCurrentPrice: unknown pizza kind" );

	return it->second.m_prices[ _size ];

} // Menu::getCurrentPrice


/*-----------------------------------------------------------------------------*/


void 
Menu::updatePrice ( PizzaKind const & _kind, PizzaSize::Enum _size, Money _price )
{
	auto it = m_pizzaPrices.find( & _kind );
	if ( it == m_pizzaPrices.end() )
		throw std::runtime_error( "Menu::updatePrice: unknown pizza kind" );

	it->second.m_prices[ _size ] = _price;

} // Menu::definePrice


/*-----------------------------------------------------------------------------*/


OrderItem 
Menu::makeOrderItem ( PizzaKind const & _kind, PizzaSize::Enum _size, int _quantity ) const
{
	return OrderItem( _kind, _size, getCurrentPrice( _kind, _size ), _quantity );

} // Menu::makeOrderItem


/*=============================================================================*/ 


} // namespace Pizzario

/*=============================================================================*/ 
