/** (C) 2015, Sergei Zaychenko, KNURE */

#ifndef _CASH_PAYMENT_PLAN_HPP_
#define _CASH_PAYMENT_PLAN_HPP_

/*-----------------------------------------------------------------------------*/

#include "model/payment_plan.hpp"

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/ 


class CashPaymentPlan
	:	public PaymentPlan
{

/*-----------------------------------------------------------------------------*/

public:

/*-----------------------------------------------------------------------------*/

	CashPaymentPlan ( bool _payed = false );
	
	PaymentPlan * clone () const override;

	std::string describe () const override;
	
	bool expectPrepayment () const override;
	
	bool wasPayed () const override;

	void setPayed ( bool _payed );

/*-----------------------------------------------------------------------------*/

private:

/*-----------------------------------------------------------------------------*/

	bool m_payed;

/*-----------------------------------------------------------------------------*/

};


/*-----------------------------------------------------------------------------*/


inline void
CashPaymentPlan::setPayed ( bool _payed )
{
	m_payed = true;

} // CashPaymentPlan::setPayed


/*=============================================================================*/ 

} // namespace Pizzario

/*=============================================================================*/ 

#endif //  _CASH_PAYMENT_PLAN_HPP_