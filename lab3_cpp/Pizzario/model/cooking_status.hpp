/** (C) 2015, Sergei Zaychenko, KNURE */

#ifndef _COOKING_STATUS_HPP_
#define _COOKING_STATUS_HPP_

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/


struct CookingStatus
{
	enum Enum
	{
			NotStarted
		,	Started
		,	Finished
		,	Cancelled

		,	Last
	};

	static const char * toString ( Enum e )
	{
		switch ( e )
		{
			case NotStarted:	return "Not started";
			case Started:		return "Started";
			case Finished:		return "Finished";
			case Cancelled:		return "Cancelled";

			default:
				return nullptr;
		}
	}
};


/*=============================================================================*/ 

} // namespace Pizzario

/*=============================================================================*/ 

#endif //  _COOKING_STATUS_HPP_
