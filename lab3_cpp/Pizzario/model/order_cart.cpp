/** (C) 2015, Sergei Zaychenko, KNURE */

#include "model/order_cart.hpp"

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/ 


OrderCart::OrderCart ()
{
} // OrderCart::OrderCart


/*-----------------------------------------------------------------------------*/


void 
OrderCart::addItem ( const OrderItem & _item )
{
	m_items.push_back( _item );

} // OrderCart::addItem


/*-----------------------------------------------------------------------------*/


void 
OrderCart::updateItem ( int _index, const OrderItem & _item )
{
	m_items.at( _index ) = _item;

} // OrderCart::updateItem


/*-----------------------------------------------------------------------------*/


void 
OrderCart::dropItem ( int _index )
{
	if ( _index < 0 || _index > static_cast< int >( m_items.size() ) )
		throw std::out_of_range( "OrderCart::dropItem" );

	m_items.erase( m_items.begin() + _index );

} // OrderCart::dropItem


/*-----------------------------------------------------------------------------*/


void 
OrderCart::clearItems ()
{
	m_items.clear();

} // OrderCart::clearItems


/*-----------------------------------------------------------------------------*/


Money
OrderCart::getCost () const
{
	Money sum;
	
	int nItems = getItemsCount();
	for ( int i = 0; i < nItems; i++ ) 
		sum += getItem( i ).getCost();

	return sum;

} // OrderCart::getCost


/*=============================================================================*/ 

} // namespace Pizzario

/*=============================================================================*/ 
