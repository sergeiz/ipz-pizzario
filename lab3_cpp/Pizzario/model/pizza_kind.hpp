/** (C) 2015, Sergei Zaychenko, KNURE */

#ifndef _PIZZA_KIND_HPP_
#define _PIZZA_KIND_HPP_

/*-----------------------------------------------------------------------------*/

#include <string>
#include <memory>

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/ 

class PizzaRecipe;

/*-----------------------------------------------------------------------------*/


class PizzaKind
{

/*-----------------------------------------------------------------------------*/

public:

/*-----------------------------------------------------------------------------*/

	PizzaKind ( std::string const & _name );

	~ PizzaKind ();

	std::string const & getName () const;
	
	void rename ( std::string const & _newName );

	std::string const & getDescription () const;
	
	void updateDescription ( const std::string & _description );

	std::string const & getImageURL () const;
	
	void setImageURL ( std::string const & _imageURL );

	PizzaRecipe & getRecipe () const;

	bool isHidden () const;

	void setHidden ( bool _hidden );

	double getAverageRating () const;

	int getRatingVotesCount () const;

	void rate ( int _rating );

	void setupRatingStats ( int _nVotes, int _nTotalRating );

/*-----------------------------------------------------------------------------*/

private:

/*-----------------------------------------------------------------------------*/
	
	PizzaKind ( const PizzaKind & );
	PizzaKind & operator = ( const PizzaKind & );

/*-----------------------------------------------------------------------------*/
	
	std::unique_ptr< PizzaRecipe > m_recipe;

	std::string m_name;

	std::string m_description;

	std::string m_imageURL;

	int m_nVotes, m_nTotalRating;

	bool m_hidden;

/*-----------------------------------------------------------------------------*/

};


/*-----------------------------------------------------------------------------*/


inline
std::string const &
PizzaKind::getName () const
{
	return m_name;

} // PizzaKind::getName


/*-----------------------------------------------------------------------------*/


inline
std::string const &
PizzaKind::getDescription () const
{
	return m_description;

} // PizzaKind::getDescription


/*-----------------------------------------------------------------------------*/


inline void
PizzaKind::updateDescription ( std::string const & _description )
{
	m_description = _description;

} // PizzaKind::updateDescription


/*-----------------------------------------------------------------------------*/


inline
std::string const & 
PizzaKind::getImageURL () const
{
	return m_imageURL;

} // PizzaKind::getImageURL 


/*-----------------------------------------------------------------------------*/


inline
void 
PizzaKind::setImageURL ( std::string const & _imageURL )
{
	m_imageURL = _imageURL;

} // PizzaKind::setImageURL


/*-----------------------------------------------------------------------------*/


inline bool
PizzaKind::isHidden () const
{
	return m_hidden;
}


/*-----------------------------------------------------------------------------*/


inline
void
PizzaKind::setHidden ( bool _hidden )
{
	m_hidden = _hidden;
}


/*-----------------------------------------------------------------------------*/


inline
PizzaRecipe & 
PizzaKind::getRecipe () const
{
	return * m_recipe;

} // PizzaKind::getRecipe


/*-----------------------------------------------------------------------------*/


inline int
PizzaKind::getRatingVotesCount () const
{
	return m_nVotes;

} // PizzaKind::getRatingVotesCount


/*=============================================================================*/ 

} // namespace Pizzario

/*=============================================================================*/ 

#endif //  _PIZZA_KIND_HPP_