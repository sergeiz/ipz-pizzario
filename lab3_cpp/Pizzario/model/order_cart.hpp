/** (C) 2015, Sergei Zaychenko, KNURE */

#ifndef _ORDER_CART_HPP_
#define _ORDER_CART_HPP_

/*-----------------------------------------------------------------------------*/

#include "model/order_item.hpp"

#include <vector>

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/


class OrderCart
{

/*-----------------------------------------------------------------------------*/

public:

/*-----------------------------------------------------------------------------*/

	OrderCart ();

	int getItemsCount () const;

	OrderItem const & getItem ( int _index ) const;

	void addItem ( const OrderItem & _item );

	void updateItem ( int _index, const OrderItem & _item );

	void dropItem ( int _index );

	void clearItems ();

	Money getCost () const;

/*-----------------------------------------------------------------------------*/

private:

/*-----------------------------------------------------------------------------*/
		
	std::vector< OrderItem > m_items;

/*-----------------------------------------------------------------------------*/

};


/*-----------------------------------------------------------------------------*/


inline int 
OrderCart::getItemsCount () const
{
	return m_items.size();

} // OrderCart::getItemsCount


/*-----------------------------------------------------------------------------*/


inline OrderItem const &
OrderCart::getItem ( int _index ) const
{
	return m_items.at( _index );

} // OrderCart::getItem


/*=============================================================================*/ 

} // namespace Pizzario

/*=============================================================================*/ 

#endif //  _ORDER_CART_HPP_
