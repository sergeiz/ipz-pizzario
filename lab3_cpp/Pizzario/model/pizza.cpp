/** (C) 2015, Sergei Zaychenko, KNURE */

#include "model/pizza.hpp"
#include "model/order.hpp"

#include <stdexcept>

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/


Pizza::Pizza ( 
		PizzaKind const & _kind
	,	PizzaSize::Enum _size
	,	Order & _parentOrder
	,	CookingStatus::Enum _initialStatus 
)
	:	m_kind( _kind )
	,	m_size( _size )
	,	m_parentOrder( _parentOrder )
	,	m_cookingStatus( _initialStatus )
{
} // Pizza::Pizza


/*-----------------------------------------------------------------------------*/


void 
Pizza::onCookingStarted ()
{
	if ( m_cookingStatus == CookingStatus::NotStarted )
	{
		m_cookingStatus = CookingStatus::Started;
		m_parentOrder.onPizzaCookingStarted();
	}
	else
		throw std::runtime_error( "Pizza::onCookingStarted: may only start cooking if not started yet" );

} // Pizza::onCookingStarted


/*-----------------------------------------------------------------------------*/


void
Pizza::onCookingCancelled ()
{
	if ( m_cookingStatus == CookingStatus::Started || m_cookingStatus == CookingStatus::NotStarted )
		m_cookingStatus = CookingStatus::Cancelled;

	else
		throw std::runtime_error( "Pizza::onCookingCancelled: may only abort unfinished pizza" );

} // PizzaKind::onCookingCancelled


/*-----------------------------------------------------------------------------*/


void 
Pizza::onCookingFinished ()
{
	if ( m_cookingStatus == CookingStatus::Started )
	{
		m_cookingStatus = CookingStatus::Finished;
		m_parentOrder.onPizzaReady();
	}
	else
		throw std::runtime_error( "Pizza::onCookingFinished: may only finish cooking if started" );

} // Pizza::onCookingFinished


/*=============================================================================*/ 

} // namespace Pizzario

/*=============================================================================*/ 
