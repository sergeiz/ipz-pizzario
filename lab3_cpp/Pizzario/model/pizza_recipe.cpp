/** (C) 2015, Sergei Zaychenko, KNURE */

#include "model/pizza_recipe.hpp"

#include <iostream>

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/ 


PizzaRecipe::PizzaRecipe ()
{
} // PizzaRecipe::PizzaRecipe


/*-----------------------------------------------------------------------------*/


int
PizzaRecipe::getIngredientsCount () const
{
	return m_ingredientsWeight.size();

} // PizzaRecipe::getIngredientsCount


/*-----------------------------------------------------------------------------*/

	
PizzaRecipe::IngredientNamesIterator 
PizzaRecipe::ingredientNamesBegin () const
{
	return IngredientNamesIterator( new IngredientsWeightKeyIterator( m_ingredientsWeight.begin() ) );

} // PizzaRecipe::ingredientNamesBegin


/*-----------------------------------------------------------------------------*/


PizzaRecipe::IngredientNamesIterator 
PizzaRecipe::ingredientNamesEnd () const
{
	return IngredientNamesIterator( new IngredientsWeightKeyIterator( m_ingredientsWeight.end() ) );

} // PizzaRecipe::ingredientNamesEnd


/*-----------------------------------------------------------------------------*/


int 
PizzaRecipe::getIngredientWeight ( std::string const & _ingredient ) const
{
	auto it = m_ingredientsWeight.find( _ingredient );
	return ( it == m_ingredientsWeight.end() ) ? 0 : it->second;

} // PizzaRecipe::getIngredientWeight


/*-----------------------------------------------------------------------------*/


void 
PizzaRecipe::addIngredient ( std::string const & _ingredient, int _weightGrams )
{
	auto it = m_ingredientsWeight.find( _ingredient );
	if ( it != m_ingredientsWeight.end() )
		throw std::runtime_error( "PizzaKind::addIngredient: duplicate ingredient");

	m_ingredientsWeight[ _ingredient ] = _weightGrams;

} // PizzaRecipe::addIngredient


/*-----------------------------------------------------------------------------*/


void 
PizzaRecipe::updateIngredient ( std::string const & _ingredient, int _weightGrams )
{
	auto it = m_ingredientsWeight.find( _ingredient );
	if ( it == m_ingredientsWeight.end() )
		throw std::runtime_error( "PizzaKind::updateIngredient: missing ingredient");

	it->second = _weightGrams;

} // PizzaRecipe::updateIngredient


/*-----------------------------------------------------------------------------*/


void 
PizzaRecipe::removeIngredient ( std::string const & _ingredient )
{
	auto it = m_ingredientsWeight.find( _ingredient );
	if ( it == m_ingredientsWeight.end() )
		throw std::runtime_error( "PizzaKind::removeIngredient: missing ingredient");

	m_ingredientsWeight.erase( it );

} // PizzaRecipe::removeIngredient


/*=============================================================================*/ 

} // namespace Pizzario

/*=============================================================================*/ 
