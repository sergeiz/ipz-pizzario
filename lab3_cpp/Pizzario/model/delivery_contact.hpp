/** (C) 2015, Sergei Zaychenko, KNURE */

#ifndef _DELIVERY_CONTACT_HPP_
#define _DELIVERY_CONTACT_HPP_

/*-----------------------------------------------------------------------------*/

#include <string>

/*=============================================================================*/ 

namespace Pizzario {

/*=============================================================================*/ 


class DeliveryContact
{

/*-----------------------------------------------------------------------------*/

public:

/*-----------------------------------------------------------------------------*/

	DeliveryContact ( 
			std::string const & _targetAddress
		,	std::string const & _phoneNumber
		,	std::string const & _comment
	);

	std::string const & getTargetAddress () const;

	std::string const & getPhoneNumber () const;

	std::string const & getComment () const;

/*-----------------------------------------------------------------------------*/

private:

/*-----------------------------------------------------------------------------*/

	std::string m_targetAddress;

	std::string m_phoneNumber;

	std::string m_comment;

/*-----------------------------------------------------------------------------*/

};


/*-----------------------------------------------------------------------------*/


inline
DeliveryContact::DeliveryContact ( 
		std::string const & _targetAddress
	,	std::string const & _phoneNumber
	,	std::string const & _comment
)
	:	m_targetAddress( _targetAddress )
	,	m_phoneNumber( _phoneNumber )
	,	m_comment( _comment )
{
} // DeliveryContact::DeliveryContact


/*-----------------------------------------------------------------------------*/


inline
std::string const &
DeliveryContact::getTargetAddress () const
{
	return m_targetAddress;

} // DeliveryContact::getTargetAddress


/*-----------------------------------------------------------------------------*/


inline
std::string const & 
DeliveryContact::getPhoneNumber () const
{
	return m_phoneNumber;

} // DeliveryContact::getPhoneNumber


/*-----------------------------------------------------------------------------*/


inline
std::string const &
DeliveryContact::getComment () const
{
	return m_comment;

} // DeliveryContact::getComment


/*=============================================================================*/ 

} // namespace Pizzario

/*=============================================================================*/ 

#endif //  _DELIVERY_CONTACT_HPP_