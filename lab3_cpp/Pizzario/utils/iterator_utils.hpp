/** (C) 2015, Sergei Zaychenko, KNURE */

#ifndef _ITERATOR_UTILS_HPP_
#define _ITERATOR_UTILS_HPP_

/*-----------------------------------------------------------------------------*/

#include <memory>

/*=============================================================================*/ 

namespace Utils {

/*=============================================================================*/


template< typename _ImplIterator >
class Iterator
{

/*-----------------------------------------------------------------------------*/

public:

/*-----------------------------------------------------------------------------*/

	typedef typename std::input_iterator_tag iterator_category;

	typedef typename _ImplIterator::reference_type reference_type;
	typedef typename _ImplIterator::reference_type value_type;
	typedef typename _ImplIterator::difference_type difference_type;
	typedef typename _ImplIterator::pointer pointer;
	typedef typename _ImplIterator::reference reference;

/*-----------------------------------------------------------------------------*/

	Iterator ( _ImplIterator * _pImpl );

	Iterator ( const Iterator & _it );

	Iterator ( Iterator && _it );

/*-----------------------------------------------------------------------------*/

	bool operator == ( Iterator< _ImplIterator > const & _it ) const;

	bool operator != ( Iterator< _ImplIterator > const & _it ) const;

	Iterator< _ImplIterator > & operator ++ ();

	reference_type operator * () const;

/*-----------------------------------------------------------------------------*/

private:

/*-----------------------------------------------------------------------------*/

	std::unique_ptr< _ImplIterator > m_impl;

/*-----------------------------------------------------------------------------*/

};


/*-----------------------------------------------------------------------------*/


template< typename _ImplIterator >
Iterator< _ImplIterator >::Iterator ( _ImplIterator * _pImpl )
	:	m_impl( _pImpl )
{
} // Iterator< _ImplIterator >::Iterator


/*-----------------------------------------------------------------------------*/


template< typename _ImplIterator >
Iterator< _ImplIterator >::Iterator ( const Iterator< _ImplIterator  > & _it )
	:	m_impl( new _ImplIterator( * _it.m_impl.get() ) )
{
} // Iterator< _ImplIterator >::Iterator


/*-----------------------------------------------------------------------------*/


template< typename _ImplIterator >
Iterator< _ImplIterator >::Iterator ( Iterator< _ImplIterator  > && _it )
	:	m_impl( std::move( _it.m_impl ) )
{
} // Iterator< _ImplIterator >::Iterator


/*-----------------------------------------------------------------------------*/


template< typename _ImplIterator >
bool 
Iterator< _ImplIterator >::operator == ( Iterator< _ImplIterator > const & _it ) const
{
	return * m_impl == * _it.m_impl;

} // Iterator< _ImplIterator >::operator ==


/*-----------------------------------------------------------------------------*/


template< typename _ImplIterator >
bool 
Iterator< _ImplIterator >::operator != ( Iterator< _ImplIterator > const & _it ) const
{
	return * m_impl != * _it.m_impl;

} // Iterator< _ImplIterator >::operator !=


/*-----------------------------------------------------------------------------*/


template< typename _ImplIterator >
Iterator< _ImplIterator > &
Iterator< _ImplIterator >::operator ++ ()
{
	++( * m_impl );
	return * this;

} // Iterator< _ImplIterator >::operator ++


/*-----------------------------------------------------------------------------*/


template< typename _ImplIterator >
typename Iterator< _ImplIterator >::reference_type 
Iterator< _ImplIterator >::operator * () const
{
	return ** m_impl;

} // Iterator< _ImplIterator >::operator *


/*=============================================================================*/ 

} // namespace Utils

/*=============================================================================*/ 

#endif //  _ITERATOR_UTILS_HPP_
