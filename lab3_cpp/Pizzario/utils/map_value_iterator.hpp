/** (C) 2015, Sergei Zaychenko, KNURE */

#ifndef _MAP_VALUE_ITERATOR_HPP_
#define _MAP_VALUE_ITERATOR_HPP_

/*-----------------------------------------------------------------------------*/

#include <memory>

/*=============================================================================*/ 

namespace Utils {

/*=============================================================================*/


template< typename _MapType >
class MapValueIterator
{

/*-----------------------------------------------------------------------------*/

public:

/*-----------------------------------------------------------------------------*/

	typedef typename _MapType::const_iterator const_map_iterator;

	typedef typename _MapType::mapped_type & reference_type;
	typedef typename _MapType::mapped_type value_type;
	typedef typename _MapType::difference_type difference_type;
	typedef typename _MapType::pointer pointer;
	typedef typename _MapType::reference reference;

/*-----------------------------------------------------------------------------*/

	MapValueIterator ( const_map_iterator _base );

/*-----------------------------------------------------------------------------*/

	bool operator == ( MapValueIterator< _MapType > _it ) const;

	bool operator != ( MapValueIterator< _MapType > _it ) const;

	MapValueIterator< _MapType > & operator ++ ();

	reference_type operator * () const;

/*-----------------------------------------------------------------------------*/

private:

/*-----------------------------------------------------------------------------*/

	const_map_iterator m_baseIt;

/*-----------------------------------------------------------------------------*/

};


/*-----------------------------------------------------------------------------*/


template< typename _MapType >
MapValueIterator< _MapType >::MapValueIterator ( const_map_iterator _baseIt )
	:	m_baseIt( _baseIt )
{
} // MapValueIterator< _MapType >::MapValueIterator


/*-----------------------------------------------------------------------------*/


template< typename _MapType >
bool 
MapValueIterator< _MapType >::operator == ( MapValueIterator< _MapType > _it ) const
{
	return m_baseIt == _it.m_baseIt;

} // MapValueIterator< _MapType >::operator ==


/*-----------------------------------------------------------------------------*/


template< typename _MapType >
bool 
MapValueIterator< _MapType >::operator != ( MapValueIterator< _MapType > _it ) const
{
	return m_baseIt != _it.m_baseIt;

} // MapValueIterator< _MapType >::operator !=


/*-----------------------------------------------------------------------------*/


template< typename _MapType >
MapValueIterator< _MapType > &
MapValueIterator< _MapType >::operator ++ ()
{
	++ m_baseIt;
	return * this;

} // MapValueIterator< _MapType >::operator ++


/*-----------------------------------------------------------------------------*/


template< typename _MapType >
typename MapValueIterator< _MapType >::reference_type 
MapValueIterator< _MapType >::operator * () const
{
	return m_baseIt->second;

} // MapValueIterator< _MapType >::operator *


/*=============================================================================*/ 

} // namespace Utils

/*=============================================================================*/ 

#endif //  _MAP_VALUE_ITERATOR_HPP_
