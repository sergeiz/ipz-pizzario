from ReportGenerator import ReportGenerator
from TestModelGenerator import TestModelGenerator
from orm.PizzarioNetworkOrm import PizzarioNetworkOrm

from io import StringIO

orm = PizzarioNetworkOrm( "sqlite:///../pizzario.db" )

modelGenerator = TestModelGenerator()
network1 = modelGenerator.generate()

stream1 = StringIO()
reportGenerator1 = ReportGenerator( stream1, network1 )
reportGenerator1.generate()

orm.save_network( network1 )

network2 = orm.load_network()

stream2 = StringIO()
reportGenerator2 = ReportGenerator( stream2, network2 )
reportGenerator2.generate()

content1 = stream1.getvalue()
content2 = stream2.getvalue()

print ( content1 )
print ( content2 )

if content1 == content2:
    print ( "Models Match" )
else:
    print ( "Mismatch between models" )
    