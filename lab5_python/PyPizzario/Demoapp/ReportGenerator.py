from model.PizzaSize import PizzaSize
from model.OrderStatus import OrderStatus
from model.CookingStatus import CookingStatus

class ReportGenerator:
    
    def __init__( self , output , network ):
        self.output = output
        self.network = network
      
    def generate( self ):
        self.show_menu( self.network.menu )
        self.show_orders( self.network.orders )
        
    def show_menu( self , m ):
        self.output.write( " ==== Menu ==== \n" )
        
        for name in m.browse_pizza_kind_names():
            kind = m.find_pizza_kind( name )
            self.output.write( "Pizza \"" + name + "\":\n" )
            self.output.write( "\tDescription: " + kind.description + "\n" )
            self.output.write( "\tImage URL: " + kind.image_url + "\n"  )

            self.show_pizza_ratings( kind )
            self.show_pizza_ingredients( kind )
            self.show_pizza_prices( kind )
            
        self.output.write(" ==== End Menu ====\n ")
        
        
    def show_pizza_ratings( self , kind ):
        self.output.write( "\tRating: " )
        self.output.write( str( kind.get_average_rating() ) )
        self.output.write( " ( " + str( kind.votes ) + " votes )\n" )
        
        
    def show_pizza_ingredients ( self , kind ):
        recipe = kind.recipe
        first = True
        self.output.write( "\tIngredients: " )

        for ingredient in recipe.browse_ingredients():
            if  not  first:
                self.output.write( ", " )
            else:
                first = False

            self.output.write( str( recipe.get_ingredient_weight( ingredient ) ) )
            self.output.write( "g " )
            self.output.write( ingredient )

        self.output.write("\n")
        
        
    def show_pizza_prices ( self , kind ):
        self.output.write( "\tPrices: " )
        self.output.write( str( kind.get_current_price( PizzaSize.SMALL ) ) )
        self.output.write( " small, " )
        self.output.write( str( kind.get_current_price( PizzaSize.MEDIUM ) ) )
        self.output.write( " medium, " )
        self.output.write( str( kind.get_current_price( PizzaSize.LARGE ) ) )
        self.output.write( " large\n" )
        
        
    def show_orders ( self , orders ):
        self.output.write( "==== Orders ====\n" )

        for order in orders:
            self.show_order( order )

        self.output.write( "==== End Orders ====\n" )
        
        
    def show_order( self , order ):
        self.output.write( "Order #" + str( order.order_id ) + ":\n" )

        self.output.write( "\tStatus: " + OrderStatus.to_string( order.status )  + "\n"  )

        self.output.write( "\tTotal cost: " + str( order.total_cost() ) + "\n"  )

        self.output.write( "\tDiscount: " + str( order.discount * 100.0 )  + "%\n" )

        self.show_delivery_contact( order.contact )
        self.show_cart( order.cart )
        self.show_ordered_pizzas( order )
        
        
    def show_delivery_contact( self , contact ):
        self.output.write( "\tContact: " + contact.address + "\n" )
        self.output.write( "\tPhone: " + contact.phone + "\n" )
        self.output.write( "\tComment: " + contact.comment + "\n" )
        
        
    def show_cart( self , cart ):
        self.output.write( "\tCart:" )

        if  cart.modifiable:
            self.output.write(" modifiable ") 
        else:
            self.output.write(" unmodifiable" ) 
            
        self.output.write( "\n" )

        n_items = cart.get_items_count()
        for i in range( 0 , n_items ):
            self.output.write( "\t  " )
            self.output.write( str( i + 1 ) )
            self.output.write( ") " )

            item = cart.get_item( i )

            self.output.write( item.kind.name )
            self.output.write( ' ' )
            self.output.write( PizzaSize.to_string( item.size ) )
            self.output.write( ", price " )
            self.output.write( str( item.fixed_price ) )
            self.output.write( ", quantity " )
            self.output.write( str( item.quantity ) )
            self.output.write( ", cost " )
            self.output.write( str( item.cost() ) )
            self.output.write( "\n" )
            
            
    def show_ordered_pizzas ( self , order ):
        if order.get_pizza_count() == 0:
            return

        self.output.write( "\tPizzas:\n" )

        n_pizzas = order.get_pizza_count()
        for i in range( 0 , n_pizzas ):
            self.output.write( "\t  " )
            self.output.write( str( i + 1 ) )
            self.output.write( ") Pizza " )

            p = order.get_pizza( i )
            self.output.write( p.kind.name )
            self.output.write( ' ' )
            self.output.write( PizzaSize.to_string( p.size ) )
            self.output.write( ", cooking status: " )
            self.output.write( CookingStatus.to_string( p.status ) )
            self.output.write( "\n" )
        
        
        