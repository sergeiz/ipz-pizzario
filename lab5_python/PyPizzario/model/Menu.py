from model.OrderItem import OrderItem

class Menu:
    
    def __init__( self ):
        self.pizza_kinds_by_name = dict()
        self.pizza_kinds_name_lst = []
        
    
    def browse_pizza_kind_names( self ):
        return iter( self.pizza_kinds_name_lst )
        
    
    def browse_pizza_kinds( self ):
        return iter( self.pizza_kinds_by_name.values() )
        
        
    def get_pizza_kinds_count( self ):
        return len( self.pizza_kinds_by_name ) 
    
    
    def find_pizza_kind ( self , name ):
        return self.pizza_kinds_by_name.get( name )
    
    
    def add_pizza_kind ( self , kind ):
        if ( self.find_pizza_kind( kind.name ) != None ):
            raise Exception( "Menu.add_pizza_kind: duplicate kind" )

        self.pizza_kinds_by_name[ kind.name ] = kind
        self.pizza_kinds_name_lst.append( kind.name )

        kind.before_name_changed += self.handle_pizza_rename;
        
        
    def delete_pizza_kind ( self , name ):
        kind = self.find_pizza_kind( name )
        if ( kind == None ):
            raise Exception( "Menu.delete_pizza_kind: unregistered kind" )

        self.pizza_kinds_by_name.pop( name )
        self.pizza_kinds_name_lst.remove( name )

        kind.before_name_changed -= self.handle_pizza_rename
        
        
    def handle_pizza_rename ( self , oldName , newName ):
        kind = self.find_pizza_kind( oldName )
        if ( kind == None ):
            raise Exception( "Menu.handle_pizza_rename: unregistered kind" );

        if ( oldName == newName ):
            return

        if ( self.find_pizza_kind( newName ) != None ):
            raise Exception( "Menu.handle_pizza_rename: target name already reserved" )
        
        self.pizza_kinds_by_name.pop( oldName )
        self.pizza_kinds_by_name[ newName ] = kind
        self.pizza_kinds_name_lst.remove( oldName )
        self.pizza_kinds_name_lst.append( newName )
        

        
        
    def make_order_item ( self , name , size, quantity ):
        kind = self.find_pizza_kind( name )
        if ( kind == None ):
            raise Exception( "Menu.MakeOrderItem: unregistered kind" )

        return OrderItem( kind, size, kind.get_current_price( size ), quantity );
