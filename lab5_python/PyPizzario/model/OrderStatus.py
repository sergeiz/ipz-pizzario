class OrderStatus:
    
    NEW = 0
    REGISTRED = 1
    COOKING = 2
    READY4DELIVERY = 3
    DELIVERING = 4
    DELIVERED = 5
    CANCELLED = 6
    
    status_as_string = [ "New", "Registered", "Cooking", "Ready", "Delivering", "Delivered", "Cancelled" ]
    
    @staticmethod
    def to_string( status ):
        return OrderStatus.status_as_string[ status ]

    @staticmethod
    def is_valid( status ):
        return status in range( OrderStatus.NEW , OrderStatus.CANCELLED + 1 )
        
    @staticmethod 
    def validate( status ):
        if OrderStatus.is_valid( status ):
            pass
        else:
            raise Exception( "Invalid OrderStatus code" )
