from model.Pizza import Pizza
from model.CookingStatus import CookingStatus
from model.PizzaKind import PizzaKind
from model.PizzaSize import PizzaSize

import unittest
from unittest.mock import Mock


class PizzaTests( unittest.TestCase ):
    
    def test_constructor_projects_fields_correctly( self ):
        kind = PizzaKind( "Carbonara" )
        p = Pizza( kind, PizzaSize.SMALL )
        
        self.assertEqual( p.kind , kind )
        self.assertEqual( p.size, PizzaSize.SMALL )
        
        
    def test_constructor_initially_not_started( self ):
        p = PizzaTests.make_simple_pizza()
        
        self.assertEqual( p.status , CookingStatus.NOT_STARTED )
        
        
    def test_cooking_started_from_initial_state_passes( self ):
        p = PizzaTests.make_simple_pizza()
        
        p.on_cooking_started()
        self.assertEqual( p.status , CookingStatus.STARTED )
        
        
    def test_cooking_started_already_started_fails( self ):
        p = PizzaTests.make_simple_pizza()
        
        p.on_cooking_started()
        self.assertRaises( Exception , p.on_cooking_started , () )
        
        
    def test_cooking_started_after_finish_fails( self ):
        p = PizzaTests.make_simple_pizza()
        p.on_cooking_started()
        p.on_cooking_finished()
        
        self.assertRaises( Exception , p.on_cooking_started , () )
        
        
    def test_cooking_started_after_cancelled_fails( self ):
        p = PizzaTests.make_simple_pizza()
        p.on_cooking_started()
        p.on_cooking_cancelled()
        
        self.assertRaises( Exception , p.on_cooking_started , () )
        
        
    def test_cooking_started_sends_event( self ):
        p = PizzaTests.make_simple_pizza()
        
        m = Mock()
        p.cooking_started += m
        p.on_cooking_started()
        
        m.assert_called_once_with()
        
        
    def test_cooking_cancelled_from_started_passes( self ):
        p = PizzaTests.make_simple_pizza()
        p.on_cooking_started()
        p.on_cooking_cancelled()
        
        self.assertEqual( p.status , CookingStatus.CANCELLED )
        
        
    def test_cooking_cancelled_from_initial_passes( self ):
        p = PizzaTests.make_simple_pizza()
        p.on_cooking_cancelled()
        
        self.assertEqual( p.status , CookingStatus.CANCELLED )
        
        
    def test_cooking_cancelled_already_cancelled_fails( self ):
        p = PizzaTests.make_simple_pizza()
        p.on_cooking_started()
        p.on_cooking_cancelled()
        
        self.assertRaises( Exception , p.on_cooking_cancelled , () )
        
        
    def test_cooking_cancelled_after_finish_fails( self ):
        p = PizzaTests.make_simple_pizza()
        p.on_cooking_started()
        p.on_cooking_finished()
        
        self.assertRaises( Exception , p.on_cooking_cancelled , () )
        
        
    def test_cooking_finished_from_initial_fails( self ):
        p = PizzaTests.make_simple_pizza()
        
        self.assertRaises( Exception , p.on_cooking_finished , () )
        
        
    def test_cooking_finished_from_started_passes( self ):
        p = PizzaTests.make_simple_pizza()
        p.on_cooking_started()
        p.on_cooking_finished()
        
        self.assertEqual( p.status , CookingStatus.FINISHED )
        
        
    def test_cooking_finished_already_finished_fails( self ):
        p = PizzaTests.make_simple_pizza()
        p.on_cooking_started()
        p.on_cooking_finished()
        
        self.assertRaises( Exception , p.on_cooking_finished , () )
        
        
    def test_cooking_finished_was_cancelled_fails( self ):
        p = PizzaTests.make_simple_pizza()
        p.on_cooking_started()
        p.on_cooking_cancelled()
        
        self.assertRaises( Exception , p.on_cooking_finished , () )
        
        
    def test_cooking_finished_sends_event( self ):
        p = PizzaTests.make_simple_pizza()
        
        m = Mock()
        p.cooking_finished += m
        
        p.on_cooking_started()
        p.on_cooking_finished()
        
        m.assert_called_once_with()
    
    @staticmethod    
    def make_simple_pizza():
        return Pizza( PizzaKind( "some" ), PizzaSize.SMALL )

            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
        
        
