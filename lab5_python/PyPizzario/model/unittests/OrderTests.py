from model.Order import Order
from model.OrderStatus import OrderStatus
from model.CookingStatus import CookingStatus
from model.OrderCart import OrderCart
from model.OrderItem import OrderItem
from model.DeliveryContact import DeliveryContact
from model.CashPaymentPlan import CashPaymentPlan
from model.WireTransferPaymentPlan import WireTransferPaymentPlan
from model.PizzaKind import PizzaKind
from model.PizzaSize import PizzaSize

import unittest
  
            
class OrderTests( unittest.TestCase ):
    
    def test_constructor_projects_properties_correctly( self ):
        cart = OrderCart()
        contact = DeliveryContact(  "", "", "" )
        pay_plan = CashPaymentPlan()
        o = Order( 1, cart, contact, pay_plan )
        
        self.assertEqual( o.order_id , 1 )
        self.assertEqual( o.cart , cart )
        self.assertEqual( o.contact , contact )
        self.assertEqual( o.pay_plan , pay_plan )
        
        
    def test_constructor_from_unmodifiable_cart_forbidden( self ):
        cart = OrderCart()
        cart.add_item( OrderTests.make_some_item() )
        cart.checkout()
        
        self.assertRaises( Exception , OrderTests.make_order_with_cart_args , ( cart ) )
        
        
    def test_constructor_no_pizzas_initially_even_when_cart_has_items( self ):
        cart = OrderCart()
        cart.add_item( OrderTests.make_some_item() )
        o = OrderTests.make_order_with_cart_args( cart )
        
        self.assertEqual( o.get_pizza_count() , 0 )
        
        
    def test_total_cost_without_discount_matches_cart_cost( self ):
        o = OrderTests.make_order_without_args()
        o.set_discount( 0.0 )
        
        self.assertEqual( o.total_cost() , o.cart.cost() )
        
        
    def test_total_cost_with_discount_reduces_cart_cost( self ):
        o = OrderTests.make_order_without_args()
        o.set_discount( 0.5 )
        
        self.assertEqual( o.total_cost() , o.cart.cost()/2 )
        
        
    def test_total_cost_with_full_discount_makes_order_free( self ):
        o = OrderTests.make_order_without_args()
        o.set_discount( 1.0 )
        
        self.assertEqual( o.total_cost() , 0 )
        
        
    def test_discount_initially_zero( self ):
        o = OrderTests.make_order_without_args()
        
        self.assertEqual( o.discount , 0.0 )
        
        
    def test_discount_set_valid_read_back( self ):
        o = OrderTests.make_order_without_args()
        o.set_discount( 0.2 )
        
        self.assertEqual( o.discount , 0.2 )
        
        
    def test_discount_set_negative_forbidden( self ):
        o = OrderTests.make_order_without_args()
        
        self.assertRaises( Exception , o.discount , -0.01 )
        
        
    def test_discount_set_more_than_100percent_forbidden( self ):
        o = OrderTests.make_order_without_args()
        
        self.assertRaises( Exception , o.discount , 1.01 )
        
        
    def test_pizzas_for_single_item_single_matching_pizza( self ):
        cart = OrderCart()
        cart.add_item( OrderTests.make_some_item( 1 ) )
        o = OrderTests.make_order_with_cart_args( cart )
        cart.checkout()
        
        self.assertEqual( o.get_pizza_count() , 1 )
        self.assertEqual( o.get_pizza( 0 ).kind , cart.get_item( 0 ).kind )
        self.assertEqual( o.get_pizza( 0 ).size , cart.get_item( 0 ).size )
        
        
    def test_pizzas_for_double_item_two_matching_pizzas( self ):
        cart = OrderCart()
        cart.add_item( OrderTests.make_some_item( 2 ) )
        o = OrderTests.make_order_with_cart_args( cart )
        cart.checkout()
        
        self.assertEqual( o.get_pizza( 0 ).kind , cart.get_item( 0 ).kind )
        self.assertEqual( o.get_pizza( 1 ).kind , cart.get_item( 0 ).kind )
        self.assertEqual( o.get_pizza( 0 ).size , cart.get_item( 0 ).size )
        self.assertEqual( o.get_pizza( 1 ).size , cart.get_item( 0 ).size )
        
        
    def test_pizzas_for_two_single_items_two_different_matching_pizzas( self ):
        cart = OrderCart()
        cart.add_item( OrderTests.make_some_item( 1 ) )
        cart.add_item( OrderTests.make_some_other_item( 1 ) )
        o = OrderTests.make_order_with_cart_args( cart )
        cart.checkout()
        
        self.assertEqual( o.get_pizza_count() , 2 )
        self.assertEqual( o.get_pizza( 0 ).kind , cart.get_item( 0 ).kind )
        self.assertEqual( o.get_pizza( 1 ).kind , cart.get_item( 1 ).kind )
        self.assertEqual( o.get_pizza( 0 ).size , cart.get_item( 0 ).size )
        self.assertEqual( o.get_pizza( 1 ).size , cart.get_item( 1 ).size )
        
        
    def test_pizzas_for_one_single_double_other_three_matching_pizzas( self ):
        cart = OrderCart()
        cart.add_item( OrderTests.make_some_item( 1 ) )
        cart.add_item( OrderTests.make_some_other_item( 2 ) )
        o = OrderTests.make_order_with_cart_args( cart )
        cart.checkout()
        
        self.assertEqual( o.get_pizza_count() , 3 )
        self.assertEqual( o.get_pizza( 0 ).kind , cart.get_item( 0 ).kind )
        self.assertEqual( o.get_pizza( 1 ).kind , cart.get_item( 1 ).kind )
        self.assertEqual( o.get_pizza( 2 ).kind , cart.get_item( 1 ).kind )
        self.assertEqual( o.get_pizza( 0 ).size , cart.get_item( 0 ).size )
        self.assertEqual( o.get_pizza( 1 ).size , cart.get_item( 1 ).size )
        self.assertEqual( o.get_pizza( 2 ).size , cart.get_item( 1 ).size )
        
        
    def test_status_initially_new( self ):
        o = OrderTests.make_order_without_args()
        
        self.assertEqual( o.status, OrderStatus.NEW )
        
        
    def test_status_after_checkout_registered( self ):
        o = OrderTests.make_order_without_args()
        o.cart.checkout()
        
        self.assertEqual( o.status, OrderStatus.REGISTRED )
        
        
    def test_status_after_single_pizza_ready_ready4delivery( self ):
        o = OrderTests.make_order_without_args()
        o.cart.checkout()
        OrderTests.cook_all_pizzas( o )
        
        self.assertEqual( o.status, OrderStatus.READY4DELIVERY )
        
        
    def test_status_after_single_pizza_started_cooking( self ):
        o = OrderTests.make_order_without_args()
        o.cart.checkout()
        
        p = o.get_pizza( 0 )
        p.on_cooking_started()
        
        self.assertEqual( o.status, OrderStatus.COOKING )
        
        
    def test_status_after_one_of_two_started_cooking( self ):
        cart = OrderCart()
        cart.add_item( OrderTests.make_some_item( 2 ) )
        o = OrderTests.make_order_with_cart_args( cart )
        cart.checkout()
        
        p = o.get_pizza( 0 )
        p.on_cooking_started()
        
        self.assertEqual( o.status, OrderStatus.COOKING )
        
        
    def test_status_after_one_of_two_ready_cooking( self ):
        cart = OrderCart()
        cart.add_item( OrderTests.make_some_item( 2 ) )
        o = OrderTests.make_order_with_cart_args( cart )
        cart.checkout()
        
        OrderTests.cook_pizza( o.get_pizza ( 0 ) )
        
        self.assertEqual( o.status, OrderStatus.COOKING )
        
        
    def test_status_after_both_ready_ready4delivery( self ):
        cart = OrderCart()
        cart.add_item( OrderTests.make_some_item( 2 ) )
        o = OrderTests.make_order_with_cart_args( cart )
        cart.checkout()
        
        OrderTests.cook_all_pizzas( o )
        
        self.assertEqual( o.status, OrderStatus.READY4DELIVERY )
        
        
    def test_status_after_started_delivery_delivering( self ):
        o = OrderTests.make_order_without_args()
        o.cart.checkout()
        OrderTests.cook_all_pizzas( o )
        
        o.on_started_delivery()
        
        self.assertEqual( o.status, OrderStatus.DELIVERING )
        
        
    def test_status_after_delivery_delivered( self ):
        o = OrderTests.make_order_without_args()
        o.cart.checkout()
        OrderTests.cook_all_pizzas( o )
        o.on_started_delivery()
        o.pay_plan.mark_payed()
        
        o.on_delivered()
        
        self.assertEqual( o.status, OrderStatus.DELIVERED )
        
        
    def test_start_or_finish_delivery_when_new_order_forbidden( self ):
        o = OrderTests.make_order_without_args()
        
        self.assertRaises( Exception , o.on_started_delivery , () )
        self.assertRaises( Exception , o.on_delivered , () )
        
        
    def test_start_or_finish_delivery_when_registered_order_forbidden( self ):
        o = OrderTests.make_order_without_args()
        o.cart.checkout()
        
        self.assertRaises( Exception , o.on_started_delivery , () )
        self.assertRaises( Exception , o.on_delivered , () )
        
        
    def test_start_or_finish_delivery_when_still_cooking_order_forbidden( self ):
        cart = OrderCart()
        cart.add_item( OrderTests.make_some_item( 2 ) )
        o = OrderTests.make_order_with_cart_args( cart )
        cart.checkout()
        
        OrderTests.cook_pizza( o.get_pizza ( 0 ) )
        
        self.assertRaises( Exception , o.on_started_delivery , () )
        self.assertRaises( Exception , o.on_delivered , () )
        
        
    def test_start_delivery_when_already_started_forbidden( self ):
        o = OrderTests.make_order_without_args()
        o.cart.checkout()
        OrderTests.cook_all_pizzas( o )
        o.on_started_delivery()
        
        self.assertRaises( Exception , o.on_started_delivery , () )
        
        
    def test_start_delivery_when_delivered_forbidden( self ):
        o = OrderTests.make_order_without_args()
        o.cart.checkout()
        OrderTests.cook_all_pizzas( o )
        o.on_started_delivery()
        o.pay_plan.mark_payed()
        o.on_delivered()
        
        self.assertRaises( Exception , o.on_started_delivery , () )
        
        
    def test_finish_delivery_when_delivery_not_started_forbidden( self ):
        o = OrderTests.make_order_without_args()
        o.cart.checkout()
        OrderTests.cook_all_pizzas( o )
        o.pay_plan.mark_payed()
        
        self.assertRaises( Exception , o.on_delivered , () )
        
        
    def test_finish_delivery_when_already_delivered_forbidden( self ):
        o = OrderTests.make_order_without_args()
        o.cart.checkout()
        OrderTests.cook_all_pizzas( o )
        o.on_started_delivery()
        o.pay_plan.mark_payed()
        
        o.on_delivered()
        
        self.assertRaises( Exception , o.on_delivered , () )
        
        
    def test_cancel_new_order_passes( self ):
        o = OrderTests.make_order_without_args()
        o.cancel()
        
        self.assertEqual( o.status, OrderStatus.CANCELLED )
        self.assertEqual( o.get_pizza_count() , 0 )
        
        
    def test_cancel_registered_order_passes( self ):
        cart = OrderCart()
        cart.add_item( OrderTests.make_some_item( 2 ) )
        o = OrderTests.make_order_with_cart_args( cart )
        cart.checkout()
        
        o.cancel()
        
        self.assertEqual( o.status, OrderStatus.CANCELLED )
        self.assertEqual( o.get_pizza_count() , 2 )
        self.assertEqual( o.get_pizza( 0 ).status , CookingStatus.CANCELLED )
        self.assertEqual( o.get_pizza( 1 ).status , CookingStatus.CANCELLED )
        
        
    def test_cancel_cooking_order_passes_and_cancels_started_pizzas( self ):
        cart = OrderCart()
        cart.add_item( OrderTests.make_some_item( 3 ) )
        o = OrderTests.make_order_with_cart_args( cart )
        cart.checkout()
        
        o.get_pizza( 0 ).on_cooking_started()
        o.get_pizza( 0 ).on_cooking_finished()
        o.get_pizza( 1 ).on_cooking_started()
        
        o.cancel()
        
        self.assertEqual( o.status, OrderStatus.CANCELLED )
        self.assertEqual( o.get_pizza_count() , 3 )
        self.assertEqual( o.get_pizza( 0 ).status , CookingStatus.FINISHED)
        self.assertEqual( o.get_pizza( 1 ).status , CookingStatus.CANCELLED )
        self.assertEqual( o.get_pizza( 2 ).status , CookingStatus.CANCELLED )
        
        
    def test_cancel_ready_order_passes_but_not_touches_pizzas( self ):
        cart = OrderCart()
        cart.add_item( OrderTests.make_some_item( 2 ) )
        o = OrderTests.make_order_with_cart_args( cart )
        cart.checkout()
        OrderTests.cook_all_pizzas( o )
        
        o.cancel()
        
        self.assertEqual( o.status, OrderStatus.CANCELLED )
        self.assertEqual( o.get_pizza_count() , 2 )
        self.assertEqual( o.get_pizza( 0 ).status , CookingStatus.FINISHED)
        self.assertEqual( o.get_pizza( 1 ).status , CookingStatus.FINISHED )
        
        
    def test_cancel_order_that_left_kitchen_forbidden( self ):
        o = OrderTests.make_order_without_args()
        o.cart.checkout()
        OrderTests.cook_all_pizzas( o )
        
        o.on_started_delivery()
        
        self.assertRaises( Exception , o.cancel , () )
        
        o.pay_plan.mark_payed()
        
        o.on_delivered()
        
        self.assertRaises( Exception , o.cancel , () )
        
        
    def test_cancel_already_cancelled_order_forbidden( self ):
        o = OrderTests.make_order_without_args()
        o.cancel()
        
        self.assertRaises( Exception , o.cancel , () )
        
        
    def test_payment_cash_not_collected_on_delivery_forbidden( self ):
        o = OrderTests.make_order_without_args()
        o.cart.checkout()
        OrderTests.cook_all_pizzas( o )
        o.on_started_delivery()
        
        self.assertRaises( Exception , o.on_delivered , () )
        
        
    def test_payment_when_prepayment_expected_cannot_checkout_without_payment( self ):
        o = OrderTests.make_order_with_payplan_args( OrderTests.make_wire_transfer_plan() )
        
        self.assertRaises( Exception , o.cart.checkout , () )
        
        
    def test_payment_when_prepayment_expected_checkout_succeeds_when_payed( self ):
        plan = OrderTests.make_wire_transfer_plan() 
        o = OrderTests.make_order_with_payplan_args( plan )
        plan.mark_payed()
        
        o.cart.checkout()
        
        
    @staticmethod    
    def make_some_item( quantity = 1 ):
        return OrderItem( PizzaKind( "some" ) , PizzaSize.SMALL , 1.0 , quantity )
    
    
    @staticmethod
    def make_some_other_item( quantity = 1 ):
        return OrderItem( PizzaKind( "other" ) , PizzaSize.MEDIUM , 2.0 , quantity )
        
    
    
    @staticmethod
    def make_order_without_args():
        cart = OrderCart()
        cart.add_item( OrderTests.make_some_item() )
        
        return OrderTests.make_order_with_cart_args( cart )
    
    
    @staticmethod
    def make_order_with_cart_args( cart ):
        return Order( 1, cart, DeliveryContact( "", "", "" ), CashPaymentPlan() )
    
    
    @staticmethod
    def make_order_with_payplan_args( plan ):
        cart = OrderCart()
        cart.add_item( OrderTests.make_some_item() )
        return Order( 1, cart, DeliveryContact( "", "", "" ), plan )
    
    
    @staticmethod
    def cook_pizza( pizza ):
        pizza.on_cooking_started()
        pizza.on_cooking_finished()
    
    @staticmethod
    def cook_all_pizzas( order ):
        for pizza in order.pizzas:
            OrderTests.cook_pizza( pizza )
            
            
    @staticmethod
    def make_wire_transfer_plan():
        return WireTransferPaymentPlan( "1234 5678 1234 5678", "Ivan Ivanov" )
