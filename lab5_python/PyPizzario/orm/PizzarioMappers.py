from sqlalchemy import Column, Integer, String, Boolean, Float, event
from sqlalchemy import Table, MetaData, ForeignKey
from sqlalchemy.orm import mapper
from sqlalchemy.orm import  relationship
from sqlalchemy.sql.sqltypes import PickleType

from model.PizzaKind import PizzaKind
from model.OrderCart import OrderCart
from model.Order import Order
from model.OrderItem import OrderItem
from model.Pizza import Pizza
from model.PizzaRecipe import PizzaRecipe
from model.DeliveryContact import DeliveryContact
from model.PaymentPlan import PaymentPlan
from model.WireTransferPaymentPlan import WireTransferPaymentPlan
from model.CashPaymentPlan import CashPaymentPlan


class PizzarioMappers:
    
    def __init__ ( self ):
        self.metadata = MetaData()
        self.create_table_metadata()
        self.create_mappers()
        self.create_load_events()

        
    def create_table_metadata( self ):
        
        self.table_pizza_kinds = Table( 
            'pizza_kinds', 
            self.metadata,
            Column( 'pizza_kind_id', Integer, primary_key = True ),
            Column( 'price_by_size', PickleType ),
            Column( 'name', String( 50 ) ),
            Column( 'description', String( 100 ) ),
            Column( 'image_url', String( 50 ) ),
            Column( 'votes', Integer ),
            Column( 'total_rating', Integer ),
            Column( 'hidden', Boolean ),
            Column( 'pizza_recipe_id', Integer, ForeignKey( 'pizza_recipes.pizza_recipe_id' ) )
        )
         
        self.table_pizza_recipes = Table( 
            'pizza_recipes', 
            self.metadata,
            Column( 'pizza_recipe_id', Integer, primary_key = True ),
            Column( 'ingredients_weight', PickleType )  
        )
        
        self.table_order_carts = Table( 
            'order_carts', 
            self.metadata,
            Column( 'order_cart_id', Integer, primary_key = True ),
            Column( 'modifiable', Boolean )
        )
        
        self.table_order_items = Table(
            'order_items', 
            self.metadata,
            Column( 'order_item_id', Integer, primary_key = True ),
            Column( 'size', Integer ),
            Column( 'order_cart_id', Integer, ForeignKey( 'order_carts.order_cart_id' ) ),
            Column( 'pizza_kind_id', Integer, ForeignKey( 'pizza_kinds.pizza_kind_id' ) ),
            Column( 'fixed_price', Float ),
            Column( 'quantity', Integer ),
        )
        
        self.table_orders = Table( 
            'orders', 
            self.metadata,
            Column( 'order_id', Integer, primary_key = True ),
            Column( 'discount', Float ),
            Column( 'status', Integer ),
            Column( 'order_cart_id', Integer, ForeignKey( 'order_carts.order_cart_id' ) ),
            Column( 'payment_plan_id', Integer, ForeignKey( 'payment_plans.payment_plan_id' ) ),
            Column( 'delivery_contact_id', Integer, ForeignKey( 'delivery_contacts.delivery_contact_id' ) ),
        )
                       
        self.table_delivery_contacts = Table( 
            'delivery_contacts', 
            self.metadata,
            Column( 'delivery_contact_id', Integer, primary_key = True ),
            Column( 'address', String ),
            Column( 'phone', String ),
            Column( 'comment', String ),
        )
        
        self.table_pizzas = Table( 
            'pizzas', 
            self.metadata,
            Column( 'pizza_id', Integer, primary_key = True ),
            Column( 'size', Integer ),
            Column( 'status', Integer ),
            Column( 'order_id', Integer, ForeignKey( 'orders.order_id' ) ),
            Column( 'pizza_kind_id', Integer, ForeignKey( 'pizza_kinds.pizza_kind_id' ) )
        )
        
        self.table_payment_plans = Table( 
            'payment_plans', 
            self.metadata,
            Column( 'payment_plan_id', Integer, primary_key = True ),
            Column( 'payed', Boolean ),
            Column( 'card_code', String ),
            Column( 'card_holder', String ),
            Column('payment_plan_type', String(1), nullable = False )
        )
        
        
    def create_mappers( self ):
        
        mapper( PizzaRecipe, self.table_pizza_recipes )

        mapper( 
            PizzaKind, 
            self.table_pizza_kinds, 
            properties =
            {
                'recipe' : relationship( PizzaRecipe, uselist = False )
            }
        )
        
        mapper( 
            OrderCart, 
            self.table_order_carts,
            properties = 
            {
                'items' : relationship( OrderItem, backref = 'cart' )
            }
        )
        
        mapper( 
            OrderItem, 
            self.table_order_items, 
            properties = 
            {
                'kind' : relationship( PizzaKind, uselist=False )
            }
        )
        
        mapper( 
            Order,
            self.table_orders, 
            properties = 
            {
                'cart'     : relationship( OrderCart, uselist = False ),
                'pizzas'   : relationship( Pizza ),
                'pay_plan' : relationship( PaymentPlan, uselist = False ),
                'contact'  : relationship( DeliveryContact, uselist = False )
            }
        )
        
        mapper( 
            Pizza, 
            self.table_pizzas, 
            properties = 
            {
                'kind' : relationship( PizzaKind, uselist = False )
            }
        )
        
        mapper( 
            PaymentPlan, 
            self.table_payment_plans, 
            polymorphic_on = self.table_payment_plans.c.payment_plan_type, 
            polymorphic_identity = 'P'
        )
        
        mapper(
            CashPaymentPlan, 
            inherits = PaymentPlan, 
            polymorphic_identity='C' 
        )
        
        mapper(
            WireTransferPaymentPlan, 
            inherits = PaymentPlan, 
            polymorphic_identity = 'W' 
        )
        
        mapper( DeliveryContact, self.table_delivery_contacts )
        
    
    def create_load_events ( self ):
        
        def attach_callback_on_load( model_class, callback ):
            event.listen( model_class, 'load', callback )
            
        callback = lambda target, context : target._init_events()
        attach_callback_on_load( PizzaKind, callback )
        attach_callback_on_load( OrderCart, callback )
        attach_callback_on_load( Pizza,     callback )


