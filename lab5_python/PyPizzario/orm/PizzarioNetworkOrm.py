
from orm.PizzarioMappers import PizzarioMappers

import sqlalchemy
import sqlalchemy.orm


from model.PizzaKind import PizzaKind
from model.Order import Order
from model.PizzarioNetwork import PizzarioNetwork


 
class PizzarioNetworkOrm:
    
    def __init__ ( self, connection_string ):
        
        self.mappers = PizzarioMappers()
        
        self.engine = sqlalchemy.create_engine( connection_string )
        self.engine.echo = False

        self.session_maker = sqlalchemy.orm.sessionmaker()
        self.session_maker.configure( bind = self.engine )

        self.mappers.metadata.drop_all( self.engine )
        self.mappers.metadata.create_all( self.engine )
    
        
    def save_network( self, network ):
        s = self.session_maker()
        for pk in network.menu.browse_pizza_kind_names():
            s.add( network.menu.find_pizza_kind( pk ) )
        for o in network.orders:
            s.add( o )
        s.commit()
        
    
    def load_network( self ):
        n = PizzarioNetwork()
        s = self.session_maker()
        for pk in s.query(PizzaKind):
            n.menu.add_pizza_kind( pk )
        for o in s.query(Order):
            n.orders.append( o )
        return n
